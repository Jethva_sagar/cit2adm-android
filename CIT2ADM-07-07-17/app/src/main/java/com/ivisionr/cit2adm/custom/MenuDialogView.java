package com.ivisionr.cit2adm.custom;

import java.util.List;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.ivisionr.cit2adm.GoomapActivity;
import com.ivisionr.cit2adm.R;
import com.ivisionr.cit2adm.Util.GPSTracker;
import com.ivisionr.cit2adm.Util.MobileUtils;
import com.ivisionr.cit2adm.Util.SharePreferenceClass;
import com.ivisionr.cit2adm.Util.SystemConstants;
import com.ivisionr.cit2adm.Util.otherStaticValue;
import com.ivisionr.cit2adm.adapter.Votesadppter;
import com.ivisionr.cit2adm.bin.Myvotes;

public class MenuDialogView extends Dialog implements
android.view.View.OnClickListener
{

	MenuAdaptore menuAdaptore;
	private int display_width, display_height;
//	final String[] menuelementarray = new String[] { "Login",
//			"My Profile","My Dashboard", "My Neighbouring", "My Votes",
//			"iVisionR Associations",
//			"Share","My Report","My Drafts", "Search Issue By Place","Parameters",
//			"Send Feedback", "Update GPS Location", "Change Password", "My Messages" };
	Context menuContext;
	String[] menu;
	Activity menuContextActivity;
	GridView gridView;
	Button dialog_volumebtn;
	Button dialog_crossbtn;
	Button menu_dialogbtn;
	CustomFunctionClass customFunctionClass;
	SharePreferenceClass sharePreferenceClass;
	private String eventTag;
	String loginState;
	GPSTracker gpsTracker;
	String userId="";
	ArrayAdapter<String> arrayAdapterforradius;
	ArrayAdapter<String> arrayAdaptersping;
	private Dialog paramdialog;
	Button btn_submit;
	Spinner spin_language, spin_radius;
	private String radiusforselection = "";
	final String[] radius = { "5km", "10km", "25km", "50km", "100km", "250km",
	"500km" };
	final String[] language = { "english", "fran�ais", "espa�ol", "Deutsch" };
	String lLanguage = "";
	private Dialog voteDialog;
	private ListView myvoteslist;

	private List<Myvotes> myvoteslistview;
	private String language1 = "";

	public MenuDialogView(Activity context)
	{
		super(context);
		this.menuContextActivity = context;
		this.menuContext = (Context) context;
		getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		// TODO Auto-generated constructor stub
		customFunctionClass = new CustomFunctionClass(context);
		sharePreferenceClass = new SharePreferenceClass(menuContext);


	}

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menudialog);
		
		//		computeScreenDimension();
		//		RelativeLayout rl = (RelativeLayout) findViewById(R.id.dia_ret);
		//
		//		rl.setLayoutParams(new RelativeLayout.LayoutParams(
		//				RelativeLayout.LayoutParams.MATCH_PARENT, (display_height / 3)));
		gridView = (GridView) findViewById(R.id.gridView1);
		dialog_volumebtn = (Button) findViewById(R.id.dialog_volumebtn);
		dialog_crossbtn = (Button) findViewById(R.id.dialog_crossbtn);
		menu_dialogbtn = (Button) findViewById(R.id.menu_dialogbtn);
		dialog_crossbtn.setOnClickListener(this);
		menu_dialogbtn.setOnClickListener(this);
		dialog_volumebtn.setOnClickListener(this);
		loginState = sharePreferenceClass
				.getValue_string(otherStaticValue.loginState);


		if(loginState.equals("yes")){
			menu=menuContext.getResources().getStringArray(R.array.menuitemchange);
		}else{
			menu=menuContext.getResources().getStringArray(R.array.menuitem);
		}
		menuAdaptore = new MenuAdaptore(menuContext, menu);

		gridView.setAdapter(menuAdaptore);

		gridView.setOnItemClickListener(new OnItemClickListener()
		{

			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int arg2,
					long arg3)
			{
				// TODO Auto-generated method stub
				
				//Log.d("DEBUG", "Menu Item Click No. 1: "+arg2);
				//Log.d("DEBUG", "Menu Item Click No. 2: "+arg3);
				
				int current_pos = arg2;
				
				Log.d("DEBUG", "Current Menu Postion: "+current_pos);
				
				String loginState = sharePreferenceClass.getValue_string(otherStaticValue.loginState);

				eventTag = ((TextView) v
						.findViewById(R.id.gridviewelement_text)).getText()
						.toString();

				//Log.d("sms", "sms" +eventTag);
				//		Toast.makeText(menuContext, eventTag, Toast.LENGTH_SHORT)
				//				.show();

				//		customFunctionClass.event_of_grideview_item(eventTag);
				if(eventTag.equals("Login")){
					if(loginState.equals("no")){
						eventTag="Login";


					}else {

						eventTag="Logout";
					}
				}else if(eventTag.equals("Logout")){
					if(loginState.equals("yes")){
						eventTag="Logout";

					}
				}
				
				
				/*if(current_pos == 0) {
					
					if (loginState.equals("no")) {
						getContext().startActivity(new Intent(getContext(), LoginActivity.class));
	
					} else {
						Toast.makeText(menuContext, R.string.loginfirst, Toast.LENGTH_LONG) .show();
						
					}
				
				}
				
				
				if(current_pos == 1) {
				
					if (loginState.equals("yes")) {
						getContext().startActivity(new Intent(getContext(), MyProfileActivity.class));
	
					} else {
						Toast.makeText(menuContext, R.string.loginfirst, Toast.LENGTH_LONG) .show();
						
					}
				
				}
				
				if(current_pos == 2) {
					
					if (loginState.equals("yes")) {
						getContext().startActivity(new Intent(getContext(), GoomapActivity.class));
	
					} else {
						Toast.makeText(menuContext, R.string.loginfirst, Toast.LENGTH_LONG) .show();
						
					}
				
				}
				
				if(current_pos == 3) {
					
					if (loginState.equals("yes")) {
						getContext().startActivity(new Intent(getContext(), MyNeighbourList.class));
	
					} else {
						Toast.makeText(menuContext, R.string.loginfirst, Toast.LENGTH_LONG) .show();
						
					}
				
				}
				
				if(current_pos == 4) {
					
					if (loginState.equals("yes")) {
						//getContext().startActivity(new Intent(getContext(), Myvotes.class));
						
						MyVotesAsync myvotesasync=new MyVotesAsync(getOwnerActivity());
						//myvotesasync.myvotesinterfaces=MyVotesAsync.this;
						myvotesasync.execute(new SharePreferenceClass(getContext()).getUserid());
						
						
	
					} else {
						Toast.makeText(menuContext, R.string.loginfirst, Toast.LENGTH_LONG) .show();
						
					}
				
				}
				
				if(current_pos == 5) {
					
					if (loginState.equals("yes")) {
						getContext().startActivity(new Intent(getContext(), IvisionrAssociation.class));
	
					} else {
						Toast.makeText(menuContext, R.string.loginfirst, Toast.LENGTH_LONG) .show();
						
					}
				
				}

				if(current_pos == 6) {

					if (loginState.equals("yes")) {

						Intent sharingIntent = new Intent(
								android.content.Intent.ACTION_SEND);
						sharingIntent.setType("text/plain");
						String shareBody = Webservicelink.serverBaseUrl;
						sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
								"iVisionR");
						sharingIntent
								.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
						getContext().startActivity(Intent.createChooser(sharingIntent, "Share via"));
						
	
					} else {
						Toast.makeText(menuContext, R.string.loginfirst, Toast.LENGTH_LONG) .show();
						
					}
				
				}
				
				if(current_pos == 7) {
					
					if (loginState.equals("yes")) {
						getContext().startActivity(new Intent(getContext(), MyReport.class));
	
					} else {
						Toast.makeText(menuContext, R.string.loginfirst, Toast.LENGTH_LONG) .show();
						
					}
				
				}
				
				if(current_pos == 8) {
					
					if (loginState.equals("yes")) {
						getContext().startActivity(new Intent(getContext(), MyDrafts.class));
	
					} else {
						Toast.makeText(menuContext, R.string.loginfirst, Toast.LENGTH_LONG) .show();
						
					}
				
				}
				
				if(current_pos == 9) {
					
					if (loginState.equals("yes")) {
						getContext().startActivity(new Intent(getContext(), Issueserchbyplace.class));
	
					} else {
						Toast.makeText(menuContext, R.string.loginfirst, Toast.LENGTH_LONG) .show();
						
					}
				
				}
				
				if(current_pos == 10) {
					
					if (loginState.equals("yes")) {
						//getContext().startActivity(new Intent(getContext(), Parameters.class));
						displayParameterDialog();
	
					} else {
						Toast.makeText(menuContext, R.string.loginfirst, Toast.LENGTH_LONG) .show();
						
					}
				
				}
				
				if(current_pos == 11) {
					
					if (loginState.equals("yes")) {
						getContext().startActivity(new Intent(getContext(), SendFeedback.class));
	
					} else {
						Toast.makeText(menuContext, R.string.loginfirst, Toast.LENGTH_LONG) .show();
						
					}
				
				}
				
				if(current_pos == 12) {
					
					if (loginState.equals("yes")) {
						gpsTracker = new GPSTracker(getContext());
						double currlat = gpsTracker.getLatitude();
						double currentlong = gpsTracker.getLongitude();
						
						userId = sharePreferenceClass.getUserid();
						
						// TODO: TO update user GPS values in database

						//new UpdateGpsAsync().execute(userId, String.valueOf(currlat), String.valueOf(currentlong));;
						new SharePreferenceClass(menuContext)
								.setUpadelocationLat(String.valueOf(currlat));
						new SharePreferenceClass(menuContext)
								.setUpadelocationLon(String.valueOf(currentlong));
	
					} else {
						Toast.makeText(menuContext, R.string.loginfirst, Toast.LENGTH_LONG) .show();
						
					}
				
				}
				
				if(current_pos == 13) {
					
					if (loginState.equals("yes")) {
						getContext().startActivity(new Intent(getContext(), EnterNewPassword.class));
	
					} else {
						Toast.makeText(menuContext, R.string.loginfirst, Toast.LENGTH_LONG) .show();
						
					}
				
				}
				
				if(current_pos == 14) {
					
					if (loginState.equals("yes")) {
						getContext().startActivity(new Intent(getContext(), MyProfileActivity.class));
	
					} else {
						Toast.makeText(menuContext, R.string.loginfirst, Toast.LENGTH_LONG) .show();
						
					}
				
				}
				
				if(current_pos == 15) {
					
					if (loginState.equals("yes")) {
						getContext().startActivity(new Intent(getContext(), MyProfileActivity.class));
	
					} else {
						Toast.makeText(menuContext, R.string.loginfirst, Toast.LENGTH_LONG) .show();
						
					}
				
				}*/
				
				

				((MyDialogFragmentListener) menuContextActivity)
				.onReturnValue(eventTag, current_pos);
				dismiss();
			}
		});

	}


	private void displayParameterDialog() {
		paramdialog = new Dialog(getContext());
		paramdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		paramdialog.setContentView(R.layout.parameters);
		paramdialog.show();

		spin_language = (Spinner) paramdialog.findViewById(R.id.spin_language);
		btn_submit = (Button) paramdialog.findViewById(R.id.btn_submit);
		spin_radius = (Spinner) paramdialog.findViewById(R.id.spin_radius);
		arrayAdaptersping = new ArrayAdapter<String>(getContext(),
				android.R.layout.simple_spinner_item, language);
		arrayAdaptersping
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spin_language.setAdapter(arrayAdaptersping);

		arrayAdapterforradius = new ArrayAdapter<String>(getContext(),
				android.R.layout.simple_dropdown_item_1line, radius);
		arrayAdaptersping
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		spin_radius.setAdapter(arrayAdapterforradius);

		spin_language.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long value) {

				/*MobileUtils.updateLanguage(GoomapActivity.this,
						GoomapActivity.this, language[position]);*/

				Log.d("DEBUG", "Languge Check: " + language[position]);
				//MobileUtils.setLanguage(MenuDialogView.this, getContext());
				
				
				
				// languagesend=arrayAdaptersping.getItem(position).toString();

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});
		spin_radius.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				radiusforselection = arrayAdapterforradius.getItem(arg2)
						.toString();
				radiusforselection = radiusforselection.replace("km", "");

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {

			}
		});
		btn_submit.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				paramdialog.dismiss();
				getContext().startActivity(new Intent(getContext(), GoomapActivity.class));
				

			}
		});

	}

	
	
	@SuppressLint("NewApi")
	private void computeScreenDimension()
	{
		WindowManager wm = (WindowManager) menuContext
				.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		Point size = new Point();
		if (android.os.Build.VERSION.SDK_INT > 13)
		{
			display.getSize(size);
			display_width = size.x;
			display_height = size.y;
		}
		else
		{
			display_width = display.getWidth(); // deprecated
			display_height = display.getHeight();
		}
	}

	@Override
	protected void onStop()
	{
		// TODO Auto-generated method stub

		super.onStop();
	}

	public void closeMenudialog()
	{
		dismiss();
	}

	public class MenuAdaptore extends BaseAdapter
	{

		private Context menucontext;
		private final String[] menuitemName;

		public MenuAdaptore(Context context, String[] menuName)
		{
			this.menucontext = context;
			this.menuitemName = menuName;

		}

		@Override
		public int getCount()
		{
			// TODO Auto-generated method stub
			return menuitemName.length;
		}

		@Override
		public Object getItem(int position)
		{
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position)
		{
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent)
		{
			// TODO Auto-generated method stub

			LayoutInflater layoutInflater = (LayoutInflater) menucontext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View gridViewElement;

			gridViewElement = layoutInflater.inflate(R.layout.gridview_row,
					parent,false);
			TextView menuelementName = (TextView) gridViewElement
					.findViewById(R.id.gridviewelement_text);
			ImageView menuelementImage = (ImageView) gridViewElement
					.findViewById(R.id.gridviewelement_image);
			menuelementName.setText(menuitemName[position]);
			String imageLogo = menu[position];
			
			
			/*
			 * 
			 * final String[] menuelementarray = new String[] { "Login",
			"My Profile","My Dashboard", "My Neighbouring", "My Votes",
			"iVisionR Associations",
			"Share","My Report","My Drafts", "Search Issue By Place","Parameters",
			"Send Feedback", "Update GPS Location", "Change Password", "My Messages" };
			 */
			
			
			if(position == 0)
			{
				menuelementImage.setImageResource(R.drawable.login_icon);
				if(loginState.equals("yes")){
					menuelementImage.setImageResource(R.drawable.logout_icon);
				}
			}
			else if(position == 1)
			{
				menuelementImage.setImageResource(R.drawable.my_profile_icon);
			}
			else if(position == 2) {
				menuelementImage.setImageResource(R.drawable.my_dashboard_icon);
			}
			else if(position == 3)
			{
				menuelementImage.setImageResource(R.drawable.neighbouring_icon);
			}
			else if(position == 4)
			{
				menuelementImage.setImageResource(R.drawable.vote_report_icon);
			}
			else if(position == 5)
			{
				menuelementImage.setImageResource(R.drawable.ivisionrassociations_icon);
			}
			else if(position == 6)
			{
				menuelementImage.setImageResource(R.drawable.add_icon);
			}
			else if(position == 7)
			{
				
				menuelementImage.setImageResource(R.drawable.send_draft_icon);
			}
			else if(position == 8)
			{
				menuelementImage.setImageResource(R.drawable.send_report_icon);
			}
			else if(position == 9)
			{
				menuelementImage.setImageResource(R.drawable.search_place_icon);
			}
			else if(position == 10)
			{
				menuelementImage.setImageResource(R.drawable.parameter_icon);
				
			}
			
			else if(position == 11) {
				//menuelementImage.setImageResource(R.drawable.send_report_icon);
				menuelementImage.setImageResource(R.drawable.send_feedback_icon);
			}
			else if(position == 12) {
				menuelementImage.setImageResource(R.drawable.gps_location_icon);
			}
			else if(position == 13) {
				
				menuelementImage.setImageResource(R.drawable.change_pass_icon);
			}
			else if(position == 14) {
				menuelementImage.setImageResource(R.drawable.message);
			}

			return gridViewElement;
		}

	}

	@Override
	public void onClick(View v)
	{
		// TODO Auto-generated method stub
		switch (v.getId())
		{
		case R.id.menu_dialogbtn:
			Log.v("menu_dialogbtn", "menu_dialogbtn");


			//Toast.makeText(menuContext, "menu_dialogbtn", Toast.LENGTH_SHORT).show();

			break;

		case R.id.dialog_volumebtn:
			Log.v("dialog_volumebtn", "menu_dialogbtn");
			//Toast.makeText(menuContext, "dialog_volumebtn", Toast.LENGTH_SHORT).show();
			break;
		case R.id.dialog_crossbtn:
			Log.v("dialog_crossbtn", "menu_dialogbtn");
			//Toast.makeText(menuContext, "dialog_crossbtn", Toast.LENGTH_SHORT).show();
			dismiss();

			break;
		}
	}
	
	private void displyvotesDialog(List<Myvotes> listmyvotes) {
		voteDialog = new Dialog(getContext());
		voteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		voteDialog.setContentView(R.layout.myvotes);
		voteDialog.show();
		myvoteslist = (ListView) voteDialog.findViewById(R.id.myvotelist);
		Log.d("DEBUG", "Number of Votes: " + listmyvotes.size());
		Votesadppter votesadpter = new Votesadppter(getContext(),
				listmyvotes);
		myvoteslist.setAdapter(votesadpter);

		// arrayAdaptersping = new ArrayAdapter<String>(this,
		// android.R.layout.simple_list_item_1, language);
		// myvoteslist.setAdapter(arrayAdaptersping);

	}

	public interface MyDialogFragmentListener
	{
		public void onReturnValue(String foo, int pos);
	}
	
	
}
