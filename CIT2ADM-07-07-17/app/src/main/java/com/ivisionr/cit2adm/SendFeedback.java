package com.ivisionr.cit2adm;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ivisionr.cit2adm.Util.SharePreferenceClass;
import com.ivisionr.cit2adm.Util.otherStaticValue;
import com.ivisionr.cit2adm.async.SendFeedbackAsync;
import com.ivisionr.cit2adm.bin.User;
import com.ivisionr.cit2adm.database.DatabaseHandler;
import com.ivisionr.cit2adm.interfaces.SendFeedbackInterface;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

public class SendFeedback extends Activity implements SendFeedbackInterface {

	DatabaseHandler db;
	ArrayList<HashMap<String, String>> userRecord;

	private SharePreferenceClass sharedPreference;

	private ProgressDialog pDialog;

	String loginState = "";

	Button btn_back, btn_feedback;
	EditText et_first_name, et_last_name, et_email_id, et_feedback_msg;

	String first_name = "", last_name = "", email_id = "", feedback_msg = "";
	private String userId="", lang="";
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.send_feedback);
		db = new DatabaseHandler(this);

		initialize();
		
		Locale currLocale = this.getResources().getConfiguration().locale;
		lang=currLocale.getDisplayLanguage();
		Log.d("DEBUG", "Send Feedback Current Locale: "+currLocale.toString());
		Log.d("DEBUG", "GoomapActivity Default Language String - "+currLocale.getDisplayLanguage());
		

		sharedPreference = new SharePreferenceClass(SendFeedback.this);
		String loginState = sharedPreference
				.getValue_string(otherStaticValue.loginState);

		if (loginState.equals("yes")) {

			//databasehandler = new databasehandler(getApplicationContext());
			//userRecord = databasehandler.getProfileAllDataFromDb();
			
			SharePreferenceClass spc = new SharePreferenceClass(getApplicationContext());
			userId = spc.getUserid();
			//userRecord = db.getProfileAllDataFromDb();
			User myUser = (User) db.getUser(userId);
			

			/*for (int i = 0; i < userRecord.size(); i++) {
				first_name = userRecord.get(i).get(UserProfile.KEY_first_name);
				last_name = userRecord.get(i).get(UserProfile.KEY_last_name);
				email_id = userRecord.get(i).get(UserProfile.KEY_email);
			}*/

			et_first_name.setText(myUser.getFirst_name());
			et_first_name.setFocusable(false);
			et_last_name.setText(myUser.getLast_name());
			et_last_name.setFocusable(false);
			et_email_id.setText(myUser.getEmail());
			et_email_id.setFocusable(false);

		}

		btn_back.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				et_first_name.setText("");
				et_last_name.setText("");
				et_email_id.setText("");
				et_feedback_msg.setText("");

				finish();

			}
		});

		btn_feedback.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				first_name = et_first_name.getText().toString();
				last_name = et_last_name.getText().toString();
				email_id = et_email_id.getText().toString();
				feedback_msg = et_feedback_msg.getText().toString();

				SendFeedbackAsync sendfeedback = new SendFeedbackAsync(
						SendFeedback.this);
				sendfeedback.sendfeedbackinterface = SendFeedback.this;
				sendfeedback.execute(first_name, last_name, email_id, feedback_msg, lang);

			}
		});
	}

	private void initialize() {
		// TODO Auto-generated method stub

		et_first_name = (EditText) findViewById(R.id.et_first_name);
		et_last_name = (EditText) findViewById(R.id.et_last_name);
		et_email_id = (EditText) findViewById(R.id.et_email_id);
		et_feedback_msg = (EditText) findViewById(R.id.et_feedback_msg);
		btn_back = (Button) findViewById(R.id.btn_back);
		btn_feedback = (Button) findViewById(R.id.btn_feedback);

	}

	@Override
	public void onStarted() {
		// TODO Auto-generated method stub

		pDialog = new ProgressDialog(SendFeedback.this);
		pDialog.setMessage("Please wait...");
		pDialog.show();

	}

	@Override
	public void onCompleted(String errorcode) {
		// TODO Auto-generated method stub

		if (pDialog.isShowing()) {
			pDialog.dismiss();
		}

		if (errorcode.equals("S21900")) {

			Toast.makeText(getApplicationContext(),
					R.string.feedbacksuccessful, Toast.LENGTH_SHORT).show();

			et_first_name.setText("");
			et_last_name.setText("");
			et_email_id.setText("");
			et_feedback_msg.setText("");

			finish();

		}

		if (errorcode.equals("S21906")) {

			Toast.makeText(getApplicationContext(),
					R.string.feedbacksuccessful, Toast.LENGTH_SHORT).show();

			et_first_name.setText("");
			et_last_name.setText("");
			et_email_id.setText("");
			et_feedback_msg.setText("");

			finish();

		}

		if (errorcode.equals("S20001")) {

			Toast.makeText(getApplicationContext(), R.string.reserved,
					Toast.LENGTH_SHORT).show();

		}

		if (errorcode.equals("S20002")) {

			Toast.makeText(getApplicationContext(),
					R.string.invalidrequestparameter, Toast.LENGTH_SHORT)
					.show();

		}

		if (errorcode.equals("S21901")) {

			Toast.makeText(getApplicationContext(), R.string.firstnamenotfound,
					Toast.LENGTH_SHORT).show();

		}

		if (errorcode.equals("S21902")) {

			Toast.makeText(getApplicationContext(), "Last Name not found.",
					Toast.LENGTH_SHORT).show();

		}

		if (errorcode.equals("S21903")) {

			Toast.makeText(getApplicationContext(), "Email not found.",
					Toast.LENGTH_SHORT).show();

		}

		if (errorcode.equals("S21904")) {

			Toast.makeText(getApplicationContext(), R.string.invalidemail,
					Toast.LENGTH_SHORT).show();

		}

		if (errorcode.equals("S21905")) {

			Toast.makeText(getApplicationContext(), R.string.commentnotfound,
					Toast.LENGTH_SHORT).show();

		}

		if (errorcode.equals("S21907")) {

			Toast.makeText(getApplicationContext(),
					R.string.uanblepostfeedback, Toast.LENGTH_SHORT).show();

		}

	}

}
