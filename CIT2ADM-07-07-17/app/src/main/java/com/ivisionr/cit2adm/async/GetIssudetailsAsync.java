package com.ivisionr.cit2adm.async;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ivisionr.cit2adm.Util.Utils;
import com.ivisionr.cit2adm.Util.Webservicelink;
import com.ivisionr.cit2adm.bin.GetIssudetailsbin;
import com.ivisionr.cit2adm.bin.Issuedetailscomments;
import com.ivisionr.cit2adm.bin.MyReportDetailscomments;
import com.ivisionr.cit2adm.interfaces.GetIssuedetailsInterface;


import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class GetIssudetailsAsync  extends AsyncTask<String , Void, String>{

	private Context _context;
	private Activity actity;
	private GetIssudetailsbin getisuedetilsbin;
	public GetIssuedetailsInterface issuedetalsinterface;
	private List<Issuedetailscomments> issuedetailscomments;
	private Issuedetailscomments  listcomments;

	public GetIssudetailsAsync(Activity activity){
		this.actity=activity;
		this._context=activity;
		issuedetailscomments=new ArrayList<Issuedetailscomments>();
	}
	@Override
	protected String doInBackground(String... params) {
		getisuedetilsbin=serch(params);
		return null;
	}
	private GetIssudetailsbin serch(String[] params) {




		String useid = params[0];
		String result="";		
		String url = Webservicelink.getissuedetails;
		getisuedetilsbin=new GetIssudetailsbin();
		try
		{
			JSONObject registerJson = new JSONObject();
			registerJson.put("id",useid );
			Log.d("checkidhere", "checkidhere" +useid);
			List<NameValuePair> namevaluepair = new ArrayList<NameValuePair>();
			namevaluepair.add(new BasicNameValuePair("data", registerJson
					.toString()));			
			String paramString = URLEncodedUtils.format(namevaluepair, "utf-8");
			url += paramString;	

			Log.d("kartickurliscomment", "kartickurliscomment" +url);
			HttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(url);			
			HttpResponse response = null;
			try {
				response = httpClient.execute(httpPost);
			} catch (ClientProtocolException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} 
			try {
				HttpEntity entity = response.getEntity();
				InputStream instream = entity.getContent();
				result = Utils.convertStreamToString(instream);	
				Log.d("checkcomm", "checkcomm" +result);
				getisuedetilsbin=getDescription(result);
				issuedetailscomments=getComments(result);
				
			} catch (IOException e) {				
				e.printStackTrace();
			}
		}catch(Exception e){
			e.printStackTrace();
			
		}

		return getisuedetilsbin;




	}
	private List<Issuedetailscomments> getComments(String result) throws JSONException {
		JSONObject jsonObjectforcomments=new JSONObject(result);
		JSONArray jsonarray=jsonObjectforcomments.getJSONArray("comments");
		Log.d("length", "length" +jsonarray.length());
		for(int i=0;i<jsonarray.length();i++){
			listcomments=new Issuedetailscomments();
			JSONObject placeObject=(JSONObject) jsonarray.get(i);
			listcomments.setId(placeObject.getString("id"));
			listcomments.setComments(placeObject.getString("comment"));
			listcomments.setFirstname(placeObject.getString("username"));
			//listcomments.setLastname(placeObject.getString("last_name"));
			listcomments.setCreated(placeObject.getString("created"));
			issuedetailscomments.add(listcomments);
			
		}
		
		return issuedetailscomments;
	}
	private GetIssudetailsbin getDescription(String result) throws JSONException {
		getisuedetilsbin=new GetIssudetailsbin();
		JSONObject jsonObject = null;
		JSONObject obj=null;
		try {
			jsonObject = new JSONObject(result);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		try {
			obj=jsonObject.getJSONObject("issue");			

		} catch (JSONException e) {
			e.printStackTrace();
		}
		getisuedetilsbin.setActive(obj.getString("active"));
		getisuedetilsbin.setUser_id(obj.getString("user_id"));
		getisuedetilsbin.setIssutypename(obj.getString("issue_type_name"));
		getisuedetilsbin.setReportid(obj.getString("report_id"));
		getisuedetilsbin.setAllow_vote(obj.getString("allow_vote"));
		getisuedetilsbin.setCreated(obj.getString("created"));
		getisuedetilsbin.setStatus(obj.getString("status"));
		getisuedetilsbin.setDescription(obj.getString("description"));
		getisuedetilsbin.setId(obj.getString("id"));
		getisuedetilsbin.setImage_1(obj.getString("image_1_max"));
		getisuedetilsbin.setIssuetypeid(obj.getString("issue_type_id"));
		getisuedetilsbin.setLat(obj.getString("lat"));
		getisuedetilsbin.setLon(obj.getString("lon"));
		getisuedetilsbin.setNum_comments(obj.getString("num_comments"));
		getisuedetilsbin.setNum_rating_plus(obj.getString("num_ratings_plus"));
		getisuedetilsbin.setNum_rating_minus(obj.getString("num_ratings_minus"));
		getisuedetilsbin.setFirstname(obj.getString("first_name"));
		getisuedetilsbin.setLastname(obj.getString("last_name"));
		getisuedetilsbin.setRated(obj.getString("rated"));
		getisuedetilsbin.setUsername(obj.getString("username"));
		
		return getisuedetilsbin;
	}
	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		issuedetalsinterface.ongetissudetailsCompleted(getisuedetilsbin);
		if(issuedetailscomments.size()>0){
		issuedetalsinterface.onissuedetailscompleted(issuedetailscomments);
		}else{
			issuedetalsinterface.onissuenodata();
		}
	}
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		issuedetalsinterface.ongetissudetailsStartrd();
	}

}
