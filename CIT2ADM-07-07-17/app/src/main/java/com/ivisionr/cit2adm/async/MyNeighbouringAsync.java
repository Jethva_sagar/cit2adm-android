package com.ivisionr.cit2adm.async;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ivisionr.cit2adm.Util.Utils;
import com.ivisionr.cit2adm.Util.Webservicelink;
import com.ivisionr.cit2adm.bin.MyNeighbourDetails;
import com.ivisionr.cit2adm.interfaces.MyNeighbour;


import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class MyNeighbouringAsync extends AsyncTask<String , Void, String> {
	private Context _context;
	private Activity actity;
	private MyNeighbourDetails myneighbouringdetails;
	private List<MyNeighbourDetails> myneighbouringlist;	
	public MyNeighbour myneighbouringinterface;
	private String countryname;
	private String lat;
	private String lng;
	private String rad;

	public MyNeighbouringAsync(Activity activity, String country, String lat, String lng,String rad) {
		this.actity=activity;
		this._context=activity;
		this.countryname=country;
		this.lat=lat;
		this.lng=lng;
		this.rad=rad;
		myneighbouringlist= new ArrayList<MyNeighbourDetails>();


	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);		
		if(myneighbouringlist.size()>0){
			myneighbouringinterface.onMyNeighbourCompleted(myneighbouringlist);
		}else{
			myneighbouringinterface.onMyNeighbouringNodata();
		}
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		myneighbouringinterface.onMyNeighbourStarted();
		
	}

	@Override
	protected String doInBackground(String... arg0) {
		try {
			myneighbouringlist=search(countryname,lat,lng);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "";
	}

	private List<MyNeighbourDetails> search(String countryname, String lat,
			String lng) throws JSONException {
		String result="";
		String url="";
		url=Webservicelink.serverNeigbhorLocation;
		JSONObject registerJson = new JSONObject();
		try {
			registerJson.put("country", countryname);
		} catch (JSONException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		try {
			registerJson.put("lat",lat);
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			registerJson.put("lon", lng);
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			registerJson.put("rad", rad);
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Log.d("rad", "rad" +rad);
		List<NameValuePair> namevaluepair = new ArrayList<NameValuePair>();
		namevaluepair.add(new BasicNameValuePair("data", registerJson
				.toString()));
		//ResponseHandler<String> resHandler = new BasicResponseHandler();
		String paramString = URLEncodedUtils.format(namevaluepair, "utf-8");
		url += paramString;			
		Log.d("urlc", "urlc" +url);

		HttpClient httpClient = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost(url);			
		HttpResponse response = null;
		try {
			response = httpClient.execute(httpPost);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		try {
			HttpEntity entity = response.getEntity();
			InputStream instream = entity.getContent();
			result = Utils.convertStreamToString(instream);	
			Log.d("valuecneighoubr", "valuecneighoubr" +result);

			Log.d("res", "res" +result);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		JSONObject jsonObject=new JSONObject(result);
		JSONArray jsonarray=jsonObject.getJSONArray("users");
		for(int i=0;i<jsonarray.length();i++){
			
			myneighbouringdetails=new MyNeighbourDetails();
			JSONObject placeObject=(JSONObject) jsonarray.get(i);
			myneighbouringdetails.setFirstname(placeObject.getString("username"));
			//myneighbouringdetails.setLastname(placeObject.getString("report_id"));
			myneighbouringdetails.setId(placeObject.getString("id"));
			myneighbouringdetails.setReport_id(placeObject.getString("report_id"));
			myneighbouringdetails.setReport_auto_id(placeObject.getString("report_auto_id"));
//			myneighbouringdetails.setCity(placeObject.getString("city"));
//			myneighbouringdetails.setLat(placeObject.getString("lat"));
//			myneighbouringdetails.setLon(placeObject.getString("lon"));
//			myneighbouringdetails.setZip(placeObject.getString("zip"));
		    myneighbouringlist.add(myneighbouringdetails);
		}
		
		
		
		return myneighbouringlist;
		
	}

}
