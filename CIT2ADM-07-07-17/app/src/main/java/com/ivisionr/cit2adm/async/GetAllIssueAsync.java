package com.ivisionr.cit2adm.async;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.ivisionr.cit2adm.Util.Utils;
import com.ivisionr.cit2adm.Util.Webservicelink;
import com.ivisionr.cit2adm.bin.GetAllIssueDetails;
import com.ivisionr.cit2adm.interfaces.GetallIssueInterface;



public class GetAllIssueAsync  extends AsyncTask<String , Void, String>{

	private Context _context;
	private Activity actity;
	private GetAllIssueDetails allIssueDetails;
	private List<GetAllIssueDetails> listgetallIssueDetails;	
	public GetallIssueInterface getallisuueinterface;
	private String checkerrorcode;

	public GetAllIssueAsync(Activity activity) {
		// TODO Auto-generated constructor stub
		this.actity = activity;
		this._context = activity;
		listgetallIssueDetails = new ArrayList<GetAllIssueDetails>();
		
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		getallisuueinterface.onGetallIssueStarted();
	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub


		try {
			listgetallIssueDetails=markeDetailsserch(params);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "";

	}

	private List<GetAllIssueDetails> markeDetailsserch(String[] params) throws JSONException {
		String result="";

		String lattitude=params[0];
		String longitude=params[1];
		//Log.d("DEBUG", "GatAllIssuesAsync Lat 1" +lattitude);
		//Log.d("DEBUG", "GatAllIssuesAsync Lon 1" +longitude);
		String url="";
		url=Webservicelink.getAllIssueInMap;

		JSONObject registerJson = new JSONObject();
		registerJson.put("lat",lattitude);
		registerJson.put("lon", longitude);
		//Log.d("DEBUG", "GatAllIssuesAsync Lat 2: " +lattitude);
		//Log.d("DEBUG", "GatAllIssuesAsync Lon 2: " +longitude);
		
		List<NameValuePair> namevaluepair = new ArrayList<NameValuePair>();
		namevaluepair.add(new BasicNameValuePair("data", registerJson.toString()));
		//ResponseHandler<String> resHandler = new BasicResponseHandler();
		String paramString = URLEncodedUtils.format(namevaluepair, "utf-8");
		url += paramString;			
		//Log.d("DEBUG", "GetAllIssue URL: " +url);

		HttpClient httpClient = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost(url);			
		HttpResponse response = null;
		try {
			response = httpClient.execute(httpPost);
		
			HttpEntity entity = response.getEntity();
			InputStream instream = entity.getContent();
			result = Utils.convertStreamToString(instream);	
			//Log.d("DEBUG", "GetAllIssue URL: " +result);

			//Log.d("res", "res" +result);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		JSONObject jsonObject=new JSONObject(result);
		checkerrorcode=jsonObject.getString("error_code");
		Log.d("DEBUG", "Check Error Code: 	" +checkerrorcode);
		
		if(jsonObject.has("issues") && jsonObject.getJSONArray("issues").length() > 0) {
			JSONArray jsonarray=jsonObject.getJSONArray("issues");
	
	
			for(int i=0;i<jsonarray.length();i++){
				allIssueDetails=new GetAllIssueDetails();
				JSONObject placeObject=(JSONObject) jsonarray.get(i);
				allIssueDetails.setCreted(placeObject.getString("created"));
				allIssueDetails.setId(placeObject.getString("id"));
				allIssueDetails.setIssuetypeid(placeObject.getString("issue_type_id"));
				allIssueDetails.setLattitude(placeObject.getString("lat"));
				allIssueDetails.setLongitude(placeObject.getString("lon"));
				allIssueDetails.setName(placeObject.getString("name"));
				allIssueDetails.setReportid(placeObject.getString("report_id"));
				allIssueDetails.setStatus(placeObject.getString("status"));
				listgetallIssueDetails.add(allIssueDetails);		
	
			}
		}
		return listgetallIssueDetails;

	}

	

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		if(checkerrorcode!= null && checkerrorcode.equals("S21000")){			
			getallisuueinterface.onGetallIssueCompleted(listgetallIssueDetails);

		}else{			
			getallisuueinterface.onGetallIssuenoData();
		}
	}





}
