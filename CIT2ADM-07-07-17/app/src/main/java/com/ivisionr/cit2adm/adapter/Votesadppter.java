package com.ivisionr.cit2adm.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ivisionr.cit2adm.R;
import com.ivisionr.cit2adm.bin.Myvotes;

public class Votesadppter extends ArrayAdapter<Myvotes>{
	private LayoutInflater layoutinflate;
	private Context mContext;

	public Votesadppter(Context context,List<Myvotes> mylist) {
		super(context, R.layout.myvoteslist,R.id.reportidv,mylist);
		this.mContext=context;
		layoutinflate=LayoutInflater.from(context);		

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		final Myvotes mylist=(Myvotes) this.getItem(position);

		TextView firstname=null,lastname=null;
		ImageView city=null;
		if(convertView==null){
			convertView=layoutinflate.inflate(R.layout.myvoteslist, null);
			firstname=(TextView) convertView.findViewById(R.id.reportidv);
			lastname=(TextView) convertView.findViewById(R.id.ratingplusv);
			city=(ImageView) convertView.findViewById(R.id.ratingminusv);
			convertView.setTag(new ViewHolder(firstname,lastname,city));


		}else{
			ViewHolder viewholder=(ViewHolder)convertView.getTag();
			firstname=viewholder.firstname;
			lastname=viewholder.lastname;
			city=viewholder.city;

		}
		firstname.setText(mylist.getReportid());
		lastname.setText(mylist.getRatingplus());
		if (mylist.getRatingminus().equals("1")) {
			city.setImageDrawable(mContext.getResources().getDrawable(R.drawable.green));
		}else{
			city.setImageDrawable(mContext.getResources().getDrawable(R.drawable.rea));
		}
		

		return convertView;
	}


	public class ViewHolder{

		private TextView firstname,lastname;
		ImageView city;
		public ViewHolder(TextView firstname,TextView lastname, ImageView city2){
			this.firstname=firstname;
			this.lastname=lastname;
			this.city=city2;


		}

		public TextView getFirstname() {
			return firstname;
		}

		public void setFirstname(TextView firstname) {
			this.firstname = firstname;
		}

		public TextView getLastname() {
			return lastname;
		}

		public void setLastname(TextView lastname) {
			this.lastname = lastname;
		}

		public ImageView getCity() {
			return city;
		}

		public void setCity(ImageView city) {
			this.city = city;
		}



	}

}
