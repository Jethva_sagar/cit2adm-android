package com.ivisionr.cit2adm.async;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.ivisionr.cit2adm.Util.Webservicelink;
import com.ivisionr.cit2adm.bin.Association;
import com.ivisionr.cit2adm.interfaces.IvisionarassociationInterface;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class IvisionarAssociationAsyns extends AsyncTask<String , Void, String> {

	private Context _context;
	private Activity actity;
	private Association ivisionarassociation;
	private List<Association> listivisionarassocition;
	public IvisionarassociationInterface ivisionarinterface;
	private String lat,lon;
	private String checkerrorcode;
	private InputStream is = null;
	private String json;

	public IvisionarAssociationAsyns(Activity activity,String lat,String lon) {
		this.actity=activity;
		this._context=activity;
		listivisionarassocition=new ArrayList<Association>();	
		this.lat=lat;
		this.lon=lon;

	}

	@Override
	protected String doInBackground(String... params) {
		try {
			listivisionarassocition=serch(params);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	private List<Association> serch(String[] params) throws JSONException {

		String result="";
		String url="";
		url=Webservicelink.getAssociation;

		try {
			// defaultHttpClient
			ResponseHandler<String> resHandler = new BasicResponseHandler();
			
			DefaultHttpClient httpClient = new DefaultHttpClient();
			
			JSONObject updateJson = new JSONObject();
			updateJson.put("lat", lat);
			updateJson.put("lon", lon);
			
			Log.d("DEBUG", "IvisionrAssoc Lat: "+lat);
			Log.d("DEBUG", "IvisionrAssoc Lon: "+lon);
			
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("data", updateJson.toString()));
			
			String paramString = URLEncodedUtils.format(nameValuePairs, "utf-8");
			url += paramString;
			HttpGet httpPost = new HttpGet(url);
			
			Log.d("DEBUG", "IvisionrAssoc URL: "+url);
			
			//String httpResponse = httpClient.execute(httpPost, resHandler);
			
			HttpResponse httpResponse = httpClient.execute(httpPost);
			HttpEntity httpEntity = httpResponse.getEntity();
			is = httpEntity.getContent(); 
			
			Log.d("DEBUG", "IvisionrAssociation Stream: " +is);

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			json = sb.toString();
		} catch (Exception e) {
			Log.e("Buffer Error", "Error converting result " + e.toString());
		}     

		JSONObject jsonObject=new JSONObject(json);
		checkerrorcode=jsonObject.getString("error_code");
		Log.d("DEBUG", "IvisionrAssociation Error Code: " +checkerrorcode);
		JSONArray jsonArray=jsonObject.getJSONArray("associations");

		for(int i=0;i<jsonArray.length();i++){
			ivisionarassociation=new Association();
			JSONObject placeObject=(JSONObject) jsonArray.get(i);
			

			if(placeObject.has("id") && !placeObject.getString("id").equals("null"))
				ivisionarassociation.setId(placeObject.getString("id"));
			else
				ivisionarassociation.setId("");
			
			if(placeObject.has("lat") && !placeObject.getString("lat").equals("null"))
				ivisionarassociation.setLat(placeObject.getString("lat"));
			else
				ivisionarassociation.setLat("");
			
			if(placeObject.has("lon") && !placeObject.getString("lon").equals("null"))
				ivisionarassociation.setLon(placeObject.getString("lon"));
			else
				ivisionarassociation.setLon("");
			
			if(placeObject.has("name") && !placeObject.getString("name").equals("null"))
				ivisionarassociation.setName(placeObject.getString("name"));
			else
				ivisionarassociation.setName("");
			
			if(placeObject.has("address_1") && !placeObject.getString("address_1").equals("null"))
				ivisionarassociation.setAddress_1(placeObject.getString("address_1"));
			else
				ivisionarassociation.setAddress_1("");
			
			if(placeObject.has("address_2") && !placeObject.getString("address_2").equals("null"))
				ivisionarassociation.setAddress_2(placeObject.getString("address_2"));
			else
				ivisionarassociation.setAddress_2("");
			
			if(placeObject.has("city") && !placeObject.getString("address_1").equals("null"))
				ivisionarassociation.setCity(placeObject.getString("city"));
			else
				ivisionarassociation.setCity("");
			
			if(placeObject.has("state") && !placeObject.getString("state").equals("null"))
				ivisionarassociation.setState(placeObject.getString("state"));
			else
				ivisionarassociation.setState("");			
			
			/*if(placeObject.has("zip") && !placeObject.getString("zip").equals("null"))
				ivisionarassociation.setZip(placeObject.getString("zip"));
			else
				ivisionarassociation.setZip("");*/
			
			if(placeObject.has("country") && !placeObject.getString("country").equals("null"))
				ivisionarassociation.setCountry(placeObject.getString("country"));
			else
				ivisionarassociation.setCountry("");
			
			listivisionarassocition.add(ivisionarassociation);


		}

		Log.d("DEBUG", "IvisionrAssociation Size: " +listivisionarassocition.size());
		return listivisionarassocition;

	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		ivisionarinterface.onIvisionarassociationstarted();

	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		if(checkerrorcode.equals("S21200")){
			ivisionarinterface.onIvisionarassociationCompleted(listivisionarassocition);

		}else{
			ivisionarinterface.onIvisionarasationNoData();
		}
	}

}
