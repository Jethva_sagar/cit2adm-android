package com.ivisionr.cit2adm.adapter;

import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ivisionr.cit2adm.R;
import com.ivisionr.cit2adm.bin.MyDraftDetails;



public class Mydraftlistdsapter extends ArrayAdapter<MyDraftDetails>{


	private LayoutInflater layoutinflate;
	@SuppressWarnings("unused")
	private Context mContext;
	private String reportissuename;
	private String reportissuedescription;
	private String reportimage;
	private Bitmap repotimagebitmap;

	public Mydraftlistdsapter(Context context,List<MyDraftDetails> mylist){
		super(context, R.layout.mydraftlist,R.id.reportissuename,mylist);
		this.mContext=context;
		layoutinflate=LayoutInflater.from(context);		

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final MyDraftDetails mydraftlist= (MyDraftDetails) this.getItem(position);
		TextView isuname=null,issudes=null;
		ImageView issueimage=null;

		if(convertView==null){
			convertView=layoutinflate.inflate(R.layout.mydraftlist, null);
			isuname=(TextView)convertView.findViewById(R.id.reportissuename);
			issudes=(TextView)convertView.findViewById(R.id.reportdescription);
			issueimage=(ImageView)convertView.findViewById(R.id.reportissueimage);
			convertView.setTag(new ViewHolder(isuname,issudes,issueimage));


		}else{

			ViewHolder viewholdwer=(ViewHolder) convertView.getTag();
			isuname=viewholdwer.reportissuename;
			issudes=viewholdwer.reportissuedescription;
			issueimage=viewholdwer.reportimage;



		}
		isuname.setText(mydraftlist.getReportissuetypename());
		issudes.setText(mydraftlist.getReportisssuedescription());
		repotimagebitmap=converStringtobitmap(mydraftlist.getReportissueimage());
		issueimage.setImageBitmap(repotimagebitmap);
		return convertView;


	}
	public class ViewHolder{
		private  TextView reportissuename,reportissuedescription;
		private ImageView reportimage;
		public ViewHolder(TextView reportissuename,TextView reportissuedescription,ImageView reportimage ){
			this.reportissuename=reportissuename;
			this.reportissuedescription=reportissuedescription;
			this.reportimage=reportimage;

		}
		public TextView getReportissuename() {
			return reportissuename;
		}
		public void setReportissuename(TextView reportissuename) {
			this.reportissuename = reportissuename;
		}
		public TextView getReportissuedescription() {
			return reportissuedescription;
		}
		public void setReportissuedescription(TextView reportissuedescription) {
			this.reportissuedescription = reportissuedescription;
		}
		public ImageView getReportimage() {
			return reportimage;
		}
		public void setReportimage(ImageView reportimage) {
			this.reportimage = reportimage;
		}

	}
	private Bitmap converStringtobitmap(String encodedString) {
		byte [] encodeByte=Base64
				.decode(encodedString,Base64.DEFAULT);
		Bitmap bitmap=BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
		return bitmap;
	}

}
