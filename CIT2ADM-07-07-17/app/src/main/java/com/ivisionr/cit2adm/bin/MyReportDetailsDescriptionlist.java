package com.ivisionr.cit2adm.bin;

public class MyReportDetailsDescriptionlist {
	
	private String issucreated="";
	private String issueicon="";
	private String issuedescription="";
	private String issuestatus="";
	private String issuesmsvalue="";
	private String issueratingplus="";
	private String issueratingminus="";
	private String issuereportid="";
	private String issuetypeid="";
	private String issueaddress="";
	
	private String id="";
	private String lat="";
	private String lon="";
	private String firstname="";
	private String lastname="";
	private String issuetypenmae="";
	
	
	
	public String getIssuetypenmae() {
		return issuetypenmae;
	}
	public void setIssuetypenmae(String issuetypenmae) {
		this.issuetypenmae = issuetypenmae;
	}
	public String getLat() {
		return lat;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public void setLat(String lat) {
		this.lat = lat;
	}
	public String getLon() {
		return lon;
	}
	public String getIssueaddress() {
		return issueaddress;
	}
	public void setIssueaddress(String issueaddress) {
		this.issueaddress = issueaddress;
	}
	public void setLon(String lon) {
		this.lon = lon;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIssuetypeid() {
		return issuetypeid;
	}
	public void setIssuetypeid(String issuetypeid) {
		this.issuetypeid = issuetypeid;
	}
	public String getIssuereportid() {
		return issuereportid;
	}
	public void setIssuereportid(String issuereportid) {
		this.issuereportid = issuereportid;
	}
	public String getIssucreated() {
		return issucreated;
	}
	public void setIssucreated(String issucreated) {
		this.issucreated = issucreated;
	}
	public String getIssueicon() {
		return issueicon;
	}
	public void setIssueicon(String issueicon) {
		this.issueicon = issueicon;
	}
	public String getIssuedescription() {
		return issuedescription;
	}
	public void setIssuedescription(String issuedescription) {
		this.issuedescription = issuedescription;
	}
	public String getIssuestatus() {
		return issuestatus;
	}
	public void setIssuestatus(String issuestatus) {
		this.issuestatus = issuestatus;
	}
	public String getIssuesmsvalue() {
		return issuesmsvalue;
	}
	public void setIssuesmsvalue(String issuesmsvalue) {
		this.issuesmsvalue = issuesmsvalue;
	}
	public String getIssueratingplus() {
		return issueratingplus;
	}
	public void setIssueratingplus(String issueratingplus) {
		this.issueratingplus = issueratingplus;
	}
	public String getIssueratingminus() {
		return issueratingminus;
	}
	public void setIssueratingminus(String issueratingminus) {
		this.issueratingminus = issueratingminus;
	}
	
			
	

}
