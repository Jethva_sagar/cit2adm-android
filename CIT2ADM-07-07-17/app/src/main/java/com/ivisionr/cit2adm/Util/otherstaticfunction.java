package com.ivisionr.cit2adm.Util;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.widget.ImageButton;
import android.widget.ImageView;

public class otherstaticfunction {
	public static int getBitmapSamplingRate(int initialWidth,
			int initialHeight, int reqWidth, int reqHeight) {
		int inSampleSize = 1;

		if (initialHeight > reqHeight || initialWidth > reqWidth) {

			// Calculate ratios of height and width to requested height and
			// width
			int heightRatio = Math.round((float) initialHeight
					/ (float) reqHeight);
			int widthRatio = Math
					.round((float) initialWidth / (float) reqWidth);

			// Choose the smallest ratio as inSampleSize value, this will
			// guarantee
			// a final image with both dimensions larger than or equal to the
			// requested height and width.

			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
		}

		return inSampleSize;
	}

	

	public static Bitmap returnBitmap(String filePath, ImageView pb) {

		Bitmap logo = null;
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(filePath, options);

		if (options.outHeight != -1 && options.outWidth != -1) {

			int width = options.outWidth;
			int height = options.outHeight;
			int newWidth = pb.getWidth();
			int newHeight = pb.getHeight();
			if (newHeight == 0 || newWidth == 0) {
				// newHeight = rlImageContainer.getHeight();
				// newWidth = rlImageContainer.getWidth();
				newHeight = 200;
				newWidth = 200;

			}

			int inSampleSize = getBitmapSamplingRate(width, height, newWidth,
					newHeight);
			options.inSampleSize = inSampleSize;
			options.inJustDecodeBounds = false;
			logo = BitmapFactory.decodeFile(filePath, options);

			// if (logo != null) nearByListViewImageMemCache.put(fileName,
			// logo);

		}
		return logo;
	}

	

	

}