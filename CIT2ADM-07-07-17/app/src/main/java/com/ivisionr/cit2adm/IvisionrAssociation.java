package com.ivisionr.cit2adm;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.Window;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ivisionr.cit2adm.Util.Utils;
import com.ivisionr.cit2adm.async.IvisionarAssociationAsyns;
import com.ivisionr.cit2adm.bin.Association;
import com.ivisionr.cit2adm.interfaces.IvisionarassociationInterface;

import java.util.ArrayList;
import java.util.List;

public class IvisionrAssociation  extends FragmentActivity implements IvisionarassociationInterface{
	private GoogleMap mGoogleMap;
	private LatLng currentPosition;
	private String lat;
	private String lon;
	private ProgressDialog mProgressDialog;
	private List<Association>  listivisionardetails;
	private MarkerOptions markerOptions;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.ivisionar_association);
		mProgressDialog=new ProgressDialog(this);
		mProgressDialog.setMessage(getResources().getText(R.string.loading));
		listivisionardetails=new ArrayList<Association>();
		Bundle bundle=getIntent().getExtras();

		if(getIntent().hasExtra("currentlat")){
			lat=bundle.getString("currentlat");

		}
		if(getIntent().hasExtra("currentlon")){
			lon=bundle.getString("currentlon");
		}
		initializemap();
		if(Utils.checkConnectivity(IvisionrAssociation.this)){
		IvisionarAssociationAsyns ivisionarasync=new IvisionarAssociationAsyns(IvisionrAssociation.this,lat,lon);
		ivisionarasync.ivisionarinterface=this;
		ivisionarasync.execute();
		}else{
			showNetworkDialog("internet");
		}
	}

	@SuppressWarnings("deprecation")
	public void showNetworkDialog(final String message){
		// final exit of application
		AlertDialog.Builder builder = new AlertDialog.Builder(IvisionrAssociation.this);
		builder.setTitle(getResources().getString(R.string.app_name));
		if(message.equals("gps")){
			builder.setMessage(getResources().getString(R.string.gps_message));
		}
		else if(message.equals("internet")){
			builder.setMessage(getResources().getString(R.string.net_message));
		}
		AlertDialog alert = builder.create();
		alert.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				if(message.equals("gps")){
					Intent i=new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);		
					startActivityForResult(i, 1);
				}
				else if(message.equals("internet")){
					Intent i=new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS);		
					startActivityForResult(i,2);		
					Intent i1=new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS);		
					startActivityForResult(i1,3);	
				}
				else{
					//do nothing
				}
			}
		});
		// Showing Alert Message
		alert.show();	
	}

	private void initializemap() {
		mGoogleMap = ((MapFragment) getFragmentManager().findFragmentById(
				R.id.mapinivisionar)).getMap();
		if(mGoogleMap != null) {

			mGoogleMap.setMyLocationEnabled(true);
			currentPosition=new LatLng(Double.parseDouble(lat),Double.parseDouble(lon));
			mGoogleMap.getUiSettings().setZoomControlsEnabled(true);
			mGoogleMap.getUiSettings().setCompassEnabled(true);
			mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);
			mGoogleMap.getUiSettings().setAllGesturesEnabled(true);
			mGoogleMap.setTrafficEnabled(true);	
			CameraPosition cameraPosition = new CameraPosition.Builder().target(currentPosition).zoom(12).build();
			mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));


		}
	}

	@Override
	public void onIvisionarassociationstarted() {
		// TODO Auto-generated method stub
		if(null != mProgressDialog){
			mProgressDialog.show();

		}

	}

	@Override
	public void onIvisionarasationNoData() {
		// TODO Auto-generated method stub
		Toast toast=Toast.makeText(this, R.string.noassociation, Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();

	}

	@Override
	public void onIvisionarassociationCompleted(
			List<Association> ivisionarassocition) {
		if(null != mProgressDialog){
			mProgressDialog.dismiss();
		}
		listivisionardetails.addAll(ivisionarassocition);
		Log.d("DEBUG", "IvisionrAssoc Size of Array: " +listivisionardetails.size());
		for(int i=0; i<listivisionardetails.size(); i++){

			try {
				markerOptions = new MarkerOptions();
				LatLng latLng = new LatLng(Double.parseDouble(listivisionardetails.get(i).getLat()), Double.parseDouble(listivisionardetails.get(i).getLon()));
				markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.open)).title(listivisionardetails.get(i).getName() + "\r\n " + listivisionardetails.get(i).getCity() + "  " + listivisionardetails.get(i).getZip());
				mGoogleMap.addMarker(markerOptions.position(latLng));
			}catch(NumberFormatException e)
			{

			}
		}

	}
}
