package com.ivisionr.cit2adm.async;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.ivisionr.cit2adm.Util.Utils;
import com.ivisionr.cit2adm.Util.Webservicelink;
import com.ivisionr.cit2adm.bin.GetUserDetailsbin;
import com.ivisionr.cit2adm.interfaces.GetUserdetailsInterfaces;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class GerUserDetailsAsync extends AsyncTask<String, Void, String> {
	private Context _context;
	private Activity actity;
	private GetUserDetailsbin getuserdetailsbin;

	public GetUserdetailsInterfaces getuserdetailsinterfaces;

	public GerUserDetailsAsync(Activity activity) {
		this.actity = activity;
		this._context = activity;

	}

	@Override
	protected String doInBackground(String... params) {
		getuserdetailsbin = serch(params);

		return null;
	}

	private GetUserDetailsbin serch(String[] params) {

		String useid = params[0];
		Log.d("user_id", useid);
		String result = "";
		String url = Webservicelink.getUserDetails;
		getuserdetailsbin = new GetUserDetailsbin();
		try {
			JSONObject registerJson = new JSONObject();
			registerJson.put("id", useid);
			List<NameValuePair> namevaluepair = new ArrayList<NameValuePair>();
			namevaluepair.add(new BasicNameValuePair("data", registerJson
					.toString()));
			String paramString = URLEncodedUtils.format(namevaluepair, "utf-8");
			url += paramString;

			Log.d("kartickurluserdetails", "kartickurluserdetails" + url);
			HttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(url);
			HttpResponse response = null;
			try {
				response = httpClient.execute(httpPost);
			} catch (ClientProtocolException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				HttpEntity entity = response.getEntity();
				InputStream instream = entity.getContent();
				result = Utils.convertStreamToString(instream);
				getuserdetailsbin = getDescription(result);
				Log.d("resultinreportall", "resultinreportall" + result);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();

		}

		return getuserdetailsbin;
	}

	private GetUserDetailsbin getDescription(String result)
			throws JSONException {
		getuserdetailsbin = new GetUserDetailsbin();
		JSONObject jsonObject = null;
		JSONObject obj = null;
		try {
			jsonObject = new JSONObject(result);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		try {
			obj = jsonObject.getJSONObject("user");

		} catch (JSONException e) {
			e.printStackTrace();
		}
		getuserdetailsbin.setId(obj.getString("id"));
		getuserdetailsbin.setFirstname(obj.getString("first_name"));
		getuserdetailsbin.setLastname(obj.getString("last_name"));
		getuserdetailsbin.setCity(obj.getString("city"));
		getuserdetailsbin.setZip(obj.getString("zip"));
		getuserdetailsbin.setCreated(obj.getString("created"));

		if (obj.has("issues")) {

			try {

				JSONObject issues = obj.getJSONObject("issues");
				if (issues != null) {

					if (issues.has("counteropen")) {

						getuserdetailsbin.setCounteropen(issues
								.getString("counteropen"));
					} else {

						getuserdetailsbin.setCounteropen("0");
					}
					if (issues.has("countercompleted")) {

						getuserdetailsbin.setCountercompleted(issues
								.getString("countercompleted"));

					} else {

						getuserdetailsbin.setCountercompleted("0");

					}
					if (issues.has("counterprogress")) {

						getuserdetailsbin.setCounterprogress(issues
								.getString("counterprogress"));
					} else {

						getuserdetailsbin.setCounterprogress("0");
					}
					if (issues.has("counterclosed")) {

						getuserdetailsbin.setCounterclosed(issues
								.getString("counterclosed"));

					} else {
						getuserdetailsbin.setCounterclosed("0");

					}

				}

			}

			catch (JSONException e) {
				e.printStackTrace();
				
				getuserdetailsbin.setCounteropen("0");
				getuserdetailsbin.setCountercompleted("0");
				getuserdetailsbin.setCounterprogress("0");
				getuserdetailsbin.setCounterclosed("0");
			}

		}

		if (obj.has("comments")) {

			JSONArray jsonarray = obj.getJSONArray("comments");

			for (int i = 0; i < jsonarray.length(); i++) {
				JSONObject jo = (JSONObject) jsonarray.get(i);
				getuserdetailsbin.setComments_counter(jo.getString("counter"));
			}

		}

		if (obj.has("rating_plus")) {

			JSONArray jsonarray = obj.getJSONArray("rating_plus");

			for (int i = 0; i < jsonarray.length(); i++) {
				JSONObject jo = (JSONObject) jsonarray.get(i);
				getuserdetailsbin
						.setRatingplus_counter(jo.getString("counter"));
			}

		}
		if (obj.has("rating_minus")) {

			JSONArray jsonarray = obj.getJSONArray("rating_minus");

			for (int i = 0; i < jsonarray.length(); i++) {
				JSONObject jo = (JSONObject) jsonarray.get(i);
				getuserdetailsbin.setRatingminus_counter(jo
						.getString("counter"));
			}

		}

		return getuserdetailsbin;
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		getuserdetailsinterfaces.ongetuserdetailscompleted(getuserdetailsbin);
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		getuserdetailsinterfaces.ongetuserdetilsstartrd();

	}

}
