package com.ivisionr.cit2adm;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ivisionr.cit2adm.Util.SharePreferenceClass;
import com.ivisionr.cit2adm.async.MynewPasswordAsync;
import com.ivisionr.cit2adm.async.ResetPasswordAsync;
import com.ivisionr.cit2adm.interfaces.ResetPasswordInterface;

public class ResetPassword extends Activity implements ResetPasswordInterface {

	private SharePreferenceClass sharedPreference;

	private ProgressDialog pDialog;

	Button btn_back, btn_submit;
	EditText et_current_password, et_new_password, et_confirm_password;

	String id = "", current_password = "", new_password = "",
			confirm_password = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.reset_password);

		initialize();

		sharedPreference = new SharePreferenceClass(ResetPassword.this);
		id = sharedPreference.getUserid();

		btn_back.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				et_current_password.setText("");
				et_new_password.setText("");
				et_confirm_password.setText("");

				finish();

			}
		});

		btn_submit.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				current_password = et_current_password.getText().toString();
				new_password = et_new_password.getText().toString();
				confirm_password = et_confirm_password.getText().toString();

				if (current_password.equals("")) {

					Toast.makeText(getApplicationContext(),
							R.string.entercurrentpassword, Toast.LENGTH_SHORT)
							.show();
				}

				else if (new_password.equals("")) {

					Toast.makeText(getApplicationContext(),
							R.string.enternewpassword, Toast.LENGTH_SHORT)
							.show();
				}

				else if (confirm_password.equals("")) {

					Toast.makeText(getApplicationContext(),
							R.string.confirmnewpassword, Toast.LENGTH_SHORT)
							.show();
				}

				else {

					ResetPasswordAsync resetpassword = new ResetPasswordAsync(
							ResetPassword.this);
					resetpassword.resetpasswordinterface = ResetPassword.this;
					resetpassword.execute(id, current_password, new_password,
							confirm_password);
					new MynewPasswordAsync(ResetPassword.this, "NewPassWord").execute(new_password);

					et_current_password.setText("");
					et_new_password.setText("");
					et_confirm_password.setText("");

				}

			}
		});
	}

	private void initialize() {
		// TODO Auto-generated method stub

		et_current_password = (EditText) findViewById(R.id.et_current_password);
		et_new_password = (EditText) findViewById(R.id.et_new_password);
		et_confirm_password = (EditText) findViewById(R.id.et_confirm_password);
		btn_back = (Button) findViewById(R.id.btn_back);
		btn_submit = (Button) findViewById(R.id.btn_submit);

	}

	@Override
	public void onStarted() {
		// TODO Auto-generated method stub

		pDialog = new ProgressDialog(ResetPassword.this);
		pDialog.setMessage("Please wait...");
		pDialog.show();

	}

	@Override
	public void onCompleted(String errorcode) {
		// TODO Auto-generated method stub

		if (pDialog.isShowing()) {
			pDialog.dismiss();
		}

		if (errorcode.equals("S21800")){

			/*Toast.makeText(getApplicationContext(),
					R.string.passwordupdated,
					Toast.LENGTH_LONG).show();
*/
			final Dialog alert_dialog = new Dialog(this);
			alert_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			alert_dialog.setContentView(R.layout.dialog_alert);
			alert_dialog.show();
			TextView alert_msg = (TextView) alert_dialog
					.findViewById(R.id.alert_msg);
			Button alert_ok = (Button) alert_dialog.findViewById(R.id.alert_ok);

			alert_msg.setText(R.string.passwordupdated);

			alert_ok.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					alert_dialog.dismiss();
					finish();
				}
			});

			//finish();

		}

		if (errorcode.equals("S20001")) {

			Toast.makeText(getApplicationContext(), R.string.reserved,
					Toast.LENGTH_SHORT).show();

		}

		if (errorcode.equals("S20002")) {

			Toast.makeText(getApplicationContext(),
					R.string.invalidrequestparameter, Toast.LENGTH_SHORT)
					.show();

		}

		if (errorcode.equals("S21801")) {

			Toast.makeText(getApplicationContext(), R.string.useridnotfound,
					Toast.LENGTH_SHORT).show();

		}

		if (errorcode.equals("S21802")) {

			Toast.makeText(getApplicationContext(),
					R.string.currentpasswordnotfound, Toast.LENGTH_SHORT).show();

		}

		if (errorcode.equals("S21803")) {

			Toast.makeText(getApplicationContext(), R.string.newpasswordnotfound,
					Toast.LENGTH_SHORT).show();

		}

		if (errorcode.equals("S21804")) {

			Toast.makeText(getApplicationContext(),
					R.string.confirmpasswordnotfound, Toast.LENGTH_SHORT).show();

		}

		if (errorcode.equals("S21805")) {

			Toast.makeText(getApplicationContext(),
					R.string.newpasswordconfirmpasswordnotmatched,
					Toast.LENGTH_SHORT).show();

		}

		if (errorcode.equals("S21806")) {

			Toast.makeText(getApplicationContext(),
					R.string.currentpasswordnotmatch,
					Toast.LENGTH_SHORT).show();

		}

		if (errorcode.equals("S21807")) {

			Toast.makeText(getApplicationContext(),
					R.string.unableupdatepassword, Toast.LENGTH_SHORT)
					.show();

		}

	}

}
