package com.ivisionr.cit2adm.async;

import java.util.HashMap;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;

public class ServerResponceCheck
{
	Context context;	

	public ServerResponceCheck(Context con)
	{
		this.context = con;

	}

	

	public boolean errorcodeCheck(String erroreCode)
	{
		HashMap<String, String> responcecodelisthashmap = set();
		Log.v("errorcodeCheck", erroreCode);
		if (responcecodelisthashmap.containsKey(erroreCode))
		{
			String mess = responcecodelisthashmap.get(erroreCode);
			alertDialogShow(mess);
			return false;
		}
		else
		{

			return true;
		}

	}

	public static HashMap<String, String> set()
	{
		HashMap<String, String> hashMap = new HashMap<String, String>();
		hashMap.put("S20001", "Reserved");
		hashMap.put("S20002", "Invalid request parameter string");

		hashMap.put("S20100",
				"Username & password combination successfully validated");
		hashMap.put("S20101", "username or password are invalid. Login failed");
		hashMap.put("S20102",
				"Either username or password has not being passed from query string");

		hashMap.put("S20200",
				"User has been successfully registered. Email sent to the user..");
		hashMap.put("S20201", "First name not found.");
		hashMap.put("S20202",
				"First name should only contain letters,dot(.),apostrophe(\') ,and hyphen(-)..");
		hashMap.put("S20203", "Last name not found..");
		hashMap.put("S20204",
				"Last name should only contain letters, dot(.), apostrophe(\') and hyphen(-)..");
		hashMap.put("S20205", "City not found..");
		hashMap.put("S20206", "Reserved.");
		hashMap.put("S20207", "Country not found..");
		hashMap.put(
				"S20208",
				"Country name should only contain letters and dot(.), comma(,), hyphen(-) and quotation(\') min 3 characters..");
		hashMap.put("S20209", "Zip code not found..");
		hashMap.put("S20210", "Email not found.");
		hashMap.put("S20211", "Invalid email address entered..");
		hashMap.put("S20212", "Username not found..");

		hashMap.put("S20213",
				"Username must only contain letters, numbers. Min 6 characters..");
		hashMap.put("S20214", "Password not found..");
		hashMap.put("S20215",
				"Password must only contain letters, numbers and $,#,@. Min 6 characters..");
		hashMap.put("S20216", "Duplicate email address exists in our record.");
		hashMap.put("S20217", "Duplicate username exists in our record.");
		hashMap.put("S20218",
				"User has been successfully registered Unable to send email to the user..");
		hashMap.put("S20706", "No users found living nearby to the user.");
		hashMap.put("S20705", "Country not passed from query string.");
		hashMap.put("S20704", "Invalid value for Longitude.");
		hashMap.put("S20703", "Longitude not found.");
		hashMap.put("S20702", "Invalid value for latitude.");
		hashMap.put("S20701", "Latitude not found.");
		hashMap.put("S20700", "User location has been successfully updated.");
		hashMap.put("S20500",
				"User profile information has been successfully updated.");
		hashMap.put("S20501", "First name not found");
		hashMap.put("S20502",
				"First name should only contain letters, dot(.), apostrophe(') and hyphen(-).");
		hashMap.put("S20503", "Last name not found.");
		hashMap.put("S20504",
				"Last name should only contain letters, dot(.), apostrophe(') and hyphen(-).");
		hashMap.put("S20505", "City not found.");
		hashMap.put("S20507", "Country not found.");
		hashMap.put(
				"S20508",
				"Country name should only contain letters and dot(.), comma(,), hyphen(-) and quotation(') min 3 characters.");
		hashMap.put("S20509", "Zip code not found.");
		hashMap.put("S20510", "Email not found.");
		hashMap.put("S20511", "Invalid email address entered.");
		hashMap.put("S20512", "Username not found.");
		hashMap.put(
				"S20513",
				"Username must only contain letters, numbers. Min 6 characters.Username must only contain letters, numbers. Min 6 characters.");
		hashMap.put("S20516", "Duplicate email address another in our record.");
		hashMap.put("S20517", "Duplicate username exists in another record.");
		hashMap.put("S20518",
				"User has been successfully registered Unable to send email to the user.");
		hashMap.put("S20519", "User Id not found.");
		hashMap.put("S20520", "User does not exists in our records.");
		hashMap.put("S20300",
				"Password successfully updated and email successfuly sent.");
		hashMap.put("S20301",
				"Password successfully updated, however, unable to send the email.");
		hashMap.put("S20302", "Unable to update password.");
		hashMap.put("S20303",
				"No user matching this email address has not been found.");
		hashMap.put("S20304",
				"Email address has not being passed from URL query string.");
		
		hashMap.put("S20800", "Issue has been successfully posted");

		return hashMap;
	}

	@SuppressWarnings("deprecation")
	public void alertDialogShow(String Message)
	{
		final AlertDialog alertDialog = new AlertDialog.Builder(context)
				.create();

		// Setting Dialog Message
		alertDialog.setMessage(Message);

		// Setting OK Button
		alertDialog.setButton("OK", new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int which)
			{
				Log.v("OK", "OK");
				alertDialog.dismiss();
			}
		});

		alertDialog.show();
	}

}