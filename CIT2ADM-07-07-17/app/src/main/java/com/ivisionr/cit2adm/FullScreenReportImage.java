package com.ivisionr.cit2adm;

import com.ivisionr.cit2adm.Util.ImageLoaderWithoutDimension;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;

public class FullScreenReportImage extends Activity {
	ImageView issueimagedialog;
	private ImageLoaderWithoutDimension imageloder;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.issueimage);
		imageloder = new ImageLoaderWithoutDimension(FullScreenReportImage.this);
		issueimagedialog=(ImageView) findViewById(R.id.issueimagedialog);
		try {
			imageloder.DisplayImage(getIntent().getStringExtra("image"),
					issueimagedialog);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
