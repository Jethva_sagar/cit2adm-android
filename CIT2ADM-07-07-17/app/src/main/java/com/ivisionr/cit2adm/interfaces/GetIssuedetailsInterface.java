package com.ivisionr.cit2adm.interfaces;

import java.util.List;

import com.ivisionr.cit2adm.bin.GetIssudetailsbin;
import com.ivisionr.cit2adm.bin.Issuedetailscomments;


public interface GetIssuedetailsInterface {
	public void ongetissudetailsStartrd();
	public void ongetissudetailsCompleted(GetIssudetailsbin list);
	public void onissuedetailscompleted(List<Issuedetailscomments> issuecomments);
	public void onissuedetailsstarted();
	public void onissuenodata();
	

}
