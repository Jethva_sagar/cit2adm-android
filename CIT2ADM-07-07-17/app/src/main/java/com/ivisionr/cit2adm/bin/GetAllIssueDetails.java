package com.ivisionr.cit2adm.bin;

import java.io.Serializable;

@SuppressWarnings("serial")
public class GetAllIssueDetails implements Serializable {
	
	private String id="";
	private String reportid="";;
	private String issuetypeid="";
	private String name="";
	private String lattitude="";
	private String longitude="";
	private String status="";
	private String creted="";
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getReportid() {
		return reportid;
	}
	public void setReportid(String reportid) {
		this.reportid = reportid;
	}
	public String getIssuetypeid() {
		return issuetypeid;
	}
	public void setIssuetypeid(String issuetypeid) {
		this.issuetypeid = issuetypeid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLattitude() {
		return lattitude;
	}
	public void setLattitude(String lattitude) {
		this.lattitude = lattitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreted() {
		return creted;
	}
	public void setCreted(String creted) {
		this.creted = creted;
	}
	
	

}
