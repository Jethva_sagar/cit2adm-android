package com.ivisionr.cit2adm.Util;

/**
 * Interface for system level constants
 */
public interface SystemConstants
{
	/*
	 * Debug Mode - If set to true intents will not get value from previous.
	 * Default values will be assigned
	 */
	Boolean isModeDebug = false;
	String APP_VERSION = "1.2";

	/*
	 * Intent - Add extended data to the intent. ( name parameter )
	 */
	String INTENT_NEWHOUSE_POSTALADDRESS_STREET = "postal_address_street";
	String INTENT_NEWHOUSE_POSTALADDRESS_DISTRICT = "postal_address_district";
	String INTENT_NEWHOUSE_POSTALADDRESS_STATE = "postal_address_state";
	String INTENT_NEWHOUSE_POSTALADDRESS_PINCODE = "postal_address_pincode";
	String INTENT_NEWHOUSE_HOUSEHOLDID = "household_id";
	String INTENT_NEWHOUSE_CENTREID = "centre_id";
	String INTENT_NEWHOUSE_USERID_INTERVIEWER = "userId_interviewer";
	String INTENT_HOUSEHOLD_OBJ = "household_obj";
	String INTENT_CENTRE_OBJ = "centre_obj";
	String INTENT_USER_OBJ = "user_obj";
	String INTENT_PATIENT_OBJ = "patient_obj";

	String INTENT_WELCOME_HOUSEHOLDID = "houseHoldId";
	String INTENT_WELCOME_USERID_NUM = "useridnumber";
	String INTENT_WELCOME_CENTREID = "centre_id";
	String INTENT_REGISTERD_PATIENT_FROM = "registerdpatient";
	String INTENT_REGISTERD_PATIENT_HOUSEID = "household_id";
	String INTENT_SEARCH_PATIENT_SEARCH_TYPE_GLOBAL = "SEARCHTYPE";
	String MSG_CONNECTION_FAILURE = "Connecction Failure ....Please try again ";

	/*
	 * Gateway / Sync related constants
	 */
	String WEBSERVICE_NAMESPACE = "http://service.solansurvey.datatemplate.com/";

	// Local - DT
	String WEBSERVICE_URL = "/dtsolansurvey-services/solansurvey/sync-services?wsdl";

	// Local KK
	// String WEBSERVICE_URL
	// ="http://192.168.1.37:8180/dtsolansurvey-services/solansurvey/sync-services?wsdl";

	String M_GATEWAY_URL = "not available";

	String URLKEY = "PrefkeyUrl";

	String WEB_SERVICE_SYNC_SEARCH_METHOD_NAME = "searchPatients";

	String WEB_SERVICE_SYNC_USER_METHOD_NAME = "getUserAndCentreDetails";
	String WEB_SERVICE_SYNC_PATIENT_METHOD_NAME = "syncPatient";

	String WEB_SERVICE_SEARCH_METHOD_NAME = "getPatientMetadata";
	String WEB_SERVICE_SEARCH_PATIENT = "searchPatients";
	String WEB_SERVICE_SYNC_CENTER_GPS_CORDINATES = "syncCentre";

	/*
	 * Interview Screen constants
	 */

	final String TXT_LABEL_SUBSECTION_ONE_PARTA = null;
	String TXT_LABEL_SUBSECTION_ONE_PARTB = "SECTION 1-->PARTB";
	String TXT_LABEL_SUBSECTION_ONE_PARTC = "SECTION 1-->PARTC";
	String TXT_LABEL_SUBSECTION_TWO_PARTA = "SECTION 2-->PARTA";
	String TXT_LABEL_SUBSECTION_TWO_PARTB = "SECTION 2-->PARTB";
	String TXT_LABEL_SUBSECTION_TWO_PARTC = "SECTION 2-->PARTC";
	String TXT_LABEL_SUBSECTION_TWO_PARTD = "SECTION 2-->PARTD";
	String TXT_LABEL_SUBSECTION_TWO_PARTE = "SECTION 2-->PARTE";
	String PREVIOUS_ACTIVITY_CLASS = "PREVIOUS_ACTIVITY_CLASS";
	String DATA_NOT_AVIALABLE = "Not Available";

	String ALERT_TITLE = "UDAY - Alert";
	String TXT_LEAVE_TOBACCO_SECTION = "Click Ok to SKIP TO QS 7";
	String TXT_LEAVE_OUTPATIENT_SECTION = "Click OK to leave Outpatinet Section";
	String TXT_LEAVE_ALCOHOL_SECTION = "Click OK to leave Alcohol Section";

	String TXT_HINT_SEC1_EDUCATIONAL_STATUS = "* A person who can both read and write with"
			+ " understanding in any language without any formal education or passed any "
			+ "minimum educational standard."
			+ "** A person, who can neither read nor write or can only read but cannot write in any language.";

	String TXT_INVALID_USERID_PASSWORD = "Invalid UserId or Password";
	String TXT_GATEWAY_URL_ERROR = "Gateway Url Error";
	String TXT_MSG_EXIT = "Are You Sure You Want to Exit";
	String TXT_MSG_UNABLE_TO_CONNECT = "Unable to Connect... Please try Again";
	String TXT_MSG_TO_EDIT_SCREEN = "Click ok to continue with patient :";

	String MSG_URL_NOT_CONFIGURED = "Gateway Url is not configured, Please configure the gateway url to continue.";
	String MSG_ENTER_USERID_PASSWORD = "Please enter Username and Password";
	String MSG_WORK_OFLINE_STATUS = "You are not Logged In ! Please connect to internet and Log in.( Work offline mode unavailable at this time )";
	String TXT_PERSIST_GATEWAY_URL = "persistgatewayurl";
	String TXT_PERSIST_REMEMBER_ME = "persistremeberme";
	String TXT_PERSIST_USER_NAME = "persistusername";
	String TXT_PERSIST_PASSWORD = "persistpassword";
	String TXT_PERSIST_LANGUAGE = "persistLanguage";
	String TXT_PERSIST_USER_ID = "persistUserId";
	String TXT_PERSIST_CENTRE_ID = "persistCentreId";
	String TXT_PERSIST_OFFLINE = "persistOffline";
	
	String MSG_EMPTY_SEARCH_PARM = "Please enter search parameter";
	String MSG_NO_RESULT_FOUND = "No result found Please search again";
	String TXT_OTHERS_VALIDATION = "Please enter details to others";

	/*
	 * Data constants
	 */
	String MISSING_VALUE = "9999";
	int MISSING_VALUE_INTEGER = 9999;
	float MISSING_VALUE_FLOAT = 9999;
	double MISSING_VALUE_DOUBLE = 9999;

	String SKIP_VALUE = "777";
	int SKIP_VALUE_INTEGER = 777;
	float SKIP_VALUE_FLOAT = 777;
	double SKIP_VALUE_DOUBLE = 777;

	String TXT_SEC6_EXPENDITURE_HEART_DISEASE = "HEART DISEASE";
	String TXT_SEC6_EXPENDITURE_STROKE = "STROKE";
	String TXT_SEC6_EXPENDITURE_DIABETICS = "DIABETICS";
	String TXT_SEC6_EXPENDITURE_DIABETICSCOMP = "DIABETICS COMPLICATION";
	String TXT_SEC6_EXPENDITURE_HIGHBP = "HIGH BLOOD PRESSURE";
	String TXT_SEC6_EXPENDITURE_CHRONICKIDNEYDISEASE = "CHRONIC KIDNEY DISEASE";

	String TXT_FOOD_GROUP_MEATS = "Meats";
	String TXT_FOOD_GROUP_POULTRY = "Poultry";
	String TXT_FOOD_GROUP_ORGANMEATS = "Organ meats";
	String TXT_FOOD_GROUP_FISH = "Fish";
	String TXT_FOOD_GROUP_SHELL_FISH = "Shell fish and crustaceans";
	String TXT_FOOD_GROUP_EGGS = "Eggs";
	String TXT_FOOD_GROUP_MILK_PRODUCTS = "Milk and milk products";
	String TXT_FOOD_GROUP_MILK_DESERTS = "Milk based desserts";
	String TXT_FOOD_GROUP_DEEP_FRIED_FOODS_WESTERN = "Deep fried foods: western style";
	String TXT_FOOD_GROUP_DEEP_FRIED_FOODS_DESI = "Deep fried foods: desi style";

	String TXT_FOOD_GROUP_WESTERN_DESERTS = "Western style desserts/sweet snacks";
	String TXT_FOOD_GROUP_MITAHI = "Mithai";
	String TXT_FOOD_GROUP_COLD_BEVERAGES = "cold beverages";
	String TXT_FOOD_GROUP_FRUITS1 = "Fruits1";
	String TXT_FOOD_GROUP_FRUITS2 = "Fruits2";
	String TXT_FOOD_GROUP_FRUIT_JUICE = "Fruit juices";
	String TXT_FOOD_GROUP_NUTS = "Nuts/seeds";
	String TXT_FOOD_GROUP_LEAFY_GREENS = "Leafy greens";
	String TXT_FOOD_GROUP_RAW_VEGS = "Other raw vegetables";
	String TXT_FOOD_GROUP_PULSES = "Legumes and pulses";
	String TXT_FOOD_GROUP_PICKLES = "Use of pickles, pickled foods";
	String TXT_FOOD_GROUP_COOKED_VEGS = "Other cooked vegetables";
	String TXT_FOOD_GROUP_CEREALS = "Refined cereals with less fibre";
	String TXT_FOOD_GROUP_GRAIN = "Whole grain";
	String TXT_FOOD_GROUP_TEA = "Tea consumption";
	String TXT_FOOD_GROUP_COFFEE = "Coffee consumption";
	String TXT_FOOD_GROUP_CHIPS = "Namkeen, Chips";
	String TXT_FOOD_GROUP_MISC = "Miscellaneous food consumption";
	String DATA_SAVE_SUCCESSFUL = "Data Save Successful";

	/*
	 * Common
	 */
	String SD_CARD_DIR_NAME = "/UDAY-Baseline/";
	String SD_CARD_DIR_NAME_HOUSEHOLD = "/UDAY-Baseline/H_Data/";
	CharSequence TXT_FEATURE_NOT_AVAILABLE = "This Feature Is Not Available in This Version !";
	String LOGGER_TAG = "UDAY-Info !";
	String SERVICE_DATE_FORMAT = "MM/dd/yyyy";
	String VIEW_DATE_FORMAT = "MM/dd/yyyy";
	String DIGEST_REALM = "uday-enterprise-server";
	String DIGEST_NONCE = "nonce";
	String DIGEST_AUTH_HEADER = "Authorization";
	Object DIGEST_QOP = "auth";

	/*
	 * Hosehold Edit Alert
	 */

	String TXT_MSG_TO_EDIT_HOUSEHOLD = "Click ok to edit Household id :";
	String GPQ_STATUS_PENDING = "Pending";
	String GPQ_STATUS_INCOMPLETE = "Incomplete";
	String GPQ_STATUS_COMPLETED = "Complete";

	String UPLOAD_STATUS_INCOMPLETE = "Incomplete";
	String UPLOAD_STATUS_COMPLETED = "Complete";
	
	String DEFAULT_PREFERENCE = "DEFAULT_PREFERENCE";
	String USER_AUTH_STATUS = "USER_AUTH_STATUS";
	boolean SYNC_ENABLED = false;
	String SEARCH_LOCAL = "SEARCH_LOCAL";
	String SEARCH_GLOBAL = "SEARCH_GLOBAL";
	
}
