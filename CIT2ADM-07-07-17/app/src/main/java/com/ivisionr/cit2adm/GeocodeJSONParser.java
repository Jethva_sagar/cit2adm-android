package com.ivisionr.cit2adm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GeocodeJSONParser {

	/** Receives a JSONObject and returns a list */
	public List<HashMap<String, String>> parse(JSONObject jObject) {

		JSONArray jPlaces = null;
		try {
			/** Retrieves all the elements in the 'places' array */
			jPlaces = jObject.getJSONArray("results");
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		/**
		 * Invoking getPlaces with the array of json object where each json
		 * object represent a place
		 */
		return getPlaces(jPlaces);
	}

	private List<HashMap<String, String>> getPlaces(JSONArray jPlaces) {
		int placesCount = jPlaces.length();
		List<HashMap<String, String>> placesList = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> place = null;

		/** Taking each place, parses and adds to list object */
		for (int i = 0; i < placesCount; i++) {
			try {
				/** Call getPlace with place JSON object to parse the place */
				place = getPlace((JSONObject) jPlaces.get(i));
				placesList.add(place);

			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		return placesList;
	}

	/** Parsing the Place JSON object */
	private HashMap<String, String> getPlace(JSONObject jPlace) {

		HashMap<String, String> place = new HashMap<String, String>();
		String formatted_address = null;
		String lat = "";
		String lng = "";
		String city = "";
		try {
			// Extracting formatted address, if available

			formatted_address = jPlace.getString("formatted_address");
//			for (int i = 0; i < formatted_address.length(); i++) {
//				JSONObject jsononject = formatted_address.getJSONObject(i);
//				JSONArray jsonarray = jsononject.getJSONArray("types");
//				for (int j = 0; j < jsonarray.length(); j++) {
//					String ss = jsonarray.getString(0);
//					if (ss.equals("locality")) {
//
//						city = jsononject.getString("long_name");
//					}
//				}
//			}

			place.put("city", formatted_address);
			
//			place.put("lat", lat);
//			place.put("lng", lng);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return place;
	}
}
