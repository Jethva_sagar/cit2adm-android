package com.ivisionr.cit2adm.custom;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ivisionr.cit2adm.R;

public class CustomDialogView extends Dialog implements
		android.view.View.OnClickListener
{

	MenuAdaptore menuAdaptore;

	static final String[] menuelementarray = new String[] { "Log in",
			"Registration", "MyProfile", "Neighbouring", "VOTE on Report ID",
			"IVisionR Associations",
			"Add/share Apps w/Facebook or Twitter or w/G+", "Parameters",
			"Send feedback", "GPS Location detail", "change Password" };
	Context menuContext;

	GridView gridView;
	Button dialog_volumebtn;
	Button dialog_crossbtn;
	Button menu_dialogbtn;

	public CustomDialogView(Context context)
	{
		super(context);
		this.menuContext = context;
		getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		// TODO Auto-generated constructor stub

	}

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menudialog);
		gridView = (GridView) findViewById(R.id.gridView1);
		dialog_volumebtn = (Button) findViewById(R.id.dialog_volumebtn);
		dialog_crossbtn = (Button) findViewById(R.id.dialog_crossbtn);
		menu_dialogbtn = (Button) findViewById(R.id.menu_dialogbtn);
		dialog_crossbtn.setOnClickListener(this);
		menu_dialogbtn.setOnClickListener(this);
		dialog_volumebtn.setOnClickListener(this);
		menuAdaptore = new MenuAdaptore(menuContext, menuelementarray);

		gridView.setAdapter(menuAdaptore);

		gridView.setOnItemClickListener(new OnItemClickListener()
		{

			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int arg2,
					long arg3)
			{
				// TODO Auto-generated method stub

				Toast.makeText(
						menuContext,
						((TextView) v.findViewById(R.id.gridviewelement_text))
								.getText(), Toast.LENGTH_SHORT).show();

			}
		});

	}

	@Override
	protected void onStop()
	{
		// TODO Auto-generated method stub

		super.onStop();
	}

	public void CustoDialoInitialize()
	{

	}

	public void closeMenudialog()
	{
		dismiss();
	}

	public class MenuAdaptore extends BaseAdapter
	{

		private Context menucontext;
		private final String[] menuitemName;

		public MenuAdaptore(Context context, String[] menuName)
		{
			this.menucontext = context;
			this.menuitemName = menuName;

		}

		@Override
		public int getCount()
		{
			// TODO Auto-generated method stub
			return menuitemName.length;
		}

		@Override
		public Object getItem(int position)
		{
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position)
		{
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent)
		{
			// TODO Auto-generated method stub

			LayoutInflater layoutInflater = (LayoutInflater) menucontext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View gridViewElement = null;
			if (convertView == null)
			{

				// gridViewElement = new View(menucontext);

				convertView = layoutInflater.inflate(R.layout.gridview_element,
						parent, false);
				RelativeLayout rrrr = (RelativeLayout) convertView
						.findViewById(R.id.rrrr);
				TextView menuelementName = (TextView) convertView
						.findViewById(R.id.gridviewelement_text);
				ImageView menuelementImage = (ImageView) convertView
						.findViewById(R.id.gridviewelement_image);
				menuelementName.setText(menuitemName[position]);

			}
			return convertView;
		}

	}

	@Override
	public void onClick(View v)
	{
		// TODO Auto-generated method stub
		switch (v.getId())
		{
		case R.id.menu_dialogbtn:
			Log.v("menu_dialogbtn", "menu_dialogbtn");
			Toast.makeText(menuContext, "menu_dialogbtn", Toast.LENGTH_SHORT)
					.show();

			break;

		case R.id.dialog_volumebtn:
			Log.v("dialog_volumebtn", "menu_dialogbtn");
			Toast.makeText(menuContext, "dialog_volumebtn", Toast.LENGTH_SHORT)
					.show();
			break;
		case R.id.dialog_crossbtn:
			Log.v("dialog_crossbtn", "menu_dialogbtn");
			Toast.makeText(menuContext, "dialog_crossbtn", Toast.LENGTH_SHORT)
					.show();

			break;
		}
	}
}
