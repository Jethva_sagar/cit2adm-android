package com.ivisionr.cit2adm.interfaces;

import java.util.List;

import com.ivisionr.cit2adm.bin.MyReportDeetails;


public interface MyReportIsue  {
	
	public void onMYReportIssuenodata();
	public void onMyReportIssueStarted();
	public void onMyReportIssue(List<MyReportDeetails> list);

}
