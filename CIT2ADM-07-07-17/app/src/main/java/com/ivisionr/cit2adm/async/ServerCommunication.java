package com.ivisionr.cit2adm.async;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.provider.Settings.Secure;
import android.util.Log;

import com.ivisionr.cit2adm.SafelifeReportPost;
import com.ivisionr.cit2adm.Util.GPSTracker;
import com.ivisionr.cit2adm.Util.SharePreferenceClass;
import com.ivisionr.cit2adm.Util.Webservicelink;
import com.ivisionr.cit2adm.bin.MyReportDeetails;
import com.ivisionr.cit2adm.bin.User;
import com.ivisionr.cit2adm.database.DatabaseHandler;

public class ServerCommunication
{
	Context context;
	DatabaseHandler db;
	private MyReportDeetails myreportDetails;
	private List<MyReportDeetails> lstmyreportDetails;
	private  int jsonlength;
	private JSONArray jsonArray;
	private JSONObject placeObject;
	private SharePreferenceClass sharedpreferenceclass;
	private GPSTracker gps;
	String user_id = "";
	
	public ServerCommunication(Context con)
	{
		this.context = con;
		db = new DatabaseHandler(con);
		
		myreportDetails=new MyReportDeetails();
		lstmyreportDetails= new ArrayList<MyReportDeetails>();
		sharedpreferenceclass=new SharePreferenceClass(context);
		gps=new GPSTracker(context);
		new SharePreferenceClass(context).setCurrentlocationLat(String.valueOf(gps.getLatitude()));
		new SharePreferenceClass(context).setCurrentlocationLon(String.valueOf(gps.getLongitude()));
	}

	public String registrationInServer(String[] registrationData)
	{
		
		/*
		 * 
		 * new UnivarsalAsynkTask(activity, "Registration").execute(
						first_name, last_name, username, password, email, city,
						country, zip, association);
		 */

		String first_name = registrationData[0];
		String last_name = registrationData[1];
		String username = registrationData[2];
		String password = registrationData[3];
		String email = registrationData[4];
		String city = registrationData[5];
		String country = registrationData[6];
		String zip = registrationData[7];
		String association=registrationData[8];
		
		Log.d("DEBUG", "Registration First Name: "+first_name);
		Log.d("DEBUG", "Registration Last Name: "+last_name);
		Log.d("DEBUG", "Registration Username: "+username);
		Log.d("DEBUG", "Registration Password: "+password);
		Log.d("DEBUG", "Registration City: "+city);
		Log.d("DEBUG", "Registration Country: "+country);
		Log.d("DEBUG", "Registration Zip: "+zip);
		Log.d("DEBUG", "Registration Email: "+email);
		Log.d("DEBUG", "Registration Security Code: "+association);
		

		String page = "no";
		String url = Webservicelink.serverRegistrationUrl;	
		try
		{
			JSONObject registerJson = new JSONObject();
			registerJson.put("first_name", first_name);

			registerJson.put("last_name", last_name);
			registerJson.put("password", password);
			registerJson.put("username", username);
			registerJson.put("city", city);
			registerJson.put("country", country);

			registerJson.put("zip", zip);
			registerJson.put("email", email);
			registerJson.put("security_code", association);
			registerJson.put("lat", gps.getLatitude());
			registerJson.put("lon", gps.getLongitude());
			
			
			

			List<NameValuePair> namevaluepair = new ArrayList<NameValuePair>();

			namevaluepair.add(new BasicNameValuePair("data", registerJson
					.toString()));

			ResponseHandler<String> resHandler = new BasicResponseHandler();

			String paramString = URLEncodedUtils.format(namevaluepair, "utf-8");

			url += paramString;
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpGet httpGet = new HttpGet(url);

			page = httpClient.execute(httpGet, resHandler);
			Log.d("DEBUG", "ServerCommunication - Registration Error Code: " +page);
			
			/*JSONObject jsobj = new JSONObject(page.toString());
			if (jsobj.has("error_code"))
			{
				page = jsobj.getString("error_code");
			}*/			

		}
		catch (ClientProtocolException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e("response", e.getMessage());
		}
		catch (JSONException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return page;

	}

	public String LoginInServer(String[] registrationData)
	{

		String username = registrationData[0];
		String password = registrationData[1];
		String securitycode=registrationData[2];
		
		String device_id = registrationData[3];
		String os_version = registrationData[4];
		String api_level = registrationData[5];
		String release = registrationData[6];
		String device = registrationData[7];
		String model = registrationData[8];
		String product = registrationData[9];
		String brand = registrationData[10];
		String manufacturer = registrationData[11];
		

		String page = "no";
		String url = Webservicelink.serverLoginUrl;
		
		Log.d("DEBUG", "SeverCommunication - Login URL: "+url);

		try
		{
			JSONObject registerJson = new JSONObject();
			registerJson.put("username", username);
			registerJson.put("password", password);
			registerJson.put("security_code", securitycode);
			registerJson.put("device_id", device_id);
			registerJson.put("os_version", os_version);
			registerJson.put("api_level", api_level);
			registerJson.put("release", release);
			registerJson.put("device", device);
			registerJson.put("model", model);
			registerJson.put("product", product);
			registerJson.put("brand", brand);
			registerJson.put("manufacturer", manufacturer);

			/*Log.d("DEBUG", "SeverCommunication - Username: "+username);
			Log.d("DEBUG", "SeverCommunication - Password: "+password);
			Log.d("DEBUG", "SeverCommunication - Security Code: "+securitycode);
			Log.d("DEBUG", "SeverCommunication - OS Version: "+os_version);
			Log.d("DEBUG", "SeverCommunication - API Level: "+api_level);
			Log.d("DEBUG", "SeverCommunication - Release: "+release);
			Log.d("DEBUG", "SeverCommunication - Device: "+device);
			Log.d("DEBUG", "SeverCommunication - Model: "+model);
			Log.d("DEBUG", "SeverCommunication - Product: "+product);
			Log.d("DEBUG", "SeverCommunication - Brand: "+brand);*/
			
			List<NameValuePair> namevaluepair = new ArrayList<NameValuePair>();

			namevaluepair.add(new BasicNameValuePair("data", registerJson.toString()));

			ResponseHandler<String> resHandler = new BasicResponseHandler();

			String paramString = URLEncodedUtils.format(namevaluepair, "utf-8");

			url += paramString;
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpGet httpGet = new HttpGet(url);

			page = httpClient.execute(httpGet, resHandler);

			//Log.d("mypage", "mypage" +page);
			Log.d("DEBUG", "SeverCommunication - Error Code: "+page);
			
			JSONObject jsobj = new JSONObject(page.toString());
			String status = "0";
			String membership_expiry_date = "";
			if(jsobj.has("error_code"))
			{
				
			
				if(jsobj.has("active"))
					
					status = jsobj.getString("active");
				else
					status = "0";
				
				Log.d("DEBUG", "SeverCommunication - Status: "+status);
				
				//new SharePreferenceClass(context).savestatus(status);
				new SharePreferenceClass(context).saveDeviceId(device_id);
				
			
				//page = jsobj.getString("error_code");
				Log.d("DEBUG", "ServerCommunication - Error Code: " +page);
				if(jsobj.has("membership_expiry_date")) {
					membership_expiry_date = jsobj.getString("membership_expiry_date");
					new SharePreferenceClass(context).saveMembershipExpiry(membership_expiry_date);
					new SharePreferenceClass(context).savestatus("1");
					Log.d("DEBUG", "ServerCommunication - Expiry: " +membership_expiry_date);
				
				}
				if( (jsobj.getString("error_code").equals("S20100") || jsobj.getString("error_code").equals("S20104")))
					userProfileJsonParsing(jsobj);
				
						
				
				
			}
			

		}
		catch (ClientProtocolException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e("Response", e.getMessage());
		}
		catch (JSONException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return page;

	}
	public String ReportInServer(String[] registrationData)
	{

		String useid = registrationData[0];
		String issuetypeid = registrationData[1];
		String image = registrationData[2];
		String description = registrationData[3];
		String latitude = registrationData[4];
		String longitude = registrationData[5];


		String page = "no";
		String url = Webservicelink.serverReportURL;
		// http://ws.geonames.org/countryCode?lat=49.03&lng=10.2
		// http://www.suprcall.com/suprcall2/point.php?data=location&latitude=22.01&longitude=80.025
		try
		{
			JSONObject registerJson = new JSONObject();
			registerJson.put("user_id", useid);
			registerJson.put("issue_type_id", issuetypeid);
			registerJson.put("image_1", image);
			registerJson.put("description", description);
			registerJson.put("lat", latitude);
			registerJson.put("lon", longitude);



			List<NameValuePair> namevaluepair = new ArrayList<NameValuePair>();

			namevaluepair.add(new BasicNameValuePair("data", registerJson
					.toString()));

			ResponseHandler<String> resHandler = new BasicResponseHandler();

			String paramString = URLEncodedUtils.format(namevaluepair, "utf-8");

			url += paramString;
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpGet httpGet = new HttpGet(url);

			page = httpClient.execute(httpGet, resHandler);
			

			Log.e("responsecountryIso", page);
			JSONObject jsobj = new JSONObject(page.toString());
			if (jsobj.has("error_code"))
			{
				page = jsobj.getString("error_code");				
			}
			

		}
		catch (ClientProtocolException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e("response", e.getMessage());
		}
		catch (JSONException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return page;

	}
	public String MyReport(String[] registrationData)
	{

		String useid = registrationData[0];

		String page = "no";
		String url = Webservicelink.serverMenuReportURL;

		try
		{
			JSONObject registerJson = new JSONObject();
			registerJson.put("user_id", useid);	

			List<NameValuePair> namevaluepair = new ArrayList<NameValuePair>();

			namevaluepair.add(new BasicNameValuePair("data", registerJson
					.toString()));

			ResponseHandler<String> resHandler = new BasicResponseHandler();

			String paramString = URLEncodedUtils.format(namevaluepair, "utf-8");

			url += paramString;
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpGet httpGet = new HttpGet(url);

			page = httpClient.execute(httpGet, resHandler);

			Log.d("DEBUG", "ServerCommunication - Error Code: " +page);

			
			JSONObject jsobj = new JSONObject(page.toString());
			if (jsobj.has("error_code"))
			{

				page = jsobj.getString("error_code");
				Log.v("error_code", page);
				if (page.equals("S20900"))
				{
					userMyReport(jsobj);
				}

			}			

		}
		catch (ClientProtocolException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e("response", e.getMessage());
		}
		
		catch (JSONException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return page;

	}
	
	public String ResetPassInServer(String[] resetData)
	{

		String emailid = resetData[0];


		String page = "";
		String url = Webservicelink.serverForgotPasswordURL;
		try
		{
			JSONObject registerJson = new JSONObject();
			registerJson.put("email", emailid);



			List<NameValuePair> namevaluepair = new ArrayList<NameValuePair>();

			namevaluepair.add(new BasicNameValuePair("data", registerJson
					.toString()));

			ResponseHandler<String> resHandler = new BasicResponseHandler();

			String paramString = URLEncodedUtils.format(namevaluepair, "utf-8");

			url += paramString;
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpGet httpGet = new HttpGet(url);

			page = httpClient.execute(httpGet, resHandler);
			

			Log.d("DEBUG", "ServerCommunication Resp: "+page);
			/*JSONObject jsobj = new JSONObject(page.toString());
			if (jsobj.has("error_code"))
			{
				page = jsobj.getString("error_code");				
			}*/
			

		}
		catch (ClientProtocolException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e("response", e.getMessage());
		}
		catch (JSONException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return page;

	}

	private void userMyReport(JSONObject jsobj) {
		JSONObject jsonjson = jsobj;
		Log.d("DEBUG", "ServerCommunication JSON " +jsonjson);
		try {
			jsonArray=jsonjson.getJSONArray("issues");
			jsonlength=jsonArray.length();			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for(int i=0;i<jsonlength;i++){
			myreportDetails=new MyReportDeetails();
			try {
				placeObject=(JSONObject) jsonArray.get(i);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			try {
				
				myreportDetails.setReport_id(placeObject.getString("report_id"));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			lstmyreportDetails.add(myreportDetails);
			Log.d("DEBUG", "ServerCommunication My Report Details: " +lstmyreportDetails);

		}


	}

	public boolean userProfileJsonParsing(JSONObject json)
	{	
		Log.d("DEBUG", "User Profile JSON: "+json.toString());
		JSONObject jsonjson = json;
		
		String user_id = "";
		
		//ContentValues contentValues = new ContentValues();
		User user = new User();
		if (jsonjson.has("user_type_id"))
		{
			String user_type_id = json.optString("user_type_id");
			//contentValues.put(UserProfile.KEY_user_type_id,	Integer.parseInt(user_type_id));
			user.setUser_type_id(user_type_id);
		}
		if (jsonjson.has("first_name"))
		{
			String first_name = json.optString("first_name");
			/*contentValues.put(UserProfile.KEY_first_name, user_type_id);
			new SharePreferenceClass(context).setUsername(user_type_id);*/
			
			user.setFirst_name(first_name);

		}
		if (jsonjson.has("last_name"))
		{
			String last_name = json.optString("last_name");
			//contentValues.put(UserProfile.KEY_last_name, user_type_id);
			user.setLast_name(last_name);
		}
		if (jsonjson.has("city"))
		{
			String city = json.optString("city");
			//contentValues.put(UserProfile.KEY_city, user_type_id);
			user.setCity(city);

		}
		if (jsonjson.has("country"))
		{
			String country = json.optString("country");
			//contentValues.put(UserProfile.KEY_country, user_type_id);
			user.setCountry(country);
		}
		if (jsonjson.has("zip"))
		{
			String zip = json.optString("zip");
			//contentValues.put(UserProfile.KEY_zipcode, user_type_id);
			user.setZipcode(zip);
		}
		if (jsonjson.has("username"))
		{
			String username = json.optString("username");
			//contentValues.put(UserProfile.KEY_username, user_type_id);
			user.setUsername(username);

		}

		if (jsonjson.has("email"))
		{
			String email = json.optString("email");
			//contentValues.put(UserProfile.KEY_email, email);
			user.setEmail(email);
		}

		if (jsonjson.has("active"))
		{
			String active = json.optString("active");
			//contentValues.put(UserProfile.KEY_active, user_type_id);
			user.setActive(active);
		}

		if (jsonjson.has("membership_expiry_date"))
		{
			String membership_expiry_date = json.optString("membership_expiry_date");
			//contentValues.put(UserProfile.KEY_membership_expiry_date, user_type_id);
			user.setMembership_expiry_date(membership_expiry_date);
		}
		if (jsonjson.has("security_code"))
		{
			String access_id = json.optString("security_code");
			//contentValues.put(UserProfile.KEY_membership_expiry_date, user_type_id);
			//user.setAccessId(access_id);
			user.setAccess_id(access_id);
		}
		if (jsonjson.has("created"))
		{
			String created = json.optString("created");
			//contentValues.put(UserProfile.KEY_created, user_type_id);
			
			Log.d("DEBUG", "Profile Created On: "+created);
			user.setCreated(created);
		}
		if (jsonjson.has("user_id"))
		{
			user_id = json.optString("user_id");
			//Log.d("DEBUG", "userid" +user_id);

			new SharePreferenceClass(context).setUserid(user_id);

			//contentValues.put(UserProfile.KEY_USER_ID, Integer.parseInt(user_id));
			
			user.setId(user_id);
		}
		
		/*db.deleteSpecificTable(UserProfile.TABLE_NAME);
		boolean retuenInsertFlag = db.insertSpecificTable(UserProfile.TABLE_NAME, contentValues);
		//Log.d("Chaeck", "" + retuenInsertFlag);
		return retuenInsertFlag;*/
		
		/*
		 * values.put(KEY_USER_TYPE_ID, user.getUser_type_id()); 							// User Id
		values.put(KEY_FIRST_NAME, user.getFirst_name()); 								// User First Name
		values.put(KEY_LAST_NAME, user.getLast_name()); 								// User Last Name
		values.put(KEY_EMAIL, user.getEmail()); 										// User Email
		values.put(KEY_USERNAME, user.getUsername()); 									// User Username		
		values.put(KEY_PASSWORD, user.getPassword()); 									// User Password		
		values.put(KEY_ORGANIZATION, user.getOrganization()); 							// User Organization		
		values.put(KEY_CITY, user.getCity()); 											// User City		
		values.put(KEY_COUNTRY, user.getCountry()); 									// User Country		
		values.put(KEY_ZIPCODE, user.getZipcode()); 									// User Zip Code		
		values.put(KEY_ACTIVE, user.getActive()); 										// User Active		
		values.put(KEY_PAID, user.getPaid()); 											// User Paid
		values.put(KEY_NOTE, user.getNote()); 											// User Note
		values.put(KEY_MEMBERSHIP_EXPIRY_DATE, user.getMembership_expiry_date()); 		// User Membership Expiry Date
		values.put(KEY_CREATED, user.getCreated()); 									// User Created
		values.put(KEY_MODIFIED, user.getModified()); 
		*/
		PackageManager manager = context.getPackageManager();
		PackageInfo info=null;
		try {
			info = manager.getPackageInfo(context.getPackageName(), 0);
			Log.d("DEBUG",info.versionName+"+++++++++++++++"+info.versionName);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/***
		 * getting device unique id
		 */
		 String device_id = Secure.getString(context.getContentResolver(),
                Secure.ANDROID_ID);
		Log.d("DEBUG", "ServerCommunication User Id: "+user_id);
		user.setDevice_id(device_id);
		user.setApp_version_code(""+info.versionCode);
		user.setApp_version_name(info.versionName);
		
		int cnt = 0;
		if(!user_id.equals("")) {
			cnt = db.countDuplicateUser(user_id);
			Log.d("DEBUG", "ServerCommunication Count User: "+cnt);
			
			if(cnt > 0)
				db.updateUser(user);
			else
				db.addUser(user);
		}
		
		return true;
		
		

	}
}