package com.ivisionr.cit2adm;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Window;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.ivisionr.cit2adm.Util.GPSTracker;
import com.ivisionr.cit2adm.Util.SharePreferenceClass;

public class MyReportCurrentLocation extends FragmentActivity{
	private SupportMapFragment supportMapFragment;
	private GoogleMap mGoogleMap;
	private LatLng currentPosition;
	private GPSTracker gpsTracker;

	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.myreportcurrentlocation);
		gpsTracker=new GPSTracker(this);
		android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
		supportMapFragment = (SupportMapFragment) fragmentManager
				.findFragmentById(R.id.mareportcurrentlocation);
		mGoogleMap = supportMapFragment.getMap();
		if(mGoogleMap != null){

			mGoogleMap.setMyLocationEnabled(true);
			currentPosition=new LatLng(Double.parseDouble(new SharePreferenceClass(MyReportCurrentLocation.this).getLat()),Double.parseDouble(new SharePreferenceClass(MyReportCurrentLocation.this).getLon()));
			mGoogleMap.getUiSettings().setZoomControlsEnabled(true);
			mGoogleMap.getUiSettings().setCompassEnabled(true);
			mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);
			mGoogleMap.getUiSettings().setAllGesturesEnabled(true);
			mGoogleMap.setTrafficEnabled(true);	
			CameraPosition cameraPosition = new CameraPosition.Builder().target(currentPosition).zoom(12).build();
			mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

		}
		
		
	}

}
