package com.ivisionr.cit2adm.bin;

public class MyNeighbourDetails {
	
	private String id="";
	private String username="";
//	private String lastname="";
//	private String city="";
//	private String zip="";
//	private String lat="";
	//private String lon="";
	private String report_id="";
	private String report_auto_id="";
	//private String lon="";
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFirstname() {
		return username;
	}
	public void setFirstname(String username) {
		this.username = username;
	}
//	public String getLastname() {
//		return lastname;
//	}
//	public void setLastname(String lastname) {
//		this.lastname = lastname;
//	}
//	public String getCity() {
//		return city;
//	}
//	public void setCity(String city) {
//		this.city = city;
//	}
//	public String getZip() {
//		return zip;
//	}
//	public void setZip(String zip) {
//		this.zip = zip;
//	}
//	public String getLat() {
//		return lat;
//	}
//	public void setLat(String lat) {
//		this.lat = lat;
//	}
//	public String getLon() {
//		return lon;
//	}
//	public void setLon(String lon) {
//		this.lon = lon;
//	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getReport_id() {
		return report_id;
	}
	public void setReport_id(String report_id) {
		this.report_id = report_id;
	}
	public String getReport_auto_id() {
		return report_auto_id;
	}
	public void setReport_auto_id(String report_auto_id) {
		this.report_auto_id = report_auto_id;
	}

}
