package com.ivisionr.cit2adm.async;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.ivisionr.cit2adm.Util.Utils;
import com.ivisionr.cit2adm.Util.Webservicelink;
import com.ivisionr.cit2adm.bin.MyReportDetailsDescriptionlist;
import com.ivisionr.cit2adm.bin.MyReportDetailscomments;
import com.ivisionr.cit2adm.interfaces.MYReportDetailsImageInterface;

public class MyRepoerDetailsImageAsync extends AsyncTask<String , Void, String> {

	private Context _context;
	private Activity actity;
	ProgressDialog pd;
	private MyReportDetailsDescriptionlist myrepotdetailsdescriptionlist;
	private List<MyReportDetailsDescriptionlist> mydescriptionlist;
	private MyReportDetailscomments myreportdetailscomments;
	private List<MyReportDetailscomments> listmyreportdetailscomments;
	public MYReportDetailsImageInterface mylistinterface;
	
	private JSONArray jsonArray;
	private JSONObject placeObject;
	private  int jsonlength;

	public MyRepoerDetailsImageAsync(Activity contextAct){

		this.actity = contextAct;
		this._context = contextAct;		
		mydescriptionlist=new ArrayList<MyReportDetailsDescriptionlist>();
		listmyreportdetailscomments= new ArrayList<MyReportDetailscomments>();

	}


	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		mylistinterface.onReportDetailsImageStarted();
		//commentsinreport.onReportIssueStarted();
	}


	@Override
	protected String doInBackground(String... params) {

		myrepotdetailsdescriptionlist=serch(params);

		return " ";
	}


	private MyReportDetailsDescriptionlist serch(String[] params) {


		String useid = params[0];
		String result="";		
		String url = Webservicelink.serverMenuReportDescriptionURL;
		myrepotdetailsdescriptionlist=new MyReportDetailsDescriptionlist();
		try
		{
			JSONObject registerJson = new JSONObject();
			registerJson.put("id",useid );	
			List<NameValuePair> namevaluepair = new ArrayList<NameValuePair>();
			namevaluepair.add(new BasicNameValuePair("data", registerJson
					.toString()));			
			String paramString = URLEncodedUtils.format(namevaluepair, "utf-8");
			url += paramString;	
			
			Log.d("DEBUG", "MyReportDetailsAysnc URL: " +url);
			HttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(url);			
			HttpResponse response = null;
			try {
				response = httpClient.execute(httpPost);
			} catch (ClientProtocolException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} 
			try {
				HttpEntity entity = response.getEntity();
				InputStream instream = entity.getContent();
				result = Utils.convertStreamToString(instream);
				Log.d("DEBUG", "MyReportDetailsAysnc Result: " +result);
				myrepotdetailsdescriptionlist=getDescription(result);
				listmyreportdetailscomments=getComments(result);
				Log.d("DEBUG","MyReportDetailsAysnc Num Records: " +listmyreportdetailscomments.size());
				
			} catch (IOException e) {				
				e.printStackTrace();
			}
		}catch(Exception e){
			e.printStackTrace();

		}

		return myrepotdetailsdescriptionlist;


	}





	private List<MyReportDetailscomments> getComments(String result) throws JSONException {
		
		JSONObject jsonObjectforcomments=new JSONObject(result);
		JSONArray jsonarray = null;
		if(jsonObjectforcomments.has("comments"))
		jsonarray = jsonObjectforcomments.getJSONArray("comments");

        if(jsonarray != null) {
            for (int i = 0; i < jsonarray.length(); i++) {
                myreportdetailscomments = new MyReportDetailscomments();
                JSONObject placeObject = (JSONObject) jsonarray.get(i);
                myreportdetailscomments.setId(placeObject.getString("id"));
                myreportdetailscomments.setFirstname(placeObject.getString("username"));
                //myreportdetailscomments.setLastname(placeObject.getString("last_name"));
                myreportdetailscomments.setCreated(placeObject.getString("created"));
                myreportdetailscomments.setComments(placeObject.getString("comment"));
                listmyreportdetailscomments.add(myreportdetailscomments);

            }
        }
		return listmyreportdetailscomments;
		
	}


	private MyReportDetailsDescriptionlist getDescription(String result) throws JSONException {
		myrepotdetailsdescriptionlist=new MyReportDetailsDescriptionlist();
		JSONObject jsonObject = null;
		JSONObject obj=null;
		try {
			jsonObject = new JSONObject(result);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			obj=jsonObject.getJSONObject("issue");
			
			Log.d("DEBUG", "MyReportDetailsAysnc Object: " +obj);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			myrepotdetailsdescriptionlist.setIssucreated(obj.getString("created"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			myrepotdetailsdescriptionlist.setIssuedescription(obj.getString("description"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			myrepotdetailsdescriptionlist.setIssueicon(obj.getString("image_1_max"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			myrepotdetailsdescriptionlist.setIssuereportid(obj.getString("report_id"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			myrepotdetailsdescriptionlist.setIssuesmsvalue(obj.getString("num_comments"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			myrepotdetailsdescriptionlist.setIssueratingplus(obj.getString("num_ratings_plus"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			myrepotdetailsdescriptionlist.setIssueratingminus(obj.getString("num_ratings_minus"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		myrepotdetailsdescriptionlist.setIssuetypeid(obj.getString("issue_type_id"));
		myrepotdetailsdescriptionlist.setIssuestatus(obj.getString("status"));
		myrepotdetailsdescriptionlist.setId(obj.getString("id"));
		myrepotdetailsdescriptionlist.setLat(obj.getString("lat"));
		myrepotdetailsdescriptionlist.setLon(obj.getString("lon"));
		myrepotdetailsdescriptionlist.setFirstname(obj.getString("username"));
		myrepotdetailsdescriptionlist.setLastname(obj.getString("last_name"));
		myrepotdetailsdescriptionlist.setIssuetypenmae(obj.getString("issue_type_name"));
		try {
			myrepotdetailsdescriptionlist.setIssueaddress(obj.getString("formatted_address"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return myrepotdetailsdescriptionlist;		
		
		
	}


	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);		
		mylistinterface.onReportDetailsImagecompleted(myrepotdetailsdescriptionlist);
		Log.d("againcheck", "againcheck" +listmyreportdetailscomments.size());
		
		
		if(listmyreportdetailscomments.size()>0){
			mylistinterface.onReportIssueWithCommentsCompleted(listmyreportdetailscomments);
		}else{
			mylistinterface.onReportIssueWithCommentsnoData();
		}

	}


}
