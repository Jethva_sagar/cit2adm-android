package com.ivisionr.cit2adm;



import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ivisionr.cit2adm.Util.SharePreferenceClass;
import com.ivisionr.cit2adm.Util.Utils;
import com.ivisionr.cit2adm.async.IssueserchbyplaceAsync;
import com.ivisionr.cit2adm.async.IssuserchbyidAsync;
import com.ivisionr.cit2adm.bin.Issueserchbyplacebin;
import com.ivisionr.cit2adm.interfaces.Issueserchbyidinterface;
import com.ivisionr.cit2adm.interfaces.Issueserchbyinterfaces;

public class Issueserchbyplace extends FragmentActivity implements Issueserchbyinterfaces,Issueserchbyidinterface{
	private SupportMapFragment supportMapFragment;
	private GoogleMap mGoogleMap;
	private LatLng currentPosition;
	private EditText serchaddress;
	private Button serchissue;
	private String address="";
	private ProgressDialog mprogressDialog;
	private List<Issueserchbyplacebin> issuelist;
	private MarkerOptions markerOptions;
	private LatLng latLng;
	private String changedlat="";
	private String changedlon="";
	private int i=0;
	private Marker marker=null;

	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.issueserchbyplace);
		serchaddress=(EditText)findViewById(R.id.editlocation);
		serchissue=(Button)findViewById(R.id.serchissue);
		issuelist=new ArrayList<Issueserchbyplacebin>();

		mprogressDialog=new ProgressDialog(Issueserchbyplace.this);
		mprogressDialog.setMessage(getResources().getText(R.string.loading));

		initializeMap();

		serchissue.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				address=serchaddress.getText().toString();

				if(Utils.checkConnectivity(Issueserchbyplace.this)){
					mGoogleMap.clear();
					//new GeocoderTask().execute(address);
					if(address.length()==11 && address.startsWith("C")){
                      
						IssuserchbyidAsync issueserchbyid=new IssuserchbyidAsync(Issueserchbyplace.this);
						issueserchbyid.issueserchbyid=Issueserchbyplace.this;
						issueserchbyid.execute(address);
						

					}else{
						IssueserchbyplaceAsync issueserchbyasync=new IssueserchbyplaceAsync(Issueserchbyplace.this);
						issueserchbyasync.issueserchbyinterface=Issueserchbyplace.this;
						issueserchbyasync.execute(address);
					}

				}else{
					showNetworkDialog("internet");
				}

			}
		});

		if(mGoogleMap != null) {
			mGoogleMap.setOnMarkerClickListener(new OnMarkerClickListener() {

				@Override
				public boolean onMarkerClick(Marker marker) {
					Issueserchbyplace.this.marker = marker;

					if (null != marker)
						if (!marker.getTitle().equals(null))
							if (marker.getTitle().length() > 0) {

								Intent intent = new Intent(Issueserchbyplace.this, Issudetails.class);
								intent.putExtra("id", marker.getTitle());
								startActivity(intent);
							}


					return false;
				}
			});
		}
	}



	private void initializeMap() {
		android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
		supportMapFragment = (SupportMapFragment) fragmentManager
				.findFragmentById(R.id.serchbyplace);
		mGoogleMap = supportMapFragment.getMap();
		if(mGoogleMap != null){

			mGoogleMap.setMyLocationEnabled(true);
			if(changedlat.length()>0 && changedlon.length()>0){            	
				currentPosition=new LatLng(Double.parseDouble(changedlat), Double.parseDouble(changedlon));
			}else{
				currentPosition=new LatLng(Double.parseDouble(new SharePreferenceClass(Issueserchbyplace.this).getLat()),Double.parseDouble(new SharePreferenceClass(Issueserchbyplace.this).getLon()));
			}
			mGoogleMap.getUiSettings().setZoomControlsEnabled(true);
			mGoogleMap.getUiSettings().setCompassEnabled(true);
			mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);
			mGoogleMap.getUiSettings().setAllGesturesEnabled(true);
			mGoogleMap.setTrafficEnabled(true);	
			CameraPosition cameraPosition = new CameraPosition.Builder().target(currentPosition).zoom(10).build();
			mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

		}

	}



	@SuppressWarnings("deprecation")
	public void showNetworkDialog(final String message){
		// final exit of application
		AlertDialog.Builder builder = new AlertDialog.Builder(Issueserchbyplace.this);
		builder.setTitle(getResources().getString(R.string.app_name));
		if(message.equals("gps")){
			builder.setMessage(getResources().getString(R.string.gps_message));
		}
		else if(message.equals("internet")){
			builder.setMessage(getResources().getString(R.string.net_message));
		}
		AlertDialog alert = builder.create();
		alert.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				if(message.equals("gps")){
					Intent i=new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);		
					startActivityForResult(i, 1);
				}
				else if(message.equals("internet")){
					Intent i=new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS);		
					startActivityForResult(i,2);		
					Intent i1=new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS);		
					startActivityForResult(i1,3);	
				}
				else{
					//do nothing
				}
			}
		});
		// Showing Alert Message
		alert.show();	
	}

	@Override
	public void Issueserchbyplacenodata() {
		// TODO Auto-generated method stub
		if(null != mprogressDialog){
			mprogressDialog.dismiss();
		}
		//changedMapview();
		Toast.makeText(Issueserchbyplace.this, R.string.noissue, Toast.LENGTH_LONG).show();
	}







	@Override
	public void inissueserchbyplacestarted() {
		// TODO Auto-generated method stub
		if(null !=mprogressDialog){
			mprogressDialog.show();
		}

	}

	@Override
	public void onissuebyplace(List<Issueserchbyplacebin> listplace,String lat,String lon) {
		issuelist.addAll(listplace);
		changedlat=lat;
		Log.d("checks","checks" +changedlat);
		Log.d("checks1","checks1" +changedlon);
		changedlon=lon;
		initializeMap();
		Log.d("checks", "checks" +issuelist.size());
		if(null !=mprogressDialog){
			mprogressDialog.dismiss();
		}
		/*mGoogleMap.clear();
		android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
		supportMapFragment = (SupportMapFragment) fragmentManager
				.findFragmentById(R.id.serchbyplace);
		mGoogleMap = supportMapFragment.getMap();
		if(mGoogleMap != null){

			mGoogleMap.setMyLocationEnabled(true);
			currentPosition=new LatLng(Double.parseDouble(lat),Double.parseDouble(lon));
			mGoogleMap.getUiSettings().setZoomControlsEnabled(true);
			mGoogleMap.getUiSettings().setCompassEnabled(true);
			mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);
			mGoogleMap.getUiSettings().setAllGesturesEnabled(true);
			mGoogleMap.setTrafficEnabled(true);	
			CameraPosition cameraPosition = new CameraPosition.Builder().target(currentPosition).zoom(12).build();
			mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

		}*/


		for(i=0; i<issuelist.size(); i++){
			markerOptions=new MarkerOptions(); 


			if(issuelist.get(i).getStatus().equals("open")){				
				latLng=new LatLng(Double.parseDouble(issuelist.get(i).getLat()),Double.parseDouble(issuelist.get(i).getLon()));				
				markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.open)).title(issuelist.get(i).getId());
				mGoogleMap.addMarker(markerOptions.position(latLng));



			}else if(issuelist.get(i).getStatus().equals("closed")){
				latLng=new LatLng(Double.parseDouble(issuelist.get(i).getLat()),Double.parseDouble(issuelist.get(i).getLon()));				
				markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.closed)).title(issuelist.get(i).getId());
				mGoogleMap.addMarker(markerOptions.position(latLng));


			}else if(issuelist.get(i).getStatus().equals("completed")){
				latLng=new LatLng(Double.parseDouble(issuelist.get(i).getLat()),Double.parseDouble(issuelist.get(i).getLon()));
				markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.completed)).title(issuelist.get(i).getId());
				mGoogleMap.addMarker(markerOptions.position(latLng));


			}else if(issuelist.get(i).getStatus().equals("progress")){
				latLng=new LatLng(Double.parseDouble(issuelist.get(i).getLat()),Double.parseDouble(issuelist.get(i).getLon()));
				markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.progress)).title(issuelist.get(i).getId());
				mGoogleMap.addMarker(markerOptions.position(latLng));


			}else if(issuelist.get(i).getStatus().equals("assigned")){
				latLng=new LatLng(Double.parseDouble(issuelist.get(i).getLat()),Double.parseDouble(issuelist.get(i).getLon()));
				markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.assigned)).title(issuelist.get(i).getId());
				mGoogleMap.addMarker(markerOptions.position(latLng));

			}
		}
	}




	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();	
		changedlat="";
		changedlon="";
		Issueserchbyplace.this.finish();
	}



	@Override
	public void onisueserchbyidstarted() {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void onissueserchbyidcompleted(String id) {
		// TODO Auto-generated method stub
		Log.d("checkissue", "checkissue" +id);
		Intent intent=new Intent(Issueserchbyplace.this,Issudetails.class);
		intent.putExtra("id", id);
		startActivity(intent);
		finish();
		
	}



	




}





