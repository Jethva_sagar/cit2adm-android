package com.ivisionr.cit2adm.async;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.ivisionr.cit2adm.Util.SharePreferenceClass;
import com.ivisionr.cit2adm.bin.MyReportDeetails;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sanjay12 on 6/12/2016.
 */
public class MynewPasswordAsync  extends AsyncTask<String, Void, String> {

    private Context _context;
    private Activity actity;
    String fuctionalityName;
    private MyReportDeetails myreportDetails;
    ProgressDialog pd;
    ServerCommunication serverCommunication;
    ServerResponceCheck serverResponceCheck;
    SharePreferenceClass sharePreferenceClass;
    private List<MyReportDeetails> lstmyreportDetails;
    private JSONArray jsonArray;
    private JSONObject placeObject;
    private int jsonlength;
    Dialog alert_dialog, alert_dialog2;
    String error = "";

    public MynewPasswordAsync(Activity contextAct, String fuctionalityName1) {
        this.actity = contextAct;
        this._context = contextAct;
        this.fuctionalityName = fuctionalityName1;

        serverCommunication = new ServerCommunication(_context);
        serverResponceCheck = new ServerResponceCheck(_context);
        sharePreferenceClass = new SharePreferenceClass(_context);
        lstmyreportDetails = new ArrayList<MyReportDeetails>();

        Log.d("DEBUG", "Universal Async Context: " + _context);

    }

    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub
        super.onPreExecute();
        pd = new ProgressDialog(_context);
        pd.setMessage("Loading");

        if(pd != null && pd.isShowing())
            pd.show();
    }

    @Override
    protected String doInBackground(String... params) {
        // TODO Auto-generated method stub
        String param = params[0];
        Log.v("Response", param);
        String page = "";
        if (fuctionalityName.equals("NewPassWord")) {
            Log.d("DEBUG", "Inside NewPassWord");
            page = serverCommunication.ResetPassInServer(params);
        }

        return page;
    }


    @Override
    protected void onPostExecute(String result) {
        // TODO Auto-generated method stub
        super.onPostExecute(result);
        Log.d("DEBUG", "UniversalAsyc Result is: " + result);
        if(pd != null && pd.isShowing())
            pd.dismiss();
    }
}