package com.ivisionr.cit2adm.Util;

import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.util.Log;

public class MobileUtils extends Activity

{
	public static void setLanguage(Activity activity, Context context) {


		try {

			SharedPreferences lPref = activity.getSharedPreferences(
					SystemConstants.DEFAULT_PREFERENCE, MODE_PRIVATE);
			String lLocaleVal = lPref.getString(
					SystemConstants.TXT_PERSIST_LANGUAGE, "");
			
			Log.d("DEBUG", "Mobile Util - " +lLocaleVal);
			Locale lLocale = new Locale(lLocaleVal);
			Locale.setDefault(lLocale);
			Configuration lConfig = new Configuration();
			lConfig.locale = lLocale;
			context.getResources().updateConfiguration(lConfig,
					context.getResources().getDisplayMetrics());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String getSavedPref(Activity activity, String key) {
		try {
			SharedPreferences lPref = activity.getSharedPreferences(
					SystemConstants.DEFAULT_PREFERENCE, MODE_PRIVATE);
			return lPref.getString(key, "");
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "";
	}

	public static void updateLanguage(Activity activity, Context context,
			String language) {
		try {
			SharedPreferences lPref = activity.getSharedPreferences(
					SystemConstants.DEFAULT_PREFERENCE, MODE_PRIVATE);
			SharedPreferences.Editor editor = lPref.edit();
			Log.d("mylanguage", "mylanguage" +language);
			editor.putString(SystemConstants.TXT_PERSIST_LANGUAGE, language);
			editor.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
