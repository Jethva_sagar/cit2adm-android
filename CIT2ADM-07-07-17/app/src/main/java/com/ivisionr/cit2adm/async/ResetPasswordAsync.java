package com.ivisionr.cit2adm.async;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import com.ivisionr.cit2adm.Util.Webservicelink;
import com.ivisionr.cit2adm.interfaces.ResetPasswordInterface;

public class ResetPasswordAsync extends AsyncTask<String, Void, String> {

	public ResetPasswordInterface resetpasswordinterface;
	private Activity activity;

	String error_code = "";
	private String page = "no";

	public ResetPasswordAsync(Activity activity) {
		this.activity = activity;
	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub

		String id = params[0];
		Log.d("id", "id" + id);

		String current_password = params[1];
		Log.d("current_password", "current_password" + current_password);

		String new_password = params[2];
		Log.d("new_password", "new_password" + new_password);

		String confirm_password = params[3];
		Log.d("confirm_password", "confirm_password" + confirm_password);

		String url = Webservicelink.resetPasswordurl;
		JSONObject jsonObject = new JSONObject();

		try {
			jsonObject.put("id", id);
			jsonObject.put("password", current_password);
			jsonObject.put("new_password", new_password);
			jsonObject.put("confirm_password", confirm_password);

			List<NameValuePair> namevaluepair = new ArrayList<NameValuePair>();
			namevaluepair.add(new BasicNameValuePair("data", jsonObject
					.toString()));
			ResponseHandler<String> resHandler = new BasicResponseHandler();
			String paramString = URLEncodedUtils.format(namevaluepair, "utf-8");
			url += paramString;
			Log.d("checkurl", "checkurl" + url);
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpPost httpGet = new HttpPost(url);
			page = httpClient.execute(httpGet, resHandler);
			JSONObject jsobj = new JSONObject(page.toString());
			Log.d("checkurl1", "checkurl1" + jsobj);

			error_code = jsobj.getString("error_code");
			//Log.d("error_code")

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return error_code;
	}

	@Override
	protected void onPostExecute(String success) {
		// TODO Auto-generated method stub
		super.onPostExecute(success);

		resetpasswordinterface.onCompleted(error_code);

	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		resetpasswordinterface.onStarted();
	}

}
