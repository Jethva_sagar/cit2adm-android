package com.ivisionr.cit2adm;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ivisionr.cit2adm.Util.SharePreferenceClass;
import com.ivisionr.cit2adm.Util.Utils;

public class NewSplashscreen  extends Activity {

	private TextView tabtocontinue;
	private LinearLayout mainicon;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.newsplashscreen);
		tabtocontinue=(TextView)findViewById(R.id.tabtocontinue);
		mainicon=(LinearLayout)findViewById(R.id.mainicon);
		mainicon.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if(Utils.checkConnectivity(NewSplashscreen.this)){
					new SharePreferenceClass(NewSplashscreen.this).setLat("");
					new SharePreferenceClass(NewSplashscreen.this).setLon("");	
					new SharePreferenceClass(NewSplashscreen.this).savestatus("");

					Intent in = new Intent(NewSplashscreen.this, LandingScreenActivity.class);
					startActivity(in);
					finish();
				}else{
					showNetworkDialog("internet");
				}

			}
		});
	}

	@SuppressWarnings("deprecation")
	public void showNetworkDialog(final String message){
		// final exit of application
		AlertDialog.Builder builder = new AlertDialog.Builder(NewSplashscreen.this);
		builder.setTitle(getResources().getString(R.string.app_name));
		if(message.equals("gps")){
			builder.setMessage(getResources().getString(R.string.gps_message));
		}
		else if(message.equals("internet")){
			builder.setMessage(getResources().getString(R.string.net_message));
		}
		AlertDialog alert = builder.create();
		alert.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				if(message.equals("gps")){
					Intent i=new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);		
					startActivityForResult(i, 1);
				}
				else if(message.equals("internet")){
					Intent i=new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS);		
					startActivityForResult(i,2);		
					Intent i1=new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS);		
					startActivityForResult(i1,3);	
				}
				else{
					//do nothing
				}
			}
		});
		// Showing Alert Message
		alert.show();	
	}


}
