package com.ivisionr.cit2adm.async;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.ivisionr.cit2adm.Util.Utils;
import com.ivisionr.cit2adm.Util.Webservicelink;
import com.ivisionr.cit2adm.interfaces.TermsandConditionInterface;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class TermeandConditionasync extends AsyncTask<String, Void, String> {
	private Context _context;
	private Activity actity;
	private String termstext="";
	public TermsandConditionInterface termscondition;
	private String termspage;
	private String url="";
	public TermeandConditionasync(Activity activity){
		this.actity = activity;
		this._context = activity;
	}
	@Override
	protected String doInBackground(String... params) {
		termspage=serch(params);
		return null;
	}
	private String serch(String[] params) {
		// TODO Auto-generated method stub
		String useid = params[0];
		url = Webservicelink.termscondition;
		String result="";
		
		try {
			JSONObject registerJson = new JSONObject();
			registerJson.put("lang", useid);
			List<NameValuePair> namevaluepair = new ArrayList<NameValuePair>();
			namevaluepair.add(new BasicNameValuePair("data", registerJson
					.toString()));
			String paramString = URLEncodedUtils.format(namevaluepair, "utf-8");
			url += paramString;

			Log.d("DEBUG", "Terms Conditions Aysnc URL: " + url);
			HttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(url);
			HttpResponse response = null;
			try {
				response = httpClient.execute(httpPost);
			} catch (ClientProtocolException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				HttpEntity entity = response.getEntity();
				InputStream instream = entity.getContent();
				result = Utils.convertStreamToString(instream);
				
				Log.d("DEBUG", "Terms Conditions Aysnc Result" + result);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();

		}
		try {
			JSONObject jsonObject=new JSONObject(result);
			termspage=jsonObject.getString("terms");
			Log.d("DEBUG", "Terms and Conditions: " +termstext);
			
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return termspage;
	}
	@Override
	protected void onPostExecute(String result) {
		
		// TODO Auto-generated method stub
		Log.d("DEBUG", "Terms and Conditions Async Page: " +termspage);
		termscondition.completedtermsandcondition(url);
	}
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		termscondition.onstarted();
	}

}
