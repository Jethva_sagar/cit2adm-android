package com.ivisionr.cit2adm.async;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.ivisionr.cit2adm.LoginActivity;
import com.ivisionr.cit2adm.R;
import com.ivisionr.cit2adm.Util.SharePreferenceClass;
import com.ivisionr.cit2adm.Util.Utils;
import com.ivisionr.cit2adm.Util.otherStaticValue;
import com.ivisionr.cit2adm.bin.MyReportDeetails;

public class UnivarsalAsynkTask extends AsyncTask<String, Void, String> {

	private Context _context;
	private Activity actity;
	String fuctionalityName;
	private MyReportDeetails myreportDetails;
	ProgressDialog pd;
	ServerCommunication serverCommunication;
	ServerResponceCheck serverResponceCheck;
	SharePreferenceClass sharePreferenceClass;
	private List<MyReportDeetails> lstmyreportDetails;
	private JSONArray jsonArray;
	private JSONObject placeObject;
	private int jsonlength;
	Dialog alert_dialog, alert_dialog2;
	String error = "";

	public UnivarsalAsynkTask(Activity contextAct, String fuctionalityName1) {
		this.actity = contextAct;
		this._context = contextAct;
		this.fuctionalityName = fuctionalityName1;
		pd = new ProgressDialog(_context);

		serverCommunication = new ServerCommunication(_context);
		serverResponceCheck = new ServerResponceCheck(_context);
		sharePreferenceClass = new SharePreferenceClass(_context);
		lstmyreportDetails = new ArrayList<MyReportDeetails>();
		
		Log.d("DEBUG", "Universal Async Context: "+_context);
		pd.setMessage(_context.getResources().getText(R.string.logginin));
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		if (null != pd) {
			pd.show();
		}

	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub
		String param = params[0];
		Log.v("Response", param);
		String page = "";
		if (fuctionalityName.equals("Registration")) {
			Log.d("DEBUG", "Inside Registration");
			page = serverCommunication.registrationInServer(params);
		} else if (fuctionalityName.equals("Login")) {
			Log.d("DEBUG", "Inside Login");
			page = serverCommunication.LoginInServer(params);

		} else if (fuctionalityName.equals("Report")) {
			Log.d("DEBUG", "Inside Report");
			page = serverCommunication.ReportInServer(params);
		} else if (fuctionalityName.equals("ForgotPass")) {
			Log.d("DEBUG", "Inside ForgotPass");
			page = serverCommunication.ResetPassInServer(params);
		}

		return page;
	}

	

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		Log.d("DEBUG", "UniversalAsyc Result is: " + result);
		pd.dismiss();
		error = "";
		
		JSONObject errorCode = null;
		
		try {
			
			if(Utils.isJSONValid(result)) {
				
				errorCode = new JSONObject(result);
				
				if (errorCode.has("error_code"))
				{
					error = errorCode.getString("error_code");				
				}
				
				// For Registration
				if (error.equals("S20200")) {
					// Log.d("checking", "checking" +result);
					String instruction = "loginpage";
	
					Toast t = Toast.makeText(_context,
							R.string.registrationsuccessfull, Toast.LENGTH_SHORT);
					t.setGravity(Gravity.CENTER, 0, 0);
					t.show();
					Intent intent = new Intent(_context, LoginActivity.class);
					_context.startActivity(intent);
					actity.finish();
					// alertDialogShow(hasmapError.get(result), instruction);
	
				} 
				else if (error.equals("S20211"))
				{
					Toast toast = Toast.makeText(_context,
							R.string.invalidemail, Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}
				else if (error.equals("S20213"))
				{
					Toast toast = Toast.makeText(_context,
							R.string.invalidusername, Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}
				else if (error.equals("S20216"))
				{
					Toast toast = Toast.makeText(_context,
							R.string.duplicateemail, Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}
				else if (error.equals("S20219"))
				{
					Toast toast = Toast.makeText(_context,
							R.string.latnotfound, Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}
				else if (error.equals("S20220"))
				{
					Toast toast = Toast.makeText(_context,
							R.string.latinvalid, Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}
				else if (error.equals("S20221"))
				{
					Toast toast = Toast.makeText(_context,
							R.string.longnotfound, Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}
				else if (error.equals("S20222"))
				{
					Toast toast = Toast.makeText(_context,
							R.string.longinvalid, Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}
				else if (error.equals("S20100")) {
					String instruction = "LoginFinish";
					sharePreferenceClass.setValue_string(
							otherStaticValue.loginState, "yes");
					Toast t = Toast.makeText(_context, R.string.successfullogin,
							Toast.LENGTH_SHORT);
					t.setGravity(Gravity.CENTER, 0, 0);
					t.show();
					actity.finish();
					// alertDialogShow(hasmapError.get(result), instruction);
					
				
					
					
					
				} else if (error.equals("S20809")) {
					// Log.d("checking", "checking" +result);
					Toast t = Toast.makeText(_context,
							R.string.successfullreportpost, Toast.LENGTH_SHORT);
					t.setGravity(Gravity.CENTER, 0, 0);
					t.show();
					actity.finish();
					pd.dismiss();
	
				} else if (error.equals("S20107")) {
					// Log.d("checking", "checking" +result);
					Toast t = Toast.makeText(_context,
							R.string.accessidnotvalid, Toast.LENGTH_SHORT);
					t.setGravity(Gravity.CENTER, 0, 0);
					t.show();
					actity.finish();
					pd.dismiss();
	
				} else if(error.equals("S20104")) {
					// Log.d("checking", "checking" +result);
					String instruction = "LoginFinish";
					sharePreferenceClass.setValue_string(otherStaticValue.loginState, "yes");
					
					
					alert_dialog = new Dialog(_context);
					alert_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					alert_dialog.setContentView(R.layout.dialog_alert);
					alert_dialog.show();
					TextView alert_msg = (TextView) alert_dialog
							.findViewById(R.id.alert_msg);
					Button alert_ok = (Button) alert_dialog
							.findViewById(R.id.alert_ok);
	
					alert_msg
							.setText(R.string.successfullloginfree);
	
					alert_ok.setOnClickListener(new OnClickListener() {
	
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
	
							alert_dialog.dismiss();
							actity.finish();
						}
					});
				
					//Toast t = Toast.makeText(_context, R.string.successfullloginfree,
							//Toast.LENGTH_LONG);
					//t.setGravity(Gravity.CENTER, 0, 0);
					//t.show();
					//actity.finish();
					
					
				} else if(error.equals("S20105")) {
					// Log.d("checking", "checking" +result);
					
					
					alert_dialog = new Dialog(_context);
					alert_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					alert_dialog.setContentView(R.layout.dialog_alert);
					alert_dialog.show();
					TextView alert_msg = (TextView) alert_dialog
							.findViewById(R.id.alert_msg);
					Button alert_ok = (Button) alert_dialog
							.findViewById(R.id.alert_ok);
	
					alert_msg
							.setText(R.string.successfullloginfreecheck);
	
					
					alert_ok.setOnClickListener(new OnClickListener() {
	
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
	
							alert_dialog.dismiss();
							
						}
					});
					
					
					
					
				} else if(error.equals("S20223")){
					Toast t = Toast.makeText(_context, R.string.checkregister,
							Toast.LENGTH_LONG);
					t.setGravity(Gravity.CENTER, 0, 0);
					t.show();
					
				} else if(error.equals("S20224")){
					Toast t = Toast.makeText(_context, R.string.promocodeexired,
							Toast.LENGTH_LONG);
					t.setGravity(Gravity.CENTER, 0, 0);
					t.show();
					
				} else if (error.equals("S20101")) {
					Log.d("DEBUG", "UniversalAsync Inside Failed Login: " +result);
					Toast t = Toast.makeText(_context, R.string.usernotexist,
							Toast.LENGTH_SHORT);
					t.setGravity(Gravity.CENTER, 0, 0);
					t.show();
					//actity.finish();
					// alertDialogShow(hasmapError.get(result), instruction);
					
				}
				
				// Start of Forget Pass
				else if (error.equals("S20301")) {
					
					
					
					alert_dialog = new Dialog(_context);
					alert_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					alert_dialog.setContentView(R.layout.dialog_alert);
					alert_dialog.show();
					TextView alert_msg = (TextView) alert_dialog
							.findViewById(R.id.alert_msg);
					Button alert_ok = (Button) alert_dialog
							.findViewById(R.id.alert_ok);
	
					alert_msg
							.setText(R.string.emailnotsent);
	
					alert_ok.setOnClickListener(new OnClickListener() {
	
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
	
							alert_dialog.dismiss();
							actity.finish();
						}
					});
			        
					
				} 
				else if (error.equals("S20302")) {
					
					Toast t = Toast.makeText(_context, R.string.passwordnotupdated,
							Toast.LENGTH_SHORT);
					t.setGravity(Gravity.CENTER, 0, 0);
					t.show();
					
				} 
				else if (error.equals("S20303")) {
					
					Toast t = Toast.makeText(_context, R.string.emailnotexist,
							Toast.LENGTH_SHORT);
					t.setGravity(Gravity.CENTER, 0, 0);
					t.show();
					
				} 
				else if (error.equals("S20303")) {
					
					Toast t = Toast.makeText(_context, R.string.passwordchanged,
							Toast.LENGTH_SHORT);
					t.setGravity(Gravity.CENTER, 0, 0);
					t.show();
					
				} 
				else if (error.equals("S20304")) {
					
					Toast t = Toast.makeText(_context, R.string.emailnotfound,
							Toast.LENGTH_SHORT);
					t.setGravity(Gravity.CENTER, 0, 0);
					t.show();
					
				} 
				else if (error.equals("S20300")) {
					Toast t = Toast.makeText(_context, R.string.passwordchanged,
							Toast.LENGTH_SHORT);
					t.setGravity(Gravity.CENTER, 0, 0);
					t.show();
					
					Intent intent = new Intent(_context, LoginActivity.class);
					_context.startActivity(intent);
					actity.finish();
					
				} 
				
				
				/*else {
					
					alertDialogShow(hasmapError.get(result), "no");
				}*/
				
			}
			
		
		} catch (JSONException e) {
			Log.d("DEBUG", "UniversalAsync Exception: "+e.toString());
		}
		
		
		
		HashMap<String, String> hasmapError = ServerResponceCheck.set();

		if (hasmapError.containsKey(result)) {

		}

	}

	@SuppressWarnings("deprecation")
	public void alertDialogShow(String Message, final String instruction) {

		final AlertDialog alertDialog = new AlertDialog.Builder(_context)
				.create();

		// Setting Dialog Message
		alertDialog.setMessage(Message);

		// Setting OK Button
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				alertDialog.dismiss();

				if (instruction.equals("loginpage")) {
					Intent intent = new Intent(_context, LoginActivity.class);
					_context.startActivity(intent);
					actity.finish();
				} else if (instruction.equals("LoginFinish")) {
					sharePreferenceClass.setValue_string(
							otherStaticValue.loginState, "yes");
					actity.finish();
				}

			}
		});

		alertDialog.show();
	}

}
