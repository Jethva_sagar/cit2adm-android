package com.ivisionr.cit2adm.async;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.ivisionr.cit2adm.Util.Webservicelink;
import com.ivisionr.cit2adm.interfaces.UpdateGpsInterface;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

public class UpdateGpsAsync extends AsyncTask<String, Void, String> {

	public UpdateGpsInterface updategpsinterface;
	private Activity activity;

	String error_code = "";
	private String page = "no";

	public UpdateGpsAsync(Activity activity) {
		this.activity = activity;
	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub

		String id = params[0];
		Log.d("id", "id" + id);

		String current_lat = params[1];
		Log.d("DEBUG", "Current Lat" + current_lat);

		String current_lon = params[2];
		Log.d("DEBUG", "Current Lon" + current_lon);

		String url = Webservicelink.updateGPSurl;
		JSONObject jsonObject = new JSONObject();

		try {
			jsonObject.put("id", id);
			jsonObject.put("lat", current_lat);
			jsonObject.put("lon", current_lon);

			List<NameValuePair> namevaluepair = new ArrayList<NameValuePair>();
			namevaluepair.add(new BasicNameValuePair("data", jsonObject
					.toString()));
			ResponseHandler<String> resHandler = new BasicResponseHandler();
			String paramString = URLEncodedUtils.format(namevaluepair, "utf-8");
			url += paramString;
			Log.d("DEBUG", "Update GPS URL: " + url);
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpGet httpGet = new HttpGet(url);
			page = httpClient.execute(httpGet, resHandler);
			JSONObject jsobj = new JSONObject(page.toString());
			Log.d("DEBUG", "Update GPS URL: " + jsobj);

			error_code = jsobj.getString("error_code");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return error_code;
	}

	@Override
	protected void onPostExecute(String success) {
		// TODO Auto-generated method stub
		super.onPostExecute(success);

		updategpsinterface.onCompleted(error_code);

	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		updategpsinterface.onStarted();
	}

}