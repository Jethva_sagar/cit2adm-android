package com.ivisionr.cit2adm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.ivisionr.cit2adm.adapter.Contactactiovationadpter;
import com.ivisionr.cit2adm.bin.Draft;
import com.ivisionr.cit2adm.database.AndroidDatabaseManager;
import com.ivisionr.cit2adm.database.DatabaseHandler;

public class MyDrafts extends Activity {
	private List<Draft> draftbin;
	private Button back;

	private ListView contatlist;
	private TextView draftdebug;
	private Contactactiovationadpter adpter;
	// private LoginDatabaseAdapter login;
	private DatabaseHandler db;
	private String issue_id = "", description = "", image = "", userid = "",
			issutypeid = "", imagename = "", draftlat = "", draftlon = "",
			language = "";
	private String TAG = "DEBUG";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activationcontact);
		draftbin = new ArrayList<Draft>();
		contatlist = (ListView) findViewById(R.id.contacactivation);
		draftdebug = (TextView) findViewById(R.id.draftdebug);

		back = (Button) findViewById(R.id.backinreporttopostdr);
		// login = new LoginDatabaseAdapter(this);
		// login = login.open();
		// dbh = new databasehandler(getApplicationContext());

		db = new DatabaseHandler(this);

		// db=openOrCreateDatabase("cit2adm.db",android.content.Context.MODE_PRIVATE,null);
		// db = dbh.getWritableDatabase();

		Log.d(TAG, "mydb: " + db);
		draftbin = db.getAllDrafts("");
		Collections.sort(draftbin);
		// draftbin=AllContactquery.getContact(db);
		Log.d("size", "size" + draftbin.size());
		adpter = new Contactactiovationadpter(this, draftbin);
		contatlist.setAdapter(adpter);

		if (draftdebug != null) {

			draftdebug.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent dbmanager = new Intent(getApplicationContext(),
							AndroidDatabaseManager.class);
					startActivity(dbmanager);

				}
			});

		}

		back.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();

			}
		});

		contatlist.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {

				issue_id = adpter.getItem(arg2).getId();

				/*
				 * description=adpter.getItem(arg2).getIssue_description();
				 * image=adpter.getItem(arg2).getIssue_image();
				 * userid=adpter.getItem(arg2).getUser_id();
				 * issutypeid=adpter.getItem(arg2).getIssue_type_id();
				 * imagename=adpter.getItem(arg2).getIssue_image();
				 * draftlat=adpter.getItem(arg2).getIssue_lat();
				 * draftlon=adpter.getItem(arg2).getIssue_lon();
				 * language=adpter.getItem(arg2).getIssue_lang();
				 */

				Log.d(TAG, "Draft Issue Id: " + issue_id);

				Intent intent = new Intent(MyDrafts.this, DraftReportPost.class);
				intent.putExtra("draft_issue_id", issue_id);

				startActivity(intent);
				MyDrafts.this.finish();
				// db.deleteDraft(issue_id);

			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.debug, menu);
		Log.d(TAG, "Inside menu");

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {

		case R.id.menu_dbviewer:
			Intent dbmanager = new Intent(getApplicationContext(),
					DatabaseHandler.class);
			startActivity(dbmanager);
			break;
		default:
			break;
		}

		return true;
	}

}
