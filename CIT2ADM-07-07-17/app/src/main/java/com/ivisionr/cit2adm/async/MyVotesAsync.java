package com.ivisionr.cit2adm.async;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ivisionr.cit2adm.Util.Webservicelink;
import com.ivisionr.cit2adm.bin.Myvotes;
import com.ivisionr.cit2adm.interfaces.Myvotesinterfaces;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class MyVotesAsync   extends AsyncTask<String, Void, String>{
	private Context _context;
	private Activity actity;
	private Myvotes myvotes;
	private List<Myvotes> listofmyvotes;
	public Myvotesinterfaces myvotesinterfaces;
	private JSONArray jsonArray;
	private JSONObject placeObject;
	private  int jsonlength;
	
	public MyVotesAsync(Activity activity){
		this.actity = activity;
		this._context = activity;
		listofmyvotes=new ArrayList<Myvotes>();
	}

	@Override
	protected String doInBackground(String... params) {
		listofmyvotes=serch(params);
		return null;
	}

	private List<Myvotes> serch(String[] params) {
		String useid = params[0];

		String page = "no";
		String url = Webservicelink.myvotes;

		try
		{
			JSONObject registerJson = new JSONObject();
			registerJson.put("user_id", useid);	
			List<NameValuePair> namevaluepair = new ArrayList<NameValuePair>();
			namevaluepair.add(new BasicNameValuePair("data", registerJson
					.toString()));
			ResponseHandler<String> resHandler = new BasicResponseHandler();
			String paramString = URLEncodedUtils.format(namevaluepair, "utf-8");
			url += paramString;			
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpGet httpGet = new HttpGet(url);
			page = httpClient.execute(httpGet, resHandler);
			Log.d("DEBUG", "Hello: " +page);
			JSONObject jsobj = new JSONObject(page.toString());
			JSONObject jsonjson = jsobj;
			
			try {
				jsonArray=jsonjson.getJSONArray("error_code");
				jsonlength=jsonArray.length();			
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			for(int i=0;i<jsonlength;i++){
				myvotes=new Myvotes();
				try {
					placeObject=(JSONObject) jsonArray.get(i);
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}


			if(placeObject.has("report_id"))
				myvotes.setReportid(placeObject.getString("report_id"));
			myvotes.setRatingplus(placeObject.getString("rating_plus"));
			myvotes.setRatingminus(placeObject.getString("rating_minus"));
			listofmyvotes.add(myvotes);
			
			}

		}	



		catch (ClientProtocolException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e("response", e.getMessage());
		}
		catch (JSONException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listofmyvotes;

	}

	@Override
	protected void onPostExecute(String result) {
		if(listofmyvotes.size()>0){
			myvotesinterfaces.onCompleted(listofmyvotes);
		}else{
			myvotesinterfaces.noData();
		}
	}

	@Override
	protected void onPreExecute() {
		myvotesinterfaces.onstarted();
	}

}
