package com.ivisionr.cit2adm.interfaces;

import java.util.List;

import com.ivisionr.cit2adm.bin.MyNeighbourDetails;


public interface MyNeighbour {
	
	public void onMyNeighbourStarted();
	public void onMyNeighbourCompleted(List<MyNeighbourDetails> list);
	public void onMyNeighbouringNodata();
	
	

}
