package com.ivisionr.cit2adm.bin;

import java.io.Serializable;

import com.ivisionr.cit2adm.Util.Utils;


@SuppressWarnings("serial")
public class MyReportDeetails  implements Serializable{
	
	private String report_id="";
	private String issue_type_name="";
	private String status="";
	private String num_comments="";
	private String description="";
	private String created="";
	private String active="";
	private String num_ratings_plus="";
	private String num_ratings_minus="";
	private String icon="";
	private String id="";
	private String address="";
	private String lat="",lan="";
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getReport_id() {
		return report_id;
	}
	public void setReport_id(String report_id) {
		this.report_id = report_id;
	}
	public String getIssue_type_name() {
		return issue_type_name;
	}
	public void setIssue_type_name(String issue_type_name) {
		this.issue_type_name = issue_type_name;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getNum_comments() {
		return num_comments;
	}
	public void setNum_comments(String num_comments) {
		this.num_comments = num_comments;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCreated() {
		return created;		
		//return Utils.convertDate(created);
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	public String getNum_ratings_plus() {
		return num_ratings_plus;
	}
	public void setNum_ratings_plus(String num_ratings_plus) {
		this.num_ratings_plus = num_ratings_plus;
	}
	public String getNum_ratings_minus() {
		return num_ratings_minus;
	}
	public void setNum_ratings_minus(String num_ratings_minus) {
		this.num_ratings_minus = num_ratings_minus;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}


	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getLan() {
		return lan;
	}

	public void setLan(String lan) {
		this.lan = lan;
	}
}
