package com.ivisionr.cit2adm.adapter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ivisionr.cit2adm.R;
import com.ivisionr.cit2adm.bin.Draft;

public class Contactactiovationadpter extends ArrayAdapter<Draft> {
	private LayoutInflater inflater;
	private Context mContext;
	private Bitmap captureimage = null;

	public Contactactiovationadpter(Context context,
			List<Draft> allcontactdetails) {
		super(context, R.layout.alllistcontact, R.id.textmyreportdetails,
				allcontactdetails);
		this.mContext = context;
		inflater = LayoutInflater.from(context);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final Draft draftItem = (Draft) this.getItem(position);
		ImageView imIssueImage = null;
		TextView tvIssueTypeName = null, tvIssueDesc = null, tvIssueDate = null;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.alllistcontact, null);
			imIssueImage = (ImageView) convertView
					.findViewById(R.id.myrepotimage);
			tvIssueTypeName = (TextView) convertView
					.findViewById(R.id.textmyreportdetails);

			tvIssueDesc = (TextView) convertView
					.findViewById(R.id.myrepotdescription);
			tvIssueDate = (TextView) convertView.findViewById(R.id.myissuedate);

			convertView.setTag(new ViewHolder(imIssueImage, tvIssueTypeName,
					tvIssueDesc, tvIssueDate));

		} else {
			ViewHolder viewHolder = (ViewHolder) convertView.getTag();
			imIssueImage = viewHolder.imIssueImage;
			tvIssueTypeName = viewHolder.tvIssueTypeName;
			tvIssueDesc = viewHolder.tvIssueDesc;

			tvIssueDate = viewHolder.tvIssueDate;

		}

		tvIssueTypeName.setText(draftItem.getIssue_type_name());
		/**
		 * format data dd MMMM YYYY
		 */
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat(
					"dd MMMM, yyyy HH:mm:ss");
			SimpleDateFormat dateFormat1 = new SimpleDateFormat(
					"yyyy-MM-dd HH:mm:ss");
			Date pdate = dateFormat1.parse(draftItem.getIssue_date());

			String date = dateFormat.format(pdate);
			// = dateFormat.format(draftItem.getIssue_date());
			tvIssueDate.setText(date);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			tvIssueDate.setText(draftItem.getIssue_date());
		}
		tvIssueDesc.setText(draftItem.getIssue_description());
		// sinflecontactname.setText(draftItem.getProfilename());
		// singlecontactno.setText("Description:"
		// +draftItem.getIssuedescription());

		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		// options.inSampleSize = 4;

		String imgPath = draftItem.getIssue_image();
		Log.d("DEBUG", "Draft Image Path 1: " + imgPath);

		try {
			if (imgPath != "") {

				Bitmap myBitmap = BitmapFactory.decodeFile(imgPath, options);

				// myBitmap = (Bitmap) data.getExtras().get("data");
				options.inJustDecodeBounds = false;

				//options.inSampleSize = 4;
				myBitmap = BitmapFactory.decodeFile(imgPath, options);

				int height = myBitmap.getHeight(), width = myBitmap.getWidth();

				if (height > 1280 && width > 960) {

					Bitmap myThumbBitmap = BitmapFactory.decodeFile(imgPath,
							options);

					imIssueImage.setImageBitmap(myThumbBitmap);
				} else {
					imIssueImage.setImageBitmap(myBitmap);

				}

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// captureimage=StringToBitMap(draftItem.getIssue_image_bin());
		// captureimage=MLRoundedImageView.getCroppedBitmap(captureimage,100);
		// imIssueImage.setImageBitmap(captureimage);
		// singleactive.setText(draftItem.getProfileactivationno());
		return convertView;
	}

	public Bitmap StringToBitMap(String encodedString) {
		try {
			byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
			Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0,
					encodeByte.length);
			return bitmap;
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	public class ViewHolder {
		private TextView tvIssueTypeName, tvIssueDesc, tvIssueLat, tvIssueLon,
				tvIssueDate, tvIssueLang;
		private ImageView imIssueImage;

		public ViewHolder(ImageView imIssueImage, TextView issueTypeName,
				TextView issueDesc, TextView issueLat, TextView issueLon,
				TextView issueDate, TextView issueLang) {
			this.imIssueImage = imIssueImage;
			this.tvIssueTypeName = issueTypeName;
			this.tvIssueDesc = issueDesc;
			this.tvIssueLat = issueLat;
			this.tvIssueLon = issueLon;
			this.tvIssueDate = issueDate;
			this.tvIssueLang = issueLang;

		}

		public ViewHolder(ImageView imIssueImage, TextView issueTypeName,
				TextView issueDesc, TextView issueDate) {
			this.imIssueImage = imIssueImage;
			this.tvIssueTypeName = issueTypeName;
			this.tvIssueDesc = issueDesc;
			this.tvIssueDate = issueDate;

		}

	}
}
