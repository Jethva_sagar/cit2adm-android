package com.ivisionr.cit2adm.interfaces;

import com.ivisionr.cit2adm.bin.Getissuebyid;

public interface Issueserchbyidinterface {
	public void onisueserchbyidstarted();
	public void onissueserchbyidcompleted(String issue);

}
