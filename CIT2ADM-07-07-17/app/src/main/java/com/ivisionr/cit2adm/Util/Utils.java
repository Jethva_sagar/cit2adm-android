package com.ivisionr.cit2adm.Util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

public class Utils {
	public static String convertStreamToString(InputStream inputStream) throws IOException {
		if (inputStream != null) {
			StringBuilder sb = new StringBuilder();
			String line;
			try {
				BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
				while ((line = reader.readLine()) != null) {
					sb.append(line).append("\n");
				}
			} finally {
				inputStream.close();
			}			
			return sb.toString();
		} else {
			return "";
		}
	}
	public static void CopyStream(InputStream is, OutputStream os)
	{
		final int buffer_size=1024;
		try
		{
			byte[] bytes=new byte[buffer_size];
			for(;;)
			{
				int count=is.read(bytes, 0, buffer_size);
				if(count==-1)
					break;
				os.write(bytes, 0, count);
			}
		}
		catch(Exception ex){}
	}
	/*Checking Connectivity Internet*/
	public static boolean checkConnectivity(Context context){
		boolean isConnected = false;
		try{
			ConnectivityManager connService = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo network = connService.getActiveNetworkInfo();    
			if(network != null) {
				if(network.isConnected()){
				isConnected = true;    
				}
			}else{
				isConnected = false;

			}   
		}catch (Exception e) {
			e.printStackTrace();

		} 
		return isConnected;
	}
	
	
	public static String convertDate(String date) {	
		
		date=date.substring(0, 10);	
		Format formatter;
		Date newdate = new Date();		
		formatter = new SimpleDateFormat("dd/mm/yyyy");
		date = formatter.format(newdate);
		return date;
		
		//return date;
		/*java.util.Date  ss1=new Date(date);
		SimpleDateFormat formatter5=new SimpleDateFormat("dd/mm/yyyy");
		String formats1 = formatter5.format(ss1);
		return formats1;*/
	}
	public static int getBitmapSamplingRate(int initialWidth,
			int initialHeight, int reqWidth, int reqHeight) {
		int inSampleSize = 1;

		if (initialHeight > reqHeight || initialWidth > reqWidth) {

			// Calculate ratios of height and width to requested height and
			// width
			int heightRatio = Math.round((float) initialHeight
					/ (float) reqHeight);
			int widthRatio = Math
					.round((float) initialWidth / (float) reqWidth);

			// Choose the smallest ratio as inSampleSize value, this will
			// guarantee
			// a final image with both dimensions larger than or equal to the
			// requested height and width.

			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
		}

		return inSampleSize;
	}
	
	
	public static String getDeviceName() {
	    String manufacturer = Build.MANUFACTURER;
	    String model = Build.MODEL;
	    if (model.startsWith(manufacturer)) {
	        return capitalize(model);
	    } else {
	        return capitalize(manufacturer) + " " + model;
	    }
	}


	private static String capitalize(String s) {
	    if (s == null || s.length() == 0) {
	        return "";
	    }
	    char first = s.charAt(0);
	    if (Character.isUpperCase(first)) {
	        return s;
	    } else {
	        return Character.toUpperCase(first) + s.substring(1);
	    }
	} 
	
		
	public static String getScreenResolution(Context context)
	{
	    WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
	    Display display = wm.getDefaultDisplay();
	    DisplayMetrics metrics = new DisplayMetrics();
	    display.getMetrics(metrics);
	    int width = metrics.widthPixels;
	    int height = metrics.heightPixels;

	    return width + "x" + height ;
	}
	
	public static boolean isJSONValid(String test) {
	    try {
	        new JSONObject(test);
	    } catch (JSONException ex) {
	        // edited, to include @Arthur's comment
	        // e.g. in case JSONArray is valid as well...
	        try {
	            new JSONArray(test);
	        } catch (JSONException ex1) {
	            return false;
	        }
	    }
	    return true;
	}
	
	
}
