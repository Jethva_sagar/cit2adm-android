package com.ivisionr.cit2adm;

import java.util.ArrayList;
import java.util.List;

import com.ivisionr.cit2adm.Util.SharePreferenceClass;
import com.ivisionr.cit2adm.Util.Utils;
import com.ivisionr.cit2adm.adapter.PlaceListAdapter;
import com.ivisionr.cit2adm.async.MyReportDetaisAsyn;
import com.ivisionr.cit2adm.bin.MyReportDeetails;
import com.ivisionr.cit2adm.interfaces.MyReportIsue;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MyReport extends Activity implements MyReportIsue {

	private String profileuserid = "";
	private SharePreferenceClass sharedPreference;
	private Activity activity;
	private ListView gridvieinmyReport;
	private List<MyReportDeetails> lstmyreportDetails;
	private MyReportDeetails myReportDetails;
	private String  TAG = "DEBUG";
	TextView txt;
	ListView lst;
	private ProgressDialog mProgressDialog;

	private String id = "";
	private String reportid;
	private Button backbutton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		activity = this;
		setContentView(R.layout.menu_myreport);
		backbutton = (Button) findViewById(R.id.myreportbackinmain);
		lstmyreportDetails = new ArrayList<MyReportDeetails>();
		sharedPreference = new SharePreferenceClass(this);
		myReportDetails = new MyReportDeetails();
		mProgressDialog = new ProgressDialog(this);
		mProgressDialog.setMessage(getResources().getText(R.string.loading));
		mProgressDialog.setCancelable(false);
		Bundle bundle = getIntent().getExtras();
		if (getIntent().hasExtra("id")) {
			id = bundle.getString("id");
		}
		gridvieinmyReport = (ListView) findViewById(R.id.gridview);

		profileuserid = sharedPreference.getUserid();
		Log.d("DEBUG", "My Report Profile Id: " + profileuserid);
		myReportDetails = new MyReportDeetails();

		if (id.length() > 0) {

			if (Utils.checkConnectivity(MyReport.this)) {
				MyReportDetaisAsyn loadPlace = new MyReportDetaisAsyn(
						MyReport.this);
				loadPlace.reportIssue = this;
				loadPlace.execute(id);
			} else {
				showNetworkDialog("internet");
			}

		} else {
			if (Utils.checkConnectivity(MyReport.this)) {

				MyReportDetaisAsyn loadPlace = new MyReportDetaisAsyn(
						MyReport.this);
				loadPlace.reportIssue = this;
				loadPlace.execute(profileuserid);
			} else {
				showNetworkDialog("internet");
			}
		}

		gridvieinmyReport.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				reportid = lstmyreportDetails.get(arg2).getId();
				// reportid=lstmyreportDetails.get().get
				Log.d("DEBUG", "reportid" + reportid);

				// Log.d("checklist", "checklist"
				// +lstmyreportDetails.get(arg2).getId());

				Intent intent = new Intent(MyReport.this,
						MyReportDetailsImage.class);
				intent.putExtra("id", reportid);
				startActivity(intent);
				finish();

			}
		});
		backbutton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				MyReport.this.finish();

			}
		});

	}

	@SuppressWarnings("deprecation")
	public void showNetworkDialog(final String message) {
		// final exit of application
		AlertDialog.Builder builder = new AlertDialog.Builder(MyReport.this);
		builder.setTitle(getResources().getString(R.string.app_name));
		if (message.equals("gps")) {
			builder.setMessage(getResources().getString(R.string.gps_message));
		} else if (message.equals("internet")) {
			builder.setMessage(getResources().getString(R.string.net_message));
		}
		AlertDialog alert = builder.create();
		alert.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				if (message.equals("gps")) {
					Intent i = new Intent(
							android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
					startActivityForResult(i, 1);
				} else if (message.equals("internet")) {
					Intent i = new Intent(
							android.provider.Settings.ACTION_WIRELESS_SETTINGS);
					startActivityForResult(i, 2);
					Intent i1 = new Intent(
							android.provider.Settings.ACTION_WIFI_SETTINGS);
					startActivityForResult(i1, 3);
				} else {
					// do nothing
				}
			}
		});
		// Showing Alert Message
		alert.show();
	}

	@Override
	public void onMyReportIssue(final List<MyReportDeetails> list) {
		lstmyreportDetails.addAll(list);
		bindlist(lstmyreportDetails);

	}

	private void bindlist(List<MyReportDeetails> lstmyreportDetails) {
		mProgressDialog.dismiss();
		PlaceListAdapter adapter = new PlaceListAdapter(this,
				lstmyreportDetails);
		gridvieinmyReport.setAdapter(adapter);

	}

	@Override
	public void onMYReportIssuenodata() {
		mProgressDialog.dismiss();
		Toast toast = Toast.makeText(getApplicationContext(),
				R.string.noreportissue, Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();

	}

	@Override
	public void onMyReportIssueStarted() {
		if (null != mProgressDialog)
			mProgressDialog.show();
	}

}
