package com.ivisionr.cit2adm.async;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ivisionr.cit2adm.Util.SharePreferenceClass;
import com.ivisionr.cit2adm.Util.Webservicelink;
import com.ivisionr.cit2adm.bin.MyReportDeetails;
import com.ivisionr.cit2adm.interfaces.MyReportIsue;





import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.util.Log;

public class MyReportDetaisAsyn extends AsyncTask<String, Void, String> {
	private Context _context;
	private Activity actity;
	String fuctionalityName;
	private MyReportDeetails myreportDetails;
	ProgressDialog pd;
	ServerCommunication serverCommunication;
	ServerResponceCheck serverResponceCheck;
	SharePreferenceClass sharePreferenceClass;
	private List<MyReportDeetails> lstmyreportDetails;
	private JSONArray jsonArray;
	private JSONObject placeObject;
	private  int jsonlength;
	public MyReportIsue reportIssue;


	public MyReportDetaisAsyn(Activity contextAct){

		this.actity = contextAct;
		this._context = contextAct;
		sharePreferenceClass = new SharePreferenceClass(_context);
		lstmyreportDetails=new ArrayList<MyReportDeetails>();

	}


	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		reportIssue.onMyReportIssueStarted();

	}


	@Override
	protected String doInBackground(String... params) {
		try {
			lstmyreportDetails=serch(params);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Log.d("lastchance2", "lastchance2" +lstmyreportDetails);
		return "";
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		Log.d("lastchance1", "lastchance1" +lstmyreportDetails);
		Log.d("arrsize", "arrsize" +lstmyreportDetails.size());
		if(lstmyreportDetails.size()>0){			
			reportIssue.onMyReportIssue(lstmyreportDetails);
		}else{
			reportIssue.onMYReportIssuenodata();
		}
	

	}

	private List<MyReportDeetails> serch(String[] params) throws JSONException{
		String useid = params[0];

		String page = "no";
		String url = Webservicelink.serverMenuReportURL;

		try
		{
			JSONObject registerJson = new JSONObject();
			registerJson.put("user_id", useid);	

			List<NameValuePair> namevaluepair = new ArrayList<NameValuePair>();

			namevaluepair.add(new BasicNameValuePair("data", registerJson
					.toString()));

			ResponseHandler<String> resHandler = new BasicResponseHandler();

			String paramString = URLEncodedUtils.format(namevaluepair, "utf-8");

			url += paramString;
			Log.d("checkurlm", "checkurlm" +url);
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpGet httpGet = new HttpGet(url);

			page = httpClient.execute(httpGet, resHandler);

			Log.d("checkmyreport", "checkmyreport" +page);

			Log.e("responsecountryIso", page);
			JSONObject jsobj = new JSONObject(page.toString());
			JSONObject jsonjson = jsobj;
			Log.d("kartickmishra", "kartickmishra" +jsonjson);
			try {
				jsonArray=jsonjson.getJSONArray("issues");
				jsonlength=jsonArray.length();			
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			for(int i=0;i<jsonlength;i++){
				myreportDetails=new MyReportDeetails();
				try {
					placeObject=(JSONObject) jsonArray.get(i);
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}


				try {

					Log.d("kl", "kl" +placeObject.getString("report_id"));
					myreportDetails.setReport_id(placeObject.getString("report_id"));
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try{
					myreportDetails.setCreated(placeObject.getString("created"));

				}catch(JSONException e){
					e.printStackTrace();

				}
				myreportDetails.setIssue_type_name(placeObject.getString("issue_type_name"));
				myreportDetails.setStatus(placeObject.getString("status"));
				myreportDetails.setDescription(placeObject.getString("description"));
				myreportDetails.setNum_comments(placeObject.getString("num_comments"));
				myreportDetails.setNum_ratings_plus(placeObject.getString("num_ratings_plus"));
				myreportDetails.setNum_ratings_minus(placeObject.getString("num_ratings_minus"));
				myreportDetails.setIcon(placeObject.getString("image_1"));
				myreportDetails.setId(placeObject.getString("id"));


				String lat = placeObject.getString("lat");
				String lan = placeObject.getString("lon");

				myreportDetails.setLat(lat);
				myreportDetails.setLan(lan);
				lstmyreportDetails.add(myreportDetails);
				Log.d("lastchance123", "lastchance123" +lstmyreportDetails);
				Log.d("sizeoflist", "sizeoflist" +lstmyreportDetails.size());
			}

		}



		catch (ClientProtocolException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e("response", e.getMessage());
		}
		catch (JSONException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lstmyreportDetails;

	}


}
