package com.ivisionr.cit2adm.async;

import com.ivisionr.cit2adm.Util.UploadUtil;
import com.ivisionr.cit2adm.interfaces.UploadImagetoServerInterface;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class UploadImagetoServer extends AsyncTask<String , Void, String> {
	
	private Context _context;
	private Activity actity;
	public UploadImagetoServerInterface uploadtoserver;
	UploadUtil up = new UploadUtil();
	
	public UploadImagetoServer(Activity activity){
		this.actity=activity;
		this._context=activity;
	}

	@Override
	protected String doInBackground(String... params) {
		uploadimage(params);
		return null;
	}

	private void uploadimage(String[] params) {
		
		int upStatus = up.uploadFile(params[0]);
		Log.d("checkstatus", "checkstatus" +upStatus);
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		uploadtoserver.onuploadCompleted();
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
	}

}
