package com.ivisionr.cit2adm.bin;

import java.io.Serializable;

public class Issueserchbyplacebin  {
	private String id="";
	private String reportid="";
	private String issuetypeid="";
	private String name="";
	private String lat="";
	private String lon="";
	private String status="";
	private String created="";
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getReportid() {
		return reportid;
	}
	public void setReportid(String reportid) {
		this.reportid = reportid;
	}
	public String getIssuetypeid() {
		return issuetypeid;
	}
	public void setIssuetypeid(String issuetypeid) {
		this.issuetypeid = issuetypeid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLat() {
		return lat;
	}
	public void setLat(String lat) {
		this.lat = lat;
	}
	public String getLon() {
		return lon;
	}
	public void setLon(String lon) {
		this.lon = lon;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}

}
