package com.ivisionr.cit2adm.interfaces;

import java.util.List;

import com.ivisionr.cit2adm.bin.Issueserchbyplacebin;


public interface Issueserchbyinterfaces {
	public void Issueserchbyplacenodata();	
	public void inissueserchbyplacestarted();

	public void onissuebyplace(
			List<Issueserchbyplacebin> list,String lat,String lon);
	
	

}
