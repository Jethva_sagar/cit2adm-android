package com.ivisionr.cit2adm;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ivisionr.cit2adm.Util.ConnectionDetector;
import com.ivisionr.cit2adm.async.UnivarsalAsynkTask;

public class LoginActivity extends ActivityExceptionDemo implements OnClickListener
{
	Button  
	loginActivity_back_btn;
	Button loginActivity_loginbtn,login_registered_btn;

	ConnectionDetector connectionDetector;
	Context context;
	EditText loginActivity_username_edittxt, loginActivity_password_edittxt,loginActivity_security_edittext;
	TextView tvForgotYouPassword;
	Activity activity;
	String android_id = "", os_version = "", api_level = "", release = "", device = "", model = "", product = "", brand = "", manufacturer = "";

	/**
	 * @author Hiren(hirenkapuria2011@gmail.com)
	 */
	

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		context = this;
		activity = this;		

		setContentView(R.layout.myloginscreen);
		loginActivity_username_edittxt = (EditText) findViewById(R.id.loginActivity_username_edittxt);
		loginActivity_password_edittxt = (EditText) findViewById(R.id.loginActivity_password_edittxt);
		loginActivity_security_edittext=(EditText) findViewById(R.id.loginActivity_security_edittxt);

		login_registered_btn = (Button) findViewById(R.id.login_registered_btn);
		loginActivity_loginbtn = (Button) findViewById(R.id.loginActivity_loginbtn);
		loginActivity_back_btn = (Button) findViewById(R.id.backinreporttopost);

		tvForgotYouPassword = (TextView) findViewById(R.id.loginActivity_forgetpass_txt);
		
		
		
		/*String _OSVERSION = System.getProperty("os.version");
		String _RELEASE = android.os.Build.VERSION.RELEASE;
		String _DEVICE = android.os.Build.DEVICE; 
		String _MODEL = android.os.Build.MODEL; 
		String _PRODUCT = android.os.Build.PRODUCT; 
		String _BRAND = android.os.Build.BRAND; 
		String _DISPLAY = android.os.Build.DISPLAY; 
		String _CPU_ABI = android.os.Build.CPU_ABI; 
		String _CPU_ABI2 = android.os.Build.CPU_ABI2; 
		String _UNKNOWN = android.os.Build.UNKNOWN; 
		String _HARDWARE = android.os.Build.HARDWARE;
		String _ID = android.os.Build.ID; 
		String _MANUFACTURER = android.os.Build.MANUFACTURER; 
		//String _SERIAL = android.os.Build.SERIAL; 
		String _USER = android.os.Build.USER; 
		String _HOST = android.os.Build.HOST;
				
		Log.d("DEBUG", "_OSVERSION: "+_OSVERSION);
		Log.d("DEBUG", "_RELEASE: "+_RELEASE);
		Log.d("DEBUG", "_DEVICE: "+_DEVICE);
		Log.d("DEBUG", "_MODEL: "+_MODEL);
		Log.d("DEBUG", "_PRODUCT: "+_PRODUCT);
		Log.d("DEBUG", "_BRAND: "+_BRAND);
		Log.d("DEBUG", "_DISPLAY: "+_DISPLAY);
		Log.d("DEBUG", "_CPU_ABI: "+_CPU_ABI);
		Log.d("DEBUG", "_CPU_ABI2: "+_CPU_ABI2);
		Log.d("DEBUG", "_UNKNOWN: "+_UNKNOWN);
		Log.d("DEBUG", "_HARDWARE: "+_HARDWARE);
		Log.d("DEBUG", "_ID: "+_ID);
		Log.d("DEBUG", "_MANUFACTURER: "+_MANUFACTURER);
		//Log.d("DEBUG", "_OSVERSION: "+_SERIAL);
		Log.d("DEBUG", "_USER: "+_USER);
		Log.d("DEBUG", "_HOST: "+_HOST);*/
		
		android_id = Secure.getString(getApplicationContext().getContentResolver(), Secure.ANDROID_ID);		
		os_version = System.getProperty("os.version");
		api_level = String.valueOf(android.os.Build.VERSION.SDK_INT);
		release = android.os.Build.VERSION.RELEASE;
		device = android.os.Build.DEVICE; 
		model = android.os.Build.MODEL; 
		product = android.os.Build.PRODUCT; 
		brand = android.os.Build.BRAND; 
		manufacturer = android.os.Build.MANUFACTURER; 

		tvForgotYouPassword.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{
				// TODO Auto-generated method stub
				Intent intent = new Intent(LoginActivity.this,
						ForgotMyPassActivity.class);
				startActivity(intent);
				finish();
			}
		});
		connectionDetector = new ConnectionDetector(this);
		if (!connectionDetector.isConnectingToInternet())
		{
			Toast.makeText(LoginActivity.this, R.string.connectionproblem,
					Toast.LENGTH_LONG).show();
			return;
		}
		login_registered_btn.setOnClickListener(this);
		loginActivity_loginbtn.setOnClickListener(this);
		loginActivity_back_btn.setOnClickListener(this);
	}

	

	@Override
	public void onClick(View v)
	{
		// TODO Auto-generated method stub
		switch (v.getId())
		{
		case R.id.login_registered_btn:

			Intent intentForTermsAndCondition = new Intent(LoginActivity.this,TermsandCondition.class);
			startActivity(intentForTermsAndCondition);
			finish();

			/*Intent intentForRegistered = new Intent(LoginActivity.this, RegisterActivity.class);
			startActivity(intentForRegistered);
			finish();*/

			break;
		case R.id.loginActivity_loginbtn:

			String username = loginActivity_username_edittxt.getText()
			.toString().trim();
			String password = loginActivity_password_edittxt.getText()
					.toString().trim();
			String sequirtycode=loginActivity_security_edittext.getText().toString();
			// os_version = "", release = "", device = "", model = "", product = "", brand = "", manufacturer = "";
			new UnivarsalAsynkTask(activity, "Login").execute(username, password, sequirtycode,
					android_id, os_version, api_level, release, device, model, product, brand, manufacturer);
			break;
		case R.id.backinreporttopost:

			finish();
			break;
		}
	}

	@Override
	public void onBackPressed()
	{
		// TODO Auto-generated method stub
		super.onBackPressed();

		finish();
	}

}
