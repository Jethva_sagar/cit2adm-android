package com.ivisionr.cit2adm;

import com.ivisionr.cit2adm.Util.ConnectionDetector;
import com.ivisionr.cit2adm.async.UnivarsalAsynkTask;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class RegisterActivity extends BaseActivity implements OnClickListener {

	EditText registration_fname_edt, registration_lname_edt,
			registration_user_edt, registration_password_edt,
			registration_confirmpass_edt, registration_email_edt,
			registration_city_edt, registration_zipe_edt, associationEt;

	Button registration_registerbtn, Registration_back_btn;
	Spinner registration_contry_spiner;
	ConnectionDetector connectionDetector;
	UnivarsalAsynkTask univarsalAsynkTask;
	Activity activity;
	Context context;
	String countryName = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.register_screen_2);
		connectionDetector = new ConnectionDetector(this);
		this.context = this;
		this.activity = this;
		registration_fname_edt = (EditText) findViewById(R.id.registration_fname_edt);
		registration_lname_edt = (EditText) findViewById(R.id.registration_lname_edt);
		registration_password_edt = (EditText) findViewById(R.id.registration_password_edt);
		registration_confirmpass_edt = (EditText) findViewById(R.id.registration_confirmpass_edt);
		registration_email_edt = (EditText) findViewById(R.id.registration_email_edt);
		registration_city_edt = (EditText) findViewById(R.id.registration_city_edt);
		registration_zipe_edt = (EditText) findViewById(R.id.registration_zipe_edt);
		registration_user_edt = (EditText) findViewById(R.id.registration_user_edt);

		registration_registerbtn = (Button) findViewById(R.id.registration_registerbtn);
		registration_registerbtn.setText("REGISTER");
		registration_registerbtn.setOnClickListener(this);
		Registration_back_btn = (Button) findViewById(R.id.Registration_back_btn);
		Registration_back_btn.setOnClickListener(this);
		registration_contry_spiner = (Spinner) findViewById(R.id.registration_contry_spiner);
		associationEt = (EditText) findViewById(R.id.associationdetils);
		// change by pinaki
		try {

			String[] mTempArray = getResources().getStringArray(
					R.array.countryname);
			int i = mTempArray.length;
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
					android.R.layout.simple_spinner_item, mTempArray);
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			registration_contry_spiner.setAdapter(adapter);
			registration_contry_spiner.setSelection(74);

		} catch (Resources.NotFoundException e) {
			return;

		}

		if (!connectionDetector.isConnectingToInternet()) {
			Toast.makeText(RegisterActivity.this, R.string.connectionproblem,
					Toast.LENGTH_LONG).show();
			return;
		}

		registration_contry_spiner
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
					public void onItemSelected(AdapterView<?> parent,
							View view, int pos, long id) {
						Object item = parent.getItemAtPosition(pos);
						countryName = String.valueOf(item);
					}

					public void onNothingSelected(AdapterView<?> parent) {
					}
				});
	}

	

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.registration_registerbtn:

			registration_confirmpass_edt.getText().toString();

			String first_name = registration_fname_edt.getText().toString();
			String last_name = registration_lname_edt.getText().toString();
			String username = registration_user_edt.getText().toString();
			String password = registration_password_edt.getText().toString();
			String conpassword = registration_confirmpass_edt.getText()
					.toString();
			String email = registration_email_edt.getText().toString();
			String city = registration_city_edt.getText().toString();
			String country = registration_contry_spiner.getSelectedItem()
					.toString();
			String zip = registration_zipe_edt.getText().toString();
			String association = associationEt.getText().toString();
			
			if (first_name.matches("")) {
				Toast toast = Toast.makeText(getApplicationContext(),
						R.string.firstnamenotfound, Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
			}
			
			else if (last_name.matches("")) {
				Toast toast = Toast.makeText(getApplicationContext(),
						R.string.lastnamenotfound, Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
			}
			

			else if (email.matches("")) {
				Toast toast = Toast.makeText(getApplicationContext(),
						R.string.emailnotfound, Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
			}
			
			else if (username.matches("")) {
				Toast toast = Toast.makeText(getApplicationContext(),
						R.string.usernamenotfound, Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
			}

			else if (password.matches("")) {
				Toast toast = Toast.makeText(getApplicationContext(),
						R.string.passwordnotfound, Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
			}
							

			else if (!password.equals(conpassword)) {
				Toast toast = Toast.makeText(getApplicationContext(),
						R.string.passwordnotmatched, Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
			}

			else if (zip.matches("")) {
				Toast toast = Toast.makeText(getApplicationContext(),
						R.string.zipnotfound, Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
			}

			else {

				new UnivarsalAsynkTask(activity, "Registration").execute(
						first_name, last_name, username, password, email, city,
						country, zip, association);
			}
			
			break;

		case R.id.Registration_back_btn:
			Intent intent = new Intent(RegisterActivity.this,
					LoginActivity.class);
			startActivity(intent);
			finish();
			break;
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
		startActivity(intent);
		finish();

	}

	@SuppressWarnings("deprecation")
	public void alertDialogShow(String Message) {
		final AlertDialog alertDialog = new AlertDialog.Builder(context)
				.create();

		// Setting Dialog Message
		alertDialog.setMessage(Message);

		// Setting OK Button
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				Log.v("OK", "OK");
				alertDialog.dismiss();
				Intent intent = new Intent(RegisterActivity.this,
						LoginActivity.class);
				startActivity(intent);
				finish();
			}
		});
		alertDialog.show();
	}
}
