package com.ivisionr.cit2adm.async;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.ivisionr.cit2adm.Util.Utils;
import com.ivisionr.cit2adm.Util.Webservicelink;
import com.ivisionr.cit2adm.bin.MyProfileData;
import com.ivisionr.cit2adm.interfaces.MyProfileInterface;

public class GetProfileAsync extends AsyncTask<String, Void, String> {
	private Context _context;
	private Activity actity;
	private MyProfileData getuserdetailsbin;

	public MyProfileInterface getuserdetailsinterfaces;

	public GetProfileAsync(Activity activity) {
		this.actity = activity;
		this._context = activity;

	}

	@Override
	protected String doInBackground(String... params) {
		getuserdetailsbin = serch(params);

		return null;
	}

	private MyProfileData serch(String[] params) {

		String useid = params[0];
		Log.d("user_id", useid);
		String result = "";
		String url = Webservicelink.profile;
		getuserdetailsbin = new MyProfileData();
		try {
			JSONObject registerJson = new JSONObject();
			registerJson.put("id", useid);
			List<NameValuePair> namevaluepair = new ArrayList<NameValuePair>();
			namevaluepair.add(new BasicNameValuePair("data", registerJson
					.toString()));
			String paramString = URLEncodedUtils.format(namevaluepair, "utf-8");
			url += paramString;

			Log.d("kartickurluserdetails", "kartickurluserdetails" + url);
			HttpClient httpClient = new DefaultHttpClient();
			HttpGet httpPost = new HttpGet(url);
			HttpResponse response = null;
			try {
				response = httpClient.execute(httpPost);
			} catch (ClientProtocolException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				HttpEntity entity = response.getEntity();
				InputStream instream = entity.getContent();
				result = Utils.convertStreamToString(instream);
				getuserdetailsbin = getDescription(result);
				Log.d("resultinreportall", "resultinreportall" + result);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();

		}

		return getuserdetailsbin;
	}

	private MyProfileData getDescription(String result)
			throws JSONException {
		getuserdetailsbin = new MyProfileData();
		JSONObject jsonObject = null;
		JSONObject obj = null;
		try {
			jsonObject = new JSONObject(result);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		// try {
		// obj = jsonObject.getJSONObject("user");
		//
		// } catch (JSONException e) {
		// e.printStackTrace();
		// }
		getuserdetailsbin.setUser_id(jsonObject.getString("user_id"));
		getuserdetailsbin.setUser_type_id(jsonObject.getString("user_type_id"));
		getuserdetailsbin.setFirst_name(jsonObject.getString("first_name"));
		getuserdetailsbin.setLast_name(jsonObject.getString("last_name"));
		getuserdetailsbin.setCity(jsonObject.getString("city"));
		getuserdetailsbin.setZip(jsonObject.getString("zip"));
		getuserdetailsbin.setCreated(jsonObject.getString("created"));
		getuserdetailsbin.setOrganization(jsonObject.getString("organization"));
		getuserdetailsbin.setCountry(jsonObject.getString("country"));
		getuserdetailsbin.setEmail(jsonObject.getString("email"));
		getuserdetailsbin.setUsername(jsonObject.getString("username"));
		getuserdetailsbin.setLat(jsonObject.getString("lat"));
		getuserdetailsbin.setLon(jsonObject.getString("lon"));
		getuserdetailsbin.setActive(jsonObject.getString("active"));
		if(jsonObject.has("association_name"))
			getuserdetailsbin.setAssociation_name(jsonObject.getString("association_name"));
		getuserdetailsbin.setSecurity_code(jsonObject.getString("security_code"));
		getuserdetailsbin.setMembership_expiry_date(jsonObject.getString("membership_expiry_date"));
		getuserdetailsbin.setModified(jsonObject.getString("modified"));
		getuserdetailsbin.setPaid(jsonObject.getString("paid"));

		// {"error_code":"S20400","note":""}

		return getuserdetailsbin;
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		getuserdetailsinterfaces.ongetmyprofilecompleted(getuserdetailsbin);
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		getuserdetailsinterfaces.ongetmyprofilestartrd();

	}

}
