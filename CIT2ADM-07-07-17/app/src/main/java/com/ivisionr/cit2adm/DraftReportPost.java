package com.ivisionr.cit2adm;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.provider.Settings.Secure;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ivisionr.cit2adm.Util.GPSTracker;
import com.ivisionr.cit2adm.Util.HttpUpload;
import com.ivisionr.cit2adm.Util.SharePreferenceClass;
import com.ivisionr.cit2adm.Util.Utils;
import com.ivisionr.cit2adm.async.GetCurrentaddressAsync;
import com.ivisionr.cit2adm.async.ReportAsync;
import com.ivisionr.cit2adm.bin.Draft;
import com.ivisionr.cit2adm.bin.MyDraftDetails;
import com.ivisionr.cit2adm.database.DatabaseHandler;
import com.ivisionr.cit2adm.interfaces.CurrentaddressInterface;
import com.ivisionr.cit2adm.interfaces.ReportInterface;
import com.ivisionr.cit2adm.interfaces.UploadImagetoServerInterface;

public class DraftReportPost extends Activity implements ReportInterface,
		UploadImagetoServerInterface, CurrentaddressInterface {

	private TextView maplatview, maplonview, issuetype;
	private RelativeLayout mapscreen;
	private ImageView mapimage;
	private ImageView cameraImage;
	private EditText description;
	private Bitmap captureImage = null;
	private GPSTracker gpsTracker;
	static double currlat, currentlong;
	private Button CameraButton;
	private static int REQUEST_IMAGE_CAPTURE = 1;
	private String descriptiontoupload = "";
	Activity activity;
	private String userid = "";
	private String issuetype_id = "";
	private String issuetypename = "";
	private Button sendReport;
	private String mImageName = "";
	private SharePreferenceClass sharedPreference;
	private ProgressDialog mUProgressDialog; // / mProgressDialog
	private ProgressDialog camprogressdialog;
	SimpleDateFormat dateFormat;
	private String issueimage = "";
	final static int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1;
	Uri imageUri = null;
	private Button backbutton;
	private Button deletebutton;
	private Button saveReport;
	Dialog dialog = null;
	Date pdate = null;
	String created = "";
	private Bitmap decodedByte = null;
	private String language = "";
	DatabaseHandler db;
	Bitmap myBitmap = null;
	// private LoginDatabaseAdapter login;

	private String decodebytestring = "";
	private String issue_id = "";
	private String draftlat = "", draftlon = "";
	private String status = "", issuetype_name = "";
	private String imgPath = "";
	private String address = "";
	final private int CAPTURE_IMAGE = 2;
	List<Draft> draftList;
	Draft draft = null;
	String lat = "", lon = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		sharedPreference = new SharePreferenceClass(this);
		// mProgressDialog=new ProgressDialog(this);
		camprogressdialog = new ProgressDialog(this);
		mUProgressDialog = new ProgressDialog(this);

		db = new DatabaseHandler(this);

		mUProgressDialog.setMessage(getResources().getText(R.string.uploading));

		Bundle b = getIntent().getExtras();

		if (getIntent().hasExtra("draft_issue_id")) {
			issue_id = b.getString("draft_issue_id");
			draft = db.getDraft(issue_id);
			issuetype_id = draft.getIssue_type_id();
			issuetype_name = draft.getIssue_type_name();
			descriptiontoupload = draft.getIssue_description();
			address = draft.getIssue_address();
			lat = draft.getIssue_lat();
			lon = draft.getIssue_lon();
			Log.d("DEBUG", "Draft Lat: " + lat);
			Log.d("DEBUG", "Draft Lon: " + lon);
		}

		userid = sharedPreference.getUserid();

		dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		pdate = new Date();
		created = dateFormat.format(pdate);

		activity = this;
		setContentView(R.layout.draftdetails);
		getDetails();

		issuetype.setText(issuetypename);

		status = new SharePreferenceClass(DraftReportPost.this).getstatus();
		status = "1";

		description.setText(descriptiontoupload);
		description.setSelection(description.getText().length());

		if (issueimage.length() > 0) {
			CameraButton.setEnabled(false);
		}
		gpsTracker = new GPSTracker(this);

		// Display image in place holder
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = 4;

		imgPath = draft.getIssue_image();
		Log.d("DEBUG", "Draft Image Path 1: " + imgPath);

		if (imgPath != "") {
			Bitmap myBitmap = BitmapFactory.decodeFile(imgPath);

			int height = myBitmap.getHeight(), width = myBitmap.getWidth();

			if (height > 1280 && width > 960) {

				Bitmap myThumbBitmap = BitmapFactory.decodeFile(imgPath,
						options);

				cameraImage.setImageBitmap(myThumbBitmap);

			} else {
				cameraImage.setImageBitmap(myBitmap);

			}

		}

		if (Utils.checkConnectivity(DraftReportPost.this)) {
			Log.d("DEBUG", "Inside current address - Lat : " + lat + " Lon: "
					+ lon);
			GetCurrentaddressAsync currentaddress = new GetCurrentaddressAsync(
					DraftReportPost.this);
			currentaddress.currentaddressInterface = this;
			currentaddress.execute(lat, lon);

		} else {
			showNetworkDialog("internet");
		}
		backbutton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				DraftReportPost.this.finish();
			}
		});
//		deletebutton.setOnClickListener(new View.OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				AlertDialog.Builder builder = new AlertDialog.Builder(
//						DraftReportPost.this);
//				builder.setTitle("CIT2ADM");
//				builder.setMessage("Are You sure want to Cancel Report");
//				builder.setPositiveButton("Yes",
//						new DialogInterface.OnClickListener() {
//
//							@Override
//							public void onClick(DialogInterface arg0, int arg1) {
//								if (Utils
//										.checkConnectivity(DraftReportPost.this)) {
//									startActivity(new Intent(
//											DraftReportPost.this,
//											GoomapActivity.class));
//								} else {
//									showNetworkDialog("internet");
//								}
//
//							}
//
//						});
//				builder.setNegativeButton("No",
//						new DialogInterface.OnClickListener() {
//
//							@Override
//							public void onClick(DialogInterface dialog,
//									int which) {
//								// TODO Auto-generated method stub
//
//							}
//						});
//				AlertDialog alert = builder.create();
//				alert.show();
//
//			}
//		});
		saveReport.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				// status = "1";

				if (status.equals("1")) {
					AlertDialog.Builder builder = new AlertDialog.Builder(
							DraftReportPost.this);
					builder.setTitle("CIT2ADM");
					builder.setMessage("Do you want to save the report now and send later on?");
					builder.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface arg0,
										int arg1) {
									descriptiontoupload = description.getText()
											.toString();

									if (descriptiontoupload.length() > 0) {
										Toast toast = Toast.makeText(
												DraftReportPost.this,
												R.string.savesuccessfull,
												Toast.LENGTH_SHORT);
										toast.setGravity(Gravity.CENTER, 0, 0);
										toast.show();

										Log.d("DEBUG",
												"Draft Report Description: "
														+ descriptiontoupload);

										db.updateDraftDescription(issue_id,
												descriptiontoupload);

									} else {
										Toast toast = Toast
												.makeText(
														DraftReportPost.this,
														R.string.writedescriptionandpick,
														Toast.LENGTH_SHORT);
										toast.setGravity(Gravity.CENTER, 0, 0);
										toast.show();
									}

								}
							});
					builder.setNegativeButton("No",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub

								}
							});
					AlertDialog alert = builder.create();
					alert.show();

				} else {
					Toast toast = Toast.makeText(getApplicationContext(),
							R.string.statuscheck, Toast.LENGTH_LONG);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}
			}
		});

		sendReport.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				// mUProgressDialog.show();
				// mImageName=System.currentTimeMillis()+".jpg";

				/*
				 * if(null !=decodedByte){ storeImage(decodedByte);
				 * 
				 * }
				 */

				Log.d("DEBUG", "Upload Image Path: " + imgPath);

				if (status.equals("1")) {

					// imgPath = getImagePath();
					Log.d("DEBUG", "Upload Image Path: " + imgPath);

					if (!imgPath.equalsIgnoreCase("")) {
						new HttpUpload(getApplicationContext(), imgPath)
								.execute();

						String[] split = imgPath.split("/");
						StringBuilder sb = new StringBuilder();

						mImageName = split[split.length - 1];

						Log.d("DEBUG", "Image Name: " + mImageName);

					}

					descriptiontoupload = description.getText().toString();

					if (descriptiontoupload.length() > 0
							&& mImageName.length() > 0) {

						if (Utils.checkConnectivity(activity)) {
							mUProgressDialog.show();
							/**
							 * getting app version name and app version code
							 */
							PackageManager manager = DraftReportPost.this
									.getPackageManager();
							PackageInfo info = null;
							try {
								info = manager.getPackageInfo(
										DraftReportPost.this.getPackageName(),
										0);
								Log.d("DEBUG", info.versionName
										+ "+++++++++++++++" + info.versionName);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

							/***
							 * getting device unique id
							 */
							String device_id = Secure.getString(
									DraftReportPost.this.getContentResolver(),
									Secure.ANDROID_ID);
							ReportAsync reportasync = new ReportAsync(activity);
							reportasync.reportinterface = DraftReportPost.this;
							reportasync.execute(userid, issuetype_id,
									mImageName, descriptiontoupload, lat, lon,
									language, imgPath, device_id, ""
											+ info.versionCode,
									info.versionName);

						} else {

							db.updateDraftDescription(issue_id,
									descriptiontoupload);

							Toast toast = Toast.makeText(
									getApplicationContext(),
									R.string.poornetworkreportnotsent,
									Toast.LENGTH_LONG);
							toast.setGravity(Gravity.CENTER, 0, 0);
							toast.show();

							Intent intent = new Intent(getApplicationContext(),
									MyDrafts.class);
							startActivity(intent);
							finish();

						}
					} else {
						Toast toast = Toast.makeText(getApplicationContext(),
								R.string.writedescriptionandpick,
								Toast.LENGTH_LONG);
						toast.setGravity(Gravity.CENTER, 0, 0);
						toast.show();
					}

				} else {
					Toast toast = Toast.makeText(getApplicationContext(),
							R.string.statuscheck, Toast.LENGTH_LONG);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}
			}

		});

		deletebutton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// db.deleteDraft(issue_id);

				AlertDialog.Builder builder = new AlertDialog.Builder(
						DraftReportPost.this);
				builder.setTitle("CIT2ADM");
				builder.setMessage("Do you want to delete the report?");
				builder.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface arg0, int arg1) {
								db.deleteDraft(issue_id);

								Intent intent = new Intent(
										getApplicationContext(),
										MyDrafts.class);
								startActivity(intent);
								finish();
							}
						});
				builder.setNegativeButton("No",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub

							}
						});
				AlertDialog alert = builder.create();
				alert.show();

			}
		});

		mapscreen.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent intent = new Intent(DraftReportPost.this,
						MyReportCurrentLocation.class);
				startActivity(intent);

			}
		});
	}

	private Bitmap converStringtobitmap(String encodedString) {
		byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
		Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0,
				encodeByte.length);
		return bitmap;
	}

	private String BitMapToString(Bitmap image) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		if (image != null) {
			image.compress(Bitmap.CompressFormat.PNG, 100, baos);
		}
		byte[] b = baos.toByteArray();
		String temp = Base64.encodeToString(b, Base64.DEFAULT);

		return temp;
	}

	@SuppressWarnings("deprecation")
	public void showNetworkDialog(final String message) {
		// final exit of application
		AlertDialog.Builder builder = new AlertDialog.Builder(
				DraftReportPost.this);
		builder.setTitle(getResources().getString(R.string.app_name));
		if (message.equals("gps")) {
			builder.setMessage(getResources().getString(R.string.gps_message));
		} else if (message.equals("internet")) {
			builder.setMessage(getResources().getString(R.string.net_message));
		}
		AlertDialog alert = builder.create();
		alert.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				if (message.equals("gps")) {
					Intent i = new Intent(
							android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
					startActivityForResult(i, 1);
				} else if (message.equals("internet")) {
					Intent i = new Intent(
							android.provider.Settings.ACTION_WIRELESS_SETTINGS);
					startActivityForResult(i, 2);
					Intent i1 = new Intent(
							android.provider.Settings.ACTION_WIFI_SETTINGS);
					startActivityForResult(i1, 3);
				} else {
					// do nothing
				}
			}
		});
		// Showing Alert Message
		alert.show();
	}

	protected void startDialog() {
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
			startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
		}

	}

	private void getDetails() {
		issuetype = (TextView) findViewById(R.id.issuetype);
		maplatview = (TextView) findViewById(R.id.locationlat);
		maplatview.setText(address);
		maplonview = (TextView) findViewById(R.id.locationlon);
		// mapImage=(TextView)findViewById(R.id.mapinreport);
		cameraImage = (ImageView) findViewById(R.id.cameraview);
		description = (EditText) findViewById(R.id.reportdescription);
		sendReport = (Button) findViewById(R.id.sendReport);
		mapscreen = (RelativeLayout) findViewById(R.id.mapintxtreport);
		backbutton = (Button) findViewById(R.id.backinreporttopost);
		deletebutton = (Button) findViewById(R.id.deletebutton);
		saveReport = (Button) findViewById(R.id.savereport);

	}

	@Override
	public void onCurrentAddress(String address) {
		// TODO Auto-generated method stub
		// maplatview.setText(address);

	}

	@Override
	public void onuploadCompleted() {
		// TODO Auto-generated method stub
		Toast toast = Toast.makeText(getApplicationContext(),
				R.string.successfully_posted_post, Toast.LENGTH_LONG);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();

	}

	@Override
	public void onReportpostastarted() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onReportpostcompleted() {
		// TODO Auto-generated method stub

		// mProgressDialog.dismiss();
		// new ImageGalleryTask().execute();

		// UploadImagetoServer uploadimagetoServer=new
		// UploadImagetoServer(ReportPostToServer.this);
		// uploadimagetoServer.uploadtoserver=ReportPostToServer.this;
		// uploadimagetoServer.execute(image_path);
		db.deleteDraft(issue_id);
		Toast toast = Toast.makeText(getApplicationContext(),
				R.string.successfully_posted_post, Toast.LENGTH_LONG);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();
		finish();

	}

	@Override
	public void onReportnetworkissue() {
		// TODO Auto-generated method stub

	}

	// Get the path from Uri
	public String getPath(Uri uri) {
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = managedQuery(uri, projection, null, null, null);
		startManagingCursor(cursor);
		int column_index = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}

	// Get the path from Uri
	public String getName(Uri uri) {

		return uri.getLastPathSegment();
	}

	public Uri getImageUri(Context inContext, Bitmap inImage) {
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
		String path = Images.Media.insertImage(inContext.getContentResolver(),
				inImage, "Title", null);
		return Uri.parse(path);
	}

	public String getRealPathFromURI(Uri uri) {
		Cursor cursor = getContentResolver().query(uri, null, null, null, null);
		cursor.moveToFirst();
		int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
		return cursor.getString(idx);
	}

	public Uri setImageUri() {
		// Store image in dcim
		File file = new File(Environment.getExternalStorageDirectory()
				+ "/DCIM/", "image" + new Date().getTime() + ".png");
		Uri imgUri = Uri.fromFile(file);
		this.imgPath = file.getAbsolutePath();
		return imgUri;
	}

	public String getImagePath() {
		return imgPath;
	}

	@Override
	public void onBackPressed() {
		Intent intent = new Intent(this, MyDrafts.class);
		startActivity(intent);
		finish();
	}

}
