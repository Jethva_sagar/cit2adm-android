package com.ivisionr.cit2adm.interfaces;

import com.ivisionr.cit2adm.bin.MyProfileData;


public interface MyProfileInterface {
	public void ongetmyprofilestartrd();
	public void ongetmyprofilecompleted(MyProfileData list);
}
