/*******************************************************************************
 * Copyright 2013 Gabriele Mariotti
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.ivisionr.cit2adm.bin;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Draft implements Comparable<Draft> {

	/*
	 * 
	 * public static final String KEY_ISSUE_ID = "id"; public static final
	 * String KEY_USER_ID = "user_id"; public static final String
	 * KEY_ISSUE_TYPE_ID = "issue_type_id"; public static final String
	 * KEY_ISSUE_TYPE_NAME = "issue_type_name"; public static final String
	 * KEY_ISSUE_IMAGE = "issue_image"; public static final String
	 * KEY_ISSUE_IMAGE_BINARY = "issue_image_bin"; public static final String
	 * KEY_ISSUE_DESC = "issue_description"; public static final String
	 * KEY_ISSUE_LAT = "issue_lat"; public static final String KEY_ISSUE_LON =
	 * "issue_lon"; public static final String KEY_ISSUE_DATE = "issue_date";
	 * public static final String KEY_ISSUE_LANG = "issue_lang";
	 */

	public String id;
	public String user_id;
	public String issue_type_id;
	public String issue_type_name;
	public String issue_image;
	public String issue_image_bin;
	public String issue_description;
	public String issue_lat;
	public String issue_lon;
	public String issue_date;
	public String issue_lang;
	public String issue_Device_id;
	public String issue_app_version_code;
	public String issue_app_version_name;
	public String issue_address;

	public String getIssue_Device_id() {
		return issue_Device_id;
	}

	public void setIssue_Device_id(String issue_Device_id) {
		this.issue_Device_id = issue_Device_id;
	}

	public String getIssue_app_version_code() {
		return issue_app_version_code;
	}

	public void setIssue_app_version_code(String issue_app_version_code) {
		this.issue_app_version_code = issue_app_version_code;
	}

	public String getIssue_app_version_name() {
		return issue_app_version_name;
	}

	public void setIssue_app_version_name(String issue_app_version_name) {
		this.issue_app_version_name = issue_app_version_name;
	}

	public String getIssue_address() {
		return issue_address;
	}

	public void setIssue_address(String issue_address) {
		this.issue_address = issue_address;
	}

	public Draft() {

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getIssue_type_id() {
		return issue_type_id;
	}

	public void setIssue_type_id(String issue_type_id) {
		this.issue_type_id = issue_type_id;
	}

	public String getIssue_type_name() {
		return issue_type_name;
	}

	public void setIssue_type_name(String issue_type_name) {
		this.issue_type_name = issue_type_name;
	}

	public String getIssue_image() {
		return issue_image;
	}

	public void setIssue_image(String issue_image) {
		this.issue_image = issue_image;
	}

	public String getIssue_image_bin() {
		return issue_image_bin;
	}

	public void setIssue_image_bin(String issue_image_bin) {
		this.issue_image_bin = issue_image_bin;
	}

	public String getIssue_description() {
		return issue_description;
	}

	public void setIssue_description(String issue_description) {
		this.issue_description = issue_description;
	}

	public String getIssue_lat() {
		return issue_lat;
	}

	public void setIssue_lat(String issue_lat) {
		this.issue_lat = issue_lat;
	}

	public String getIssue_lon() {
		return issue_lon;
	}

	public void setIssue_lon(String issue_lon) {
		this.issue_lon = issue_lon;
	}

	public String getIssue_date() {
		return issue_date;
	}

	public void setIssue_date(String issue_date) {
		this.issue_date = issue_date;
	}

	public String getIssue_lang() {
		return issue_lang;
	}

	public void setIssue_lang(String issue_lang) {
		this.issue_lang = issue_lang;
	}

	@Override
	public int compareTo(Draft another) {
		// TODO Auto-generated method stub

		
		SimpleDateFormat	dateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	//	Date pdate = dateFormat1.parse(draftItem.getIssue_date());
		Date beginupd;
		long milli1=0;
		long milli2=0 ;
		try {
			beginupd = dateFormat1.parse(another.getIssue_date());
			milli1	 = beginupd.getTime();
			Date beginupd1 =dateFormat1.parse(this.issue_date);
			 milli2 = beginupd1.getTime();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return (int)(milli1-milli2);
	}

}
