package com.ivisionr.cit2adm;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ivisionr.cit2adm.async.UnivarsalAsynkTask;

public class ForgotMyPassActivity extends ActivityExceptionDemo {

	Button btnForgotPasswordBack, btnSubmit;
	TextView tvEmail;
	EditText etEmail;
	Activity activity;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.forgotmypassword);
		btnForgotPasswordBack = (Button) findViewById(R.id.backinreporttopostdr);
		btnSubmit = (Button) findViewById(R.id.btnSubmit);
		tvEmail = (TextView) findViewById(R.id.tvEmail);
		etEmail = (EditText) findViewById(R.id.etEmail);
		
		this.activity = this;
		
		btnForgotPasswordBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ForgotMyPassActivity.this, LoginActivity.class);
				startActivity(intent);
				finish();
			}
		});
		
		
		btnSubmit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				String email = etEmail.getText().toString();
				Log.d("DEBUG", "ForgotMyPass Email: "+email);
				
				if (email.matches("")) {
					Toast toast = Toast.makeText(getApplicationContext(),
							R.string.emailnotfound, Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}
				else {
					new UnivarsalAsynkTask(activity, "ForgotPass").execute(email);
				}
				
			}
		});
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		Intent intent = new Intent(ForgotMyPassActivity.this,
				LoginActivity.class);
		startActivity(intent);
		finish();
	}
}
