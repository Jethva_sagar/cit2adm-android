package com.ivisionr.cit2adm.async;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import com.ivisionr.cit2adm.Util.Webservicelink;
import com.ivisionr.cit2adm.interfaces.SendFeedbackInterface;

public class SendFeedbackAsync extends AsyncTask<String, Void, String> {

	public SendFeedbackInterface sendfeedbackinterface;
	private Activity activity;

	String error_code = "";
	private String page = "no";

	public SendFeedbackAsync(Activity activity) {
		this.activity = activity;
		
		
		
		
		
	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub

		String first_name = params[0];
		Log.d("DEBUG", "Feedback first name" + first_name);

		String last_name = params[1];
		Log.d("DEBUG", "Feedback last name" + last_name);

		String email_id = params[2];
		Log.d("DEBUG", "Feedback email id" + email_id);

		String feedback_msg = params[3];
		Log.d("DEBUG", "Feedback Message" + feedback_msg);
		
		String lang = params[4];
		Log.d("DEBUG", "Feedback Language" + lang);
		
		

		String url = Webservicelink.feedbackurl;
		JSONObject jsonObject = new JSONObject();

		try {
			jsonObject.put("first_name", first_name);
			jsonObject.put("last_name", last_name);
			jsonObject.put("email", email_id);
			jsonObject.put("comment", feedback_msg);
			jsonObject.put("lang", lang);

			List<NameValuePair> namevaluepair = new ArrayList<NameValuePair>();
			namevaluepair.add(new BasicNameValuePair("data", jsonObject
					.toString()));
			ResponseHandler<String> resHandler = new BasicResponseHandler();
			String paramString = URLEncodedUtils.format(namevaluepair, "utf-8");
			url += paramString;
			Log.d("DEBUG", "Send Feedback URL: " + url);
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpGet httpGet = new HttpGet(url);
			page = httpClient.execute(httpGet, resHandler);
			JSONObject jsobj = new JSONObject(page.toString());
			Log.d("DEBUG", "Send Feedback JSON" + jsobj);

			error_code = jsobj.getString("error_code");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return error_code;
	}

	@Override
	protected void onPostExecute(String success) {
		// TODO Auto-generated method stub
		super.onPostExecute(success);

		sendfeedbackinterface.onCompleted(error_code);

	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		sendfeedbackinterface.onStarted();
	}

}
