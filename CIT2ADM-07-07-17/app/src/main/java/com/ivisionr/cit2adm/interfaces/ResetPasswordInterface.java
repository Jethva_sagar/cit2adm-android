package com.ivisionr.cit2adm.interfaces;

public interface ResetPasswordInterface {
	
	public void onStarted();
	public void onCompleted(String errorcode);

}
