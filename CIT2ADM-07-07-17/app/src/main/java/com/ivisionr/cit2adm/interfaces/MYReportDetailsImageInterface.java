package com.ivisionr.cit2adm.interfaces;

import java.util.List;

import com.ivisionr.cit2adm.bin.MyReportDetailsDescriptionlist;
import com.ivisionr.cit2adm.bin.MyReportDetailscomments;


public interface MYReportDetailsImageInterface {
	
	public void onReportDetailsImagenoData();
	public void onReportDetailsImageStarted();
	public void onReportDetailsImagecompleted(MyReportDetailsDescriptionlist list);
	public void onReportIssueWithCommentsnoData();
	public void onReportIssueStarted();
	public void onReportIssueWithCommentsCompleted(List<MyReportDetailscomments> list);

}
