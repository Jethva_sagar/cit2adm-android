package com.ivisionr.cit2adm;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ivisionr.cit2adm.Util.ConnectionDetector;
import com.ivisionr.cit2adm.Util.GPSTracker;
import com.ivisionr.cit2adm.Util.LanguesCheckClass;
import com.ivisionr.cit2adm.Util.MobileUtils;
import com.ivisionr.cit2adm.Util.SharePreferenceClass;
import com.ivisionr.cit2adm.Util.SystemConstants;
import com.ivisionr.cit2adm.Util.Utils;
import com.ivisionr.cit2adm.Util.otherStaticValue;
import com.ivisionr.cit2adm.adapter.Votesadppter;
import com.ivisionr.cit2adm.async.GetAllIssueAsync;
import com.ivisionr.cit2adm.async.MyVotesAsync;
import com.ivisionr.cit2adm.async.ReportStatesAsync;
import com.ivisionr.cit2adm.async.ServerResponceCheck;
import com.ivisionr.cit2adm.async.UpdateGpsAsync;
import com.ivisionr.cit2adm.bin.GetAllIssueDetails;
import com.ivisionr.cit2adm.bin.MyReportDeetails;
import com.ivisionr.cit2adm.bin.Myvotes;
import com.ivisionr.cit2adm.bin.Reportstates;
import com.ivisionr.cit2adm.bin.User;
import com.ivisionr.cit2adm.custom.MenuDialogView;
import com.ivisionr.cit2adm.custom.MenuDialogView.MyDialogFragmentListener;
import com.ivisionr.cit2adm.custom.ReportDialogView;
import com.ivisionr.cit2adm.database.DatabaseHandler;
import com.ivisionr.cit2adm.interfaces.GetallIssueInterface;
import com.ivisionr.cit2adm.interfaces.Myvotesinterfaces;
import com.ivisionr.cit2adm.interfaces.ReportstatesInterfaces;
import com.ivisionr.cit2adm.interfaces.UpdateGpsInterface;
import com.ivisionr.cit2adm.marshmallow.MarshMallowPermission;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class GoomapActivity extends FragmentActivity implements
		OnClickListener, MyDialogFragmentListener, GetallIssueInterface,
		ReportstatesInterfaces, UpdateGpsInterface, Myvotesinterfaces {

	RelativeLayout GoomapActivity_menubtn, GoomapActivity_norepotbtn,
			GoomapActivity_repotbtn;
	private Context context;

	ConnectionDetector connectionDetector;
	private GoogleMap googleMap;
	GPSTracker gpsTracker;
	private String currentLat = "", currentLang = "";
	MarkerOptions markercurrent, markerneighbour;
	DatabaseHandler db;
	static double currlat, currentlong;
	static String country;
	static Boolean neighbourBtnChacker = false;
	MenuDialogView menuCustomDialogView;
	ReportDialogView reportDialogView;
	SafeLifeApplication application;
	ServerResponceCheck serverResponceCheck;
	private ProgressDialog mProgressDialog;
	private SupportMapFragment supportMapFragment;
	private GoogleMap mGoogleMap;
	private LatLng currentPosition;
	private MarkerOptions markerOptions;
	private GetAllIssueDetails getallissue;
	private Marker marker = null;
	private String reportid = "";
	private List<MyReportDeetails> list;
	private MyReportDeetails myreportdetauils;
	private LatLng latLng;
	private int i = 0;
	private List<GetAllIssueDetails> allissuedetails;
	private String lat, lon;
	private String latitudecurrent = "", longitudecurrent = "";
	private Dialog dialog;
	private Reportstates binreportsttes;
	private TextView openglobal, open30days, open180days, assignedglobal,
			assigned30days, assigned180days, inprogressglobal,
			inprogress30days, inprogress180days, completedlobal,
			completed30days, completed180days, closedlobal, closed30days,
			closed180days;
	private Dialog paramdialog;
	TextView wifi_status, gps_status, network_status;

	Button btn_submit;
	// Spinner spin_language, spin_radius;
	private String radiusforselection = "";

	String lLanguage = "";

	// final String[] language = { "english", "fran�ais", "espa�ol",
	// "Deutsch"
	// };
	// final String[] radius = { "5km", "10km", "25km", "50km", "100km",
	// "250km","500km" };
	ArrayAdapter<String> arrayAdapterforradius;
	ArrayAdapter<String> arrayAdaptersping;
	private Dialog voteDialog;
	private ListView myvoteslist;

	private List<Myvotes> myvoteslistview;
	private TextView menytxt, reportstatestxt, reporttxt;

	LanguesCheckClass languesCheckClass;
	String langues = "en";
	GoomapActivity goomapActivity;
	SharePreferenceClass sharePreferenceClass;
	ArrayList<HashMap<String, String>> userRecord, userNeighBoreData;
	SharedPreferences sharedPreferences;
	private String languagesend = "";
	private String TAG = "DEBUG";
	private String userId = "";
	private int lpos = 0;
	Dialog alert_dialog;
	private String language = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.newmap);

		languesCheckClass = new LanguesCheckClass(this);
		langues = languesCheckClass.GetCurrentLangues();
		Log.d("DEBUG", "GoomapActivity - Language: " + langues);
		db = new DatabaseHandler(this);
		language = MobileUtils.getSavedPref(this,SystemConstants.TXT_PERSIST_LANGUAGE);
		Log.d("DEBUG", "Inside Language Saved: " + language);
		/***
		 * set preference blank
		 */
		SharePreferenceClass.setPreferences(GoomapActivity.this, "description", "");
		SharePreferenceClass.setPreferences(GoomapActivity.this, "mImageName", "");

		/**
		 * Again language set for app language reset
		 */
		//language=langues;
		String lang = "en_us";
		String loca = "en";
		if (language.equals("Franais")) {
			language = "fra";
			lang = "fr_fr";
			loca = "fr";
		} else if (language.equals("Espaol")) {
			language = "esp";
			lang = "es_es";
			loca = "es";
		} else if (language.equals("Deutsch")) {
			language = "deu";
			lang = "de_de";
			loca = "de";
		} else
			language = "eng";

		/**
		 * Again language set for app language reset
		 */
		Log.d("DEBUG", "loca " + langues);
		Locale locale = new Locale(langues);
		Locale.setDefault(locale);
		Configuration config = new Configuration();
		config.locale = locale;
		getBaseContext().getResources().updateConfiguration(config,
				getBaseContext().getResources().getDisplayMetrics());
		getIntent().putExtra("language", lang);

		// String savedLang = getIntent().getStringExtra("language");

		// Log.d("DEBUG", "Saved Lang: "+savedLang);

		/*
		 * Locale current = getResources().getConfiguration().locale;
		 * 
		 * Log.d("DEBUG", "GoomapActivity Default Locale String - "+current);
		 * 
		 * Log.d("DEBUG",
		 * "GoomapActivity Default Country String - "+current.getDisplayCountry
		 * ()); Log.d("DEBUG",
		 * "GoomapActivity Default Display String - "+current.getDisplayName());
		 * Log.d("DEBUG",
		 * "GoomapActivity Default Language String - "+current.getDisplayLanguage
		 * ());
		 * 
		 * 
		 * Locale locale = new Locale("hi"); Locale.setDefault(locale);
		 * Configuration config = new Configuration(); config.locale = locale;
		 * getBaseContext().getResources().updateConfiguration(config,
		 * getBaseContext().getResources().getDisplayMetrics());
		 */

		application = (SafeLifeApplication) getApplication();
		mProgressDialog = new ProgressDialog(GoomapActivity.this);
		mProgressDialog.setMessage(getResources().getText(R.string.loading));
		mProgressDialog.setCancelable(false);
		GoomapActivity_menubtn = (RelativeLayout) findViewById(R.id.GoomapActivity_menubtn);
		GoomapActivity_norepotbtn = (RelativeLayout) findViewById(R.id.GoomapActivity_norepotbtn);
		GoomapActivity_repotbtn = (RelativeLayout) findViewById(R.id.GoomapActivity_repotbtn);

		menytxt = (TextView) findViewById(R.id.menutxt);
		reportstatestxt = (TextView) findViewById(R.id.reportstatestxt);
		reporttxt = (TextView) findViewById(R.id.reporttxt);

		GoomapActivity_menubtn.setOnClickListener(this);
		GoomapActivity_norepotbtn.setOnClickListener(this);
		GoomapActivity_repotbtn.setOnClickListener(this);
		connectionDetector = new ConnectionDetector(this);
		sharePreferenceClass = new SharePreferenceClass(this);
		serverResponceCheck = new ServerResponceCheck(this);
		sharedPreferences = this.getSharedPreferences("NeighBourData", 0);
		getallissue = new GetAllIssueDetails();
		list = new ArrayList<MyReportDeetails>();
		myreportdetauils = new MyReportDeetails();
		allissuedetails = new ArrayList<GetAllIssueDetails>();
		myvoteslistview = new ArrayList<Myvotes>();

		gpsTracker = new GPSTracker(this);
		currlat = gpsTracker.getLatitude();
		currentlong = gpsTracker.getLongitude();
		this.context = GoomapActivity.this;

		// Log.d("DEBUG", "GMap: " + currlat);
		new SharePreferenceClass(GoomapActivity.this).setLat(String
				.valueOf(currlat));
		new SharePreferenceClass(GoomapActivity.this).setLon(String
				.valueOf(currentlong));
		latitudecurrent = new SharePreferenceClass(GoomapActivity.this)
				.getLat();
		longitudecurrent = new SharePreferenceClass(GoomapActivity.this)
				.getLon();
		// Log.d("DEBUG", "Long: " + longitudecurrent);
		// Log.d("DEBUG", "GMPL: " + latitudecurrent.length());

		String mem_expiry_date = new SharePreferenceClass(GoomapActivity.this)
				.getPromoExpiryDate();


		/*LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE );
		boolean statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		if(!statusOfGPS){

			AlertDialog.Builder builder = new AlertDialog.Builder(GoomapActivity.this);
			builder.setTitle(getResources().getString(R.string.gps_message));
			AlertDialog alert = builder.create();
			alert.setButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
				}
			});
			// Showing Alert Message
			alert.show();

		}*/


		Log.d("DEBUG", "Date Exp.: " + mem_expiry_date);

		try {

			// String someDate = mem_expiry_date;
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			if(mem_expiry_date != null && mem_expiry_date.length() > 0) {
				Date date = sdf.parse(mem_expiry_date);
				long milli = date.getTime();
			}

		} catch (Exception ex) {
            ex.printStackTrace();
		}

		if (Utils.checkConnectivity(GoomapActivity.this)) {

            if(MarshMallowPermission.checkPermissionForAccessCrossLocation(GoomapActivity.this)) {

                initilizeMap();
                /*
                 * if(latitudecurrent.equals(0.0) && longitudecurrent.equals(0.0)){
                 * showNetworkDialog("gps");
                 *
                 * }else{ initilizeMap(); }
                 */

            } else {

                MarshMallowPermission.requestPermissionForAccessCrossLocation(GoomapActivity.this);
            }
		} else {

			showNetworkDialog("internet");
		}

		/* Checking Connectivity Internet */
		if (Utils.checkConnectivity(GoomapActivity.this)) {

			if (latitudecurrent.length() > 0 && longitudecurrent.length() > 0) {
				GetAllIssueAsync getallissue = new GetAllIssueAsync(GoomapActivity.this);
				getallissue.getallisuueinterface = this;
				getallissue.execute(latitudecurrent, longitudecurrent);
			} else {
				Toast toast = Toast.makeText(GoomapActivity.this,R.string.locationproblem, Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
			}
		} else {
			showNetworkDialog("internet");
		}

		try {
			PackageInfo info = getPackageManager().getPackageInfo(
					"com.ivisionr.cit2adm"/*"safelifeapps.safelifeapps.safelifeapps"*/,
					PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				// Log.d("DEBUG", "Check Hash: "+
				// Base64.encodeToString(md.digest(), Base64.DEFAULT));

			}
		} catch (NameNotFoundException e) {
            e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {

		}
		if (mGoogleMap != null) {
			mGoogleMap.setOnMarkerClickListener(new OnMarkerClickListener() {

				@Override
				public boolean onMarkerClick(Marker marker) {
					GoomapActivity.this.marker = marker;

					if (null != marker)
						if (!marker.getTitle().equals(null))
							if (marker.getTitle().length() > 0) {

								Intent intent = new Intent(GoomapActivity.this, Issudetails.class);
								intent.putExtra("id", marker.getTitle());
								startActivity(intent);
							}

					return false;
				}
			});
		}
	}

	@SuppressWarnings("deprecation")
	public void showNetworkDialog(final String message) {
		// final exit of application
		AlertDialog.Builder builder = new AlertDialog.Builder(GoomapActivity.this);
		builder.setTitle(getResources().getString(R.string.app_name));
		if (message.equals("gps")) {
			builder.setMessage(getResources().getString(R.string.gps_message));
		} else if (message.equals("internet")) {
			builder.setMessage(getResources().getString(R.string.net_message));
		}
		AlertDialog alert = builder.create();
		alert.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				if (message.equals("gps")) {
					Intent i = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
					startActivityForResult(i, 1);
				} else if (message.equals("internet")) {
					Intent i = new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS);
					startActivityForResult(i, 2);
					Intent i1 = new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS);
					startActivityForResult(i1, 3);
				} else {
					// do nothing
				}
			}
		});
		// Showing Alert Message
		alert.show();
	}

	@Override
	public void onBackPressed() {

		AlertDialog.Builder builder = new AlertDialog.Builder(GoomapActivity.this);
		builder.setTitle("CIT2ADM");
		builder.setMessage(getResources().getString(R.string.closeapp));
		builder.setPositiveButton("Quit",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						finish();

					}
				});
		builder.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub

					}
				});
		AlertDialog alert = builder.create();
		alert.show();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		// initilizeMap();

		LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE );
		boolean statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		if(!statusOfGPS){

			AlertDialog.Builder builder = new AlertDialog.Builder(GoomapActivity.this);
			builder.setTitle(getResources().getString(R.string.gps_message));
			AlertDialog alert = builder.create();
			alert.setButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
				}
			});
			// Showing Alert Message
			alert.show();

		}


	}



	/* Integrating Google maps */
	private void initilizeMap() {

		mGoogleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();

		if (mGoogleMap != null) {

			mGoogleMap.setMyLocationEnabled(true);
			currentPosition = new LatLng(Double.parseDouble(latitudecurrent),Double.parseDouble(longitudecurrent));
			mGoogleMap.getUiSettings().setZoomControlsEnabled(true);
			mGoogleMap.getUiSettings().setCompassEnabled(true);
			mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);
			mGoogleMap.getUiSettings().setAllGesturesEnabled(true);
			mGoogleMap.setTrafficEnabled(true);
			CameraPosition cameraPosition = new CameraPosition.Builder().target(currentPosition).zoom(16).build();
			mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.GoomapActivity_menubtn:
			menuCustomDialogView = new MenuDialogView(this);
			menuCustomDialogView.show();
			break;

		case R.id.GoomapActivity_repotbtn:
			reportDialogView = new ReportDialogView(this, languagesend);
			reportDialogView.show();
			break;
		case R.id.GoomapActivity_norepotbtn:
			displayDialog();
			break;
		}
	}

	protected void displayDialog() {
		dialog = new Dialog(GoomapActivity.this, R.style.FullHeightDialog);
		dialog.setContentView(R.layout.reportstaes);
		dialog.show();

		ReportStatesAsync reportstateasync = new ReportStatesAsync(GoomapActivity.this);
		reportstateasync.reportstaesinterface = GoomapActivity.this;
		reportstateasync.execute();

		openglobal = (TextView) dialog.findViewById(R.id.openglobal);
		open30days = (TextView) dialog.findViewById(R.id.open30days);
		open180days = (TextView) dialog.findViewById(R.id.open180days);

		assignedglobal = (TextView) dialog.findViewById(R.id.assignedglobal);
		assigned30days = (TextView) dialog.findViewById(R.id.assigned30days);
		assigned180days = (TextView) dialog.findViewById(R.id.assigned180days);

		inprogressglobal = (TextView) dialog.findViewById(R.id.progressglobal);
		inprogress30days = (TextView) dialog.findViewById(R.id.progress30days);
		inprogress180days = (TextView) dialog
				.findViewById(R.id.progress180days);

		completedlobal = (TextView) dialog.findViewById(R.id.completedglobal);
		completed30days = (TextView) dialog.findViewById(R.id.completed30days);
		completed180days = (TextView) dialog.findViewById(R.id.completed180daya);

		closedlobal = (TextView) dialog.findViewById(R.id.closedglobal);
		closed30days = (TextView) dialog.findViewById(R.id.closed30days);
		closed180days = (TextView) dialog.findViewById(R.id.closed180days);

	}

	@Override
	public void onReturnValue(String eventTag, int menuindex) {
		// TODO Auto-generated method stub
		event_of_grideview_item(eventTag, menuindex);
	}

	public void event_of_grideview_item(String eventTag, int lpos) {
		Log.d("DEBUG", "Position: " + lpos);
		if (lpos == 0) {

			String loginState = sharePreferenceClass
					.getValue_string(otherStaticValue.loginState);

			if (eventTag.equals("Login")) {

				if (loginState.equals("no")) {
					Intent in = new Intent(this, LoginActivity.class);
					startActivity(in);

				} else {
					Toast.makeText(this, R.string.alreadylogin,Toast.LENGTH_LONG).show();
				}

			}

			else {

				if (loginState.equals("yes")) {
					sharePreferenceClass.setValue_string(otherStaticValue.loginState, "no");
					Toast t = Toast.makeText(this, R.string.successfullogout,Toast.LENGTH_SHORT);
					t.setGravity(Gravity.CENTER, 0, 0);
					t.show();
					menuCustomDialogView.dismiss();

				} else {
					Toast.makeText(this, R.string.loginfirst, Toast.LENGTH_LONG).show();
				}

			}

			// finish();
		} else if (lpos == 1) {
			String loginState = sharePreferenceClass.getValue_string(otherStaticValue.loginState);
			if (loginState.equals("yes")) {
				Intent myProfileIntent = new Intent(this,MyProfileActivity.class);
				startActivity(myProfileIntent);

			} else {
				Toast.makeText(this, R.string.loginfirst, Toast.LENGTH_LONG).show();
			}

		} else if (lpos == 2) {
			startActivity(new Intent(this, GoomapActivity.class));
			finish();

		} else if (lpos == 3) {

			// userRecord = databasehandler.getProfileAllDataFromDb();
			SharePreferenceClass spc = new SharePreferenceClass(
					getApplicationContext());
			userId = spc.getUserid();
			// userRecord = db.getProfileAllDataFromDb();
			User myUser = (User) db.getUser(userId);

			String loginState = sharePreferenceClass
					.getValue_string(otherStaticValue.loginState);
			if (loginState.equals("yes")) {
				// for (int i = 0; i < userRecord.size(); i++) {
				// country = userRecord.get(i).get(UserProfile.KEY_country);
				country = myUser.getCountry();
				// Log.d(TAG, country);
				// }

				Intent intent = new Intent(GoomapActivity.this, MyNeighbourList.class);
				intent.putExtra("country", country);
				intent.putExtra("currentlat", latitudecurrent);
				intent.putExtra("currentlon", longitudecurrent);
				intent.putExtra("radiusforselection", radiusforselection);
				startActivity(intent);

			} else {

				Toast.makeText(this, R.string.loginfirst, Toast.LENGTH_LONG).show();
			}
		} else if (lpos == 4) {
			// userRecord = databasehandler.getProfileAllDataFromDb();

			String loginState = sharePreferenceClass.getValue_string(otherStaticValue.loginState);
			if (loginState.equals("yes")) {

				MyVotesAsync myvotesasync = new MyVotesAsync(GoomapActivity.this);
				myvotesasync.myvotesinterfaces = this;
				myvotesasync.execute(new SharePreferenceClass(GoomapActivity.this).getUserid());

			} else {
				Toast.makeText(this, R.string.loginfirst, Toast.LENGTH_LONG).show();
			}

		} else if (lpos == 5) {

			String loginState = sharePreferenceClass.getValue_string(otherStaticValue.loginState);
			if (loginState.equals("yes")) {

				// Intent intent = new Intent(this, IvisionrAssociation.class);
				Intent intent = new Intent(this, AssociationListActivity.class);
				intent.putExtra("currentlat", latitudecurrent);
				intent.putExtra("currentlon", longitudecurrent);
				startActivity(intent);
			} else {
				Toast.makeText(this, R.string.loginfirst, Toast.LENGTH_LONG).show();
			}

		} else if (lpos == 6) {

			String SHARING_TEXT = "I am using Cit2Adm mobile application to report pedestrian safety issue. You can also do the same and contribute towards the benefit of the Society" +
					"\nDownload from here : https://play.google.com/store/apps/details?id="+getPackageName();
			Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.imgpsh_fullsize);
			Intent sharingIntent = new Intent(Intent.ACTION_SEND);
			sharingIntent.setType("image/text/plain");
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			largeIcon.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
			File f = new File(Environment.getExternalStorageDirectory() + File.separator + "temporary_file.jpg");
			try {
				f.createNewFile();
				FileOutputStream fo = new FileOutputStream(f);
				fo.write(bytes.toByteArray());
			} catch (IOException e) {
				e.printStackTrace();
			}
			sharingIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file:///sdcard/temporary_file.jpg"));
			//startActivity(Intent.createChooser(share, "Share Image"));


                /*Intent sharingIntent = new Intent(
						android.content.Intent.ACTION_SEND);
				sharingIntent.setType("text/plain");*/
			sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "iVisionR");
			sharingIntent.putExtra(Intent.EXTRA_TEXT, SHARING_TEXT);
			    /*sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT,
						*//*shareBody*//*SHARING_TEXT);*/
			startActivity(Intent.createChooser(sharingIntent, "Share via"));

		} else if (lpos == 7) {

			String loginState = sharePreferenceClass
					.getValue_string(otherStaticValue.loginState);
			if (loginState.equals("yes")) {
				Intent myProfileIntent = new Intent(this, MyReport.class);
				startActivity(myProfileIntent);

			} else {
				Toast.makeText(this, R.string.loginfirst, Toast.LENGTH_LONG)
						.show();
			}

		} else if (lpos == 8) {
			String loginState = sharePreferenceClass
					.getValue_string(otherStaticValue.loginState);
			if (loginState.equals("yes")) {

				startActivity(new Intent(this, MyDrafts.class));
			} else {
				Toast.makeText(this, R.string.loginfirst, Toast.LENGTH_LONG).show();
			}

		} else if (lpos == 9) {
			String loginState = sharePreferenceClass.getValue_string(otherStaticValue.loginState);
			if (loginState.equals("yes")) {

				Intent intent = new Intent(this, Issueserchbyplace.class);
				intent.putExtra("currentlat", latitudecurrent);
				intent.putExtra("currentlon", longitudecurrent);
				startActivity(intent);
			} else {
				Toast.makeText(this, R.string.loginfirst, Toast.LENGTH_LONG).show();
			}

		} else if (lpos == 10) {
			// displayParameterDialog();
			startActivity(new Intent(GoomapActivity.this, Parameters.class));
			finish();

		} else if (lpos == 11) {

			Intent intent = new Intent(this, SendFeedback.class);

			startActivity(intent);

		} else if (lpos == 12) {

			String id = sharePreferenceClass.getUserid();

			UpdateGpsAsync updategps = new UpdateGpsAsync(GoomapActivity.this);
			updategps.updategpsinterface = GoomapActivity.this;
			updategps.execute(id, latitudecurrent, longitudecurrent);
			new SharePreferenceClass(GoomapActivity.this)
					.setUpadelocationLat(latitudecurrent);
			new SharePreferenceClass(GoomapActivity.this)
					.setUpadelocationLon(longitudecurrent);

		} else if (lpos == 13) {
			String loginState = sharePreferenceClass
					.getValue_string(otherStaticValue.loginState);
			if (loginState.equals("yes")) {
				Intent myProfileIntent = new Intent(this, ResetPassword.class);
				startActivity(myProfileIntent);

			} else {
				Toast.makeText(this, R.string.loginfirst, Toast.LENGTH_LONG)
						.show();
			}
		} else if (lpos == 14) {

			// Intent dbmanager = new
			// Intent(getApplicationContext(),AndroidDatabaseManager.class);
			// startActivity(dbmanager);

			/*
			 * Toast toast = Toast.makeText(getApplicationContext(),
			 * R.string.comingsoon, Toast.LENGTH_SHORT);
			 * toast.setGravity(Gravity.CENTER, 0, 0); toast.show();
			 */

			alert_dialog = new Dialog(this);
			alert_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			alert_dialog.setContentView(R.layout.dialog_alert);
			alert_dialog.show();
			TextView alert_msg = (TextView) alert_dialog
					.findViewById(R.id.alert_msg);
			Button alert_ok = (Button) alert_dialog.findViewById(R.id.alert_ok);

			alert_msg.setText(R.string.mymessagetext);

			alert_ok.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					alert_dialog.dismiss();
					// finish();
				}
			});

		}

	}

	private void displyvotesDialog(List<Myvotes> listmyvotes) {
		voteDialog = new Dialog(GoomapActivity.this);
		voteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		voteDialog.setContentView(R.layout.myvotes);
		voteDialog.show();
		myvoteslist = (ListView) voteDialog.findViewById(R.id.myvotelist);
		Log.d("DEBUG", "Number of Votes: " + listmyvotes.size());
		Votesadppter votesadpter = new Votesadppter(GoomapActivity.this,
				listmyvotes);
		myvoteslist.setAdapter(votesadpter);

		// arrayAdaptersping = new ArrayAdapter<String>(this,
		// android.R.layout.simple_list_item_1, language);
		// myvoteslist.setAdapter(arrayAdaptersping);

	}

	@Override
	public void onGetallIssueStarted() {
		if (null != mProgressDialog) {
			mProgressDialog.show();
		}

	}

	@Override
	public void onGetallIssuenoData() {

		if (null != mProgressDialog) {
			mProgressDialog.dismiss();
		}
		Toast.makeText(GoomapActivity.this, R.string.noissue, Toast.LENGTH_LONG).show();

	}

	/* Integrating Markers in Google Maps and nearest location */
	@Override
	public void onGetallIssueCompleted(final List<GetAllIssueDetails> lists) {

		allissuedetails.addAll(lists);

		if (null != mProgressDialog && mProgressDialog.isShowing())

		{
			try {
				mProgressDialog.dismiss();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		try {
            if(mGoogleMap != null) {
                for (i = 0; i < allissuedetails.size(); i++) {
                    markerOptions = new MarkerOptions();

                    if (allissuedetails.get(i).getIssuetypeid().equals("11")) {
                        latLng = new LatLng(Double.parseDouble(allissuedetails.get(
                                i).getLattitude()),
                                Double.parseDouble(allissuedetails.get(i)
                                        .getLongitude()));
                        markerOptions.icon(
                                BitmapDescriptorFactory
                                        .fromResource(R.drawable.completed)).title(
                                allissuedetails.get(i).getId());
                        mGoogleMap.addMarker(markerOptions.position(latLng));
                    }

                    if (allissuedetails.get(i).getStatus().equals("open")) {
                        latLng = new LatLng(Double.parseDouble(allissuedetails.get(
                                i).getLattitude()),
                                Double.parseDouble(allissuedetails.get(i)
                                        .getLongitude()));
                        markerOptions.icon(
                                BitmapDescriptorFactory
                                        .fromResource(R.drawable.open)).title(
                                allissuedetails.get(i).getId());
                        mGoogleMap.addMarker(markerOptions.position(latLng));

                    } else if (allissuedetails.get(i).getStatus().equals("closed")) {
                        latLng = new LatLng(Double.parseDouble(allissuedetails.get(
                                i).getLattitude()),
                                Double.parseDouble(allissuedetails.get(i)
                                        .getLongitude()));
                        markerOptions.icon(
                                BitmapDescriptorFactory
                                        .fromResource(R.drawable.closed)).title(
                                allissuedetails.get(i).getId());
                        mGoogleMap.addMarker(markerOptions.position(latLng));

                    } else if (allissuedetails.get(i).getStatus()
                            .equals("completed")) {
                        latLng = new LatLng(Double.parseDouble(allissuedetails.get(
                                i).getLattitude()),
                                Double.parseDouble(allissuedetails.get(i)
                                        .getLongitude()));
                        markerOptions.icon(
                                BitmapDescriptorFactory
                                        .fromResource(R.drawable.completed)).title(
                                allissuedetails.get(i).getId());
                        mGoogleMap.addMarker(markerOptions.position(latLng));

                    } else if (allissuedetails.get(i).getStatus()
                            .equals("progress")) {
                        latLng = new LatLng(Double.parseDouble(allissuedetails.get(
                                i).getLattitude()),
                                Double.parseDouble(allissuedetails.get(i)
                                        .getLongitude()));
                        markerOptions.icon(
                                BitmapDescriptorFactory
                                        .fromResource(R.drawable.progress)).title(
                                allissuedetails.get(i).getId());
                        mGoogleMap.addMarker(markerOptions.position(latLng));

                    } else if (allissuedetails.get(i).getStatus()
                            .equals("assigned")) {
                        latLng = new LatLng(Double.parseDouble(allissuedetails.get(
                                i).getLattitude()),
                                Double.parseDouble(allissuedetails.get(i)
                                        .getLongitude()));
                        markerOptions.icon(
                                BitmapDescriptorFactory
                                        .fromResource(R.drawable.assigned)).title(
                                allissuedetails.get(i).getId());
                        mGoogleMap.addMarker(markerOptions.position(latLng));

                    }
                }
            }
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void onreportstatesstaerted() {
		// TODO Auto-generated method stub
		if (null != mProgressDialog) {

			mProgressDialog.setMessage(getResources().getString(
					R.string.loading));
			mProgressDialog.show();

		}

	}

	@Override
	public void onreportstatescompleted(Reportstates reportstatesbin) {

		if (null != mProgressDialog) {
			mProgressDialog.dismiss();
		}

		openglobal.setText(reportstatesbin.getOpenglobal());
		open30days.setText(reportstatesbin.getOpen30days());
		Log.e("response", reportstatesbin.getOpenglobal() + ""
				+ reportstatesbin.getOpen30days());
		open180days.setText(reportstatesbin.getOpen180days());
		assignedglobal.setText(reportstatesbin.getAssignedglobal());
		assigned30days.setText(reportstatesbin.getAssigned30days());
		assigned180days.setText(reportstatesbin.getAssigned180days());
		inprogressglobal.setText(reportstatesbin.getInprogressglobal());
		inprogress30days.setText(reportstatesbin.getInprogress30days());
		inprogress180days.setText(reportstatesbin.getInprogress180days());
		completedlobal.setText(reportstatesbin.getCompletedglobal());
		completed30days.setText(reportstatesbin.getCompleted30days());
		completed180days.setText(reportstatesbin.getCompleted180days());
		closedlobal.setText(reportstatesbin.getClosedglobal());
		Log.e("response", reportstatesbin.getClosedglobal());
		closed30days.setText(reportstatesbin.getClosed30days());
		closed180days.setText(reportstatesbin.getClosed180days());

	}

	@Override
	public void onStarted() {
		// TODO Auto-generated method stub

		if (null != mProgressDialog) {
			mProgressDialog
					.setMessage(getResources().getText(R.string.loading));
			mProgressDialog.show();

		}

	}

	@Override
	public void onCompleted(String errorcode) {
		// TODO Auto-generated method stub

		if (null != mProgressDialog) {
			mProgressDialog.dismiss();
		}

		if (errorcode.equals("S20001")) {

			Toast.makeText(getApplicationContext(), R.string.reserved,
					Toast.LENGTH_SHORT).show();

		}

		if (errorcode.equals("S20002")) {

			Toast.makeText(getApplicationContext(),
					R.string.invalidrequestparameter, Toast.LENGTH_SHORT)
					.show();

		}

		if (errorcode.equals("S20600")) {

			Toast.makeText(getApplicationContext(), R.string.locationupdated,
					Toast.LENGTH_SHORT).show();

		}

		if (errorcode.equals("S20601")) {

			Toast.makeText(getApplicationContext(), R.string.latnotfound,
					Toast.LENGTH_SHORT).show();

		}

		if (errorcode.equals("S20602")) {

			Toast.makeText(getApplicationContext(), R.string.latinvalid,
					Toast.LENGTH_SHORT).show();

		}

		if (errorcode.equals("S20603")) {

			Toast.makeText(getApplicationContext(), R.string.longnotfound,
					Toast.LENGTH_SHORT).show();

		}

		if (errorcode.equals("S20604")) {

			Toast.makeText(getApplicationContext(), R.string.longinvalid,
					Toast.LENGTH_SHORT).show();

		}

		if (errorcode.equals("S20605")) {

			Toast.makeText(getApplicationContext(), R.string.useridnotpassed,
					Toast.LENGTH_SHORT).show();

		}

		if (errorcode.equals("S20606")) {

			Toast.makeText(getApplicationContext(), R.string.usernotexist,
					Toast.LENGTH_SHORT).show();

		}

		if (errorcode.equals("S20607")) {

			Toast.makeText(getApplicationContext(),
					R.string.unableupdatelocation, Toast.LENGTH_SHORT).show();

		}

	}

	@Override
	public void onstarted() {
		// TODO Auto-generated method stub
		if (null != mProgressDialog) {
			mProgressDialog.setMessage(getResources().getString(
					R.string.loading));
			mProgressDialog.show();
		}
	}

	@Override
	public void onCompleted(List<Myvotes> listmyvotes) {
		Log.d("DEBUG", "Vote: " + listmyvotes.size());

		displyvotesDialog(listmyvotes);
		if (null != mProgressDialog) {
			mProgressDialog.dismiss();

		}

	}

	@Override
	public void noData() {
		if (null != mProgressDialog) {
			mProgressDialog.dismiss();
			Toast toast = Toast.makeText(GoomapActivity.this, R.string.novotes,
					Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.CENTER, 0, 0);
			toast.show();

		}

	}

}
