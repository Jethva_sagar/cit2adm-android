package com.ivisionr.cit2adm.bin;

public class Myvotes {

	private String reportid="";
	private String ratingplus="";
	private String ratingminus="";
	public String getReportid() {
		return reportid;
	}
	public void setReportid(String reportid) {
		this.reportid = reportid;
	}
	public String getRatingplus() {
		return ratingplus;
	}
	public void setRatingplus(String ratingplus) {
		this.ratingplus = ratingplus;
	}
	public String getRatingminus() {
		return ratingminus;
	}
	public void setRatingminus(String ratingminus) {
		this.ratingminus = ratingminus;
	}
	
}
