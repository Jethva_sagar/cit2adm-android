package com.ivisionr.cit2adm.interfaces;

public interface RateIssueInterface {
	public void onrateIssueStarted();
	public void onrateIssueCompleted();
	public void onrateIssueDuel();

}
