package com.ivisionr.cit2adm.Util;

import java.util.Locale;

import android.content.Context;
import android.util.Log;

public class LanguesCheckClass{
	Context context;
	SharePreferenceClass sharePreferenceClass;
	public LanguesCheckClass(Context con){
		this.context=con;
		sharePreferenceClass=new SharePreferenceClass(context);
	}
	public String GetCurrentLangues(){

		String langues1=Locale.getDefault().getDisplayLanguage();
		String langues2=Locale.getDefault().getLanguage();
		
		Log.d("DEBUG", "LanguesCheck Display Language: "+langues1);
		Log.d("DEBUG", "LanguesCheck Language: "+langues2);
		
		
		
		String returnLangues="";
		if(!langues2.equals("")){
			returnLangues=langues2;
			sharePreferenceClass.setValue_string(otherStaticValue.mobile_langues, langues2);
		}else{
			returnLangues="en";
			sharePreferenceClass.setValue_string(otherStaticValue.mobile_langues, "en");
		}
		return returnLangues;
	}
}
