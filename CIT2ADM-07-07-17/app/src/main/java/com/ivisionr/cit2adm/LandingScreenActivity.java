package com.ivisionr.cit2adm;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.ivisionr.cit2adm.Util.ConnectionDetector;
import com.ivisionr.cit2adm.Util.Utils;

public class LandingScreenActivity extends ActivityExceptionDemo
{

	//	private static int SPLASH_TIME_OUT = 4000;
	ConnectionDetector connectionDetector;
	private Button tcBtn, continueBtn;
	private TextView poweredby;
	

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.landingscreen);
		
		
		connectionDetector = new ConnectionDetector(this);
		
		tcBtn = (Button)findViewById(R.id.tcBtn);
		continueBtn = (Button)findViewById(R.id.continueBtn);
		
		//poweredby = (TextView)findViewById(R.id.poweredby);
		//poweredby.setTypeface(null, Typeface.BOLD_ITALIC);
		
		if(Utils.checkConnectivity(LandingScreenActivity.this)){
			
			
			
			tcBtn.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View v)
				{
					// TODO Auto-generated method stub
					
					Intent in = new Intent(LandingScreenActivity.this,
							TermsActivity.class);
					startActivity(in);
					finish();
					
					
					
				}
			});
			
			continueBtn.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View v)
				{
					// TODO Auto-generated method stub
					
					Intent in = new Intent(LandingScreenActivity.this,
							GoomapActivity.class);
					startActivity(in);
					finish();
					
					
					
				}
			});
			
			
		}else{
			showNetworkDialog("internet");
		}
		
	
	}

	@SuppressWarnings("deprecation")
	public void showNetworkDialog(final String message){
		// final exit of application
		AlertDialog.Builder builder = new AlertDialog.Builder(LandingScreenActivity.this);
		builder.setTitle(getResources().getString(R.string.app_name));
		if(message.equals("gps")){
			builder.setMessage(getResources().getString(R.string.gps_message));
		}
		else if(message.equals("internet")){
			builder.setMessage(getResources().getString(R.string.net_message));
		}
		AlertDialog alert = builder.create();
		alert.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				if(message.equals("gps")){
					Intent i=new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);		
					startActivityForResult(i, 1);
				}
				else if(message.equals("internet")){
					Intent i=new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS);		
					startActivityForResult(i,2);		
					Intent i1=new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS);		
					startActivityForResult(i1,3);	
				}
				else{
					//do nothing
				}
			}
		});
		// Showing Alert Message
		alert.show();	
	}
		
	
	@Override
  	protected void onResume() {
  		Log.d("DEBUG", "App Resumed");
  		super.onResume();
  	}
  	
	

		
}
