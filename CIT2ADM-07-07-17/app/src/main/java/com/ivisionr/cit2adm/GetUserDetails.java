package com.ivisionr.cit2adm;

import com.ivisionr.cit2adm.Util.Utils;
import com.ivisionr.cit2adm.async.GerUserDetailsAsync;
import com.ivisionr.cit2adm.bin.GetUserDetailsbin;
import com.ivisionr.cit2adm.interfaces.GetUserdetailsInterfaces;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class GetUserDetails extends Activity implements
		GetUserdetailsInterfaces {
	private String id;
	private GetUserDetailsbin getuserdetailsbin;
	private TextView userfirstname, userlastname, usercity, userzip;
	private TextView open_counter, closed_counter, process_counter,
			completed_counter, comments_counter, rating_plus_counter,
			rating_minus_counter;
	private ProgressDialog mprogressDialog;
	private Button backbutton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.getuserdetails);
		getComponents();
		mprogressDialog = new ProgressDialog(this);
		mprogressDialog.setMessage(getResources().getText(R.string.loading));
		getuserdetailsbin = new GetUserDetailsbin();

		Bundle bundle = getIntent().getExtras();

		if (getIntent().hasExtra("id")) {
			id = bundle.getString("id");
		}

		if (Utils.checkConnectivity(GetUserDetails.this)) {
			GerUserDetailsAsync getuserdetails = new GerUserDetailsAsync(
					GetUserDetails.this);
			getuserdetails.getuserdetailsinterfaces = this;
			getuserdetails.execute(id);
		} else {
			showNetworkDialog("internet");
		}
		backbutton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				GetUserDetails.this.finish();

			}
		});

	}

	@SuppressWarnings("deprecation")
	public void showNetworkDialog(final String message) {
		// final exit of application
		AlertDialog.Builder builder = new AlertDialog.Builder(
				GetUserDetails.this);
		builder.setTitle(getResources().getString(R.string.app_name));
		if (message.equals("gps")) {
			builder.setMessage(getResources().getString(R.string.gps_message));
		} else if (message.equals("internet")) {
			builder.setMessage(getResources().getString(R.string.net_message));
		}
		AlertDialog alert = builder.create();
		alert.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				if (message.equals("gps")) {
					Intent i = new Intent(
							android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
					startActivityForResult(i, 1);
				} else if (message.equals("internet")) {
					Intent i = new Intent(
							android.provider.Settings.ACTION_WIRELESS_SETTINGS);
					startActivityForResult(i, 2);
					Intent i1 = new Intent(
							android.provider.Settings.ACTION_WIFI_SETTINGS);
					startActivityForResult(i1, 3);
				} else {
					// do nothing
				}
			}
		});
		// Showing Alert Message
		alert.show();
	}

	private void getComponents() {

		userfirstname = (TextView) findViewById(R.id.userfirstname);
		userlastname = (TextView) findViewById(R.id.userlastname);
		usercity = (TextView) findViewById(R.id.usercity);
		userzip = (TextView) findViewById(R.id.userzip);
		backbutton = (Button) findViewById(R.id.myneighbourback);
		open_counter = (TextView) findViewById(R.id.open_counter);
		closed_counter = (TextView) findViewById(R.id.closed_counter);
		process_counter = (TextView) findViewById(R.id.process_counter);
		completed_counter = (TextView) findViewById(R.id.completed_counter);
		comments_counter = (TextView) findViewById(R.id.comments_counter);
		rating_plus_counter = (TextView) findViewById(R.id.rating_plus_counter);
		rating_minus_counter = (TextView) findViewById(R.id.rating_minus_counter);
	}

	@Override
	public void ongetuserdetilsstartrd() {
		// TODO Auto-generated method stub
		if (null != mprogressDialog) {
			mprogressDialog.show();
		}

	}

	@Override
	public void ongetuserdetailscompleted(GetUserDetailsbin list) {
		// TODO Auto-generated method stub
		getuserdetailsbin = list;
		if (null != mprogressDialog) {
			mprogressDialog.dismiss();
		}
		userfirstname.setText(getuserdetailsbin.getFirstname());
		userlastname.setText(getuserdetailsbin.getLastname());
		usercity.setText(getuserdetailsbin.getCity());
		userzip.setText(getuserdetailsbin.getZip());
       if(getuserdetailsbin.getCounteropen().equals("")){
    	  
    	   open_counter.setText("0");
    	   
       }else{
    	   open_counter.setText(getuserdetailsbin.getCounteropen());
       }
		open_counter.setText(getuserdetailsbin.getCounteropen());

		closed_counter.setText(getuserdetailsbin.getCounterclosed());

		process_counter.setText(getuserdetailsbin.getCounterprogress());

		completed_counter.setText(getuserdetailsbin.getCountercompleted());

		comments_counter.setText(getuserdetailsbin.getComments_counter());

		rating_plus_counter.setText(getuserdetailsbin.getRatingplus_counter());

		rating_minus_counter
				.setText(getuserdetailsbin.getRatingminus_counter());

	}

}
