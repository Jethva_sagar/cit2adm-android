package com.ivisionr.cit2adm.bin;

public class Reportstates {
	
	public String openglobal="";
	public String open30days="";
	public String open180days="";
	public String assignedglobal="";
	public String assigned30days="";
	public String assigned180days="";
	public String inprogressglobal="";
	public String inprogress30days="";
	public String inprogress180days="";
	public String completedglobal="";
	public String completed30days="";
	public String completed180days="";
	public String closedglobal="";
	public String closed30days="";
	public String closed180days="";
	public String getOpenglobal() {
		return openglobal;
	}
	public void setOpenglobal(String openglobal) {
		this.openglobal = openglobal;
	}
	public String getOpen30days() {
		return open30days;
	}
	public void setOpen30days(String open30days) {
		this.open30days = open30days;
	}
	public String getOpen180days() {
		return open180days;
	}
	public void setOpen180days(String open180days) {
		this.open180days = open180days;
	}
	public String getAssignedglobal() {
		return assignedglobal;
	}
	public void setAssignedglobal(String assignedglobal) {
		this.assignedglobal = assignedglobal;
	}
	public String getAssigned30days() {
		return assigned30days;
	}
	public void setAssigned30days(String assigned30days) {
		this.assigned30days = assigned30days;
	}
	public String getAssigned180days() {
		return assigned180days;
	}
	public void setAssigned180days(String assigned180days) {
		this.assigned180days = assigned180days;
	}
	public String getInprogressglobal() {
		return inprogressglobal;
	}
	public void setInprogressglobal(String inprogressglobal) {
		this.inprogressglobal = inprogressglobal;
	}
	public String getInprogress30days() {
		return inprogress30days;
	}
	public void setInprogress30days(String inprogress30days) {
		this.inprogress30days = inprogress30days;
	}
	public String getInprogress180days() {
		return inprogress180days;
	}
	public void setInprogress180days(String inprogress180days) {
		this.inprogress180days = inprogress180days;
	}
	public String getCompletedglobal() {
		return completedglobal;
	}
	public void setCompletedglobal(String completedglobal) {
		this.completedglobal = completedglobal;
	}
	public String getCompleted30days() {
		return completed30days;
	}
	public void setCompleted30days(String completed30days) {
		this.completed30days = completed30days;
	}
	public String getCompleted180days() {
		return completed180days;
	}
	public void setCompleted180days(String completed180days) {
		this.completed180days = completed180days;
	}
	public String getClosedglobal() {
		return closedglobal;
	}
	public void setClosedglobal(String closedglobal) {
		this.closedglobal = closedglobal;
	}
	public String getClosed30days() {
		return closed30days;
	}
	public void setClosed30days(String closed30days) {
		this.closed30days = closed30days;
	}
	public String getClosed180days() {
		return closed180days;
	}
	public void setClosed180days(String closed180days) {
		this.closed180days = closed180days;
	}

}
