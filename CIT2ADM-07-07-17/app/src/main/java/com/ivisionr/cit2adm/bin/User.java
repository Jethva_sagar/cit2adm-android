/*******************************************************************************
 * Copyright 2013 Gabriele Mariotti
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.ivisionr.cit2adm.bin;

public class User {

	/*
	 * 
	 * public static final String KEY_ID = "id";
	public static final String KEY_USER_TYPE_ID = "user_type_id";
	public static final String KEY_FIRST_NAME = "first_name";
	public static final String KEY_LAST_NAME = "last_name";
	public static final String KEY_EMAIL = "email";
	public static final String KEY_USERNAME = "username";
	public static final String KEY_PASSWORD = "password";
	public static final String KEY_ORGANIZATION = "organization";
	public static final String KEY_CITY = "city";
	public static final String KEY_COUNTRY = "country";
	public static final String KEY_ZIPCODE = "zipcode";
	public static final String KEY_ACTIVE = "active";
	public static final String KEY_PAID = "paid";
	public static final String KEY_NOTE = "note";
	public static final String KEY_MEMBERSHIP_EXPIRY_DATE = "membership_expiry_date";
	public static final String KEY_CREATED = "created";
	public static final String KEY_MODIFIED = "modified";
	 */
	public String id;
	public String user_type_id;
	public String first_name;  
    public String last_name;
    public String email; 
    public String username; 
    public String password;
    public String organization;
    public String city;
    public String country;
    public String zipcode; 
    public String active;     
    public String paid; 
    public String note; 
    public String membership_expiry_date; 
    public String assoc_code;
    public String access_id;
    
    public String created; 
    public String device_id; 
    public String app_version_code; 
    public String app_version_name; 
    public String modified; 
    
    
    public User() {
    	
    }

    public String getDevice_id() {
		return device_id;
	}


	public void setDevice_id(String device_id) {
		this.device_id = device_id;
	}


	public String getApp_version_code() {
		return app_version_code;
	}


	public void setApp_version_code(String app_version_code) {
		this.app_version_code = app_version_code;
	}


	public String getApp_version_name() {
		return app_version_name;
	}


	public void setApp_version_name(String app_version_name) {
		this.app_version_name = app_version_name;
	}


	

	public String getAccess_id() {
		return access_id;
	}


	public void setAccess_id(String access_id) {
		this.access_id = access_id;
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getUser_type_id() {
		return user_type_id;
	}


	public void setUser_type_id(String user_type_id) {
		this.user_type_id = user_type_id;
	}


	public String getFirst_name() {
		return first_name;
	}


	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}


	public String getLast_name() {
		return last_name;
	}


	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getOrganization() {
		return organization;
	}


	public void setOrganization(String organization) {
		this.organization = organization;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public String getCountry() {
		return country;
	}


	public void setCountry(String country) {
		this.country = country;
	}


	public String getZipcode() {
		return zipcode;
	}


	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}


	public String getActive() {
		return active;
	}


	public void setActive(String active) {
		this.active = active;
	}


	public String getPaid() {
		return paid;
	}


	public void setPaid(String paid) {
		this.paid = paid;
	}


	public String getNote() {
		return note;
	}


	public void setNote(String note) {
		this.note = note;
	}


	public String getMembership_expiry_date() {
		return membership_expiry_date;
	}


	public void setMembership_expiry_date(String membership_expiry_date) {
		this.membership_expiry_date = membership_expiry_date;
	}


	public String getCreated() {
		return created;
	}


	public void setCreated(String created) {
		this.created = created;
	}


	public String getModified() {
		return modified;
	}


	public void setModified(String modified) {
		this.modified = modified;
	}


	public String getAssoc_code() {
		return assoc_code;
	}


	public void setAssoc_code(String assoc_code) {
		this.assoc_code = assoc_code;
	}

    
    
    
    
    

}
