package com.ivisionr.cit2adm.custom;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

import android.util.Log;

public class UploadManager
{

	public static String uploadDataToUrlPost(
			List<NameValuePair> nameValuePairs, String urlPost)
			throws UnsupportedEncodingException, ClientProtocolException,
			IOException
	{

		HttpClient httpClient = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost(urlPost);
		httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		HttpResponse httpResponse = httpClient.execute(httpPost);
		Log.d("Http Response:", httpResponse.toString());
		HttpEntity httpEntity = httpResponse.getEntity();
		InputStream is = httpEntity.getContent();

		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder builder = new StringBuilder();

		for (String line = null; (line = reader.readLine()) != null;)
		{
			builder.append(line).append("\n");
		}
		// JSONTokener tokener = new JSONTokener(builder.toString());
		Log.d("Json Response:rr ", builder.toString());

		return builder.toString();

	}

	public static String uploadDataToUrlGet(List<NameValuePair> nameValuePairs,
			String urlGet) throws UnsupportedEncodingException,
			ClientProtocolException, IOException
	{

		ResponseHandler<String> resHandler = new BasicResponseHandler();

		String paramString = URLEncodedUtils.format(nameValuePairs, "utf-8");
		urlGet += paramString;
		Log.d("rl", urlGet);

		HttpClient httpClient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(urlGet);

		String httpResponse = httpClient.execute(httpGet, resHandler);
		//		Log.d("Http Response:", httpResponse.toString());
		//		HttpEntity httpEntity = httpResponse.getEntity();
		//		InputStream is = httpEntity.getContent();
		//
		//		BufferedReader reader = new BufferedReader(
		//				new InputStreamReader(is));
		//		StringBuilder builder = new StringBuilder();
		//
		//		for (String line = null; (line = reader.readLine()) != null;) {
		//			builder.append(line).append("\n");
		//		}
		//		// JSONTokener tokener = new JSONTokener(builder.toString());
		//		Log.d("Json Response:rr ", builder.toString());

		Log.d("httpResponse", httpResponse);
		// JSONObject jsObjForContact=new JSONObject(page);

		//		JSONObject jsonobj = new JSONObject(httpResponse);

		return httpResponse;

	}
}
