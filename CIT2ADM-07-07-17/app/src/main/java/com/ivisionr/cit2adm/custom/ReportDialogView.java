package com.ivisionr.cit2adm.custom;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ivisionr.cit2adm.LoginActivity;
import com.ivisionr.cit2adm.R;
import com.ivisionr.cit2adm.SafelifeReportPost;
import com.ivisionr.cit2adm.Util.SharePreferenceClass;
import com.ivisionr.cit2adm.Util.otherStaticValue;

public class ReportDialogView extends Dialog implements
android.view.View.OnClickListener
{

	ReportAdaptore reportAdaptore;
	Activity menuContextreportActivity;

	private int display_width, display_height;
	final String[] reportelementarray = new String[] { "Sidewalk Obstacle",
			"Road Sign High < 2.30m", "Sidewalk Large < 1.40 Meter",
			"Pavement Covering", "Post", "Street Equipment", "Guter",
			"Crossfall", "Lighting", "Tree", "Good Example",
			"Improvement Needed", "Train Station", "Airport", "Others" };
	Context menuContext;
	String[] report;
	String language="";

	GridView gridView;

	Button report_dialog_crossbtn;
	Button report_dialogbtn;
	CustomFunctionClass customFunctionClass;
	SharePreferenceClass sharePreferenceClass;

	public ReportDialogView(Activity context,String language)
	{
		super(context);
		this.menuContextreportActivity = context;
		this.menuContext = (Context) context;
		this.language=language;
		getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		customFunctionClass = new CustomFunctionClass(context);
		sharePreferenceClass = new SharePreferenceClass(menuContext);

	}
	@SuppressLint("NewApi")
	private void computeScreenDimension()
	{
		WindowManager wm = (WindowManager) menuContext
				.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		Point size = new Point();
		if (android.os.Build.VERSION.SDK_INT > 13)
		{
			display.getSize(size);
			display_width = size.x;
			display_height = size.y;
		}
		else
		{
			display_width = display.getWidth(); // deprecated
			display_height = display.getHeight();
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.reportdialog);
		gridView = (GridView) findViewById(R.id.report_gridView1);

		report_dialog_crossbtn = (Button) findViewById(R.id.report_dialog_crossbtn);
		report_dialogbtn = (Button) findViewById(R.id.report_dialogbtn);
		report_dialog_crossbtn.setOnClickListener(this);
		report_dialogbtn.setOnClickListener(this);
		report=menuContext.getResources().getStringArray(R.array.reportitem);

		reportAdaptore = new ReportAdaptore(menuContext, report);

		gridView.setAdapter(reportAdaptore);

		gridView.setOnItemClickListener(new OnItemClickListener()
		{

			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int arg2,
					long arg3)
			{
				
				int current_pos = arg2;
				
				String eventTag = ((TextView) v
						.findViewById(R.id.gridviewelement_text)).getText()
						.toString();

				if(current_pos == 0) {
					String loginState = sharePreferenceClass
							.getValue_string(otherStaticValue.loginState);
					if(loginState.equals("yes")){				

						Intent intent=new Intent(getContext(),SafelifeReportPost.class);
						intent.putExtra("issuetype", "1");
						intent.putExtra("issutypename", "Sidewalk Obstacle");
						intent.putExtra("language", language);
						getContext().startActivity(intent);


					}else{
						Intent intent=new Intent(getContext(),LoginActivity.class);
						getContext().startActivity(intent);
					}

				} else if(current_pos == 1) {

					String loginState = sharePreferenceClass
							.getValue_string(otherStaticValue.loginState);
					if(loginState.equals("yes")){
						Intent intent=new Intent(getContext(),SafelifeReportPost.class);
						intent.putExtra("issuetype", "2");
						intent.putExtra("issutypename", "Road sign High < 2.30m");
						intent.putExtra("language", language);
						getContext().startActivity(intent);

					}else{
						Intent intent=new Intent(getContext(),LoginActivity.class);
						getContext().startActivity(intent);
					}

				} else if(current_pos == 2) {

					String loginState = sharePreferenceClass
							.getValue_string(otherStaticValue.loginState);
					if(loginState.equals("yes")){
						Intent intent=new Intent(getContext(),SafelifeReportPost.class);
						intent.putExtra("issuetype", "3");
						intent.putExtra("issutypename", "Sidewalk Large < 1.40 meter");
						intent.putExtra("language", language);
						getContext().startActivity(intent);

					}else{
						Intent intent=new Intent(getContext(),LoginActivity.class);
						getContext().startActivity(intent);
					}

				}else if(current_pos == 3) {

					String loginState = sharePreferenceClass
							.getValue_string(otherStaticValue.loginState);
					if(loginState.equals("yes")){
						Intent intent=new Intent(getContext(),SafelifeReportPost.class);
						intent.putExtra("issuetype", "4");
						intent.putExtra("issutypename", "Pavement covering");
						intent.putExtra("language", language);
						getContext().startActivity(intent);

					}else{
						Intent intent=new Intent(getContext(),LoginActivity.class);
						getContext().startActivity(intent);
					}

				} else if(current_pos == 4) { 

					String loginState = sharePreferenceClass
							.getValue_string(otherStaticValue.loginState);
					if(loginState.equals("yes")){
						Intent intent=new Intent(getContext(),SafelifeReportPost.class);
						intent.putExtra("issuetype", "5");
						intent.putExtra("issutypename", "Post");
						intent.putExtra("language", language);
						getContext().startActivity(intent);

					}else{
						Intent intent=new Intent(getContext(),LoginActivity.class);
						getContext().startActivity(intent);
					}

				} else if(current_pos == 5) {

					String loginState = sharePreferenceClass
							.getValue_string(otherStaticValue.loginState);
					if(loginState.equals("yes")){
						Intent intent=new Intent(getContext(),SafelifeReportPost.class);
						intent.putExtra("issuetype", "6");
						intent.putExtra("issutypename", "Street equipment");
						intent.putExtra("language", language);
						getContext().startActivity(intent);

					}else{
						Intent intent=new Intent(getContext(),LoginActivity.class);
						getContext().startActivity(intent);
					}

				} else if(current_pos == 6) {

					String loginState = sharePreferenceClass
							.getValue_string(otherStaticValue.loginState);
					if(loginState.equals("yes")){
						Intent intent=new Intent(getContext(),SafelifeReportPost.class);
						intent.putExtra("issuetype", "7");
						intent.putExtra("issutypename", "Guter");
						intent.putExtra("language", language);
						getContext().startActivity(intent);

					}else{
						Intent intent=new Intent(getContext(),LoginActivity.class);
						getContext().startActivity(intent);
					}

				} else if(current_pos == 7) {

					String loginState = sharePreferenceClass
							.getValue_string(otherStaticValue.loginState);
					if(loginState.equals("yes")){
						Intent intent=new Intent(getContext(),SafelifeReportPost.class);
						intent.putExtra("issuetype", "8");
						intent.putExtra("issutypename", "Crossfall");
						intent.putExtra("language", language);
						getContext().startActivity(intent);

					}else{
						Intent intent=new Intent(getContext(),LoginActivity.class);
						getContext().startActivity(intent);
					}

				} else if(current_pos == 8) {

					String loginState = sharePreferenceClass
							.getValue_string(otherStaticValue.loginState);
					if(loginState.equals("yes")){
						Intent intent=new Intent(getContext(),SafelifeReportPost.class);
						intent.putExtra("issuetype", "9");
						intent.putExtra("issutypename", "Lighting");
						intent.putExtra("language", language);
						getContext().startActivity(intent);

					}else{
						Intent intent=new Intent(getContext(),LoginActivity.class);
						getContext().startActivity(intent);
					}

				} else if(current_pos == 9) {

					String loginState = sharePreferenceClass
							.getValue_string(otherStaticValue.loginState);
					if(loginState.equals("yes")){
						Intent intent=new Intent(getContext(),SafelifeReportPost.class);
						intent.putExtra("issuetype", "10");
						intent.putExtra("issutypename", "Tree");
						intent.putExtra("language", language);
						getContext().startActivity(intent);

					}else{
						Intent intent=new Intent(getContext(),LoginActivity.class);
						getContext().startActivity(intent);
					}

				} else if(current_pos == 10) {

					String loginState = sharePreferenceClass
							.getValue_string(otherStaticValue.loginState);
					if(loginState.equals("yes")){
						Intent intent=new Intent(getContext(),SafelifeReportPost.class);
						intent.putExtra("issuetype", "11");
						intent.putExtra("issutypename", "Good example");
						intent.putExtra("language", language);
						getContext().startActivity(intent);

					}else{
						Intent intent=new Intent(getContext(),LoginActivity.class);
						getContext().startActivity(intent);
					}

				} else if(current_pos == 11) {

					String loginState = sharePreferenceClass
							.getValue_string(otherStaticValue.loginState);
					if(loginState.equals("yes")){
						Intent intent=new Intent(getContext(),SafelifeReportPost.class);
						intent.putExtra("issuetype", "12");
						intent.putExtra("issutypename", "Improvement needed");
						intent.putExtra("language", language);
						getContext().startActivity(intent);

					}else{
						Intent intent=new Intent(getContext(),LoginActivity.class);
						getContext().startActivity(intent);
					}

				} else if(current_pos == 12) {

					String loginState = sharePreferenceClass
							.getValue_string(otherStaticValue.loginState);
					if(loginState.equals("yes")){
						Intent intent=new Intent(getContext(),SafelifeReportPost.class);
						intent.putExtra("issuetype", "13");
						intent.putExtra("issutypename", "Train station");
						intent.putExtra("language", language);
						getContext().startActivity(intent);

					}else{
						Intent intent=new Intent(getContext(),LoginActivity.class);
						getContext().startActivity(intent);
					}

				} else if(current_pos == 13) {

					String loginState = sharePreferenceClass
							.getValue_string(otherStaticValue.loginState);
					if(loginState.equals("yes")){
						Intent intent=new Intent(getContext(),SafelifeReportPost.class);
						intent.putExtra("issuetype", "14");
						intent.putExtra("issutypename", "Airport");
						intent.putExtra("language", language);
						getContext().startActivity(intent);

					}else{
						Intent intent=new Intent(getContext(),LoginActivity.class);
						getContext().startActivity(intent);
					}

				} else if(current_pos == 14) {

					String loginState = sharePreferenceClass
							.getValue_string(otherStaticValue.loginState);
					if(loginState.equals("yes")){
						Intent intent=new Intent(getContext(),SafelifeReportPost.class);
						intent.putExtra("issuetype", "15");
						intent.putExtra("issutypename", "Others");
						intent.putExtra("language", language);
						getContext().startActivity(intent);

					}else{
						Intent intent=new Intent(getContext(),LoginActivity.class);
						getContext().startActivity(intent);
					}

				}




			}
		});


	}

	@Override
	protected void onStop()
	{
		// TODO Auto-generated method stub

		super.onStop();
	}

	public void CustoDialoInitialize()
	{

	}

	public void closeMenudialog()
	{
		dismiss();
	}

	public class ReportAdaptore extends BaseAdapter
	{

		private Context reportcontext;
		private final String[] reportitemName;

		public ReportAdaptore(Context context, String[] reportName)
		{
			this.reportcontext = context;
			this.reportitemName = reportName;

		}

		@Override
		public int getCount()
		{
			// TODO Auto-generated method stub
			return reportitemName.length;
		}

		@Override
		public Object getItem(int position)
		{
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position)
		{
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent)
		{
			// TODO Auto-generated method stub

			LayoutInflater layoutInflater = (LayoutInflater) reportcontext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View gridViewElement = null;

			// gridViewElement = new View(menucontext);

//			convertView = layoutInflater.inflate(R.layout.gridview_row
//					null);
			convertView = layoutInflater.inflate(R.layout.gridview_row,
					parent,false);
			RelativeLayout rrrr = (RelativeLayout) convertView
					.findViewById(R.id.rrrr);
			TextView menuelementName = (TextView) convertView
					.findViewById(R.id.gridviewelement_text);
			ImageView menuelementImage = (ImageView) convertView
					.findViewById(R.id.gridviewelement_image);
			menuelementName.setText(reportitemName[position]);

			String imageLogo = reportitemName[position];
			if(position == 0)
			{
				menuelementImage.setImageResource(R.drawable.sidewalk_obstacle_icon);
			}
			else if(position == 1)
			{
				menuelementImage.setImageResource(R.drawable.road_sign_high_icon);
			}
			else if(position == 2)
			{
				menuelementImage.setImageResource(R.drawable.sidewalk_large_icon);
			}
			else if(position == 3)
			{
				menuelementImage.setImageResource(R.drawable.pavement_covering_icon);
			}
			else if(position == 4)
			{
				menuelementImage.setImageResource(R.drawable.lamppost);
			}
			else if(position == 5)
			{
				menuelementImage.setImageResource(R.drawable.street_equipment_icon);
			}
			else if(position == 6)
			{
				menuelementImage.setImageResource(R.drawable.guter_icon);
			}
			else if(position == 7)
			{
				menuelementImage.setImageResource(R.drawable.crossfall_icon);
			}
			else if(position == 8)
			{
				menuelementImage.setImageResource(R.drawable.lighting_icon);
			}
			else if(position == 9)
			{
				menuelementImage.setImageResource(R.drawable.tree_icon);
			}
			else if(position == 10)
			{
				menuelementImage.setImageResource(R.drawable.good_example_icon);
			}
			else if(position == 11)
			{
				menuelementImage.setImageResource(R.drawable.improvement_needed_icon);
			}
			else if(position == 12)
			{
				menuelementImage.setImageResource(R.drawable.train_station_icon);
			}
			else if(position == 13)
			{
				menuelementImage.setImageResource(R.drawable.airport_icon);
			}
			else if(position == 14)
			{
				menuelementImage.setImageResource(R.drawable.other_icon);
			}

			return convertView;
		}

	}

	@Override
	public void onClick(View v)
	{
		// TODO Auto-generated method stub
		switch (v.getId())
		{
		case R.id.report_dialogbtn:
			Log.v("menu_dialogbtn", "menu_dialogbtn");
			//Toast.makeText(menuContext, "menu_dialogbtn", Toast.LENGTH_SHORT).show();

			break;

		case R.id.report_dialog_crossbtn:
			Log.v("dialog_crossbtn", "menu_dialogbtn");
			//Toast.makeText(menuContext, "dialog_crossbtn", Toast.LENGTH_SHORT).show();
			dismiss();
			break;
		}
	}
	public interface MyDialogFragmentListener
	{
		public void onReturnValue(String foo);
	}
}
