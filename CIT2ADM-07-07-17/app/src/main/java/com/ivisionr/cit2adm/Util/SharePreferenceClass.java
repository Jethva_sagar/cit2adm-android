package com.ivisionr.cit2adm.Util;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;

public class SharePreferenceClass{
	private static final String USER_PREFS = "SafelifeApplication";
	private SharedPreferences appSharedPrefs;
	private SharedPreferences.Editor prefsEditor;
	private String userid;
	private String username;
	private String maplatitude;
	private String maplongitude;

	private String  reportImage;
	private String reportDescription;
	private String reportlat;
	private String reportlon;
	private String reportissuetypename;
	private String reportissuetypeid;
	private String reportimagename;
	private String lat;
	private String lon;
	/*********** get shared preferences *****************/
	public static String getPreferences(Context con, String key) 
	{
		SharedPreferences sharedPreferences = con.getSharedPreferences(
				"CIT2ADM_pref", 0);
		String value = sharedPreferences.getString(key, "0");
		return value;

	}
	
	/*********** set shared preferences *****************/
	public static void setPreferences(Context con, String key, String value)
	{
		// save the data
		SharedPreferences preferences = con.getSharedPreferences(
				"CIT2ADM_pref", 0);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString(key, value);
		editor.commit();
	}
	public SharePreferenceClass(Context context){
		this.appSharedPrefs = context.getSharedPreferences(USER_PREFS, Activity.MODE_PRIVATE);
		this.prefsEditor = appSharedPrefs.edit();
	}
	
	public void saveDeviceId(String deviceid){
		prefsEditor.putString("deviceid", deviceid);
		prefsEditor.commit();
	}
	public String getDeviceId(){
		return appSharedPrefs.getString("deviceid", "");
	}

	public void saveMembershipExpiry(String expiry){
		prefsEditor.putString("membershipexpiry", expiry);
		prefsEditor.commit();
	}
	public String getMembershipExpiry(){
		return appSharedPrefs.getString("membershipexpiry", "");
	}
	
	public void savestatus(String status){
		prefsEditor.putString("status", status);
		prefsEditor.commit();
	}
	public String getstatus(){
		return appSharedPrefs.getString("status", "");
	}

	public void savecurrentlocationlat(String lat){
		prefsEditor.putString("lat", lat);
		prefsEditor.commit();
	}
	public String getcurrentlocationlat(){
		return appSharedPrefs.getString("lat", "");
	}
	
	
	
	public void saveserchplacelat(String lat){
		prefsEditor.putString("lat", lat);
		prefsEditor.commit();
	}
	public String getsaveserchplacelat(){
		return appSharedPrefs.getString("lat", "");
	}
	
	public void saveserchplacelon(String lat){
		prefsEditor.putString("lat", lat);
		prefsEditor.commit();
	}
	public String getsaveserchplacelon(){
		return appSharedPrefs.getString("lat", "");
	}
	public void savecurrentlocationlon(String lon){
		prefsEditor.putString("lon", lon);
		prefsEditor.commit();
	}
	public String getcurrentlocationlon(){
		return appSharedPrefs.getString("lon", "");
	}

	public String getLat() {
		return appSharedPrefs.getString("lat", "");
	}
	public void setLat(String lat) {
		prefsEditor.putString("lat", lat);
		prefsEditor.commit();
	}
	public String getLon() {
		return appSharedPrefs.getString("lon", "");
	}
	public void setLon(String lon) {
		prefsEditor.putString("lon", lon);
		prefsEditor.commit();
	}
	public String getReportimagename() {
		return appSharedPrefs.getString("reportimagename", "");
	}
	public void setReportimagename(String reportimagename) {
		prefsEditor.putString("reportimagename", reportimagename);
		prefsEditor.commit();
	}
	public String getReportissuetypeid() {
		return appSharedPrefs.getString("issuetypeid", "");
	}
	public void setReportissuetypeid(String reportissuetypeid) {
		prefsEditor.putString("issuetypeid", reportissuetypeid);
		prefsEditor.commit();
	}
	public String getMaplatitude() {
		return appSharedPrefs.getString("maplatitude", "");
	}
	public void setMaplatitude(String maplatitude) {
		prefsEditor.putString("maplatitude", "");
		prefsEditor.commit();
	}
	public void setCurrentlocationLat(String currentlat){
		prefsEditor.putString("currentlat", currentlat);
		prefsEditor.commit();
	}
	public String getCurrentlocationLat(){
		return appSharedPrefs.getString("currentlat", "");
	}
	public void setUpadelocationLat(String currentlat){
		prefsEditor.putString("updatelat", currentlat);
		prefsEditor.commit();
	}
	public String getUpadelocationLat(){
		return appSharedPrefs.getString("updatelat", "");
	}
	public void setUpadelocationLon(String currentlat){
		prefsEditor.putString("updatelon", currentlat);
		prefsEditor.commit();
	}
	public String getUpadelocationLon(){
		return appSharedPrefs.getString("updatelon", "");
	}
	public void setCurrentlocationLon(String currentlon){
		prefsEditor.putString("currentlon", currentlon);
		prefsEditor.commit();
	}
	public String getCurrentlocationLon(){
		return appSharedPrefs.getString("currentlon", "");
	}
	
	public String getReportImage() {
		return appSharedPrefs.getString("reportimage", "");
	}
	public void setReportImage(String reportImage) {
		prefsEditor.putString("reportimage", reportImage);
		prefsEditor.commit();
	}
	public String getReportDescription() {
		return appSharedPrefs.getString("reportdescription", "");
	}
	public void setReportDescription(String reportDescription) {
		prefsEditor.putString("reportdescription", reportDescription);
		prefsEditor.commit();
	}
	public String getReportlat() {
		return appSharedPrefs.getString("reportlat", "");
	}
	public void setReportlat(String reportlat) {
		prefsEditor.putString("reportlat", reportlat);
		prefsEditor.commit();
	}
	public String getReportlon() {
		return appSharedPrefs.getString("reportlon","");
	}
	public void setReportlon(String reportlon) {
		prefsEditor.putString("reportlon", reportlon);
		prefsEditor.commit();
	}
	public String getReportissuetypename() {
		return appSharedPrefs.getString("reportissuetypename", "");
	}
	public void setReportissuetypename(String reportissuetypename) {
		prefsEditor.putString("reportissuetypename", reportissuetypename);
		prefsEditor.commit();
	}
	public String getMaplongitude() {
		return appSharedPrefs.getString("maplongitude", "");
	}
	public void setMaplongitude(String maplongitude) {
		prefsEditor.putString("maplongitude", "");
		prefsEditor.commit();
	}
	public String getUsername() {
		return appSharedPrefs.getString("username", "");
	}
	public void setUsername(String username) {
		prefsEditor.putString("usernmae", username);
		prefsEditor.commit();
	}
	public String getUserid() {
		return appSharedPrefs.getString("userid", "");
	}
	public void setUserid(String userid) {
		prefsEditor.putString("userid", userid);
		prefsEditor.commit();
	}
	public void setPromoExpiryDate(String expiry_date) {
		prefsEditor.putString("promo_expiry_date", expiry_date);
		prefsEditor.commit();
	}
	public String getPromoExpiryDate() {
		return appSharedPrefs.getString("promo_expiry_date", "");
	}
	public int getValue_int(String intKeyValue) {
		return appSharedPrefs.getInt(intKeyValue, 0);
	}


	public String getValue_string(String stringKeyValue) {
		return appSharedPrefs.getString(stringKeyValue, "no");
	}
	public void setValue_int12(String intKeyValue,int _intValue) {

		prefsEditor.putInt(intKeyValue, _intValue).commit();
	}
	public void setValue_string( String stringKeyValue,String _stringValue) {
		if(_stringValue.trim().length()>0){
			prefsEditor.putString(stringKeyValue, _stringValue).commit();
		}
	}
	public void setValue_int(String intKeyValue) {

		prefsEditor.putInt(intKeyValue,0).commit();
	}
	public void setValue_string( String stringKeyValue) {
		if(stringKeyValue.trim().length()>0){
			prefsEditor.putString(stringKeyValue,"no").commit();
		}
	}

}