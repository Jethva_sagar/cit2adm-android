package com.ivisionr.cit2adm.async;

import java.io.IOException;

import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.ivisionr.cit2adm.Util.Webservicelink;
import com.ivisionr.cit2adm.interfaces.Postcommentsinterfaces;



import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class PostCommentAsync  extends AsyncTask<String , Void, String>{



	private Context _context;
	private Activity actity;
	public Postcommentsinterfaces postcommentinterfaces;
	private String page="no";
	private String status="no";

	public PostCommentAsync(Activity activity){
		this.actity=actity;
		this._context=actity;
	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub
		try {
			postcomments(params);
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	private void postcomments(String[] params) throws JSONException, ClientProtocolException, IOException {

		String useid = params[0];
		String reportid=params[1];
		String comments=params[2];
		String result="";		
		String url = Webservicelink.postComments;
		JSONObject jsonObject=new JSONObject();
		jsonObject.put("user_id", useid);
		jsonObject.put("issue_id", reportid);
		jsonObject.put("comment", comments);


		List<NameValuePair> namevaluepair = new ArrayList<NameValuePair>();
		namevaluepair.add(new BasicNameValuePair("data", jsonObject
				.toString()));
		ResponseHandler<String> resHandler = new BasicResponseHandler();
		String paramString = URLEncodedUtils.format(namevaluepair, "utf-8");
		url += paramString;	
		Log.d("checkurlcomment", "checkurlcomment" +url);
		DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(url);
		page = httpClient.execute(httpGet, resHandler);	
		Log.d("checkpageff", "checkpageff" +page);
		JSONObject jsobj = new JSONObject(page.toString());
		if (jsobj.has("error_code"))
		{
			status = jsobj.getString("error_code");	
			Log.d("valuechecknow", "valuecheck" +status);


		}	


	}
	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		if(status.equals("S21400") || status.equals("S21407")){
			postcommentinterfaces.onpostcommentcompleted();		
		}else if(status.equals("S21404")){
			postcommentinterfaces.onPostCommentlength();
		}else{
			postcommentinterfaces.onpostcommentcompletednodata();
		}


	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		postcommentinterfaces.onpostcommentstarted();
	}


}
