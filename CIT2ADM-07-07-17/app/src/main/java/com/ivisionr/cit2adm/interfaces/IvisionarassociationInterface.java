package com.ivisionr.cit2adm.interfaces;

import java.util.List;

import com.ivisionr.cit2adm.bin.Association;


public interface IvisionarassociationInterface {
	public void onIvisionarassociationstarted();
	public void onIvisionarasationNoData();
	public void onIvisionarassociationCompleted(List<Association> ivisionarassocition);
	

}
