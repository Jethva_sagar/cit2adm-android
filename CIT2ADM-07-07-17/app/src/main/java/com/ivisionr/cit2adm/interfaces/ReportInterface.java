package com.ivisionr.cit2adm.interfaces;

public interface ReportInterface {
	
	public void onReportpostastarted();
	public void onReportpostcompleted();
	public void onReportnetworkissue();

}
