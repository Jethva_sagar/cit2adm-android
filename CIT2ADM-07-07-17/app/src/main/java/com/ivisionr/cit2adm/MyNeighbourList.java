package com.ivisionr.cit2adm;

import java.util.ArrayList;
import java.util.List;

import com.ivisionr.cit2adm.Util.Utils;
import com.ivisionr.cit2adm.adapter.MyNeighbourlistAdapter;
import com.ivisionr.cit2adm.async.MyNeighbouringAsync;
import com.ivisionr.cit2adm.bin.MyNeighbourDetails;
import com.ivisionr.cit2adm.interfaces.MyNeighbour;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class MyNeighbourList extends Activity implements MyNeighbour {

	private String countryname;
	private String lat, lon;

	private ProgressDialog mProgressDialog;
	private List<MyNeighbourDetails> myneighbourdetails;
	private ListView list;
	private Button back;
	private String rad = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.myneighbourmainlist);
		mProgressDialog = new ProgressDialog(MyNeighbourList.this);
		myneighbourdetails = new ArrayList<MyNeighbourDetails>();
		list = (ListView) findViewById(R.id.list);
		back = (Button) findViewById(R.id.myneighbourback);

		Bundle bundle = getIntent().getExtras();

		if (getIntent().hasExtra("country")) {
			countryname = bundle.getString("country");
		}
		if (getIntent().hasExtra("currentlat")) {
			lat = bundle.getString("currentlat");

		}
		if (getIntent().hasExtra("currentlon")) {
			lon = bundle.getString("currentlon");
		}
		if (getIntent().hasExtra("radiusforselection")) {
			rad = bundle.getString("radiusforselection");
		}
		mProgressDialog.setTitle(getResources().getString(R.string.usersnearby));
		mProgressDialog.setMessage(getResources().getString(R.string.loading));

		/*Log.d("country", "countryname" + countryname);
		Log.d("currlat", "currlatname" + lat);
		Log.d("currentlong", "currentlongname" + lon);
		Log.d("cu", "cu" + rad);*/

		if (Utils.checkConnectivity(MyNeighbourList.this)) {
			MyNeighbouringAsync mtneighbouring = new MyNeighbouringAsync(
					MyNeighbourList.this, countryname, lat, lon, rad);
			mtneighbouring.myneighbouringinterface = this;
			mtneighbouring.execute("");
		} else {
			showNetworkDialog("internet");
		}

		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {

				String id = myneighbourdetails.get(arg2).getId();
				Log.d("DEBUG", "My Neibourhood Item Id: " + id);
				Intent intent = new Intent(MyNeighbourList.this,
						GetUserDetails.class);
				intent.putExtra("id", id);
				startActivity(intent);

			}
		});
		back.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				finish();

			}
		});

	}

	@SuppressWarnings("deprecation")
	public void showNetworkDialog(final String message) {
		// final exit of application
		AlertDialog.Builder builder = new AlertDialog.Builder(
				MyNeighbourList.this);
		builder.setTitle(getResources().getString(R.string.app_name));
		if (message.equals("gps")) {
			builder.setMessage(getResources().getString(R.string.gps_message));
		} else if (message.equals("internet")) {
			builder.setMessage(getResources().getString(R.string.net_message));
		}
		AlertDialog alert = builder.create();
		alert.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				if (message.equals("gps")) {
					Intent i = new Intent(
							android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
					startActivityForResult(i, 1);
				} else if (message.equals("internet")) {
					Intent i = new Intent(
							android.provider.Settings.ACTION_WIRELESS_SETTINGS);
					startActivityForResult(i, 2);
					Intent i1 = new Intent(
							android.provider.Settings.ACTION_WIFI_SETTINGS);
					startActivityForResult(i1, 3);
				} else {
					// do nothing
				}
			}
		});
		// Showing Alert Message
		alert.show();
	}

	@Override
	public void onMyNeighbourStarted() {
		if (null != mProgressDialog) {
			mProgressDialog.show();
		}

	}

	@Override
	public void onMyNeighbourCompleted(List<MyNeighbourDetails> list) {

		myneighbourdetails.addAll(list);
		bindList(myneighbourdetails);
		mProgressDialog.dismiss();

	}

	private void bindList(List<MyNeighbourDetails> myneighbourdetails2) {

		MyNeighbourlistAdapter myneighbourlist = new MyNeighbourlistAdapter(
				this, myneighbourdetails);
		list.setAdapter(myneighbourlist);

	}

	@Override
	public void onMyNeighbouringNodata() {

		mProgressDialog.dismiss();
		Toast.makeText(getApplicationContext(), R.string.noneighbour,
				Toast.LENGTH_LONG).show();

	}

}
