package com.ivisionr.cit2adm.interfaces;

import java.util.List;

import com.ivisionr.cit2adm.bin.GetAllIssueDetails;


public interface GetallIssueInterface {
	public void onGetallIssueStarted();
	public void onGetallIssuenoData();
	public void onGetallIssueCompleted(List<GetAllIssueDetails> list);

}
