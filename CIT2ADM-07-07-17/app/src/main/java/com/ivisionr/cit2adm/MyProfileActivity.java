package com.ivisionr.cit2adm;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.ivisionr.cit2adm.Util.SharePreferenceClass;
import com.ivisionr.cit2adm.Util.Utils;
import com.ivisionr.cit2adm.async.GetProfileAsync;
import com.ivisionr.cit2adm.bin.MyProfileData;
import com.ivisionr.cit2adm.bin.User;
import com.ivisionr.cit2adm.database.DatabaseHandler;
import com.ivisionr.cit2adm.interfaces.MyProfileInterface;

public class MyProfileActivity extends ActivityExceptionDemo implements
		MyProfileInterface {

	Button btnBackToMap, btnEditProfile;
	SharePreferenceClass sharePreferenceClass;
	DatabaseHandler db;
	ArrayList<HashMap<String, String>> userRecord;
	HashMap<String, String> hmGetRecord;
	TextView tvFirstName, tvLastName, tvUserName, tvEmailId, tvCity, tvZip,
			tvDor, tvExpriry, tvAcitve, tvPaid, tvUserStatus, tvCountry,
			tcAccessId, myprofile_date_of_registration_tv, myprofile_expriy_tv,
			myprofile_accessID_tv;
	Spinner spCountry;
	String[] country, userStatus;
	TextView location;
	private Button backbutton;
	private String userId = "";
	private ProgressDialog mprogressDialog;
	private MyProfileData getuserdetailsbin;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.myprofile_screen);
		sharePreferenceClass = new SharePreferenceClass(getApplicationContext());
		mprogressDialog = new ProgressDialog(this);
		mprogressDialog.setMessage(getResources().getText(R.string.loading));
		mprogressDialog.setCancelable(false);
		getuserdetailsbin = new MyProfileData();

		connectToXml();
		db = new DatabaseHandler(this);
		SharePreferenceClass spc = new SharePreferenceClass(
				getApplicationContext());
		userId = spc.getUserid();
		Log.d("DEBUG", "MyProfileActivity - User Id: " + userId);
		
		
		
		User myUser = (User) db.getUser(userId);

		userStatus = getResources().getStringArray(R.array.myuserstatus);
		country = getResources().getStringArray(R.array.countryname);

		tvEmailId.setText(myUser.getEmail());
		tvFirstName.setText(myUser.getFirst_name());
		tvLastName.setText(myUser.getLast_name());
		if (myUser.getActive() != null && myUser.getActive().equals("0"))
			tvAcitve.setText(this.getString(R.string.Not_Acitivate));
		else
			tvAcitve.setText(this.getString(R.string.Acitivate));
		myprofile_date_of_registration_tv.setText(myUser.getCreated());
		
		/**
		 * Add by hiren
		 * Change date format
		 */
		try {
			//2016-05-24 20:26:25
			if(myUser.getMembership_expiry_date() != null && myUser.getMembership_expiry_date().length() != 0) {
				SimpleDateFormat inputDateFormate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yy hh:mm:ss");
				Date date = inputDateFormate.parse(myUser.getMembership_expiry_date());
				myprofile_expriy_tv.setText(dateFormatter.format(date));//myUser.getMembership_expiry_date());
			} else {
				myprofile_expriy_tv.setText("");
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		Log.d("DEBUG", "Access Id: " + myUser.getAccess_id());

		myprofile_accessID_tv.setText(myUser.getAccess_id());
		tvDor.setText(myUser.getCreated());
		tvCity.setText(myUser.getCity());

		if (myUser.getUser_type_id() != null) {
			if (myUser.getUser_type_id().equals("1"))
				tvUserStatus.setText(this.getString(R.string.Non_subscriber));
			else if (myUser.getUser_type_id().equals("2"))
				tvUserStatus.setText(this.getString(R.string.Bronze));
			else if (myUser.getUser_type_id().equals("3"))
				tvUserStatus.setText(this.getString(R.string.Silver));
			else if (myUser.getUser_type_id().equals("4"))
				tvUserStatus.setText(this.getString(R.string.Gold));
		}
		tvPaid.setText(myUser.getPaid());
		
		tvUserName.setText(myUser.getUsername());

		tvCountry.setText(myUser.getCountry());

		try {

			SimpleDateFormat dateFormat1 = new SimpleDateFormat(
					"yyyy-MM-dd HH:mm:ss");
			SimpleDateFormat dateFormat2 = new SimpleDateFormat(
					"dd/MM/yyyy HH:mm:ss");
			String membershipExpiry = myUser.getMembership_expiry_date();

			Date expDate = dateFormat1.parse(membershipExpiry);

			// pdate = new Date();
			String membershipExpiry2 = dateFormat2.format(expDate);
			Log.d("DEBUG", "Membership Exp. Format 1: " + membershipExpiry);
			Log.d("DEBUG", "Membership Exp. Format 2: " + membershipExpiry2);
			Log.d("DEBUG", "Access Id: " + myUser.getAccess_id());

			//tvExpriry.setText(membershipExpiry2);

		} catch (Exception ex) {
			Log.d("DEBUG", "Expection Handled at 1");
		}

		// tcAccessId.setText(myUser.getAccess_id());

		// tvExpriry.setText(myUser.getMembership_expiry_date());
		tvZip.setText(myUser.getZipcode());
		Log.d("DEBUG",
				"My Profile Lat: "
						+ new SharePreferenceClass(MyProfileActivity.this)
								.getCurrentlocationLat());

		/*
		 * if(new
		 * SharePreferenceClass(MyProfileActivity.this).getUpadelocationLat
		 * ().equals(new
		 * SharePreferenceClass(MyProfileActivity.this).getCurrentlocationLat())
		 * && new
		 * SharePreferenceClass(MyProfileActivity.this).getCurrentlocationLon
		 * ().equals(new
		 * SharePreferenceClass(MyProfileActivity.this).getUpadelocationLon())){
		 * location.setText(new
		 * SharePreferenceClass(MyProfileActivity.this).getCurrentlocationLat
		 * ()+","+new
		 * SharePreferenceClass(MyProfileActivity.this).getCurrentlocationLon
		 * ()); } else { location.setText(new
		 * SharePreferenceClass(MyProfileActivity
		 * .this).getUpadelocationLat()+","+new
		 * SharePreferenceClass(MyProfileActivity.this).getUpadelocationLon());
		 * }
		 */
		if (Utils.checkConnectivity(MyProfileActivity.this)) {
			GetProfileAsync getuserdetails = new GetProfileAsync(
					MyProfileActivity.this);
			getuserdetails.getuserdetailsinterfaces = this;
			getuserdetails.execute(myUser.getId());
		} else {
			showNetworkDialog("internet");
		}
		String loca = "";

		if (myUser.getCity() != null && myUser.getCity() != "")
			loca = myUser.getCity() + ", ";
		if (myUser.getCountry() != null && myUser.getCountry() != "")
			loca += myUser.getCountry();

		location.setText(loca);

		btnBackToMap.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

				finish();

			}
		});

		btnEditProfile.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(),EditProfile.class);
				startActivity(intent);
				finish();

			}
		});

	}

	public void connectToXml() {
		btnEditProfile = (Button) findViewById(R.id.btn_edit_myprofile);
		btnBackToMap = (Button) findViewById(R.id.myprofile_back_btn);
		tvFirstName = (TextView) findViewById(R.id.myprofile_fname_tv);
		tvLastName = (TextView) findViewById(R.id.myprofile_lname_tv);
		tvUserName = (TextView) findViewById(R.id.myprofile_user_tv);
		tvEmailId = (TextView) findViewById(R.id.myprofile_email_tv);
		tvCity = (TextView) findViewById(R.id.myprofile_city_tv);
		tvZip = (TextView) findViewById(R.id.myprofile_zipe_tv);
		tvExpriry = (TextView) findViewById(R.id.myprofile_expriy_tv);
		tvAcitve = (TextView) findViewById(R.id.myprofile_activation_tv);
		tvDor = (TextView) findViewById(R.id.myprofile_date_of_registration_tv);
		tvPaid = (TextView) findViewById(R.id.myprofile_paid_tv);
		tvCountry = (TextView) findViewById(R.id.myprofile_country_tv);
		tvUserStatus = (TextView) findViewById(R.id.myprofile_status_tv);
		location = (TextView) findViewById(R.id.locationprofile);
		tcAccessId = (TextView) findViewById(R.id.myprofile_access_id);
		myprofile_date_of_registration_tv = (TextView) findViewById(R.id.myprofile_date_of_registration_tv);
		myprofile_expriy_tv = (TextView) findViewById(R.id.myprofile_expriy_tv);
		myprofile_accessID_tv = (TextView) findViewById(R.id.myprofile_accessID_tv);

	}

	@SuppressWarnings("deprecation")
	public void showNetworkDialog(final String message) {
		// final exit of application
		AlertDialog.Builder builder = new AlertDialog.Builder(
				MyProfileActivity.this);
		builder.setTitle(getResources().getString(R.string.app_name));
		if (message.equals("gps")) {
			builder.setMessage(getResources().getString(R.string.gps_message));
		} else if (message.equals("internet")) {
			builder.setMessage(getResources().getString(R.string.net_message));
		}
		AlertDialog alert = builder.create();
		alert.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				if (message.equals("gps")) {
					Intent i = new Intent(
							android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
					startActivityForResult(i, 1);
				} else if (message.equals("internet")) {
					Intent i = new Intent(
							android.provider.Settings.ACTION_WIRELESS_SETTINGS);
					startActivityForResult(i, 2);
					Intent i1 = new Intent(
							android.provider.Settings.ACTION_WIFI_SETTINGS);
					startActivityForResult(i1, 3);
				} else {
					// do nothing
				}
			}
		});
		// Showing Alert Message
		alert.show();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
		Intent intent = new Intent(this, GoomapActivity.class);
		startActivity(intent);
		finish();
	}

	@Override
	public void ongetmyprofilestartrd() {
		// TODO Auto-generated method stub
		if (null != mprogressDialog) {
			mprogressDialog.show();
		}
	}

	@Override
	public void ongetmyprofilecompleted(MyProfileData list) {
		// TODO Auto-generated method stub
		getuserdetailsbin = list;
		if (null != mprogressDialog) {
			mprogressDialog.dismiss();
		}
		tvEmailId.setText(getuserdetailsbin.getEmail());
		tvFirstName.setText(getuserdetailsbin.getFirst_name());
		tvLastName.setText(getuserdetailsbin.getLast_name());
		if (getuserdetailsbin.getActive() != null && getuserdetailsbin.getActive().equals("0"))
			tvAcitve.setText(this.getString(R.string.Acitivate));
		else
			tvAcitve.setText(this.getString(R.string.Not_Acitivate));
		myprofile_date_of_registration_tv.setText(getuserdetailsbin.getCreated());
		Log.d("DIBS", "Expiry Date: "+getuserdetailsbin.getMembership_expiry_date());
		Log.d("DIBS", "Access Id: "+getuserdetailsbin.getSecurity_code());
		//myprofile_expriy_tv.setText(getuserdetailsbin.getMembership_expiry_date());
		myprofile_accessID_tv.setText(getuserdetailsbin.getSecurity_code());
		tvDor.setText(getuserdetailsbin.getCreated());
		tvCity.setText(getuserdetailsbin.getCity());

		if (getuserdetailsbin.getUser_type_id() != null) {
			if (getuserdetailsbin.getUser_type_id().equals("1"))
				tvUserStatus.setText(this.getString(R.string.Non_subscriber));
		else if (getuserdetailsbin.getUser_type_id().equals("2"))
			tvUserStatus.setText(this.getString(R.string.Bronze));
		else if (getuserdetailsbin.getUser_type_id().equals("3"))
			tvUserStatus.setText(this.getString(R.string.Silver));
		else if (getuserdetailsbin.getUser_type_id().equals("4"))
			tvUserStatus.setText(this.getString(R.string.Gold));
		}
		tvPaid.setText(getuserdetailsbin.getPaid());
		// {"error_code":"S20400"}
		//
		tvUserName.setText(getuserdetailsbin.getUsername());

		tvCountry.setText(getuserdetailsbin.getCountry());

		try {

			SimpleDateFormat dateFormat1 = new SimpleDateFormat(
					"yyyy-MM-dd HH:mm:ss");
			SimpleDateFormat dateFormat2 = new SimpleDateFormat(
					"dd/MM/yyyy HH:mm:ss");
			String membershipExpiry = getuserdetailsbin.getMembership_expiry_date();

			Date expDate = dateFormat1.parse(membershipExpiry);

			// pdate = new Date();
			String membershipExpiry2 = dateFormat2.format(expDate);
			Log.d("DEBUG", "Membership Exp. Format 1: " + membershipExpiry);
			Log.d("DEBUG", "Membership Exp. Format 2: " + membershipExpiry2);

			myprofile_expriy_tv.setText(membershipExpiry2);

		} catch (Exception ex) {
			Log.d("DEBUG", "Expection Handled at 1");
		}

		// tcAccessId.setText(myUser.getAccess_id());

		// tvExpriry.setText(myUser.getMembership_expiry_date());
		tvZip.setText(getuserdetailsbin.getZip());
		Log.d("DEBUG",
				"My Profile Lat: "
						+ new SharePreferenceClass(MyProfileActivity.this)
								.getCurrentlocationLat());

		/*
		 * if(new
		 * SharePreferenceClass(MyProfileActivity.this).getUpadelocationLat
		 * ().equals(new
		 * SharePreferenceClass(MyProfileActivity.this).getCurrentlocationLat())
		 * && new
		 * SharePreferenceClass(MyProfileActivity.this).getCurrentlocationLon
		 * ().equals(new
		 * SharePreferenceClass(MyProfileActivity.this).getUpadelocationLon())){
		 * location.setText(new
		 * SharePreferenceClass(MyProfileActivity.this).getCurrentlocationLat
		 * ()+","+new
		 * SharePreferenceClass(MyProfileActivity.this).getCurrentlocationLon
		 * ()); } else { location.setText(new
		 * SharePreferenceClass(MyProfileActivity
		 * .this).getUpadelocationLat()+","+new
		 * SharePreferenceClass(MyProfileActivity.this).getUpadelocationLon());
		 * }
		 */
		
		String loca = "";

		if (getuserdetailsbin.getCity() != null && getuserdetailsbin.getCity() != "")
			loca = getuserdetailsbin.getCity() + ", ";
		if (getuserdetailsbin.getCountry() != null && getuserdetailsbin.getCountry() != "")
			loca += getuserdetailsbin.getCountry();

		location.setText(loca);
	}

}
