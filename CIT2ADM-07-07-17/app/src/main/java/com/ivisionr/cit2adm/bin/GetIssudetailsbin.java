package com.ivisionr.cit2adm.bin;

public class GetIssudetailsbin {
	
	
	private String id="";
	private String user_id="";
	private String reportid="";
	private String issutypename="";
	private String username="";
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	private String issuetypeid="";
	private String description="";
	private String lat="";
	private String lon="";
	private String image_1="";
	private String active="";
	private String status="";
	private String allow_vote="";
	private String created="";
	private String num_comments="";
	private String num_rating_plus="";
	private String num_rating_minus="";
	private String firstname="";
	private String lastname="";
	private String rated="";
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getReportid() {
		return reportid;
	}
	public void setReportid(String reportid) {
		this.reportid = reportid;
	}
	public String getIssuetypeid() {
		return issuetypeid;
	}
	public void setIssuetypeid(String issuetypeid) {
		this.issuetypeid = issuetypeid;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getLat() {
		return lat;
	}
	public void setLat(String lat) {
		this.lat = lat;
	}
	public String getLon() {
		return lon;
	}
	public void setLon(String lon) {
		this.lon = lon;
	}
	public String getImage_1() {
		return image_1;
	}
	public void setImage_1(String image_1) {
		this.image_1 = image_1;
	}
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getAllow_vote() {
		return allow_vote;
	}
	public void setAllow_vote(String allow_vote) {
		this.allow_vote = allow_vote;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public String getNum_comments() {
		return num_comments;
	}
	public void setNum_comments(String num_comments) {
		this.num_comments = num_comments;
	}
	public String getNum_rating_plus() {
		return num_rating_plus;
	}
	public void setNum_rating_plus(String num_rating_plus) {
		this.num_rating_plus = num_rating_plus;
	}
	public String getNum_rating_minus() {
		return num_rating_minus;
	}
	public void setNum_rating_minus(String num_rating_minus) {
		this.num_rating_minus = num_rating_minus;
	}
	public String getIssutypename() {
		return issutypename;
	}
	public void setIssutypename(String issutypename) {
		this.issutypename = issutypename;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getRated() {
		return rated;
	}
	public void setRated(String rated) {
		this.rated = rated;
	}
	
	 

}
