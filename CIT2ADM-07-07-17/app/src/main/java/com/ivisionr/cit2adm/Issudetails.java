package com.ivisionr.cit2adm;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import com.ivisionr.cit2adm.Util.ImageLoaderWithoutDimension;
import com.ivisionr.cit2adm.Util.SharePreferenceClass;
import com.ivisionr.cit2adm.Util.Utils;
import com.ivisionr.cit2adm.async.GetIssudetailsAsync;
import com.ivisionr.cit2adm.async.PostCommentAsync;
import com.ivisionr.cit2adm.async.RateIssueAsync;
import com.ivisionr.cit2adm.bin.GetIssudetailsbin;
import com.ivisionr.cit2adm.bin.Issuedetailscomments;
import com.ivisionr.cit2adm.interfaces.GetIssuedetailsInterface;
import com.ivisionr.cit2adm.interfaces.Postcommentsinterfaces;
import com.ivisionr.cit2adm.interfaces.RateIssueInterface;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("ResourceAsColor")
public class Issudetails extends Activity implements GetIssuedetailsInterface,Postcommentsinterfaces,RateIssueInterface,OnClickListener{
	TextView reportid,repotcreateddate,reportdescription,reportstatus,reporttype,prifilename;
	TextView noofcomments,noofpositivecomments,noofnegativecomments,issuecommentstxt,txtstatus;
	private ImageView ratingplus,ratingminus;
	TextView CommentName,commentsdate,comment;

	ImageView issueimage;
	private GetIssudetailsbin getissuedetails;
	private String id;
	private ImageLoaderWithoutDimension imageloder;
	private ProgressDialog mProgressDialog;
	private Button commentssubmit;
	private Dialog dialog;
	private String commentstxt;
	private String userid,issuetypeid;
	private List<Issuedetailscomments> issuedetailscomments;
	private TextView list;
	private Button backbutton;
	private String checkvalue="1";
	private Button share;
	private ImageView mapreport;
	private ImageView isuueimage;


	@SuppressLint("NewApi") @Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.issuedetails);
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	    StrictMode.setThreadPolicy(policy);
		getissuedetails=new GetIssudetailsbin();
		issuedetailscomments=new ArrayList<Issuedetailscomments>();
		imageloder=new ImageLoaderWithoutDimension(Issudetails.this);
		mProgressDialog=new ProgressDialog(Issudetails.this);
		mProgressDialog.setMessage("Loading...");
		getDetails();

		Bundle b=getIntent().getExtras();
		if(getIntent().hasExtra("id")){
			id=b.getString("id");	
			Log.d("checkid", "checkid" +id);

		}

		if(Utils.checkConnectivity(Issudetails.this)){
			GetIssudetailsAsync getissuedetails=new GetIssudetailsAsync(Issudetails.this);
			getissuedetails.issuedetalsinterface=Issudetails.this;
			getissuedetails.execute(id);
		}else{
			showNetworkDialog("internet");
		}
		commentssubmit.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				displayDialog();
			}
		});
		ratingplus.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(getissuedetails.getUser_id().equals(new SharePreferenceClass(Issudetails.this).getUserid())){

					Toast toast=Toast.makeText(Issudetails.this, R.string.cannotrateownissue, Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();

				}else{

					RateIssueAsync rateissueasync=new RateIssueAsync(Issudetails.this);
					rateissueasync.rateissueinterface=Issudetails.this;
					rateissueasync.execute(userid,id,checkvalue);


				}


			}
		});
		ratingminus.setOnClickListener(new View.OnClickListener() {


			@Override
			public void onClick(View v) {
				checkvalue="-1";

				// TODO Auto-generated method stub
				if(getissuedetails.getUser_id().equals(new SharePreferenceClass(Issudetails.this).getUserid())){
					Toast toast=Toast.makeText(Issudetails.this, R.string.cannotrateownissue, Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}
				else{


					RateIssueAsync rateissueasync=new RateIssueAsync(Issudetails.this);
					rateissueasync.rateissueinterface=Issudetails.this;
					rateissueasync.execute(userid,id,checkvalue);


				}

			}
		});
		backbutton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Issudetails.this.finish();
			}
		});
		share.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				
				Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND); 
				sharingIntent.setType("text/plain");
				String shareBody = "http://cit2adm.prologicsoft.net/html/";
				sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "iVisionR");
				sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
				startActivity(Intent.createChooser(sharingIntent, "Share via"));
				//displyShareDialog();

			}
		});
		isuueimage.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				displayDialogforImage();
				
			}
		});


	}

	protected void displayDialogforImage() {
		dialog=new Dialog(Issudetails.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.issueimage);       
		dialog.show();	
		ImageView image=(ImageView)dialog.findViewById(R.id.issueimagedialog);
		imageloder.DisplayImage(getissuedetails.getImage_1(), image);
		
		
	}

	protected void displyShareDialog() {
		dialog=new Dialog(Issudetails.this, R.style.FullHeightDialog);
		dialog.setContentView(R.layout.sharedialog);       
		dialog.show();	

		ImageView crossbutton=(ImageView)dialog.findViewById(R.id.closeshare);
		ImageView facebookbutton =(ImageView)dialog.findViewById(R.id.fbshare);
		ImageView twitterbutton =(ImageView)dialog.findViewById(R.id.twittershare);
		ImageView googleplusbutton=(ImageView)dialog.findViewById(R.id.googleplusshare);
		crossbutton.setOnClickListener(this);
		facebookbutton.setOnClickListener(this);
		twitterbutton.setOnClickListener(this);
		googleplusbutton.setOnClickListener(this);

	}

	@SuppressWarnings("deprecation")
	public void showNetworkDialog(final String message){
		// final exit of application
		AlertDialog.Builder builder = new AlertDialog.Builder(Issudetails.this);
		builder.setTitle(getResources().getString(R.string.app_name));
		if(message.equals("gps")){
			builder.setMessage(getResources().getString(R.string.gps_message));
		}
		else if(message.equals("internet")){
			builder.setMessage(getResources().getString(R.string.net_message));
		}
		AlertDialog alert = builder.create();
		alert.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				if(message.equals("gps")){
					Intent i=new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);		
					startActivityForResult(i, 1);
				}
				else if(message.equals("internet")){
					Intent i=new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS);		
					startActivityForResult(i,2);		
					Intent i1=new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS);		
					startActivityForResult(i1,3);	
				}
				else{
					//do nothing
				}
			}
		});
		// Showing Alert Message
		alert.show();	
	}

	protected void displayDialog() {
		dialog=new Dialog(Issudetails.this, R.style.FullHeightDialog);
		dialog.setContentView(R.layout.range_dialog);       
		dialog.show();	


		final EditText range_input=(EditText)dialog.findViewById(R.id.range_input);	
		Button submit_btn=(Button) dialog.findViewById(R.id.submit_btn);
		Button cancel_btn=(Button) dialog.findViewById(R.id.cancel_btn);



		submit_btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				commentstxt=range_input.getText().toString();
				if(commentstxt.length()>0){						

					if(Utils.checkConnectivity(Issudetails.this)){
						PostCommentAsync postcommentasync=new PostCommentAsync(Issudetails.this);
						postcommentasync.postcommentinterfaces=Issudetails.this;						
						postcommentasync.execute(new SharePreferenceClass(Issudetails.this).getUserid(),id,commentstxt);
					}else{
						showNetworkDialog("internet");
					}


				}

			}
		});

		cancel_btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();

			}
		});



	}

	private void getDetails() {

		issueimage=(ImageView)findViewById(R.id.issuemyrepotimagei);
		reportid=(TextView)findViewById(R.id.issuetextmyreportdetailsi);
		repotcreateddate=(TextView)findViewById(R.id.issuetextmyreportdetailstimei);
		reportdescription=(TextView)findViewById(R.id.issuemyrepotdescriptioni);
		reportstatus=(TextView)findViewById(R.id.issuemyissuestatusi);
		reporttype=(TextView)findViewById(R.id.issuemyreportissuetypei);
		noofcomments=(TextView)findViewById(R.id.issuemysmsvaluei);
		noofpositivecomments=(TextView)findViewById(R.id.issuemylikevaluei);
		noofnegativecomments=(TextView)findViewById(R.id.issuemydislikevaluei);
		commentssubmit=(Button)findViewById(R.id.issuemyimagei);
		ratingplus=(ImageView)findViewById(R.id.issuereporslikei);
		ratingminus=(ImageView)findViewById(R.id.issuereportsdislikei);		
		txtstatus=(TextView)findViewById(R.id.test1);
		list=(TextView)findViewById(R.id.listcommentsis);	
		backbutton=(Button)findViewById(R.id.issuemyreportback);
		share=(Button)findViewById(R.id.issuemyreportshare);
		mapreport=(ImageView)findViewById(R.id.mapreport);
		isuueimage=(ImageView)findViewById(R.id.issuemyrepotimagei);


	}

	@Override
	public void ongetissudetailsStartrd() {
		// TODO Auto-generated method stub
		if(null !=mProgressDialog){
			mProgressDialog.setCancelable(false);
			mProgressDialog.show();
		}

	}

	@Override
	public void ongetissudetailsCompleted(GetIssudetailsbin list) {
		// TODO Auto-generated method stub
		if(null !=mProgressDialog){
			mProgressDialog.dismiss();
		}
		getissuedetails=list;       
		imageloder.DisplayImage(getissuedetails.getImage_1(), issueimage);
		reportid.setText(getissuedetails.getUsername());
		repotcreateddate.setText(getissuedetails.getCreated());
		reportdescription.setText(getissuedetails.getDescription());
		txtstatus.setText(getissuedetails.getStatus().toUpperCase());
		reporttype.setText(getissuedetails.getIssutypename() +" [" +getissuedetails.getReportid()+"]");
		noofcomments.setText(getissuedetails.getNum_comments());
		noofpositivecomments.setText(getissuedetails.getNum_rating_plus());
		noofnegativecomments.setText(getissuedetails.getNum_rating_minus());
		userid=getissuedetails.getUser_id();
		issuetypeid=getissuedetails.getIssuetypeid();
		//mapreport.setImageBitmap(getGoogleMapThumbnail(Double.parseDouble(getissuedetails.getLat()),Double.parseDouble(getissuedetails.getLon())));


	}

	public static Bitmap getGoogleMapThumbnail(double lati, double longi){
        String URL = "http://maps.google.com/maps/api/staticmap?center=" +lati + "," + longi + "&zoom=15&size=600x300&sensor=false";
        Bitmap bmp = null;
        HttpClient httpclient = new DefaultHttpClient();   
        HttpGet request = new HttpGet(URL); 

        InputStream in = null;
        try {
            in = httpclient.execute(request).getEntity().getContent();
            bmp = BitmapFactory.decodeStream(in);
            in.close();
        } catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return bmp;
    }

	@Override
	public void onpostcommentstarted() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onpostcommentcompleted() {		
		dialog.dismiss();
		Toast toast=Toast.makeText(Issudetails.this, R.string.commentpostsuccessfull, Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();
	}

	@Override
	public void onrateIssueStarted() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onrateIssueCompleted() {
		// TODO Auto-generated method stub

		if(checkvalue.equals("1")){
			ratingplus.setBackgroundResource(R.drawable.like_icon_2);
		}else{
			ratingminus.setBackgroundResource(R.drawable.unlike_icon_2);

		}




	}

	@Override
	public void onissuedetailscompleted(List<Issuedetailscomments> issuecomments) {

		issuedetailscomments.addAll(issuecomments);	
		//ListCommentsAdapter listcommentsadpter=new ListCommentsAdapter(this,issuedetailscomments);
		//list.setAdapter(listcommentsadpter);

		String comm="";
		for(int i=0;i<issuedetailscomments.size();i++){

			comm=comm+"\n" +issuedetailscomments.get(i).getComments()+"\n"+"- By "+ issuedetailscomments.get(i).getFirstname() +" "+issuedetailscomments.get(i).getLastname()+" Posted on "+issuedetailscomments.get(i).getCreated()+"\n\n";

		}


		list.setText(comm);



	}

	@Override
	public void onissuedetailsstarted() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onissuenodata() {

		list.setText("No Comments Yet");
	}

	@Override
	public void onpostcommentcompletednodata() {
		// TODO Auto-generated method stub
		dialog.dismiss();
		Toast toast=Toast.makeText(Issudetails.this, R.string.commentposterror, Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();


	}

	@Override
	public void onrateIssueDuel() {
		// TODO Auto-generated method stub
		Toast toast=Toast.makeText(Issudetails.this, R.string.alreadyrated, Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();

	}

	@Override
	public void onPostCommentlength() {
		// TODO Auto-generated method stub
		Toast toast=Toast.makeText(Issudetails.this, R.string.enter25character, Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.closeshare:
			dialog.dismiss();			
			break;

		case R.id.fbshare:
			//startActivity(new Intent(Issudetails.this,FacebookShare.class));
			break;
		case R.id.twittershare:
			startActivity(new Intent(Issudetails.this,TwitterShare.class));
			break;
		case R.id.googleplusshare:
			break;
		}

	}


}
