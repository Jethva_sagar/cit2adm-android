package com.ivisionr.cit2adm.Util;



import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;
 
public class UploadUtil extends Activity {
    
    
    int serverResponseCode = 0;
    DataInputStream inStream = null;
    String path = null;
       
    String upLoadServerUri = "http://cit2adm.prologicsoft.net/webservices/image_submit.php";
    
    /**********  File Path *************/
    
	

	public int uploadFile(String sourceFileUri) {
          
    	  
    	  String fileName = sourceFileUri;
    	  
    	  Log.d("filecheckpath", "filecheckpath" +fileName);
    	  
    	  Log.d("Dibs-7", fileName);
 
          HttpURLConnection conn = null;
          DataOutputStream dos = null;  
          String lineEnd = "\r\n";
          String twoHyphens = "--";
          String boundary = "*****";
          int bytesRead, bytesAvailable, bufferSize;
          byte[] buffer;
          int maxBufferSize = 1 * 1024 * 1024; 
          File sourceFile = new File(sourceFileUri); 
          
          if (!sourceFile.isFile()) {
        	  
	           //dialog.dismiss(); 
	           
	           /*Log.e("uploadFile", "Source File not exist :"
	        		               +uploadFilePath + "" + uploadFileName);*/
	           
	           runOnUiThread(new Runnable() {
	               public void run() {
	            	   /*messageText.setText("Source File not exist :"
	            			   +uploadFilePath + "" + uploadFileName);*/
	               }
	           }); 
	           
	           return 0;
           
          }
          else
          {
	           try { 
	        	   
	            	 // open a URL connection to the Servlet
	               FileInputStream fileInputStream = new FileInputStream(sourceFile);
	               URL url = new URL(upLoadServerUri);
	               
	               // Open a HTTP  connection to  the URL
	               conn = (HttpURLConnection) url.openConnection(); 
	               conn.setDoInput(true); // Allow Inputs
	               conn.setDoOutput(true); // Allow Outputs
	               conn.setUseCaches(false); // Don't use a Cached Copy
	               conn.setRequestMethod("POST");
	               conn.setRequestProperty("Connection", "Keep-Alive");
	               conn.setRequestProperty("ENCTYPE", "multipart/form-data");
	               conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
	               conn.setRequestProperty("uploaded_file", fileName); 
	               
	               dos = new DataOutputStream(conn.getOutputStream());
	     
	               dos.writeBytes(twoHyphens + boundary + lineEnd); 
	               dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\""
	            		                     + fileName + "\"" + lineEnd);
	               
	               dos.writeBytes(lineEnd);
	     
	               // create a buffer of  maximum size
	               bytesAvailable = fileInputStream.available(); 
	     
	               bufferSize = Math.min(bytesAvailable, maxBufferSize);
	               buffer = new byte[bufferSize];
	     
	               // read file and write it into form...
	               bytesRead = fileInputStream.read(buffer, 0, bufferSize);  
	                 
	               while (bytesRead > 0) {
	            	   
	                 dos.write(buffer, 0, bufferSize);
	                 bytesAvailable = fileInputStream.available();
	                 bufferSize = Math.min(bytesAvailable, maxBufferSize);
	                 bytesRead = fileInputStream.read(buffer, 0, bufferSize);   
	                 
	                }
	     
	               // send multipart form data necesssary after file data...
	               dos.writeBytes(lineEnd);
	               dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
	     
	               // Responses from the server (code and message)
	               serverResponseCode = conn.getResponseCode();
	               
	               Log.d("checkcode", "checkcode" +serverResponseCode);
	               String serverResponseMessage = conn.getResponseMessage();
	                
	               Log.i("uploadFile", "HTTP Response is : " 
	            		   + serverResponseMessage + ": " + serverResponseCode);
	               

					/*inStream = new DataInputStream(conn.getInputStream());
					String str;

					while ((str = inStream.readLine()) != null) {
						path = str;
						//Log.e("Debug", "Server Response " + path);

					}
					// Toast.makeText(contractor_report.this, path,
					// Toast.LENGTH_LONG).show();
					// System.out.println(str);

					inStream.close();
					JSONObject tokener = new JSONObject(path);
					Log.d("checkresponse", "checkresponse" +tokener);
					Log.v("tokener", String.valueOf(tokener.length()));

					//sucess = tokener.getString("success");
					/*if (sucess.equalsIgnoreCase("yes")) {
						// "AddChild_screenActivity", token_id, child_name,sex, day,
						// month, year, childimage
						// child_id, child_name, sex, child_image_path, day_child,
						// month_child, year_child

						// db_handler.addChild_details(path_image[4], path_image[3],
						// path_image[5], path_image[6], path_image[7],
						// path_image[8], path_image[9]);
						serverconnectioc_for_updateChile_localdatabase(token_id);

					}*/

				
	               
	               
	               if(serverResponseCode == 200){
	            	   
	                   runOnUiThread(new Runnable() {
	                        public void run() {
	                        	
	                        	//String msg = "File Upload Completed.\n\n See uploaded file here : \n\n"
	                        		       //   +" http://cit2adm.prologicsoft.net/webservices/images.php"
	                        		       //   ;
	                        	
	                        	//messageText.setText(msg);
	                            /*Toast.makeText(UploadUtil.this, "File Upload Complete.", 
	                            		     Toast.LENGTH_SHORT).show();*/
	                        }
	                    });                
	               }    
	               
	               //close the streams //
	               fileInputStream.close();
	               dos.flush();
	               dos.close();
	                
	          } catch (MalformedURLException ex) {
	        	  
	              //dialog.dismiss();  
	              ex.printStackTrace();
	              
	              runOnUiThread(new Runnable() {
	                  public void run() {
	                	  //messageText.setText("MalformedURLException Exception : check script url.");
	                      //Toast.makeText(UploadUtil.this, "MalformedURLException", Toast.LENGTH_SHORT).show();
	                  }
	              });
	              
	              Log.e("Upload file to server", "error: " + ex.getMessage(), ex);  
	          } catch (Exception e) {
	        	  
	              //dialog.dismiss();  
	              e.printStackTrace();
	              
	              runOnUiThread(new Runnable() {
	                  public void run() {
	                	  //messageText.setText("Got Exception : see logcat ");
	                      /*Toast.makeText(UploadUtil
	                    		  .this, "Got Exception : see logcat ", 
	                    		  Toast.LENGTH_SHORT).show();*/
	                  }
	              });
	              Log.e("Upload file to server Exception", "Exception : " 
	            		                           + e.getMessage(), e);  
	          }
	          //dialog.dismiss();       
	          return serverResponseCode; 
	          
           } // End else block 
         }
	
	
	/*public void upload(String filepath) throws IOException
    {
		HttpClient httpclient = new DefaultHttpClient();
		httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);

		HttpPost httppost = new HttpPost(upLoadServerUri);
		File file = new File("/data/data/com.tigo/databases/exercise");

		MultipartEntity mpEntity = new MultipartEntity();
		ContentBody cbFile = new FileBody(file);
		mpEntity.addPart("userfile", cbFile);

		httppost.setEntity(mpEntity);
		System.out.println("executing request " + httppost.getRequestLine());
		HttpResponse response = httpclient.execute(httppost);
		HttpEntity resEntity = response.getEntity();

		System.out.println(response.getStatusLine());
		if (resEntity != null) {
		System.out.println(EntityUtils.toString(resEntity));
		}
		if (resEntity != null) {
		resEntity.consumeContent();
		}

		httpclient.getConnectionManager().shutdown();
    }*/
	
	
		 
}