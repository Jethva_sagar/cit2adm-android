package com.ivisionr.cit2adm.constant;

import android.os.Environment;

/**
 * Created by bimaldesai on 15/06/16.
 */
public class Constant {

    public static final String APPLICATION_DIRECTORY = Environment.getExternalStorageDirectory() + "/CIT2ADM/";
    public static final String PHOTO = "Temp.jpg";
    public static final String CRASH_REPORT_DIR = "Crash_Reports/";
}
