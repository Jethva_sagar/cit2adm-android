package com.ivisionr.cit2adm;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.ivisionr.cit2adm.Util.MobileUtils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Locale;

public class Parameters extends Activity{
	
	Button btn_submit;
	Spinner spin_language,spin_radius;
	TextView wifi_status, gps_status, network_status, camera_resolution, labVersionCode, myprofile_header_text;
	// , devicename, 
	// screen_resolution, display, screen_mode, flash_mode, max_zoom, image_quality;
	ToggleButton tbWifi, tbGPS, tbNetwork;
	CheckBox chkNotifyRatingSendEmail, chkNotifyRatingSendAlert, chkNotifyCommentSendEmail, chkNotifyCommentSendAlert;
	RadioButton rbRatingAlertSound, rbRatingAlertSilent, rbCommentAlertSound,rbCommentAlertSilent;
	private String radiusforselection="";

	String lLanguage = "";
	final String[] language = { "english", "Franais", "Espaol", "Deutsch" };
	final String[] radius={"5km","10km","25km","50km","100km","250km","500km"};
	ArrayAdapter<String> arrayAdapterforradius;
	SafeLifeApplication application;
	private Button backbutton;
	int iPos = 0;
	int versionCode;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.parameter);
		
		application = (SafeLifeApplication) getApplication();

		spin_language = (Spinner) findViewById(R.id.spin_language);
		btn_submit = (Button) findViewById(R.id.btn_submit);
		spin_radius=(Spinner)findViewById(R.id.spin_radius);
		backbutton=(Button)findViewById(R.id.backinreporttopostdr);
		
		wifi_status = (TextView)findViewById(R.id.wifi_status);
		gps_status = (TextView)findViewById(R.id.gps_status);
		network_status = (TextView)findViewById(R.id.network_status);
		myprofile_header_text = (TextView) findViewById(R.id.myprofile_header_text);


		String versionName = "";
		int versionCode = 0;

		int sdkLevel = 0;
		PackageManager pm = getPackageManager();
		try {
			PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			ApplicationInfo applicationInfo = pm.getApplicationInfo(getPackageName(), 0);
			versionName = pInfo.versionName;
			versionCode = pInfo.versionCode;
			if (applicationInfo != null) {
				sdkLevel = applicationInfo.targetSdkVersion;
			}
		}
		catch (PackageManager.NameNotFoundException e)
		{
			e.printStackTrace();
		}
		String vString = "Parameters (API LEVEL - "+sdkLevel+", V "+versionCode+" , "+versionName+")";
		myprofile_header_text.setText(vString);
		/*
		myprofile_header_text.setText(
				getString(R.string.params)+" "
						sdkLevel+", API Level-"+String.valueOf(
						versionCode)+", "+versionName+")");*/

		//camera_resolution = (TextView)findViewById(R.id.camera_resolution);
		//devicename = (TextView)findViewById(R.id.devicename);
		//screen_resolution = (TextView)findViewById(R.id.screen_resolution);
		//flash_mode = (TextView)findViewById(R.id.flash_mode);
		//max_zoom = (TextView)findViewById(R.id.max_zoom);
		//image_quality = (TextView)findViewById(R.id.image_quality);
		
		
		/*chkNotifyRatingSendEmail = (CheckBox)findViewById(R.id.chkNotifyRatingSendEmail);
		chkNotifyRatingSendAlert = (CheckBox)findViewById(R.id.chkNotifyRatingSendAlert);
		chkNotifyCommentSendEmail = (CheckBox)findViewById(R.id.chkNotifyCommentSendEmail);
		chkNotifyCommentSendAlert = (CheckBox)findViewById(R.id.chkNotifyCommentSendAlert);*/
		
		rbRatingAlertSound = (RadioButton)findViewById(R.id.rbRatingAlertSound);
		rbRatingAlertSilent = (RadioButton)findViewById(R.id.rbRatingAlertSilent);
		rbCommentAlertSound = (RadioButton)findViewById(R.id.rbCommentAlertSound);
		rbCommentAlertSilent = (RadioButton)findViewById(R.id.rbCommentAlertSilent);
		
		
		
		//labVersionCode = (TextView)findViewById(R.id.labVersionCode);
		
		
		
		
		tbWifi = (ToggleButton) findViewById(R.id.tbWifi);		
		tbGPS = (ToggleButton) findViewById(R.id.tbGPS);
		tbNetwork = (ToggleButton) findViewById(R.id.tbNetwork);
		
		
		/*try {
			versionCode = this.getPackageManager().getPackageInfo(this.getPackageName(), 0).versionCode;
			Log.d("DEBUG", "Parameters Version Code: "+versionCode);
			labVersionCode.setText(versionCode);

		} catch(Exception ex) {Log.d("DEBUG", "Parameter Exception: "+ex.toString());}*/
		
		
		ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, language);
		arrayAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spin_language.setAdapter(arrayAdapter);
		
		arrayAdapterforradius = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, radius);
		arrayAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		
		
		ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

		if (mWifi.isConnected()) {
		    // Do whatever
			Log.d("DEBUG", "Wifi is connected");
			//wifi_status.setText("Wifi connected");
			tbWifi.setChecked(true);
		}
		else {
			Log.d("DEBUG", "Wifi is disconnected");
			tbWifi.setChecked(false);
			//wifi_status.setText("Wifi not connected");
			
		}
		
		
		
		tbWifi.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
		
		WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		    if(isChecked)
		    {
		        Log.d("DEBUG","Wifi set to true");
		        wifiManager.setWifiEnabled(true);
		    }
		    else
		    {
		        Log.d("DEBUG","Wifi set to false");
		        wifiManager.setWifiEnabled(false);
		    }

		}
		});
		
		
		final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE );

		if (!manager.isProviderEnabled( LocationManager.GPS_PROVIDER)) {
			Log.d("DEBUG", "GPS is connected");
			tbGPS.setChecked(true);
		}
		  
		else {
			Log.d("DEBUG", "GPS is connected");
			tbGPS.setChecked(true);
		}
		
		tbGPS.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		    if(isChecked)
		    {
		        Log.d("DEBUG","GPS set to true");
		        turnGPSOn();
		    }
		    else
		    {
		        Log.d("DEBUG","GPS set to false");
		        turnGPSOff();
		    }

		}
		});
		
		
		if(isDataAvailable()) {
			Log.d("DEBUG", "Data network is connected");
			//wifi_status.setText("Wifi connected");
			tbNetwork.setChecked(true);
		}
		else {
			
			Log.d("DEBUG", "Data network is disconnected");
			//wifi_status.setText("Wifi connected");
			tbNetwork.setChecked(false);
			
		}
		
		
		tbNetwork.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		    if(isChecked)
		    {
		        Log.d("DEBUG","Network data set to true");
		        try {
		        setMobileDataEnabled(true);
		        } catch(Exception ex) {}
		    }
		    else
		    {
		        Log.d("DEBUG","Network data set to false");
		        try {
			        setMobileDataEnabled(false);
			    } catch(Exception ex) {}
		    }

		}
	
		
	});		
		

	
	spin_radius.setAdapter(arrayAdapterforradius);
	
	
	
	Locale current = getResources().getConfiguration().locale;
	
	Log.d("DEBUG", "Parameter Current Locale: "+current.toString());
	
	if(current.toString().equalsIgnoreCase("Franais"))
		spin_language.setSelection(1);
	else if(current.toString().equalsIgnoreCase("Espaol"))
		spin_language.setSelection(2);
	else if(current.toString().equalsIgnoreCase("Deutsch"))
		spin_language.setSelection(3);
	else
		spin_language.setSelection(0);
	

	spin_language.setOnItemSelectedListener(new OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view,
				int position, long value) {
			
			iPos = position;
			lLanguage = language[position];
			
			//MobileUtils.updateLanguage(Parameters.this,	Parameters.this, lLanguage);
			//Log.d("DEBUG", "Check Language: " +language[position]);
			
			
			

		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub

		}
	});
	spin_radius.setOnItemSelectedListener(new OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1,
				int arg2, long arg3) {
			radiusforselection = arrayAdapterforradius.getItem(arg2).toString();
			
			
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			
			
		}
	});
	
	backbutton.setOnClickListener(new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Intent intent = new Intent(getApplicationContext(), GoomapActivity.class);
			startActivity(intent);
			finish();
		}
	});

	btn_submit.setOnClickListener(new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub				
			/*if(!lLanguage.equals(""))
				getIntent().putExtra("language", lLanguage);
			else
				getIntent().putExtra("language", "english");*/
			
			String lang = "en_us";
			String loca = "en";
			
			if(iPos == 1) {
				lang = "fr_fr";
				loca = "fr";
			}
			else if(iPos == 2) {
				lang = "es_es";
				loca = "es";
			}
			else if(iPos == 3) {
				lang = "de_de";
				loca = "de";
			}
			
			int isRatingSendEmail = ((CheckBox) findViewById(R.id.chkNotifyRatingSendEmail)).isChecked() ? 1:0 ;
			int isRatingSendAlert = ((CheckBox) findViewById(R.id.chkNotifyRatingSendAlert)).isChecked() ? 1:0;
			int isCommentSendEmail = ((CheckBox) findViewById(R.id.chkNotifyCommentSendEmail)).isChecked() ? 1:0;
			int isCommentSendAlert = ((CheckBox) findViewById(R.id.chkNotifyCommentSendAlert)).isChecked() ? 1:0;
			
			
			
			
			int idRating = ((RadioGroup)findViewById( R.id.rgNotifyRating )).getCheckedRadioButtonId();
			int idComment = ((RadioGroup)findViewById( R.id.rgNotifyComment )).getCheckedRadioButtonId();
			
			String notifyRating = getAtmos( idRating );
			String notifyComment = getAtmos( idComment );
			
			
			Log.d("DEBUG", "Parameter Rating: "+notifyRating);
			Log.d("DEBUG", "Parameter Comment: "+notifyComment);
			Log.d("DEBUG", "Parameter Is Rating Send Email: "+isRatingSendEmail);
			Log.d("DEBUG", "Parameter Is Rating Send Alert: "+isRatingSendAlert);
			Log.d("DEBUG", "Parameter Is Comment Send Email: "+isCommentSendEmail);
			Log.d("DEBUG", "Parameter Is Comment Send Alert: "+isCommentSendAlert);
			
			Locale locale = new Locale(loca);
	  		Locale.setDefault(locale);
	  		Configuration config = new Configuration();
	  		config.locale = locale;
	  		getBaseContext().getResources().updateConfiguration(config,
	  		      getBaseContext().getResources().getDisplayMetrics());
			
			
			getIntent().putExtra("language", lang);
			
			MobileUtils.updateLanguage(Parameters.this,	Parameters.this, language[iPos]);
			Log.d("DEBUG", "Language Name: " +language[iPos]);
			Log.d("DEBUG", "Language Code: " +lang);
			
			
			Intent refresh = new Intent(getApplicationContext(), GoomapActivity.class);
			startActivity(refresh);//Start the same Activity
			finish(); //finish Activity.
			
			Parameters.this.finish();
			
			// TODO: Call web services to save information to the remote database.
			
			//finish();
			

			//Intent intent = new Intent(getApplicationContext(), GoomapActivity.class);
			//startActivity(intent);
			//finish();
			

		}
	});
	
	
	


}
	
	

	public void turnGPSOn(){
	    try
	    {
	
	    String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
	
	
	    if(!provider.contains("gps")){ //if gps is disabled
	        final Intent poke = new Intent();
	        poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
	        poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
	        poke.setData(Uri.parse("3"));
	        sendBroadcast(poke);
	    }
	    }
	    catch (Exception e) {
	
	    }
	}
	
	// Method to turn off the GPS
	public void turnGPSOff(){
	    String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
	
	    if(provider.contains("gps")){ //if gps is enabled
	        final Intent poke = new Intent();
	        poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
	        poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
	        poke.setData(Uri.parse("3"));
	        sendBroadcast(poke);
	    }
	}
	
	private boolean isDataAvailable() {
		ConnectivityManager conxMgr = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
	
	     NetworkInfo mobileNwInfo = conxMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
	     NetworkInfo wifiNwInfo   = conxMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
	
	    return ((mobileNwInfo== null? false : mobileNwInfo.isConnected() )|| (wifiNwInfo == null? false : wifiNwInfo.isConnected()));
	 }
	
	private void setMobileDataEnabled(boolean enabled) throws ClassNotFoundException, NoSuchFieldException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
	    final ConnectivityManager conman = (ConnectivityManager)  getSystemService(Context.CONNECTIVITY_SERVICE);
	    final Class conmanClass = Class.forName(conman.getClass().getName());
	    final Field connectivityManagerField = conmanClass.getDeclaredField("mService");
	    connectivityManagerField.setAccessible(true);
	    final Object connectivityManager = connectivityManagerField.get(conman);
	    final Class connectivityManagerClass =  Class.forName(connectivityManager.getClass().getName());
	    final Method setMobileDataEnabledMethod = connectivityManagerClass.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
	    setMobileDataEnabledMethod.setAccessible(true);
	
	    setMobileDataEnabledMethod.invoke(connectivityManager, enabled);
	}
	
	
	private String getAtmos( int id ) {
		String atmos = "";
	    switch( id ) {
	        case R.id.rbRatingAlertSound:
	            atmos = "1";
	            break;
	        case R.id.rbRatingAlertSilent:
	            atmos = "2";
	            break;
	        case R.id.rbCommentAlertSound:
	            atmos = "3";
	             break;
	        case R.id.rbCommentAlertSilent:
	            atmos = "4";
	        break;
	        
	        
	        
	    }

	    return atmos;
	}
	
	
	@Override
	public void onBackPressed()
	{
		// TODO Auto-generated method stub
		//super.onBackPressed();
		Intent intent = new Intent(this, GoomapActivity.class);
		//startActivity(intent);
		//Parameters.this.finish();
		//finish();
	}
	
}


	
