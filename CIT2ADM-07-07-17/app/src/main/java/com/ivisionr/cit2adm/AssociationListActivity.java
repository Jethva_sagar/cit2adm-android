package com.ivisionr.cit2adm;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.ivisionr.cit2adm.Util.Utils;
import com.ivisionr.cit2adm.adapter.MyAssocListAdapter;
import com.ivisionr.cit2adm.async.IvisionarAssociationAsyns;
import com.ivisionr.cit2adm.bin.Association;
import com.ivisionr.cit2adm.interfaces.IvisionarassociationInterface;

public class AssociationListActivity extends Activity implements IvisionarassociationInterface {

	private String countryname;
	private String lat, lon;

	private ProgressDialog mProgressDialog;
	private List<Association> myAssocDetails;
	private ListView list;
	private Button back;
	private String rad = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.associationlist);
		mProgressDialog = new ProgressDialog(AssociationListActivity.this);
		myAssocDetails = new ArrayList<Association>();
		list = (ListView) findViewById(R.id.list);
		back = (Button) findViewById(R.id.myneighbourback);

		Bundle bundle = getIntent().getExtras();

		if (getIntent().hasExtra("country")) {
			countryname = bundle.getString("country");
		}
		if (getIntent().hasExtra("currentlat")) {
			lat = bundle.getString("currentlat");

		}
		if (getIntent().hasExtra("currentlon")) {
			lon = bundle.getString("currentlon");
		}
		if (getIntent().hasExtra("radiusforselection")) {
			rad = bundle.getString("radiusforselection");
		}
		mProgressDialog.setTitle(getResources().getString(R.string.assocnearby));
		mProgressDialog.setMessage(getResources().getString(R.string.loading));

		/*Log.d("country", "countryname" + countryname);
		Log.d("currlat", "currlatname" + lat);
		Log.d("currentlong", "currentlongname" + lon);
		Log.d("cu", "cu" + rad);*/

		if (Utils.checkConnectivity(AssociationListActivity.this)) {
			/*MyNeighbouringAsync mtneighbouring = new MyNeighbouringAsync(
					AssociationListActivity.this, countryname, lat, lon, rad);
			mtneighbouring.myneighbouringinterface = this;
			mtneighbouring.execute("");*/
			
			IvisionarAssociationAsyns ivisionarasync=new IvisionarAssociationAsyns(AssociationListActivity.this,lat,lon);
			ivisionarasync.ivisionarinterface=this;
			ivisionarasync.execute();
			
		} else {
			showNetworkDialog("internet");
		}

		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {

				String id = myAssocDetails.get(arg2).getId();
				Log.d("DEBUG", "Association Id: " + id);
				Intent intent = new Intent(AssociationListActivity.this,
						IvisionrAssociation.class);
				intent.putExtra("id", id);
				intent.putExtra("currentlat", myAssocDetails.get(arg2).getLat());
				intent.putExtra("currentlon", myAssocDetails.get(arg2).getLon());
				
				
				startActivity(intent);

			}
		});
		back.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				finish();

			}
		});

	}

	@SuppressWarnings("deprecation")
	public void showNetworkDialog(final String message) {
		// final exit of application
		AlertDialog.Builder builder = new AlertDialog.Builder(
				AssociationListActivity.this);
		builder.setTitle(getResources().getString(R.string.app_name));
		if (message.equals("gps")) {
			builder.setMessage(getResources().getString(R.string.gps_message));
		} else if (message.equals("internet")) {
			builder.setMessage(getResources().getString(R.string.net_message));
		}
		AlertDialog alert = builder.create();
		alert.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				if (message.equals("gps")) {
					Intent i = new Intent(
							android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
					startActivityForResult(i, 1);
				} else if (message.equals("internet")) {
					Intent i = new Intent(
							android.provider.Settings.ACTION_WIRELESS_SETTINGS);
					startActivityForResult(i, 2);
					Intent i1 = new Intent(
							android.provider.Settings.ACTION_WIFI_SETTINGS);
					startActivityForResult(i1, 3);
				} else {
					// do nothing
				}
			}
		});
		// Showing Alert Message
		alert.show();
	}

	@Override
	public void onIvisionarassociationstarted() {
		// TODO Auto-generated method stub
		if(null != mProgressDialog){
			mProgressDialog.show();

		}

	}
	
	

	@Override
	public void onIvisionarassociationCompleted(List<Association> list) {

		myAssocDetails.addAll(list);
		bindList(myAssocDetails);
		mProgressDialog.dismiss();

	}

	private void bindList(List<Association> myAssocDetails2) {

		MyAssocListAdapter assocList = new MyAssocListAdapter(
				this, myAssocDetails);
		list.setAdapter(assocList);

	}

	@Override
	public void onIvisionarasationNoData() {

		mProgressDialog.dismiss();
		Toast.makeText(getApplicationContext(), R.string.noneighbour,
				Toast.LENGTH_LONG).show();

	}
	
	
    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.association, menu);
		return true;
	}
    
    @Override
	public boolean onOptionsItemSelected(MenuItem item) {
    	
	    switch (item.getItemId()) {
	    	case R.id.mapview:
	    		Intent intent = new Intent(AssociationListActivity.this, IvisionrAssociation.class);
	    		startActivity(intent);
	    	break;
	    	default:
	    	break;
	    }
	    
	    return true;
    }

}
