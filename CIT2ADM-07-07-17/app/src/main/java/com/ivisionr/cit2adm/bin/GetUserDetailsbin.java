package com.ivisionr.cit2adm.bin;

public class GetUserDetailsbin {
	private String id="";
	private String firstname="";
	private String lastname="";
	private String city="";
	private String zip="";
	private String created="";
	private String comments_counter="";
	private String ratingplus_counter="";
	private String ratingminus_counter="";
	private String issue_status="";
	private String issue_counter="";
	private String counteropen="";
	private String counterprogress="";
	private String countercompleted="";
	private String counterclosed="";
	public String getCounteropen() {
		return counteropen;
	}
	public void setCounteropen(String counteropen) {
		this.counteropen = counteropen;
	}
	public String getCounterprogress() {
		return counterprogress;
	}
	public void setCounterprogress(String counterprogress) {
		this.counterprogress = counterprogress;
	}
	public String getCountercompleted() {
		return countercompleted;
	}
	public void setCountercompleted(String countercompleted) {
		this.countercompleted = countercompleted;
	}
	public String getCounterclosed() {
		return counterclosed;
	}
	public void setCounterclosed(String counterclosed) {
		this.counterclosed = counterclosed;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public String getComments_counter() {
		return comments_counter;
	}
	public void setComments_counter(String comments_counter) {
		this.comments_counter = comments_counter;
	}
	public String getRatingplus_counter() {
		return ratingplus_counter;
	}
	public void setRatingplus_counter(String ratingplus_counter) {
		this.ratingplus_counter = ratingplus_counter;
	}
	public String getRatingminus_counter() {
		return ratingminus_counter;
	}
	public void setRatingminus_counter(String ratingminus_counter) {
		this.ratingminus_counter = ratingminus_counter;
	}
	public String getIssue_status() {
		return issue_status;
	}
	public void setIssue_status(String issue_status) {
		this.issue_status = issue_status;
	}
	public String getIssue_counter() {
		return issue_counter;
	}
	public void setIssue_counter(String issue_counter) {
		this.issue_counter = issue_counter;
	}




}
