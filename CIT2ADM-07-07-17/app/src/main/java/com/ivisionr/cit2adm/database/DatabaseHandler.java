package com.ivisionr.cit2adm.database;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Message;
import android.util.Log;

import com.ivisionr.cit2adm.bin.Draft;
import com.ivisionr.cit2adm.bin.User;

public class DatabaseHandler extends SQLiteOpenHelper {

	// All Static variables
	// Database Version
	private static final int DATABASE_VERSION = 9;

	// Database Name
	private static final String DATABASE_NAME = "SafeLive.db";

	// Users table name

	private static final String TABLE_USERS = "users";
	private static final String TABLE_DRAFTS = "drafts";
	
	// Users Table Columns names
	
	
	
	public static final String KEY_ID = "id";
	public static final String KEY_USER_TYPE_ID = "user_type_id";
	public static final String KEY_FIRST_NAME = "first_name";
	public static final String KEY_LAST_NAME = "last_name";
	public static final String KEY_EMAIL = "email";
	public static final String KEY_USERNAME = "username";
	public static final String KEY_PASSWORD = "password";
	public static final String KEY_ORGANIZATION = "organization";
	public static final String KEY_CITY = "city";
	public static final String KEY_COUNTRY = "country";
	public static final String KEY_ZIPCODE = "zipcode";
	public static final String KEY_ACTIVE = "active";
	public static final String KEY_PAID = "paid";
	public static final String KEY_NOTE = "note";
	public static final String KEY_MEMBERSHIP_EXPIRY_DATE = "membership_expiry_date";
	public static final String KEY_CREATED = "created";
	public static final String KEY_MODIFIED = "modified";
	public static final String KEY_DEVICEID = "deviceid";
	public static final String KEY_APP_VERSION_CODE = "app_version_code";
	public static final String KEY_APP_VERSION_NAME= "app_version_name";
	public static final String KEY_ADDRESS= "address";
	public static final String KEY_ACCESS_ID= "access_id";
	
	public static final String KEY_ISSUE_ID = "id";
	public static final String KEY_USER_ID = "user_id";
	public static final String KEY_ISSUE_TYPE_ID = "issue_type_id";
	public static final String KEY_ISSUE_TYPE_NAME = "issue_type_name";
	public static final String KEY_ISSUE_IMAGE = "issue_image";
	public static final String KEY_ISSUE_IMAGE_BINARY = "issue_image_bin";
	public static final String KEY_ISSUE_DESC = "issue_description";
	public static final String KEY_ISSUE_LAT = "issue_lat";
	public static final String KEY_ISSUE_LON = "issue_lon";
	public static final String KEY_ISSUE_DATE = "issue_date";
	public static final String KEY_ISSUE_LANG = "issue_lang";
	public static final String KEY_ISSUE_DEVICEID = "issue_deviceid";
	public static final String KEY_ISSUE_APP_VERSION_CODE = "issue_app_version_code";
	public static final String KEY_ISSUE_APP_VERSION_NAME= "issue_app_version_name";
	public static final String KEY_ISSUE_ADDRESS= "issue_address";
	
	
	
	public DatabaseHandler(Context context) {		
		super(context, DATABASE_NAME, null, DATABASE_VERSION);		
	}

	// Creating Tables
	@Override
	public void onCreate(SQLiteDatabase db) {
		
		String CREATE_USER_TABLE = "CREATE TABLE " + TABLE_USERS + "("
				+ KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ KEY_USER_TYPE_ID+ " INTEGER,"
				+ KEY_FIRST_NAME + " TEXT,"
				+ KEY_LAST_NAME + " TEXT," 
				+ KEY_EMAIL+ " TEXT," 
				+ KEY_USERNAME + " TEXT,"
				+ KEY_PASSWORD + " TEXT,"
				+ KEY_ORGANIZATION + " TEXT,"
				+ KEY_CITY + " TEXT," 
				+ KEY_COUNTRY + " TEXT," 
				+ KEY_ZIPCODE + " TEXT,"
				+ KEY_ACTIVE + " INTEGER," 
				+ KEY_PAID + " INTEGER," 
				+ KEY_NOTE + " TEXT,"
				+ KEY_CREATED + " DATETIME,"
				+ KEY_MODIFIED + " DATETIME,"
				+ KEY_MEMBERSHIP_EXPIRY_DATE + " DATETIME,"
				+ KEY_DEVICEID + " TEXT,"
				+ KEY_APP_VERSION_CODE + " TEXT,"
				+ KEY_APP_VERSION_NAME + " TEXT,"
				+ KEY_ACCESS_ID + " TEXT "+ ")";

		db.execSQL(CREATE_USER_TABLE);
		
		
		
		final String CREATE_DRAFT_TABLE = "create table "+TABLE_DRAFTS+
				"( " +KEY_ISSUE_ID+" INTEGER PRIMARY KEY AUTOINCREMENT,"
				+" "+KEY_USER_ID+" INTEGER, "
				+" "+KEY_ISSUE_TYPE_ID+"  INTEGER, "
				+" "+KEY_ISSUE_TYPE_NAME+" TEXT, "
				+" "+KEY_ISSUE_IMAGE+" TEXT, "
				+" "+KEY_ISSUE_IMAGE_BINARY+" TEXT, "
				+" "+KEY_ISSUE_DESC+" TEXT, "
				+" "+KEY_ISSUE_LAT+" TEXT, "
				+" "+KEY_ISSUE_LON+" TEXT, "
				+" "+KEY_ISSUE_DATE+" DATETIME, "
				+" "+KEY_ISSUE_DEVICEID+" TEXT, "
				+" "+KEY_ISSUE_APP_VERSION_CODE+" TEXT, "
				+" "+KEY_ISSUE_APP_VERSION_NAME+" TEXT, "
				+" "+KEY_ISSUE_ADDRESS+" TEXT, "
				+" "+KEY_ISSUE_LANG+" TEXT);";
		db.execSQL(CREATE_DRAFT_TABLE);
		
		
		
		
		
	}

	// Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_DRAFTS);
		
		// Create tables again
		onCreate(db);
	}

	/**
	 * All CRUD(Create, Read, Update, Delete) Operations
	 */

	// Adding new user
	public void addUser(User user) {
		SQLiteDatabase db = this.getWritableDatabase();

		
		ContentValues values = new ContentValues();
		values.put(KEY_ID, user.getId()); 												// User Id
		values.put(KEY_USER_TYPE_ID, user.getUser_type_id()); 							// User Type Id
		values.put(KEY_FIRST_NAME, user.getFirst_name()); 								// User First Name
		values.put(KEY_LAST_NAME, user.getLast_name()); 								// User Last Name
		values.put(KEY_EMAIL, user.getEmail()); 										// User Email
		values.put(KEY_USERNAME, user.getUsername()); 									// User Username		
		values.put(KEY_PASSWORD, user.getPassword()); 									// User Password		
		values.put(KEY_ORGANIZATION, user.getOrganization()); 							// User Organization		
		values.put(KEY_CITY, user.getCity()); 											// User City		
		values.put(KEY_COUNTRY, user.getCountry()); 									// User Country		
		values.put(KEY_ZIPCODE, user.getZipcode()); 									// User Zip Code		
		values.put(KEY_ACTIVE, user.getActive()); 										// User Active		
		values.put(KEY_PAID, user.getPaid()); 											// User Paid
		values.put(KEY_NOTE, user.getNote()); 											// User Note
		values.put(KEY_MEMBERSHIP_EXPIRY_DATE, user.getMembership_expiry_date()); 		// User Membership Expiry Date
		values.put(KEY_CREATED, user.getCreated()); 									// User Created
		values.put(KEY_MODIFIED, user.getModified()); 									// User Modified
		values.put(KEY_DEVICEID, user.getDevice_id()); 									// User device_id
		values.put(KEY_APP_VERSION_CODE, user.getApp_version_code()); 									// User app version code
		values.put(KEY_APP_VERSION_NAME, user.getApp_version_name()); 									// User app versio name
		values.put(KEY_ACCESS_ID, user.getAccess_id()); 
		
		Log.d("DEBUG", "Inside Add");
		// Inserting Row
		db.insert(TABLE_USERS, null, values);
		db.close(); // Closing database connection
		
	}
	
	
	public int countDuplicateUser(String id) {
		// Select All Query
		SQLiteDatabase db = this.getReadableDatabase();
		
		/*Cursor cursor = db.query(TABLE_USERS, new String[] { KEY_USER_ID}, USER_KEY_NAME + "=?",
				new String[] {user_name}, null, null, null, null);*/
		
		String selectQuery = "SELECT  COUNT("+KEY_ID+") AS counter FROM " + TABLE_USERS + " WHERE "+KEY_ID+" = "+id;
		Log.d("DEBUG", "SQL: "+selectQuery);
		Cursor cursor = db.rawQuery(selectQuery, null);
		
		
		/*Cursor cursor = db.query(TABLE_USERS, new String[] {"COUNT("+KEY_USER_ID+") AS COUNT_GOALS"}, 
				"UPPER("+USER_KEY_NAME+")" + "=?",
				new String[] {"UPPER("+user_name+")"}, null, null, null, null);*/
		
		int cnt = 0;
		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {				
				cnt = cursor.getInt(0);
				//Log.d("DEBUG", "User Counter: "+cnt);
			} while (cursor.moveToNext());
		}
		
		
		cursor.close();
		db.close();

		// return user list
		return cnt;
		
	}
	
	
	
	public String getLastUserId() {
		// Select All Query
		String selectQuery = "SELECT  max(id) AS maxid FROM " + TABLE_USERS;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		
		String maxid = "";

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				maxid = cursor.getString(0);
				
				// Adding user to list
				
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();

		// return user list
		return maxid;
	}
	


	// Getting single user
	public User getUser(String id) {
		SQLiteDatabase db = this.getReadableDatabase();
		
		User user = new User();
		
		

		/*Cursor cursor = db.query(TABLE_USERS, new String[] { KEY_ID, KEY_USER_TYPE_ID, KEY_FIRST_NAME, KEY_LAST_NAME, 
				KEY_EMAIL, KEY_USERNAME, KEY_PASSWORD, KEY_ORGANIZATION, 
				KEY_CITY, KEY_COUNTRY, KEY_ZIPCODE, KEY_ACTIVE, KEY_PAID, KEY_NOTE, KEY_MEMBERSHIP_EXPIRY_DATE, KEY_CREATED, KEY_MODIFIED }, KEY_ID + "=?",
				new String[] { id }, null, null, null, null);*/
		
		String selectQuery = "SELECT * FROM " + TABLE_USERS + " WHERE "+KEY_ID+" = "+id;

		
		Cursor cursor = db.rawQuery(selectQuery, null);
	
		
		Log.d("DEBUG", "DBHandler - Record Count: "+cursor.getCount());
		if (cursor != null && cursor.getCount() > 0)
		{
			cursor.moveToFirst();
			
			//0 + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
			//1+ KEY_USER_TYPE_ID+ " INTEGER,"
			//2+ KEY_FIRST_NAME + " TEXT,"
			//3+ KEY_LAST_NAME + " TEXT," 
			//4+ KEY_EMAIL+ " TEXT," 
			//5+ KEY_USERNAME + " TEXT,"
			//6+ KEY_PASSWORD + " TEXT,"
			//7+ KEY_ORGANIZATION + " TEXT,"
			//8+ KEY_CITY + " TEXT," 
			//9+ KEY_COUNTRY + " TEXT," 
			//10+ KEY_ZIPCODE + " TEXT,"
			//11+ KEY_ACTIVE + " INTEGER," 
			//12+ KEY_PAID + " INTEGER," 
			//13+ KEY_NOTE + " TEXT,"
			//14+ KEY_CREATED + " DATETIME,"
			//15+ KEY_MODIFIED + " DATETIME,"
			//16+ KEY_MEMBERSHIP_EXPIRY_DATE + " DATETIME,"
			//17+ KEY_DEVICEID + " TEXT,"
			//18+ KEY_APP_VERSION_CODE + " TEXT,"
			//19+ KEY_APP_VERSION_NAME + " TEXT,"
			//20+ KEY_ACCESS_ID + " TEXT "+ ")";
			
			user.setId(String.valueOf(cursor.getInt(0)));
			user.setUser_type_id(String.valueOf(cursor.getInt(1)));
			user.setFirst_name(cursor.getString(2));
			user.setLast_name(cursor.getString(3));
			user.setEmail(cursor.getString(4));
			user.setUsername(cursor.getString(5));
			user.setPassword(cursor.getString(6));
			user.setOrganization(cursor.getString(7));
			user.setCity(cursor.getString(8));
			user.setCountry(cursor.getString(9));
			user.setZipcode(cursor.getString(10));
			user.setActive(cursor.getString(11));
			user.setPaid(cursor.getString(12));
			user.setNote(cursor.getString(13));
			user.setCreated(cursor.getString(14));
			user.setModified(cursor.getString(15));
			user.setMembership_expiry_date(cursor.getString(16));
			user.setAccess_id(cursor.getString(20));
			
		}


		
		cursor.close();
		db.close();
		// return user
		return user;
	}
	
	
	
	public int countUsers() {
		// Select All Query
		String selectQuery = "SELECT  COUNT("+KEY_ID+") AS counter FROM " + TABLE_USERS;

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		
		int counter = 0;

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				counter = cursor.getInt(0);
				
				// Adding user to list
				
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();

		// return user list
		return counter;
	}
	
	/*public String getUserByCode(String code) {
		
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.query(TABLE_USERS, new String[] { KEY_USER_ID}, USER_KEY_CODE + "=?",
				new String[] {code}, null, null, null, null);
		
		
		String id = "";
		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				
				id = cursor.getString(0);
			} while (cursor.moveToNext());
		}
		
		
		cursor.close();
		db.close();

		// return user list
		return id;
	}*/
	
	
	/*
	 * values.put(KEY_USER_TYPE_ID, user.getUser_type_id()); 							// User Id
values.put(KEY_FIRST_NAME, user.getFirst_name()); 									// User Name
values.put(KEY_LAST_NAME, user.getLast_name()); 								// User Company
values.put(KEY_EMAIL, user.getEmail()); 										// User Company
values.put(KEY_USERNAME, user.getUsername()); 										// User Mob No		
values.put(KEY_PASSWORD, user.getPassword()); 										// User Email		
values.put(KEY_ORGANIZATION, user.getOrganization()); 								// User Reg Id		
values.put(KEY_CITY, user.getCity()); 									// User Reg Id		
values.put(KEY_COUNTRY, user.getCountry()); 									// User Image		
values.put(KEY_ZIPCODE, user.getZipcode()); 								// User Last Activity		
values.put(KEY_ACTIVE, user.getActive()); 									// User Is Online		
values.put(KEY_PAID, user.getPaid()); 										// User Active
values.put(KEY_NOTE, user.getNote()); 										// User Active
values.put(KEY_MEMBERSHIP_EXPIRY_DATE, user.getMembership_expiry_date()); 					// User Active
values.put(KEY_CREATED, user.getCreated()); 									// User Active
values.put(KEY_MODIFIED, user.getModified()); 
	 */

	// Updating single user
	public int updateUser(User user) {
		SQLiteDatabase db = this.getWritableDatabase();


		ContentValues values = new ContentValues();
		values.put(KEY_USER_TYPE_ID, user.getUser_type_id()); 							// User Type Id
		values.put(KEY_FIRST_NAME, user.getFirst_name()); 								// User First Name
		values.put(KEY_LAST_NAME, user.getLast_name()); 								// User Last Name
		values.put(KEY_EMAIL, user.getEmail()); 										// User Email
		values.put(KEY_USERNAME, user.getUsername()); 									// User Username	
		values.put(KEY_PASSWORD, user.getPassword()); 									// User Password		
		values.put(KEY_ORGANIZATION, user.getOrganization()); 							// User Organization		
		values.put(KEY_CITY, user.getCity()); 											// User City		
		values.put(KEY_COUNTRY, user.getCountry()); 									// User Country		
		values.put(KEY_ZIPCODE, user.getZipcode()); 									// User Zip Code		
		values.put(KEY_ACTIVE, user.getActive()); 										// User Active		
		values.put(KEY_PAID, user.getPaid()); 											// User Paid
		values.put(KEY_NOTE, user.getNote()); 											// User Note
		values.put(KEY_MEMBERSHIP_EXPIRY_DATE, user.getMembership_expiry_date()); 		// User Membership Expiry Date
		values.put(KEY_CREATED, user.getCreated()); 									// User Created
		values.put(KEY_MODIFIED, user.getModified()); 									// User Modified
		values.put(KEY_DEVICEID, user.getDevice_id()); 									// User device_id
		values.put(KEY_APP_VERSION_CODE, user.getApp_version_code()); 								// User app version code
		values.put(KEY_APP_VERSION_NAME, user.getApp_version_name()); 									// User app versio name
		values.put(KEY_ACCESS_ID, user.getAccess_id());
		

		// updating row
		return db.update(TABLE_USERS, values, KEY_ID + " = ?",
				new String[] { String.valueOf(user.getId()) });
	}

	// Deleting single user
	public int deleteUser(int user_id) {
		SQLiteDatabase db = this.getWritableDatabase();
		int res = db.delete(TABLE_USERS, KEY_ID + " = ?",
				new String[] { String.valueOf(user_id) });
		db.close();
		return res;
	}
	
	
	// Getting users Count
	public int getUsersCount() {
		String countQuery = "SELECT  * FROM " + TABLE_USERS;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		cursor.close();
		db.close();

		// return count
		return cursor.getCount();
	}
	
	// Deleting all users
	public int deleteAllUsers() {
		SQLiteDatabase db = this.getWritableDatabase();
		 String DELETEPASSCODE_DETAIL = "DELETE FROM "+TABLE_USERS+";";
         db.execSQL(DELETEPASSCODE_DETAIL);
         return 0;
	}
	
	// Deleting all tasks
	public int deleteAllDrafts() {
		SQLiteDatabase db = this.getWritableDatabase();
		 String DELETEPASSCODE_DETAIL = "DELETE FROM "+TABLE_DRAFTS+";";
         db.execSQL(DELETEPASSCODE_DETAIL);
         return 0;
	}
		
	

	// Deleting tasks user
	public int deleteDraftsByUser(String user_id) {
		SQLiteDatabase db = this.getWritableDatabase();
		int res = db.delete(TABLE_DRAFTS, KEY_USER_ID + " = ?",
				new String[] { user_id });
		db.close();
		
		return res;
	}
	
	
	
	
	///////////////////// **** Draft **** /////////////////////
	

	
	// Adding New Draft
	public void addDraft(Draft draft) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
    	
		//values.put(KEY_ISSUE_ID, draft.getId());
		values.put(KEY_USER_ID, draft.getUser_id()); 	// User Name
		values.put(KEY_ISSUE_TYPE_ID, draft.getIssue_type_id());			
		values.put(KEY_ISSUE_TYPE_NAME, draft.getIssue_type_name());			
		values.put(KEY_ISSUE_IMAGE, draft.getIssue_image());			
		values.put(KEY_ISSUE_IMAGE_BINARY, draft.getIssue_image_bin());		
		values.put(KEY_ISSUE_DESC, draft.getIssue_description());
		values.put(KEY_ISSUE_LAT, draft.getIssue_lat());
		values.put(KEY_ISSUE_LON, draft.getIssue_lon());
		values.put(KEY_ISSUE_DATE, draft.getIssue_date());
		values.put(KEY_ISSUE_DEVICEID, draft.getIssue_Device_id());
		values.put(KEY_ISSUE_APP_VERSION_CODE, draft.getIssue_app_version_code());
		values.put(KEY_ISSUE_APP_VERSION_NAME, draft.getIssue_app_version_name());
		values.put(KEY_ISSUE_ADDRESS, draft.getIssue_address());
		
		values.put(KEY_ISSUE_LANG, draft.getIssue_lang());
		
		Log.d("DEBUG", "Draft User Id: " + draft.getUser_id());
		Log.d("DEBUG", "Draft Type Id: " + draft.getIssue_type_id());
		Log.d("DEBUG", "Draft Type Name: " + draft.getIssue_type_name());
		Log.d("DEBUG", "Draft Image: " + draft.getIssue_image());
		Log.d("DEBUG", "Draft Image Bin: " + draft.getIssue_image_bin());
		Log.d("DEBUG", "Draft Description: " + draft.getIssue_description());
		Log.d("DEBUG", "Draft Lat: " + draft.getIssue_lat());
		Log.d("DEBUG", "Draft  Lon: " + draft.getIssue_lon());
		Log.d("DEBUG", "Draft  Date: " + draft.getIssue_date());
		Log.d("DEBUG", "Draft Lang: " + draft.getIssue_lang());
		Log.d("DEBUG", "Draft DEVICEID: " + draft.getIssue_Device_id());
		Log.d("DEBUG", "Draft APPVERION_CODE: " + draft.getIssue_app_version_code());
		Log.d("DEBUG", "Draft APP_VERSION_NAME: " + draft.getIssue_app_version_name());
		Log.d("DEBUG", "Draft ADDRESSS: " + draft.getIssue_address());
		
		
		// Inserting Row
		db.insert(TABLE_DRAFTS, null, values);
		db.close(); // Closing database connection
	}
	


	// Updating single task
	public int updateDraft(Draft draft) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		int res = 0;
		
		String mid = draft.getId();
		
		String query = "SELECT "+KEY_ISSUE_ID+" FROM drafts WHERE "+KEY_ISSUE_ID+" = ?";
						
		Cursor cursor = db.rawQuery(query, new String[] {mid});
		
		
		if (cursor.moveToFirst()) {
			do {
				
				res = 2;
								
			} while (cursor.moveToNext());
			
		}
		cursor.close();
		
		if(res == 0) {
			
			values.put(KEY_ISSUE_ID, draft.getId());
			values.put(KEY_USER_ID, draft.getUser_id()); 	// User Name
			values.put(KEY_ISSUE_TYPE_ID, draft.getIssue_type_id());			
			values.put(KEY_ISSUE_TYPE_NAME, draft.getIssue_type_name());			
			values.put(KEY_ISSUE_IMAGE, draft.getIssue_image());			
			values.put(KEY_ISSUE_IMAGE_BINARY, draft.getIssue_image_bin());		
			values.put(KEY_ISSUE_DESC, draft.getIssue_description());
			values.put(KEY_ISSUE_LAT, draft.getIssue_lat());
			values.put(KEY_ISSUE_LON, draft.getIssue_lon());
			values.put(KEY_ISSUE_DATE, draft.getIssue_date());
			values.put(KEY_ISSUE_LANG, draft.getIssue_lang());
			
			/*int res = db.update(TABLE_DRAFTS, values, TASK_KEY_ID + " = ?",
					new String[] { String.valueOf(task.getId()) });*/
			
			res = db.update(TABLE_DRAFTS, values, KEY_ISSUE_ID + " = "+draft.getId(), null);

		}

		db.close();

		// updating row
		return res;
	}
	
	
	// Updating single task
		public int updateDraftDescription(String mid,  String description) {
			SQLiteDatabase db = this.getWritableDatabase();

			ContentValues values = new ContentValues();
			int res = 0;
			
			
			String query = "SELECT "+KEY_ISSUE_ID+" FROM drafts WHERE "+KEY_ISSUE_ID+" = ?";
							
			Cursor cursor = db.rawQuery(query, new String[] {mid});
			
			
			if (cursor.moveToFirst()) {
				do {
					
					values.put(KEY_ISSUE_DESC, description);
					
					res = db.update(TABLE_DRAFTS, values, KEY_ISSUE_ID + " = "+mid, null);
									
				} while (cursor.moveToNext());
				
			}
			cursor.close();
			
			

			db.close();

			// updating row
			return res;
		}

	// Deleting single task
	public int deleteDraft(String id) {
		SQLiteDatabase db = this.getWritableDatabase();
		int deleted = db.delete(TABLE_DRAFTS, KEY_ISSUE_ID + " = ?",
				new String[] { String.valueOf(id) });
		db.close();
		
		return deleted;
	}

	// Delete message status
	/*public int updateDraftStatus(int id, int status) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date date = new Date();
		
		//values.put(TASK_KEY_ID, task.getId());
		values.put(TASK_KEY_STATUS, status);
		values.put(TASK_KEY_COMPLETED, dateFormat.format(date));
		
		String strFilter = TASK_KEY_ID+" = " + id;
		
		int updated = db.update(TABLE_DRAFTS, values, strFilter, null);
		
		db.close();
		
		return updated;
	}*/
	
	
	
	// Getting tasks Count
	public int getDraftCountByUser(String id) {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = null;
		
		//db.query(distinct, table, columns, selection, selectionArgs, groupBy, having, orderBy, limit, cancellationSignal);
		//db.query(table, columns, selection, selectionArgs, groupBy, having, orderBy)
		
		cursor = db.query(TABLE_DRAFTS, new String[] {KEY_ISSUE_ID}, 
				KEY_USER_ID + "=?",
			new String[] { String.valueOf(id) }, KEY_USER_ID, null, null, null);
		
		int taskCount = cursor.getCount();
		cursor.close();
		db.close();
		return taskCount;

		
	}
	
	// Getting tasks Count
	public int countDuplicateDraft(String id) {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = null;
		
		//db.query(distinct, table, columns, selection, selectionArgs, groupBy, having, orderBy, limit, cancellationSignal);
		//db.query(table, columns, selection, selectionArgs, groupBy, having, orderBy)
		
		cursor = db.query(TABLE_DRAFTS, new String[] {KEY_ISSUE_ID}, 
				KEY_ISSUE_ID + "=?",
			new String[] { String.valueOf(id) }, KEY_ISSUE_ID, null, null, null);
		
		int taskCount = cursor.getCount();
		cursor.close();
		db.close();
		return taskCount;

		
	}
	
	public int countDrafts() {
		
		// Select All Query
		String selectQuery = "SELECT  COUNT("+KEY_ISSUE_ID+") AS counter FROM " + TABLE_DRAFTS;

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		
		int counter = 0;

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				counter = cursor.getInt(0);
				
				// Adding user to list
				
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();

		// return user list
		return counter;
	}
	
	
	public List<Draft> getAllDrafts(String lastid) {
		SQLiteDatabase db = this.getReadableDatabase();
		List<Draft> msgList = new ArrayList<Draft>();
		String selectQuery = "";
		Cursor cursor = null;

		// selectQuery = "SELECT  M.*, U." + USER_KEY_DESIGNATION + ", U." + USER_KEY_COMPANY + " FROM " + TABLE_DRAFTS + " M, " + TABLE_USERS + " U WHERE U." + KEY_USER_ID + " = M." + MESSAGES_KEY_USER_ID;
		//if(lastid == null || lastid == "")
		selectQuery = "SELECT * FROM " + TABLE_DRAFTS ;
		//else
		//	selectQuery = "SELECT  * FROM " + TABLE_DRAFTS + " WHERE "+KEY_ISSUE_ID+" > '"+lastid+"'";
		
		cursor = db.rawQuery(selectQuery, null);

		
		// looping through all rows and adding to list
		if (cursor!= null && cursor.moveToFirst()) {
			do {
				
				//Log.d("DEBUG", "Id: "+String.valueOf(cursor.getInt(0)));
				
				/*draft.setUser_id(String.valueOf(cursor.getInt(1)));				
				draft.setIssue_type_id(String.valueOf(cursor.getInt(2)));
				draft.setIssue_type_name(cursor.getString(3));
				draft.setIssue_image(cursor.getString(4));
				draft.setIssue_image_bin(cursor.getString(5));
				draft.setIssue_description(cursor.getString(6));
				draft.setIssue_lat(cursor.getString(7));
				draft.setIssue_lon(cursor.getString(8));
				draft.setIssue_date(cursor.getString(9));
				draft.setIssue_lang(cursor.getString(10));*/
				

				Draft draft = new Draft();
				draft.setId(String.valueOf(cursor.getInt(0)));
				draft.setUser_id(String.valueOf(cursor.getInt(1)));				
				draft.setIssue_type_id(String.valueOf(cursor.getInt(2)));
				draft.setIssue_type_name(cursor.getString(3));
				draft.setIssue_image(cursor.getString(4));
				draft.setIssue_image_bin(cursor.getString(5));
				draft.setIssue_description(cursor.getString(6));
				draft.setIssue_lat(cursor.getString(7));
				draft.setIssue_lon(cursor.getString(8));
				draft.setIssue_date(cursor.getString(9));
				draft.setIssue_Device_id(cursor.getString(10));
				draft.setIssue_app_version_code(cursor.getString(11));
				draft.setIssue_app_version_name(cursor.getString(12));
				draft.setIssue_address(cursor.getString(13));
				draft.setIssue_lang(cursor.getString(14));
				
				
				//Log.d("DEBUG", "Name: "+cursor.getString(2));
				// Adding user to list
				msgList.add(draft);


			} while (cursor.moveToNext());
			cursor.close();
		}
		
		db.close();
		return msgList;
		
	}
	
	
	

	public ArrayList<Cursor> getData(String Query){
		//get writable database
		SQLiteDatabase sqlDB = this.getWritableDatabase();
		String[] columns = new String[] { "mesage" };
		//an array list of cursor to save two cursors one has results from the query 
		//other cursor stores error message if any errors are triggered
		ArrayList<Cursor> alc = new ArrayList<Cursor>(2);
		MatrixCursor Cursor2= new MatrixCursor(columns);
		alc.add(null);
		alc.add(null);
		
		
		try{
			String maxQuery = Query ;
			//execute the query results will be save in Cursor c
			Cursor c = sqlDB.rawQuery(maxQuery, null);
			

			//add value to cursor2
			Cursor2.addRow(new Object[] { "Success" });
			
			alc.set(1,Cursor2);
			if (null != c && c.getCount() > 0) {

				
				alc.set(0,c);
				c.moveToFirst();
				
				return alc ;
			}
			return alc;
		} catch(SQLException sqlEx){
			Log.d("printing exception", sqlEx.getMessage());
			//if any exceptions are triggered save the error message to cursor an return the arraylist
			Cursor2.addRow(new Object[] { ""+sqlEx.getMessage() });
			alc.set(1,Cursor2);
			return alc;
		} catch(Exception ex){

			Log.d("printing exception", ex.getMessage());

			//if any exceptions are triggered save the error message to cursor an return the arraylist
			Cursor2.addRow(new Object[] { ""+ex.getMessage() });
			alc.set(1,Cursor2);
			return alc;
			
			
		}

		
	}
	
	
	
	
	// Getting single user
		public Draft getDraft(String id) {
			SQLiteDatabase db = this.getReadableDatabase();
			
			Draft draft = new Draft();
			
			/*
			 * public static final String KEY_ISSUE_ID = "id";
	public static final String KEY_USER_ID = "user_id";
	public static final String KEY_ISSUE_TYPE_ID = "issue_type_id";
	public static final String KEY_ISSUE_TYPE_NAME = "issue_type_name";
	public static final String KEY_ISSUE_IMAGE = "issue_image";
	public static final String KEY_ISSUE_IMAGE_BINARY = "issue_image_bin";
	public static final String KEY_ISSUE_DESC = "issue_description";
	public static final String KEY_ISSUE_LAT = "issue_lat";
	public static final String KEY_ISSUE_LON = "issue_lon";
	public static final String KEY_ISSUE_DATE = "issue_date";
	public static final String KEY_ISSUE_LANG = "issue_lang";
	
	
	*/
			

			Cursor cursor = db.query(TABLE_DRAFTS, new String[] { KEY_ISSUE_ID, KEY_USER_ID, KEY_ISSUE_TYPE_ID, KEY_ISSUE_TYPE_NAME, 
					KEY_ISSUE_IMAGE, KEY_ISSUE_IMAGE_BINARY, KEY_ISSUE_DESC, KEY_ISSUE_LAT, KEY_ISSUE_LON, KEY_ISSUE_DATE,KEY_ISSUE_DEVICEID,KEY_ISSUE_APP_VERSION_CODE,KEY_ISSUE_APP_VERSION_NAME,KEY_ISSUE_ADDRESS, KEY_ISSUE_LANG }, KEY_ISSUE_ID + "=?",
				new String[] { String.valueOf(id) }, null, null, null, null);
			
			if (cursor != null && cursor.getCount() > 0)
			{
				cursor.moveToFirst();
				
				
				
				draft.setId(String.valueOf(cursor.getInt(0)));
				draft.setUser_id(String.valueOf(cursor.getInt(1)));				
				draft.setIssue_type_id(String.valueOf(cursor.getInt(2)));
				draft.setIssue_type_name(cursor.getString(3));
				draft.setIssue_image(cursor.getString(4));
				draft.setIssue_image_bin(cursor.getString(5));
				draft.setIssue_description(cursor.getString(6));
				draft.setIssue_lat(cursor.getString(7));
				draft.setIssue_lon(cursor.getString(8));
				draft.setIssue_date(cursor.getString(9));
				draft.setIssue_Device_id(cursor.getString(10));
				draft.setIssue_app_version_code(cursor.getString(11));
				draft.setIssue_app_version_name(cursor.getString(12));
				draft.setIssue_address(cursor.getString(13));
				draft.setIssue_lang(cursor.getString(14));
				
			
				
			}


			
			cursor.close();
			db.close();
			// return user
			return draft;
		}
		
	
	////////////////////////////////// Common Usages //////////////////////////////////
	public void deleteSpecificTable(String tableName)
	{
		SQLiteDatabase db = this.getWritableDatabase();

		String deleteQuery = "delete from " + tableName;
		//	db.delete(tableName, null, null);
		db.execSQL(deleteQuery);

	}
	
	
	public boolean insertSpecificTable(String tableName, ContentValues contentvalue)
	{

		boolean returnInsertResponce = false;

		SQLiteDatabase db = this.getWritableDatabase();

		try
		{

			// Cursor cursor=db.query(UserProfile.TABLE_NAME, null, null, null,
			// null, null, null);

			db.insert(tableName, null, contentvalue);
			returnInsertResponce = true;
		}
		catch (SQLException e)
		{
			return false;
		}
		db.close();
		return returnInsertResponce;

	}

	
}
