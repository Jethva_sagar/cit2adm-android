package com.ivisionr.cit2adm.async;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import com.ivisionr.cit2adm.Util.Utils;
import com.ivisionr.cit2adm.Util.Webservicelink;
import com.ivisionr.cit2adm.interfaces.MyReportdelete;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class MyIssuedeleteAsync extends AsyncTask<String, Void, String> {
	private Context _context;
	private Activity actity;
	public MyReportdelete myreportdelete;
	private String errorcode="";
	public MyIssuedeleteAsync(Activity activity){
		this.actity = activity;
		this._context = activity;
		
	}
	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stu
		serch(params);
		return null;
	}
	private void serch(String[] params) {
		String reportid=params[0];
		String userid=params[1];
		 String result="";
		String url = Webservicelink.deleteissue;
		
		try {
			JSONObject registerJson = new JSONObject();
			registerJson.put("report_id", reportid);
			registerJson.put("user_id", userid);
			List<NameValuePair> namevaluepair = new ArrayList<NameValuePair>();
			namevaluepair.add(new BasicNameValuePair("data", registerJson
					.toString()));
			String paramString = URLEncodedUtils.format(namevaluepair, "utf-8");
			url += paramString;

			Log.d("kartickurluserdetails", "kartickurluserdetails" + url);
			HttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(url);
			HttpResponse response = null;
			try {
				response = httpClient.execute(httpPost);
			} catch (ClientProtocolException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				HttpEntity entity = response.getEntity();
				InputStream instream = entity.getContent();
				result = Utils.convertStreamToString(instream);	
				JSONObject jsonobjet=new JSONObject(result);
				errorcode=jsonobjet.getString("error_code");
				Log.d("cheking", "cheking" + result);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();

		}
		
		
	}
	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		if(errorcode.equals("S22000")){
			myreportdelete.onCompleted();
		}
	}
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		
	}

}
