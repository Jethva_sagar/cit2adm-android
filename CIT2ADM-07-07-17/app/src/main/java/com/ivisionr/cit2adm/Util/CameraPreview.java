package com.ivisionr.cit2adm.Util;

/**
 * @author Jose Davis Nidhin
 */

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.List;

import android.content.Context;
import android.content.res.Configuration;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.Size;
import android.os.Build;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;

public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {
	private SurfaceHolder mHolder;
	private Camera mCamera;
	 List<Size> mSupportedPreviewSizes;

	public CameraPreview(Context context, Camera camera) {
		super(context);
		mCamera = camera;

		// Install a SurfaceHolder.Callback so we get notified when the
		// underlying surface is created and destroyed.
		mHolder = getHolder();
		mHolder.addCallback(this);
		// deprecated setting, but required on Android versions prior to 3.0
		mHolder.setType(SurfaceHolder.SURFACE_TYPE_NORMAL);
	}

	public void surfaceCreated(SurfaceHolder holder) {
		// The Surface has been created, now tell the camera where to draw the preview.
		try {
			mCamera.setPreviewDisplay(holder);
			mCamera.startPreview();
		} catch (IOException e) {

		}
	}

	public void surfaceDestroyed(SurfaceHolder holder) {
		// empty. Take care of releasing the Camera preview in your activity.
	}

	public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
		// If your preview can change or rotate, take care of those events here.
		// Make sure to stop the preview before resizing or reformatting it.

		if (mHolder.getSurface() == null){
			// preview surface does not exist
			return;
		}

		// stop preview before making changes
		try {
			mCamera.stopPreview();
		} catch (Exception e){
			// ignore: tried to stop a non-existent preview
		}

		// set preview size and make any resize, rotate or
		// reformatting changes here

		// start preview with new settings
		try {
			mCamera.setPreviewDisplay(mHolder);
			mCamera.startPreview();

		} catch (Exception e){

		}
	}

	public void setCamera(Camera camera) {
    	mCamera = camera;
    	if (mCamera != null) {
    		mSupportedPreviewSizes = mCamera.getParameters().getSupportedPreviewSizes();
    		requestLayout();

    		// get Camera parameters
    		Camera.Parameters params = mCamera.getParameters();
    		params.setFlashMode(Parameters.FLASH_MODE_TORCH);
    		
    		
    		if (Integer.parseInt(Build.VERSION.SDK) >= 8)
    	        setDisplayOrientation(mCamera, 90);
    	    else
    	    {
    	        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
    	        {
    	        	params.set("orientation", "portrait");
    	        	params.set("rotation", 90);
    	        }
    	        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
    	        {
    	        	params.set("orientation", "landscape");
    	        	params.set("rotation", 90);
    	        }
    	    }   
    		
    		
    		//params.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
			//mCamera.setParameters(params);
    		
    	/*	if (getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE)                         {
    			params.set("orientation", "portrait");
                camera.setDisplayOrientation(0);
                params.setRotation(0);
           }
                else {
                	params.set("orientation", "landscape");
                     camera.setDisplayOrientation(90);
                     params.setRotation(90);
           }*/
    		
    		
    		List<String> flashModes = params.getSupportedFlashModes();
    		if (flashModes.contains(android.hardware.Camera.Parameters.FLASH_MODE_AUTO))
    		{
    			params.setFlashMode(Parameters.FLASH_MODE_AUTO);
    		}
    		mCamera.setParameters(params);
    		mCamera.startPreview();

    		/*List<String> focusModes = params.getSupportedFocusModes();
    		//params.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
    		//String flashmode=params.getFlashMode();
    		//Log.d("checkflas", "checkflash" +flashmode);
    		
    		Log.d("checkfocusmode", "checkfocusmode" +focusModes);
    		if (focusModes.contains(Camera.Parameters.FOCUS_MODE_FIXED)) {
    			// set the focus mode
    			params.setFocusMode(Camera.Parameters.FOCUS_MODE_FIXED); 
    			mCamera.setParameters(params);
    			mCamera.startPreview();
    			
    		}*/
    	}
    }

	protected void setDisplayOrientation(Camera camera, int angle){
    Method downPolymorphic;
    try
    {
        downPolymorphic = camera.getClass().getMethod("setDisplayOrientation", new Class[] { int.class });
        if (downPolymorphic != null)
            downPolymorphic.invoke(camera, new Object[] { angle });
    }
    catch (Exception e1)
    {
    }
}

}