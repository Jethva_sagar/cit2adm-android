package com.ivisionr.cit2adm.database;

public class DraftIssue
{
	
	public static final String TABLE_NAME = "myDraftIssues";
	public static final String KEY_ID = "id";
	public static final String KEY_USER_ID = "user_id";
	public static final String KEY_ISSUE_TYPE_ID = "issue_type_id";
	public static final String KEY_ISSUE_TYPE_NAME = "issue_type_name";
	public static final String KEY_ISSUE_IMAGE = "issue_image";
	public static final String KEY_ISSUE_IMAGE_BINARY = "issue_image_bin";
	public static final String KEY_ISSUE_DESC = "issue_description";
	public static final String KEY_ISSUE_LAT = "issue_lat";
	public static final String KEY_ISSUE_LON = "issue_lon";
	public static final String KEY_ISSUE_DATE = "issue_date";
	public static final String KEY_ISSUE_LANG = "issue_lang";

	

}
