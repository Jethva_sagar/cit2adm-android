package com.ivisionr.cit2adm;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.location.Address;
import android.location.Geocoder;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.provider.Settings.Secure;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ivisionr.cit2adm.Util.GPSTracker;
import com.ivisionr.cit2adm.Util.HttpUpload;
import com.ivisionr.cit2adm.Util.SharePreferenceClass;
import com.ivisionr.cit2adm.Util.Utils;
import com.ivisionr.cit2adm.async.GetCurrentaddressAsync;
import com.ivisionr.cit2adm.async.ReportAsync;
import com.ivisionr.cit2adm.bin.Draft;
import com.ivisionr.cit2adm.constant.Constant;
import com.ivisionr.cit2adm.database.DatabaseHandler;
import com.ivisionr.cit2adm.interfaces.CurrentaddressInterface;
import com.ivisionr.cit2adm.interfaces.ReportInterface;
import com.ivisionr.cit2adm.interfaces.UploadImagetoServerInterface;

public class SafelifeReportPost extends Activity implements ReportInterface,
		UploadImagetoServerInterface, CurrentaddressInterface {

	private TextView maplatview, maplonview, issuetype;
	private RelativeLayout mapscreen;
	private ImageView mapimage;
	private ImageView cameraImage;
	private EditText description;
	private Bitmap captureImage = null;
	private GPSTracker gpsTracker;
	static double currlat, currentlong;
	private Button CameraButton;
	private static int REQUEST_IMAGE_CAPTURE = 1;
	private static String descriptiontoupload = "";
	Activity activity;
	private String userid = "";
	private String issuetype_id = "";
	private String issuetypename = "";
	private Button sendReport;
	private static String mImageName = "";
	private SharePreferenceClass sharedPreference;
	private ProgressDialog mUProgressDialog; // / mProgressDialog
	private ProgressDialog camprogressdialog;
	SimpleDateFormat dateFormat;
	private String issueimage = "";
	final static int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1;
	Uri imageUri = null;
	private Camera camera;
	private Button backbutton;
	private RelativeLayout cameraclick;
	private Button cancelinReport;
	private Button saveReport;
	// Dialog dialog = null;
	Date pdate = null;
	String created = "";
	String versionName = "", versionCode = "", membershipExpiry = "";
	private Bitmap decodedByte = null;
	private String language = "";
	DatabaseHandler db;
	Bitmap myBitmap = null;
	// private LoginDatabaseAdapter login;
	ProgressDialog dialog;
	private String decodebytestring = "";
	private String draftlat = "", draftlon = "";
	private String status = "0";
	private static String imgPath = "";
	final private int CAPTURE_IMAGE = 2;
	List<Draft> draftList;
	Dialog alert_dialog;
	List<HashMap<String, String>> places = null;
	List<Address> addresses = null;
	ImageView gpsiamge;
    ProgressDialog progressDialog;
    private Date expiredate;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		sharedPreference = new SharePreferenceClass(this);
		// mProgressDialog=new ProgressDialog(this);
		camprogressdialog = new ProgressDialog(this);
		mUProgressDialog = new ProgressDialog(this);
        progressDialog  = new ProgressDialog(this);
		// login = new LoginDatabaseAdapter(this);
		// login = login.open();
		places = new ArrayList<HashMap<String, String>>();

		dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		pdate = new Date();
		created = dateFormat.format(pdate);

		db = new DatabaseHandler(this);

		camprogressdialog.setMessage(getResources().getText(
				R.string.loadingcamera));
		mUProgressDialog.setMessage(getResources().getText(R.string.uploading));
        progressDialog.setMessage("Please wait...");
		Bundle b = getIntent().getExtras();

		if (getIntent().hasExtra("userid")) {
			userid = b.getString("userid");
		}
		if (getIntent().hasExtra("issutypeid")) {
			issuetype_id = b.getString("issutypeid");
		}
		if (getIntent().hasExtra("issutypename")) {
			issuetypename = b.getString("issutypename");
		}

		alert_dialog = new Dialog(this);
		alert_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		alert_dialog.setContentView(R.layout.dialog_alert);
		alert_dialog.show();
		TextView alert_msg = (TextView) alert_dialog
				.findViewById(R.id.alert_msg);
		Button alert_ok = (Button) alert_dialog.findViewById(R.id.alert_ok);

		alert_msg.setText(R.string.votes);

		alert_ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				alert_dialog.dismiss();
				// finish();
			}
		});

		try {

			versionName = getPackageManager().getPackageInfo(getPackageName(),
					0).versionName;
			versionCode = String.valueOf(getPackageManager().getPackageInfo(
					getPackageName(), 0).versionCode);

		} catch (Exception ex) {
		}

		// Log.d("DEBUG", "Inside Language Saved: "+language);

//		language = MobileUtils.getSavedPref(this,
//				SystemConstants.TXT_PERSIST_LANGUAGE);
//		Log.d("DEBUG", "Inside Language Saved: " + language);
//		/**
//		 * Again language set for app language reset
//		 */
//		String lang = "en_us";
//		String loca = "en";
//		if (language.startsWith("fran")) {
//			language = "fra";
//			lang = "fr_fr";
//			loca = "fr";
//		} else if (language.startsWith("espa")) {
//			language = "esp";
//			lang = "es_es";
//			loca = "es";
//		} else if (language.equals("Deutsch")) {
//			language = "deu";
//			lang = "de_de";
//			loca = "de";
//		} else
//			language = "eng";
//
//		/**
//		 * Again language set for app language reset
//		 */
//
//		Locale locale = new Locale(loca);
//		Locale.setDefault(locale);
//		Configuration config = new Configuration();
//		config.locale = locale;
//		getBaseContext().getResources().updateConfiguration(config,
//				getBaseContext().getResources().getDisplayMetrics());
//		getIntent().putExtra("language", lang);

		if (getIntent().hasExtra("issuetype")) {
			issuetype_id = b.getString("issuetype");

		}

		try {

			membershipExpiry = new SharePreferenceClass(getApplicationContext())
					.getMembershipExpiry();

			if (membershipExpiry != null && membershipExpiry != "") {
				long millisecond1 = pdate.getTime();
				Date expDate = dateFormat.parse(membershipExpiry);
				long millisecond2 = expDate.getTime();

				Log.d("DEBUG", "Millisec 1: " + millisecond1);
				Log.d("DEBUG", "Millisec 2: " + millisecond2);

				long millsec = millisecond2 - millisecond1;

				Log.d("DEBUG", "Diff in Millisec: " + millsec);

				if (millsec < 0) {

					Toast toast = Toast.makeText(SafelifeReportPost.this,
							R.string.membershipexpired, Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();

					Intent intent = new Intent(SafelifeReportPost.this,
							GoomapActivity.class);
					startActivity(intent);
				} else {
					new SharePreferenceClass(getApplicationContext())
							.savestatus("1");
					status = "1";
				}

			}

			Log.d("DEBUG", "Membership Expiry: " + membershipExpiry);

		} catch (Exception ex) {
		}

		activity = this;
		setContentView(R.layout.postreport);
		getDetails();

		issuetype.setText(issuetypename);

		status = new SharePreferenceClass(SafelifeReportPost.this).getstatus();




        //2016-11-23 10:05:00
        String isMamberExpire = new SharePreferenceClass(SafelifeReportPost.this).getMembershipExpiry();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            expiredate = format.parse(isMamberExpire);

        } catch (ParseException e) {
            e.printStackTrace();
        }




        // status="1";

		/*
		 * if(null !=descriptiontoupload && null !=cameraImage){
		 * description.setText(descriptiontoupload);
		 * cameraImage.setImageBitmap(captureImage);
		 * 
		 * } if(issueimage.length()>0){ CameraButton.setEnabled(false); }
		 */

		gpsTracker = new GPSTracker(this);
		currlat = gpsTracker.getLatitude();
		currentlong = gpsTracker.getLongitude();

		Log.d("DEBUG", "My Current Lat: " + currlat);
		Log.d("DEBUG", "My Current Lon: " + currentlong);

		/*
		 * if(null !=decodedByte){ //decodedByte1 =
		 * Bitmap.createScaledBitmap(decodedByte,
		 * cameraImage.getWidth(),cameraImage.getHeight(),true);
		 * cameraImage.setImageBitmap(decodedByte); }
		 */

		userid = sharedPreference.getUserid();

		if (Utils.checkConnectivity(SafelifeReportPost.this)) {
			Log.d("DEBUG", "Inside current address");
			GetCurrentaddressAsync currentaddress = new GetCurrentaddressAsync(
					SafelifeReportPost.this);
			currentaddress.currentaddressInterface = this;
			currentaddress.execute(String.valueOf(currlat),
					String.valueOf(currentlong));

		} else {
			showNetworkDialog("internet");
		}
		backbutton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				SafelifeReportPost.this.finish();
			}
		});
		cancelinReport.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				AlertDialog.Builder builder = new AlertDialog.Builder(
						SafelifeReportPost.this);
				builder.setTitle("CIT2ADM");
				builder.setMessage(getResources().getString(
						R.string.cancelreport));
				builder.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface arg0, int arg1) {
								if (Utils
										.checkConnectivity(SafelifeReportPost.this)) {
									startActivity(new Intent(
											SafelifeReportPost.this,
											GoomapActivity.class));
									finish();
								} else {
									showNetworkDialog("internet");
								}

							}

						});
				builder.setNegativeButton("No",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub

							}
						});
				AlertDialog alert = builder.create();
				alert.show();

			}
		});
		description.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				SharePreferenceClass.setPreferences(activity, "description",
						s.toString());

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

		saveReport.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				// status = "1";

				// if(status.equals("1")){
				AlertDialog.Builder builder = new AlertDialog.Builder(
						SafelifeReportPost.this);
				builder.setTitle(getResources().getString(R.string.postreport));
				builder.setMessage(getResources()
						.getString(R.string.savereport));
				builder.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface arg0, int arg1) {
								descriptiontoupload = description.getText()
										.toString();
								Log.d("DEBUG", "Upload Image Path: " + imgPath);
								if (descriptiontoupload.length() > 0) {

								} else {
									descriptiontoupload = SharePreferenceClass
											.getPreferences(activity,
													"description");
								}
								if (mImageName.length() > 0) {

								} else {
									mImageName = SharePreferenceClass
											.getPreferences(activity,
													"mImageName");
								}
								if (mImageName.length() > 0
										&& !mImageName.equals("0")) {

								} else {
									String[] split = imgPath.split("/");
									StringBuilder sb = new StringBuilder();

									mImageName = split[split.length - 1];

									Log.d("DEBUG", "Image Name: " + mImageName);
								}
								Log.d("DEBUG", "descriptiontoupload: "
										+ descriptiontoupload + "+++++"
										+ "upload");

								Log.d("DEBUG", "Image Name: " + mImageName);
								if (descriptiontoupload.length() > 0
										&& mImageName.length() > 0
										&& !mImageName.equals("0")) {
									Log.d("DEBUG", "My Image Name: "
											+ mImageName);
									Toast toast = Toast.makeText(
											SafelifeReportPost.this,
											R.string.savesuccessfull,
											Toast.LENGTH_SHORT);
									toast.setGravity(Gravity.CENTER, 0, 0);
									toast.show();
									/**
									 * getting app version name and app version
									 * code
									 */
									PackageManager manager = SafelifeReportPost.this
											.getPackageManager();
									PackageInfo info = null;
									try {
										info = manager.getPackageInfo(
												SafelifeReportPost.this
														.getPackageName(), 0);
										Log.d("DEBUG", info.versionName
												+ "+++++++++++++++"
												+ info.versionName);
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}

									/***
									 * getting device unique id
									 */
									String device_id = Secure.getString(
											SafelifeReportPost.this
													.getContentResolver(),
											Secure.ANDROID_ID);
									Log.d("DEBUG", "Photo Bitmap: "
											+ BitMapToString(decodedByte));

									Log.d("DEBUG", "Saving...");
									Draft draft = new Draft();
									draft.setUser_id(userid);
									draft.setIssue_type_id(issuetype_id);
									draft.setIssue_type_name(issuetypename);
									draft.setIssue_description(descriptiontoupload);
									draft.setIssue_lat(String.valueOf(currlat));
									draft.setIssue_lon(String
											.valueOf(currentlong));
									draft.setIssue_image(imgPath);
									draft.setIssue_image_bin("");
									draft.setIssue_date(created);
									draft.setIssue_lang(language);
									draft.setIssue_Device_id(device_id);
									draft.setIssue_app_version_code(""
											+ info.versionCode);
									draft.setIssue_app_version_name(info.versionName);
									draft.setIssue_address(maplatview.getText()
											.toString());
									db.addDraft(draft);
									finish();
								} else {
									Toast toast = Toast.makeText(
											SafelifeReportPost.this,
											R.string.writedescriptionandpick,
											Toast.LENGTH_SHORT);
									toast.setGravity(Gravity.CENTER, 0, 0);
									toast.show();
								}

							}
						});
				builder.setNegativeButton("No",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub

							}
						});
				AlertDialog alert = builder.create();
				alert.show();

				/*
				 * }else{ Toast toast= Toast.makeText(getApplicationContext(),
				 * R.string.statuscheck, Toast.LENGTH_LONG);
				 * toast.setGravity(Gravity.CENTER, 0, 0); toast.show(); }
				 */
			}
		});

		cameraclick.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

                //Toast.makeText(getApplicationContext(),"Camera click", Toast.LENGTH_LONG).show();
				// camprogressdialog.show();

				mImageName = "";

				/*
				 * Intent cameraIntent = new
				 * Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
				 * cameraUri = setImageUri();
				 * 
				 * startActivityForResult(cameraIntent, CAPTURE_IMAGE);
				 */

				// Uri cameraUri = setImageUri();
				final Intent intent = new Intent( MediaStore.ACTION_IMAGE_CAPTURE);
				intent.putExtra(MediaStore.EXTRA_OUTPUT, getTempPhotoPath());
				startActivityForResult(intent, CAPTURE_IMAGE);

			}
		});

		sendReport.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				// mImageName=System.currentTimeMillis()+".jpg";

				/*
				 * if(null !=decodedByte){ storeImage(decodedByte);
				 * 
				 * }
				 */

				Log.d("DEBUG", "Upload Image Path: " + imgPath);

				if (status.equals("1")) {

                    if (new Date().before(expiredate)) {


                        // imgPath = getImagePath();
                        Log.d("DEBUG", "Upload Image Path: " + imgPath);

                        if (!imgPath.equalsIgnoreCase("")) {
                            new HttpUpload(getApplicationContext(), imgPath)
                                    .execute();

                            String[] split = imgPath.split("/");
                            StringBuilder sb = new StringBuilder();

                            mImageName = split[split.length - 1];

                            Log.d("DEBUG", "Image Name: " + mImageName);

                        }

                        descriptiontoupload = description.getText().toString();
                        if (descriptiontoupload.length() > 0) {

                        } else {
                            descriptiontoupload = SharePreferenceClass
                                    .getPreferences(activity, "description");
                        }
                        if (mImageName.length() > 0) {

                        } else {
                            mImageName = SharePreferenceClass.getPreferences(
                                    activity, "mImageName");
                        }
                        Log.d("DEBUG", "descriptiontoupload: "
                                + descriptiontoupload + "+++++" + "upload");

                        Log.d("DEBUG", "Image Name Len: " + mImageName.length());

                        if (descriptiontoupload.length() > 0
                                && mImageName.length() > 0
                                && !mImageName.equals("0")) {

                            Log.d("DEBUG", "My Image Name: " + mImageName);
                            if (Utils.checkConnectivity(activity)) {
                                // mUProgressDialog.show();
                                mUProgressDialog.setCancelable(false);
                                mUProgressDialog.show();
                                /**
                                 * getting app version name and app version code
                                 */
                                PackageManager manager = SafelifeReportPost.this
                                        .getPackageManager();
                                PackageInfo info = null;
                                try {
                                    info = manager.getPackageInfo(
                                            SafelifeReportPost.this
                                                    .getPackageName(), 0);
                                    Log.d("DEBUG", info.versionName
                                            + "+++++++++++++++" + info.versionName);
                                } catch (Exception e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                                String deviceid = new SharePreferenceClass(
                                        getApplicationContext()).getDeviceId();
                                Log.d("DEBUG", "Device Id: " + deviceid);
                                ReportAsync reportasync = new ReportAsync(activity);
                                reportasync.reportinterface = SafelifeReportPost.this;

                                reportasync.execute(userid, issuetype_id,
                                        mImageName, descriptiontoupload,
                                        String.valueOf(currlat),
                                        String.valueOf(currentlong), language,
                                        imgPath, deviceid, "" + info.versionCode,
                                        info.versionName);

                                SharePreferenceClass.setPreferences(activity,
                                        "description", "");

                                SharePreferenceClass.setPreferences(activity,
                                        "mImageName", "");

                                // mUProgressDialog.hide();

                            } else {

                                Draft draft = new Draft();
                                draft.setUser_id(userid);
                                draft.setIssue_type_id(issuetype_id);
                                draft.setIssue_type_name(issuetypename);
                                draft.setIssue_description(descriptiontoupload);
                                draft.setIssue_lat(String.valueOf(currlat));
                                draft.setIssue_lon(String.valueOf(currentlong));
                                draft.setIssue_image("");
                                draft.setIssue_image_bin("");
                                draft.setIssue_date(created);
                                draft.setIssue_lang(language);
                                db.addDraft(draft);

                                // login.insertEntry(userid, issuetype_id,
                                // issuetypename, mImageName, descriptiontoupload,
                                // String.valueOf(currlat),
                                // String.valueOf(currentlong),
                                // BitMapToString(decodedByte), created, language);

                                Toast toast = Toast.makeText(
                                        getApplicationContext(),
                                        R.string.poornetworkreportnotsent,
                                        Toast.LENGTH_LONG);
                                toast.setGravity(Gravity.CENTER, 0, 0);
                                toast.show();
                                finish();
                            }
                        } else {
                            Toast toast = Toast.makeText(getApplicationContext(),
                                    R.string.writedescriptionandpick,
                                    Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        }
                    } else {
                        Toast toast = Toast.makeText(getApplicationContext(),
                                R.string.membershipexpired, Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }

				} else {
					Toast toast = Toast.makeText(getApplicationContext(),
							R.string.statuscheck, Toast.LENGTH_LONG);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}
			}

		});

		mapscreen.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent intent = new Intent(SafelifeReportPost.this,
						MyReportCurrentLocation.class);
				startActivity(intent);

			}
		});
	}

	/**
	 * get the temp photo URI
	 * @return
	 */
	private Uri getTempPhotoPath() {



		File imagesFolder = new File(Constant.APPLICATION_DIRECTORY);

		if (!imagesFolder.exists() && !imagesFolder.isDirectory())
			imagesFolder.mkdirs();

		/**
		 * Store the capture photo  as a "kWh360Temp.jpg" photo into application directory
		 */
		File output = new File(imagesFolder, Constant.PHOTO);
		this.imgPath = output.getAbsolutePath();
		SharePreferenceClass.setPreferences(activity, "imgPath", output.getAbsolutePath());
		return Uri.fromFile(output);
	}

    private File getTempFilePhotoPath() {

        File imagesFolder = new File(Constant.APPLICATION_DIRECTORY+Constant.PHOTO);
         if(imagesFolder.exists())
            return imagesFolder;
        else
            return null;
    }



	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        String sourcePicturePath = null;

		if (resultCode == Activity.RESULT_OK) {

			if (requestCode == CAPTURE_IMAGE) {

                if (getTempFilePhotoPath() != null && getTempFilePhotoPath().getPath().length() > 0) {

                    sourcePicturePath = getTempFilePhotoPath().getPath();

                } else {

                    sourcePicturePath = getRealPathFromURI(getTempPhotoPath());
                }

                if(sourcePicturePath != null)
                    myBitmap = BitmapFactory.decodeFile(sourcePicturePath);

                if(myBitmap == null)
                    myBitmap = getLastCapturedImageBitMap();

                if(myBitmap == null && data != null)
                    myBitmap = (Bitmap) data.getExtras().get("data");

                cameraImage.setImageBitmap(myBitmap);

				imgPath = getImagePath();

				/*if (imgPath.equals("")) {
					imgPath = SharePreferenceClass.getPreferences(activity,"imgPath");
				}

				Log.d("imgPath", imgPath);

				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inJustDecodeBounds = true;
                options.inJustDecodeBounds = false;
                options.inSampleSize = 0;

				myBitmap = BitmapFactory.decodeFile(imgPath, options);

				if(myBitmap == null && data != null)
					myBitmap = (Bitmap) data.getExtras().get("data");


                if(myBitmap == null)
                    myBitmap = getLastCapturedImageBitMap();

				// myBitmap = (Bitmap) data.getExtras().get("data");
				*//*options.inJustDecodeBounds = false;
				options.inSampleSize = 0;

				if(myBitmap == null)
                    myBitmap = BitmapFactory.decodeFile(imgPath, options);*//*

				try {

					ExifInterface exif = new ExifInterface(imgPath);
					int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
					Log.d("EXIF", "Exif: " + orientation);
					Matrix matrix = new Matrix();

					if (orientation == 6) {
						matrix.postRotate(90);
					} else if (orientation == 3) {
						matrix.postRotate(180);
					} else if (orientation == 8) {
						matrix.postRotate(270);
					}

					if (orientation > 0) {

						myBitmap = Bitmap.createBitmap(myBitmap, 0, 0,
								myBitmap.getWidth(), myBitmap.getHeight(),
								matrix, true); // rotating bitmap

						Uri imgUri = getImageUri(this, myBitmap);
						imgPath = getRealPathFromURI(imgUri);
						SharePreferenceClass.setPreferences(activity, "mImageName", getName(imgUri));
						mImageName = getName(imgUri);

						// Log.d("DEBUG", "My Image Name: "+mImageName);
						// Log.d("DEBUG", "My Image Bitmap: "+myBitmap);

						int height = myBitmap.getHeight(), width = myBitmap.getWidth();

                        if (height > 1280 && width > 960) {

							Bitmap myThumbBitmap = BitmapFactory.decodeFile(imgPath, options);

							//cameraImage.setImageBitmap(myThumbBitmap);

						} else {

							//cameraImage.setImageBitmap(myBitmap);

						}

					} else {

						int height = myBitmap.getHeight(), width = myBitmap.getWidth();
						if (height > 1280 && width > 960) {

                            if(imgPath.length() > 0) {

                                Bitmap myThumbBitmap = BitmapFactory.decodeFile(imgPath, options);
                                //cameraImage.setImageBitmap(myThumbBitmap);

                            } else {

                                //cameraImage.setImageBitmap(myBitmap);
                            }

						} else {

							//cameraImage.setImageBitmap(myBitmap);

						}
					}

				} catch (Exception e) {

                    e.printStackTrace();
				}*/

			} else {

				super.onActivityResult(requestCode, resultCode, data);
			}
		}
		// camprogressdialog.hide();
		SharePreferenceClass.setPreferences(activity, "imgPath", "0");
	}

    private Bitmap getLastCapturedImageBitMap() {

        // Find the last picture
        String[] projection = new String[]{
                MediaStore.Images.ImageColumns._ID,
                MediaStore.Images.ImageColumns.DATA,
                MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME,
                MediaStore.Images.ImageColumns.DATE_TAKEN,
                MediaStore.Images.ImageColumns.MIME_TYPE
        };
        final Cursor cursor = getContentResolver()
                .query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection, null,
                        null, MediaStore.Images.ImageColumns.DATE_TAKEN + " DESC");

        // Put it in the image view
        if (cursor.moveToFirst()) {
            String imageLocation = cursor.getString(1);
            File imageFile = new File(imageLocation);

            if (imageFile.exists()) {   // TODO: is there a better way to do this?
                return BitmapFactory.decodeFile(imageLocation);
            }
        }
        return  null;

    }

	private Bitmap converStringtobitmap(String encodedString) {
		byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
		Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0,
				encodeByte.length);
		return bitmap;
	}

	private String BitMapToString(Bitmap image) {

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		if (image != null) {
			image.compress(Bitmap.CompressFormat.PNG, 100, baos);
		}
		byte[] b = baos.toByteArray();
		String temp = Base64.encodeToString(b, Base64.DEFAULT);

		return temp;
	}

	@SuppressWarnings("deprecation")
	public void showNetworkDialog(final String message) {

		// final exit of application
		AlertDialog.Builder builder = new AlertDialog.Builder(
				SafelifeReportPost.this);
		builder.setTitle(getResources().getString(R.string.app_name));
		if (message.equals("gps")) {
			builder.setMessage(getResources().getString(R.string.gps_message));
		} else if (message.equals("internet")) {
			builder.setMessage(getResources().getString(R.string.net_message));
		}
		AlertDialog alert = builder.create();
		alert.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				if (message.equals("gps")) {
					Intent i = new Intent(
							android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
					startActivityForResult(i, 1);
				} else if (message.equals("internet")) {
					Intent i = new Intent(
							android.provider.Settings.ACTION_WIRELESS_SETTINGS);
					startActivityForResult(i, 2);
					Intent i1 = new Intent(
							android.provider.Settings.ACTION_WIFI_SETTINGS);
					startActivityForResult(i1, 3);
				} else {
					// do nothing
				}
			}
		});
		// Showing Alert Message
		alert.show();
	}

	protected void startDialog() {
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
			startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
		}

	}

	private void getDetails() {
		issuetype = (TextView) findViewById(R.id.issuetype);
		maplatview = (TextView) findViewById(R.id.locationlat);
		maplonview = (TextView) findViewById(R.id.locationlon);
		// mapImage=(TextView)findViewById(R.id.mapinreport);
		cameraclick = (RelativeLayout) findViewById(R.id.reportcam);
		cameraImage = (ImageView) findViewById(R.id.cameraview);
		description = (EditText) findViewById(R.id.reportdescription);
		sendReport = (Button) findViewById(R.id.sendReport);
		mapscreen = (RelativeLayout) findViewById(R.id.mapintxtreport);
		backbutton = (Button) findViewById(R.id.backinreporttopost);
		cancelinReport = (Button) findViewById(R.id.cancelbutton);
		saveReport = (Button) findViewById(R.id.savereport);
		gpsiamge = (ImageView) findViewById(R.id.gpsiamge);
		gpsiamge.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {

                  progressDialog.show();
				  // TODO Auto-generated method stub
				  final Intent poke = new Intent();
			        poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
			        poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
			        poke.setData(Uri.parse("3"));
			        sendBroadcast(poke);
			        if (Utils.checkConnectivity(SafelifeReportPost.this)) {
						Log.d("DEBUG", "Inside current address");
						GetCurrentaddressAsync currentaddress = new GetCurrentaddressAsync(
								SafelifeReportPost.this);
						currentaddress.currentaddressInterface = SafelifeReportPost.this;
						currentaddress.execute(String.valueOf(currlat),
								String.valueOf(currentlong));

					} else {
						showNetworkDialog("internet");
					}
			}
		});
		
	}

	@Override
	public void onCurrentAddress(String address) {
		// TODO Auto-generated method stub
		// Log.d();
		// address = "";
		if (address == "") {
			try {
				Geocoder geo = new Geocoder(
						SafelifeReportPost.this.getApplicationContext(),
						Locale.getDefault());
				addresses = geo.getFromLocation(currlat, currentlong, 1);
				// addresses.clear();
				if (addresses.size() > 0) {

					if (addresses.get(0).getFeatureName() != null)
						address = addresses.get(0).getFeatureName().toString();

					if (addresses.get(0).getPremises() != null)
						address += " " + addresses.get(0).getPremises();

					// if(addresses.get(0).getAddressLine(0) != null)
					// address += "Line 0: " +
					// addresses.get(0).getAddressLine(0);

					if (addresses.get(0).getAddressLine(1) != null)
						address += " " + addresses.get(0).getAddressLine(1);

					// if(addresses.get(0).getAddressLine(2) != null)
					// address += "Line 2: " +
					// addresses.get(0).getAddressLine(2);

					/*
					 * if(addresses.get(0).getSubLocality() != null) address +=
					 * " " + addresses.get(0).getSubLocality();
					 */

					if (addresses.get(0).getLocality() != null)
						address += " " + addresses.get(0).getLocality();

					/*
					 * if(addresses.get(0).getSubAdminArea() != null) address +=
					 * " " + addresses.get(0).getSubAdminArea();
					 */

					if (addresses.get(0).getAdminArea() != null)
						address += " " + addresses.get(0).getAdminArea();

					if (addresses.get(0).getPostalCode() != null)
						address += " " + addresses.get(0).getPostalCode();

					if (addresses.get(0).getCountryName() != null)
						address += " " + addresses.get(0).getCountryName();

					Log.d("DEBUG", "Address: " + address);

				} else {

					String url = "https://maps.googleapis.com/maps/api/geocode/json?";

					String address1 = "latlng=" + currlat + "," + currentlong;

					String sensor = "sensor=false";

					// url , from where the
					// geocoding data is fetched
					url = url + address1 + "&" + sensor;

					// Instantiating
					// DownloadTask to
					// get places from Google
					// Geocoding service
					// in a non-ui thread
					Log.d("geocodeurl", url);
					DownloadTask downloadTask = new DownloadTask();

					// Start downloading the
					// geocoding places
					downloadTask.execute(url);

				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				String url = "https://maps.googleapis.com/maps/api/geocode/json?";

				String address1 = "latlng=" + currlat + "," + currentlong;

				String sensor = "sensor=false";

				// url , from where the
				// geocoding data is fetched
				url = url + address1 + "&" + sensor;

				// Instantiating
				// DownloadTask to
				// get places from Google
				// Geocoding service
				// in a non-ui thread
				Log.d("geocodeurl", url);
				DownloadTask downloadTask = new DownloadTask();

				// Start downloading the
				// geocoding places
				downloadTask.execute(url);
                progressDialog.dismiss();
			}

		}

        Log.i("===> Addresss <===",address);
		maplatview.setText(address);
        progressDialog.dismiss();

	}

	@Override
	public void onuploadCompleted() {
		// TODO Auto-generated method stub
		Toast toast = Toast.makeText(getApplicationContext(),
				R.string.successfully_posted_post, Toast.LENGTH_LONG);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();

	}

	@Override
	public void onReportpostastarted() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onReportpostcompleted() {
		// TODO Auto-generated method stub

		// mProgressDialog.dismiss();
		// new ImageGalleryTask().execute();

		// UploadImagetoServer uploadimagetoServer=new
		// UploadImagetoServer(ReportPostToServer.this);
		// uploadimagetoServer.uploadtoserver=ReportPostToServer.this;
		// uploadimagetoServer.execute(image_path);

		Toast toast = Toast.makeText(getApplicationContext(),
				R.string.successfully_posted_post, Toast.LENGTH_LONG);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();
		finish();

	}

	@Override
	public void onReportnetworkissue() {
		// TODO Auto-generated method stub

	}

	// Get the path from Uri
	public String getPath(Uri uri) {
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = managedQuery(uri, projection, null, null, null);
		startManagingCursor(cursor);
		int column_index = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}

	// Get the path from Uri
	public String getName(Uri uri) {

		return uri.getLastPathSegment();
	}

	public Uri getImageUri(Context inContext, Bitmap inImage) {
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
		String path = Images.Media.insertImage(inContext.getContentResolver(),
				inImage, "Title", null);
		return Uri.parse(path);
	}

	public String getRealPathFromURI(Uri uri) {
		Cursor cursor = getContentResolver().query(uri, null, null, null, null);
		cursor.moveToFirst();
		int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
		return cursor.getString(idx);
	}

	public Uri setImageUri() {

		// Store image in dcim
		File file = new File(Environment.getExternalStorageDirectory()+ "/DCIM/", +new Date().getTime() + ".jpg");
		Uri imgUri = Uri.fromFile(file);
		this.imgPath = file.getAbsolutePath();
		SharePreferenceClass.setPreferences(activity, "imgPath", file.getAbsolutePath());
		return imgUri;
	}

	 
	public String getImagePath() {
		return imgPath;
	}

	public void SetImagePath(String imgpath) {
		SafelifeReportPost.imgPath = imgpath;
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		// initilizeMap();

		/*
		 * Locale current = getResources().getConfiguration().locale;
		 * 
		 * Log.d("DEBUG", "GoomapActivity Default Locale String - "+current);
		 * 
		 * Log.d("DEBUG",
		 * "GoomapActivity Default Country String - "+current.getDisplayCountry
		 * ()); Log.d("DEBUG",
		 * "GoomapActivity Default Display String - "+current.getDisplayName());
		 * Log.d("DEBUG",
		 * "GoomapActivity Default Language String - "+current.getDisplayLanguage
		 * ());
		 * 
		 * 
		 * 
		 * Locale locale = new Locale(current.toString());
		 * Locale.setDefault(locale); Configuration config = new
		 * Configuration(); config.locale = locale;
		 * getBaseContext().getResources().updateConfiguration(config,
		 * getBaseContext().getResources().getDisplayMetrics());
		 */

	}

	/**
	 * gecodeing google api for getting address
	 */
	private String downloadUrl(String strUrl) throws IOException {
		String data = "";
		InputStream iStream = null;
		HttpURLConnection urlConnection = null;
		try {
			URL url = new URL(strUrl);

			// Creating an http connection to communicate with url
			urlConnection = (HttpURLConnection) url.openConnection();

			// Connecting to url
			urlConnection.connect();

			// Reading data from url
			iStream = urlConnection.getInputStream();

			BufferedReader br = new BufferedReader(new InputStreamReader(
					iStream));

			StringBuffer sb = new StringBuffer();

			String line = "";
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

			data = sb.toString();

			br.close();

		} catch (Exception e) {
            e.printStackTrace();
		} finally {
			iStream.close();
			urlConnection.disconnect();
		}

		return data;

	}

	/** A class, to download Places from Geocoding webservice */
	private class DownloadTask extends AsyncTask<String, Integer, String> {

		String data = null;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			dialog = new ProgressDialog(SafelifeReportPost.this);
			dialog.setMessage("Getting your address...");
			dialog.setCancelable(false);
			dialog.setIndeterminate(false);
			if(dialog != null && dialog.isShowing())
				dialog.show();
		}

		// Invoked by execute() method of this object
		@Override
		protected String doInBackground(String... url) {
			try {
				data = downloadUrl(url[0]);
			} catch (Exception e) {
				Log.d("Background Task", e.toString());
			}
			return data;
		}

		// Executed after the complete execution of doInBackground() method
		@Override
		protected void onPostExecute(String result) {

			// Instantiating ParserTask which parses the json data from
			// Geocoding webservice
			// in a non-ui thread
			ParserTask parserTask = new ParserTask();

			// Start parsing the places in JSON format
			// Invokes the "doInBackground()" method of the class ParseTask
			parserTask.execute(result);
		}

	}

	/** A class to parse the Geocoding Places in non-ui thread */
	class ParserTask extends
			AsyncTask<String, Integer, List<HashMap<String, String>>> {

		JSONObject jObject;

		// Invoked by execute() method of this object
		@Override
		protected List<HashMap<String, String>> doInBackground(
				String... jsonData) {

			GeocodeJSONParser parser = new GeocodeJSONParser();

			try {
				jObject = new JSONObject(jsonData[0]);

				/** Getting the parsed data as a an ArrayList */
				if (places != null) {
					places.clear();
				}

				places = parser.parse(jObject);

			} catch (Exception e) {
				Log.d("Exception", e.toString());
			}
			return places;
		}

		// Executed after the complete execution of doInBackground() method
		@Override
		protected void onPostExecute(List<HashMap<String, String>> list) {

			// Clears all the existing markers
			// mMap.clear();
			if (dialog != null && dialog.isShowing()) {
				dialog.dismiss();
			}

			for (int i = 0; i < list.size(); i++) {
				Log.d("data", list.toString());
				// Creating a marker
				maplatview.setText(list.get(0).get("city"));

			}

		}
	}
}
