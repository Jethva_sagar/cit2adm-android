package com.ivisionr.cit2adm.database;

public class UserProfile
{
	public static final String TABLE_NAME = "users";
	public static final String KEY_ID = "id";
	public static final String KEY_user_type_id = "user_type_id";
	public static final String KEY_USER_ID = "user_id";
	public static final String KEY_first_name = "first_name";
	public static final String KEY_last_name = "last_name";
	public static final String KEY_email = "email";
	public static final String KEY_username = "username";
	public static final String KEY_password = "password";
	public static final String KEY_organization = "organization";
	public static final String KEY_city = "city";
	public static final String KEY_country = "country";
	public static final String KEY_zipcode = "zipcode";
	public static final String KEY_active = "active";
	public static final String KEY_paid = "paid";
	public static final String KEY_note = "note";
	public static final String KEY_membership_expiry_date = "membership_expiry_date";
	public static final String KEY_created = "created";
	public static final String KEY_modified = "modified";
	

}
