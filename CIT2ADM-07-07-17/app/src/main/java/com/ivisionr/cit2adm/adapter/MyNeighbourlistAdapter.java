package com.ivisionr.cit2adm.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ivisionr.cit2adm.R;
import com.ivisionr.cit2adm.bin.MyNeighbourDetails;

public class MyNeighbourlistAdapter extends ArrayAdapter<MyNeighbourDetails> {
	private LayoutInflater layoutinflate;
	private Context mContext;

	public MyNeighbourlistAdapter(Context context,
			List<MyNeighbourDetails> mylist) {
		super(context, R.layout.myneighbourlist, R.id.firstname, mylist);
		this.mContext = context;
		layoutinflate = LayoutInflater.from(context);

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		final MyNeighbourDetails myneighbourlist = (MyNeighbourDetails) this
				.getItem(position);

		TextView firstname = null, lastname = null, city = null, zip = null;
		if (convertView == null) {
			convertView = layoutinflate.inflate(R.layout.myneighbourlist, null);
			firstname = (TextView) convertView.findViewById(R.id.firstname);
			// lastname=(TextView) convertView.findViewById(R.id.lastname);
			city = (TextView) convertView.findViewById(R.id.cityname);
			//zip = (TextView) convertView.findViewById(R.id.zipcode);
			
			convertView.setTag(new ViewHolder(firstname, lastname, city, zip));

		} else {
			ViewHolder viewholder = (ViewHolder) convertView.getTag();
			firstname = viewholder.firstname;
			lastname = viewholder.lastname;
			city = viewholder.city;
			zip = viewholder.zip;
		}
		firstname.setText(myneighbourlist.getFirstname());
		// lastname.setText(myneighbourlist.getLastname());
		city.setText(myneighbourlist.getReport_id());
	//	zip.setText("(" + myneighbourlist.getCity() + ")");
		return convertView;
	}

	public class ViewHolder {

		private TextView firstname, lastname, city, zip;

		public ViewHolder(TextView firstname, TextView lastname, TextView city,
				TextView zip) {
			this.firstname = firstname;
			this.lastname = lastname;
			this.city = city;
			this.zip = zip;

		}

		public TextView getFirstname() {
			return firstname;
		}

		public void setFirstname(TextView firstname) {
			this.firstname = firstname;
		}

		public TextView getLastname() {
			return lastname;
		}

		public void setLastname(TextView lastname) {
			this.lastname = lastname;
		}

		public TextView getCity() {
			return city;
		}

		public void setCity(TextView city) {
			this.city = city;
		}

		public TextView getZip() {
			return zip;
		}

		public void setZip(TextView zip) {
			this.zip = zip;
		}

	}

}
