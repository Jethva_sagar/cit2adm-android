package com.ivisionr.cit2adm.async;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.ivisionr.cit2adm.Util.Webservicelink;
import com.ivisionr.cit2adm.interfaces.RateIssueInterface;

public class RateIssueAsync  extends AsyncTask<String , Void, String>{

	
	private Context _context;
	private Activity actity;	
	private String page="no";
	public RateIssueInterface rateissueinterface;
	
	public RateIssueAsync(Activity activity){
		this.actity=activity;
		this._context=activity;
	}

	@Override
	protected String doInBackground(String... params) {
	   try {
		rateissue(params);
	} catch (ClientProtocolException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		return null;
	}

	private void rateissue(String[] params) throws JSONException, ClientProtocolException, IOException {
		
		String useid = params[0];
		String reportid=params[1];
		Log.d("checkid", "checkid" +reportid);
		String comments=params[2];				
		String url = Webservicelink.rateissue;
		JSONObject jsonObject=new JSONObject();
		jsonObject.put("user_id", useid);
		jsonObject.put("issue_id", reportid);
		jsonObject.put("rating", comments);


		List<NameValuePair> namevaluepair = new ArrayList<NameValuePair>();
		namevaluepair.add(new BasicNameValuePair("data", jsonObject
				.toString()));
		ResponseHandler<String> resHandler = new BasicResponseHandler();
		String paramString = URLEncodedUtils.format(namevaluepair, "utf-8");
		url += paramString;	
		Log.d("checkurlrate", "checkurlrate" +url);
		DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(url);
		page = httpClient.execute(httpGet, resHandler);	
		Log.d("kartickrating", "kartickrating" +page);
		JSONObject jsobj = new JSONObject(page.toString());
		if (jsobj.has("error_code"))
		{
			page = jsobj.getString("error_code");
			Log.d("rate", "rate" +page);
			
				}	
		
		
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		if(page.equals("S21500")){
		rateissueinterface.onrateIssueCompleted();
		}else if(page.equals("S21505")){
			rateissueinterface.onrateIssueDuel();
			
		}
		
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		rateissueinterface.onrateIssueStarted();
	}
}
