package com.ivisionr.cit2adm;

import android.app.Application;

import com.ivisionr.cit2adm.constant.Constant;

import org.acra.annotation.ReportsCrashes;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@ReportsCrashes(formKey = "")
public class SafeLifeApplication extends Application {

	public String getStringByLang(String identifier) {
		String mode = "fr";

		return mode.equals("en") ? getResources().getString(
				getResources().getIdentifier(identifier, "string",
						getPackageName())) : getResources().getString(
				getResources().getIdentifier(identifier + "_fr", "string",
						getPackageName()));

	}

	@Override
	public void onCreate() {

		super.onCreate();
		//ACRA.init(this);

		// instantiate the report sender with the email credentials.
		// these will be used to send the crash report
		//ACRAReportSender reportSender = new ACRAReportSender("hirenkapuria2011@gmail.com", "Bellatrix@221089");

		// register it with ACRA.
		// ACRA.getErrorReporter().setReportSender(reportSender);

		// Setup handler for uncaught exceptions.
		Thread.setDefaultUncaughtExceptionHandler (new Thread.UncaughtExceptionHandler() {
			@Override
			public void uncaughtException (Thread thread, Throwable e) {
				writeCrashReportToFile(thread, e);
			}
		});


	}

	private void writeCrashReportToFile(Thread thread, Throwable exe) {

		DateFormat dateFormat = new SimpleDateFormat("dd_MM_yyyy_hhmmss");
		String imgcurTime = dateFormat.format(new Date());

		//File root = android.os.Environment.getExternalStorageDirectory();
		File dir = new File(Constant.APPLICATION_DIRECTORY+""+Constant.CRASH_REPORT_DIR);
		if(!dir.exists())
			dir.mkdirs();
		File file = new File(dir, imgcurTime+".txt");

		try {

			FileOutputStream f = new FileOutputStream(file);
			PrintWriter pw = new PrintWriter(f);
			exe.printStackTrace(pw);
			//pw.println(exe.printStackTrace()); //your string which you want to store
			pw.flush();
			pw.close();
			f.close();
			// thread.stop();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
