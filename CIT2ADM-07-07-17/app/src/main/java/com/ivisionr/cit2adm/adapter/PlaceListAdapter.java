package com.ivisionr.cit2adm.adapter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ivisionr.cit2adm.GeocodeJSONParser;
import com.ivisionr.cit2adm.R;
import com.ivisionr.cit2adm.SafelifeReportPost;
import com.ivisionr.cit2adm.Util.ImageLoader;
import com.ivisionr.cit2adm.Util.ImageLoaderWithoutDimension;
import com.ivisionr.cit2adm.Util.SharePreferenceClass;
import com.ivisionr.cit2adm.Util.Utils;
import com.ivisionr.cit2adm.Util.Webservicelink;
import com.ivisionr.cit2adm.async.GetCurrentaddressAsync;
import com.ivisionr.cit2adm.bin.MyReportDeetails;
import com.ivisionr.cit2adm.interfaces.CurrentaddressInterface;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

public class PlaceListAdapter extends ArrayAdapter<MyReportDeetails> {

	private LayoutInflater inflater;
	@SuppressWarnings("unused")
	private Context mContext;
	private SharePreferenceClass sharedpreference;
	private ImageLoaderWithoutDimension imageloader;
	ImageLoader image;
	private String photoheight="100";
	private String photowidth="50";
	private Activity activity;

	List<Address> addresses = null;
	static double currlat, currentlong;
	List<HashMap<String, String>> places = null;
	TextView firstname=null,datecreted=null,issuetype=null,status=null,description=null,
			sms=null,likevalue=null,dislikevalue=null,addressreport=null;
	public PlaceListAdapter(Activity activity, List<MyReportDeetails> news_arrayList) {
		super(activity.getApplicationContext(), R.layout.myreportdetails,R.id.textmyreportdetails, news_arrayList);
		this.mContext = activity.getApplicationContext();
		this.activity = activity;
		inflater = LayoutInflater.from(activity.getApplicationContext()) ;
		sharedpreference= new SharePreferenceClass(activity.getApplicationContext());
		imageloader=new ImageLoaderWithoutDimension(activity.getApplicationContext());
		//image=new ImageLoader(context);


	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		final MyReportDeetails surveyList = (MyReportDeetails) this.getItem( position );


		ImageView image=null;		

		if(convertView==null){

			convertView = inflater.inflate(R.layout.myreportdetails, null);
			firstname=(TextView) convertView.findViewById(R.id.textmyreportdetails);
			datecreted=(TextView) convertView.findViewById(R.id.textmyreportdetailstime);
			issuetype=(TextView) convertView.findViewById(R.id.myreportissuetype);
			status=(TextView) convertView.findViewById(R.id.test11);
			description=(TextView)convertView.findViewById(R.id.myrepotdescription);
			sms=(TextView)convertView.findViewById(R.id.mysmsvalue);
			likevalue=(TextView) convertView.findViewById(R.id.mylikevalue);
			dislikevalue=(TextView) convertView.findViewById(R.id.mydislikevalue);
			image=(ImageView) convertView.findViewById(R.id.myrepotimage);
			addressreport = (TextView)convertView.findViewById(R.id.txtreportaddress);


			convertView.setTag(new ViewHolder(firstname,datecreted,issuetype,status,
					description,sms,likevalue,dislikevalue,image,addressreport));

		}else{

			ViewHolder viewHolder = (ViewHolder) convertView.getTag();
			firstname=viewHolder.firstname;
			datecreted=viewHolder.datecreted;
			issuetype=viewHolder.issuetype;
			status=viewHolder.status;
			description=viewHolder.description;
			sms=viewHolder.sms;
			likevalue=viewHolder.likevalue;
			dislikevalue=viewHolder.dislikevalue;
			image=viewHolder.image;
			addressreport= viewHolder.txtreportaddress;

		}


		firstname.setText(surveyList.getReport_id());
		datecreted.setText(surveyList.getCreated());
		issuetype.setText(surveyList.getIssue_type_name());
		status.setText(surveyList.getStatus());
		description.setText(surveyList.getDescription());
		sms.setText(surveyList.getNum_comments());
		likevalue.setText(surveyList.getNum_ratings_plus());
		dislikevalue.setText(surveyList.getNum_ratings_minus());

		Double lat = Double.valueOf(surveyList.getLat());
		Double lan = Double.valueOf(surveyList.getLan());

		new Address(activity,lat,lan).execute();
		addressreport.setText("Loading..");
		String result="";
		String url="";
		url= Webservicelink.currentAddress;
		String address="";
 	// If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
		/*try
		{
			JSONObject registerJson = new JSONObject();
			registerJson.put("lat",lat);
			registerJson.put("lon", lan);

			List<NameValuePair> namevaluepair = new ArrayList<NameValuePair>();
			namevaluepair.add(new BasicNameValuePair("data", registerJson
					.toString()));
			//ResponseHandler<String> resHandler = new BasicResponseHandler();
			String paramString = URLEncodedUtils.format(namevaluepair, "utf-8");
			url += paramString;
			Log.d("DEBUG", "URL: " +url);

			HttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(url);
			HttpResponse response = null;
			try {
				response = httpClient.execute(httpPost);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				HttpEntity entity = response.getEntity();
				InputStream instream = entity.getContent();
				result = Utils.convertStreamToString(instream);
				Log.d("DEBUG", "Address URL: " +result);

				Log.d("DEBUG", "Result: " +result);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			JSONObject jsonObject=new JSONObject(result);
			Log.d("DEBUG", "URL: " +url);
			Log.d("DEBUG", "Result: " +result);
			//if(jsonObject.has("formatted_address"))
			if(jsonObject.has("jsonObject"))
				address=jsonObject.getString("formatted_address");
			Log.d("DEBUG", "Address: " +address);

		}
		catch (Exception e)
		{

		}*/
		//addressreport.setText(address+"..");
		//imageloader.DisplayImage(surveyList.getIcon(), image,photoheight,photowidth);
		imageloader.DisplayImage(surveyList.getIcon(), image);
		//new DisplayImageFromURL(image).execute(surveyList.getIcon());
		return convertView;
	}


	private class DisplayImageFromURL extends AsyncTask<String, Void, Bitmap>
	{
        ImageView bmImage;
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
          //  pd = new ProgressDialog(MainActivity.this);
          //  pd.setMessage("Loading...");
           // pd.show();
        }
        public DisplayImageFromURL(ImageView bmImage) {
            this.bmImage = bmImage;
        }
        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }

            return mIcon11;

        }
        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
          //  pd.dismiss();
        }
    }



	public class ViewHolder {

		private TextView datecreted,issuetype,status,description,sms,likevalue,dislikevalue,firstname,txtreportaddress;
		private ImageView image;
		public ViewHolder(TextView firstname,TextView datecreted, TextView issuetype, TextView status,
						  TextView description, TextView sms, TextView likevalue, TextView dislikevalue,ImageView image,TextView address) {
			this.datecreted=datecreted;
			this.firstname=firstname;
			this.issuetype=issuetype;
			this.status=status;
			this.description=description;
			this.sms=sms;
			this.likevalue=likevalue;
			this.dislikevalue=dislikevalue;
			this.image=image;
			this.txtreportaddress = address;
		}



		public TextView getFirstname() {
			return firstname;
		}


		public void setFirstname(TextView firstname) {
			this.firstname = firstname;
		}


		public ImageView getImage() {
			return image;
		}


		public void setImage(ImageView image) {
			this.image = image;
		}


		public TextView getIssuetype() {
			return issuetype;
		}


		public void setIssuetype(TextView issuetype) {
			this.issuetype = issuetype;
		}


		public TextView getStatus() {
			return status;
		}


		public void setStatus(TextView status) {
			this.status = status;
		}


		public TextView getDescription() {
			return description;
		}


		public void setDescription(TextView description) {
			this.description = description;
		}


		public TextView getSms() {
			return sms;
		}


		public void setSms(TextView sms) {
			this.sms = sms;
		}


		public TextView getLikevalue() {
			return likevalue;
		}


		public void setLikevalue(TextView likevalue) {
			this.likevalue = likevalue;
		}


		public TextView getDislikevalue() {
			return dislikevalue;
		}


		public void setDislikevalue(TextView dislikevalue) {
			this.dislikevalue = dislikevalue;
		}

		public ViewHolder(TextView datecreted) {
			// TODO Auto-generated constructor stub
			this.datecreted=datecreted;
		}


		public TextView getDatecreted() {
			return datecreted;
		}


		public void setDatecreted(TextView datecreted) {
			this.datecreted = datecreted;
		}

		public TextView getTxtreportaddress() {
			return txtreportaddress;
		}

		public void setTxtreportaddress(TextView txtreportaddress) {
			this.txtreportaddress = txtreportaddress;
		}
	}

	 class Address extends AsyncTask<String , Void, String> {


		private Context _context;
		private Activity actity;
		private String address = "";
		public CurrentaddressInterface currentaddressInterface;
		double lat, lan;

		public Address(Activity activity, double lat, double lan) {
			// TODO Auto-generated constructor stub
			this.actity = activity;
			this._context = activity;
			this.lan = lan;
			this.lat = lat;

		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... params) {
			try {
				currentaddress(params);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		private void currentaddress(String[] params) throws JSONException {
			// TODO Auto-generated method stubString result="";
			String result = "";
			String lattitude = lat + "";
			String longitude = lan + "";
			Log.d("DEBUG", "Lat: " + lattitude);
			Log.d("DEBUG", "Lon: " + longitude);
			String url = "";
			url = Webservicelink.currentAddress;

			JSONObject registerJson = new JSONObject();
			registerJson.put("lat", lattitude);
			registerJson.put("lon", longitude);

			List<NameValuePair> namevaluepair = new ArrayList<NameValuePair>();
			namevaluepair.add(new BasicNameValuePair("data", registerJson
					.toString()));
			//ResponseHandler<String> resHandler = new BasicResponseHandler();
			String paramString = URLEncodedUtils.format(namevaluepair, "utf-8");
			url += paramString;
			Log.d("DEBUG", "URL: " + url);

			HttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(url);
			HttpResponse response = null;
			try {
				response = httpClient.execute(httpPost);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				HttpEntity entity = response.getEntity();
				InputStream instream = entity.getContent();
				result = Utils.convertStreamToString(instream);
				Log.d("DEBUG", "Address URL: " + result);

				Log.d("DEBUG", "Result: " + result);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			JSONObject jsonObject = new JSONObject(result);
			Log.d("DEBUG", "URL: " + url);
			Log.d("DEBUG", "Result: " + result);
			//if(jsonObject.has("formatted_address"))
			if (jsonObject.has("jsonObject"))
				address = jsonObject.getString("formatted_address");
			Log.d("DEBUG", "Address: " + address);


		}

		@Override
		protected void onPostExecute(String result)
		{
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			addressreport.setText(address);
		}

	}
} 
