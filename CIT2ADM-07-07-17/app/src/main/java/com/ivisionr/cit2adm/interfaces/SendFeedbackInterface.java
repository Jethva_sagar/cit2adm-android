package com.ivisionr.cit2adm.interfaces;

public interface SendFeedbackInterface {
	
	public void onStarted();
	public void onCompleted(String errorcode);

}
