package com.ivisionr.cit2adm.bin;

public class MyDraftDetails {
	private String reportissuetypename="";	
	private String reportisssuedescription="";
	private String reportissueimage="";
	private String reportlat="";
	private String reportlon="";
	private String reportissuetypeid="";
	private String reportImagename="";
	public String getReportImagename() {
		return reportImagename;
	}
	public void setReportImagename(String reportImagename) {
		this.reportImagename = reportImagename;
	}
	public String getReportissuetypeid() {
		return reportissuetypeid;
	}
	public void setReportissuetypeid(String reportissuetypeid) {
		this.reportissuetypeid = reportissuetypeid;
	}
	public String getReportissuetypename() {
		return reportissuetypename;
	}
	public void setReportissuetypename(String reportissuetypename) {
		this.reportissuetypename = reportissuetypename;
	}
	public String getReportisssuedescription() {
		return reportisssuedescription;
	}
	public void setReportisssuedescription(String reportisssuedescription) {
		this.reportisssuedescription = reportisssuedescription;
	}
	public String getReportissueimage() {
		return reportissueimage;
	}
	public void setReportissueimage(String reportissueimage) {
		this.reportissueimage = reportissueimage;
	}
	public String getReportlat() {
		return reportlat;
	}
	public void setReportlat(String reportlat) {
		this.reportlat = reportlat;
	}
	public String getReportlon() {
		return reportlon;
	}
	public void setReportlon(String reportlon) {
		this.reportlon = reportlon;
	}
}
