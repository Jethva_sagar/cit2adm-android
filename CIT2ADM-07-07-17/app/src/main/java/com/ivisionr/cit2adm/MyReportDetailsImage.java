package com.ivisionr.cit2adm;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ivisionr.cit2adm.Util.Base65;
import com.ivisionr.cit2adm.Util.ImageLoaderWithoutDimension;
import com.ivisionr.cit2adm.Util.SharePreferenceClass;
import com.ivisionr.cit2adm.Util.Utils;
import com.ivisionr.cit2adm.async.MyIssuedeleteAsync;
import com.ivisionr.cit2adm.async.MyRepoerDetailsImageAsync;
import com.ivisionr.cit2adm.async.PostCommentAsync;
import com.ivisionr.cit2adm.bin.GetAllIssueDetails;
import com.ivisionr.cit2adm.bin.GetIssudetailsbin;
import com.ivisionr.cit2adm.bin.MyReportDetailsDescriptionlist;
import com.ivisionr.cit2adm.bin.MyReportDetailscomments;
import com.ivisionr.cit2adm.interfaces.GetallIssueInterface;
import com.ivisionr.cit2adm.interfaces.MYReportDetailsImageInterface;
import com.ivisionr.cit2adm.interfaces.MyReportdelete;
import com.ivisionr.cit2adm.interfaces.Postcommentsinterfaces;
import com.ivisionr.cit2adm.interfaces.RateIssueInterface;

public class MyReportDetailsImage extends Activity implements
		MYReportDetailsImageInterface, GetallIssueInterface,
		Postcommentsinterfaces, RateIssueInterface, MyReportdelete {

	String SHARING_TEXT = "I am using Cit2Adm mobile application to report pedestrian safety issue. You can also do the same and contribute towards the benefit of the Society" +
			"\nDownload from here : https://play.google.com/store/apps/details?id=";

	private String id;
	TextView descriptiodatecreated, descriptionsms, descriptionplusvalue,
			descriptionminusvalue, descriptiontxt, issuetype;
	TextView issueid, comments;
	private MyReportDetailsDescriptionlist myreportdetails;
	ImageView imageindescription;
	private ImageLoaderWithoutDimension imageloder;
	private List<MyReportDetailscomments> listreportwithcomments;
	private MyReportDetailscomments reportdetailswithcomments;
	private String reportid;
	private Button commentssubmit;
	private String commentstxt;
	private Dialog dialog;
	private ImageView ratingplus, ratingminus;
	private String userid, issuetype_id;
	private ProgressDialog mProgressDialog;
	private GetIssudetailsbin getissuedetailsbin;
	private TextView listcomments, listcomentsname, myreportaddress;
	private Button backbutton;
	private Button sharebutton;
	private TextView myreporttype;
	private ImageView deletereport;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.myreportdetailsimage);
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);
		getissuedetailsbin = new GetIssudetailsbin();

		mProgressDialog = new ProgressDialog(MyReportDetailsImage.this);
		mProgressDialog.setMessage(getResources().getText(R.string.loading));
		imageloder = new ImageLoaderWithoutDimension(MyReportDetailsImage.this);
		ratingplus = (ImageView) findViewById(R.id.reporslikei);
		ratingminus = (ImageView) findViewById(R.id.reportsdislikei);
		descriptiodatecreated = (TextView) findViewById(R.id.textmyreportdetailstimei);
		issueid = (TextView) findViewById(R.id.textmyreportdetailsi);
		imageindescription = (ImageView) findViewById(R.id.myrepotimagei);
		descriptionsms = (TextView) findViewById(R.id.mysmsvaluei);
		descriptionplusvalue = (TextView) findViewById(R.id.mylikevaluei);
		descriptionminusvalue = (TextView) findViewById(R.id.mydislikevaluei);
		descriptiontxt = (TextView) findViewById(R.id.myrepotdescriptioni);
		listcomments = (TextView) findViewById(R.id.listcommentsi);
		issuetype = (TextView) findViewById(R.id.test111);
		commentssubmit = (Button) findViewById(R.id.myimagei);
		listreportwithcomments = new ArrayList<MyReportDetailscomments>();
		myreportdetails = new MyReportDetailsDescriptionlist();
		backbutton = (Button) findViewById(R.id.myreportback);
		sharebutton = (Button) findViewById(R.id.myreportshare);
		//myrepoticon = (ImageView) findViewById(R.id.myreportrmapicon);
		myreporttype = (TextView) findViewById(R.id.myreporttype);
		deletereport = (ImageView) findViewById(R.id.reportdelete);
		
		myreportaddress = (TextView) findViewById(R.id.myreportaddress);
		
		imageindescription.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent=new Intent(MyReportDetailsImage.this,FullScreenReportImage.class);
				intent.putExtra("image", myreportdetails.getIssueicon());
				startActivity(intent);
				
			}
		});
		Bundle b = getIntent().getExtras();
		if (getIntent().hasExtra("id")) {
			id = b.getString("id");
			Log.d("DEBUG", "MyReportDetailsImage Id: " + id);
		}
		if (Utils.checkConnectivity(MyReportDetailsImage.this)) {
			MyRepoerDetailsImageAsync myreportdetailsimage = new MyRepoerDetailsImageAsync(
					MyReportDetailsImage.this);
			myreportdetailsimage.mylistinterface = this;
			myreportdetailsimage.execute(id);
		} else {
			showNetworkDialog("internet");
		}
		commentssubmit.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				displayDialog();

			}
		});
		ratingplus.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {

				Toast toast = Toast.makeText(MyReportDetailsImage.this,
						R.string.cannotrateownissue, Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();

			}
		});
		ratingminus.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				Toast toast = Toast.makeText(MyReportDetailsImage.this,
						R.string.cannotrateownissue, Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();

			}
		});
		backbutton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(MyReportDetailsImage.this,
						MyReport.class));
				MyReportDetailsImage.this.finish();

			}
		});
		sharebutton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

                Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.imgpsh_fullsize);
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("image/text/plain");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                largeIcon.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                File f = new File(Environment.getExternalStorageDirectory() + File.separator + "temporary_file.jpg");
                try {
                    f.createNewFile();
                    FileOutputStream fo = new FileOutputStream(f);
                    fo.write(bytes.toByteArray());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                sharingIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file:///sdcard/temporary_file.jpg"));
                //startActivity(Intent.createChooser(share, "Share Image"));


                /*Intent sharingIntent = new Intent(
						android.content.Intent.ACTION_SEND);
				sharingIntent.setType("text/plain");*/
				sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "iVisionR");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, SHARING_TEXT+""+getPackageName());

                /*sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT,
						*//*shareBody*//*SHARING_TEXT);*/
				startActivity(Intent.createChooser(sharingIntent, "Share via"));

			}
		});
		deletereport.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				AlertDialog.Builder builder = new AlertDialog.Builder(
						MyReportDetailsImage.this);
				builder.setTitle("CIT2ADM");
				builder.setMessage("Are You sure want to delete your own  Report");
				builder.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface arg0, int arg1) {
								if (Utils
										.checkConnectivity(MyReportDetailsImage.this)) {
									MyIssuedeleteAsync myissuedeleteasync = new MyIssuedeleteAsync(
											MyReportDetailsImage.this);
									myissuedeleteasync.myreportdelete = MyReportDetailsImage.this;
									// Log.d("cc", "cc" +Base65.encodeBytes(new
									// SharePreferenceClass(MyReportDetailsImage.this).getUserid().getBytes()));
									myissuedeleteasync.execute(
											Base65.encodeBytes(myreportdetails
													.getIssuereportid()
													.getBytes()),
											Base65.encodeBytes(new SharePreferenceClass(
													MyReportDetailsImage.this)
													.getUserid().getBytes()));

								} else {
									showNetworkDialog("internet");
								}

							}
						});
				builder.setNegativeButton("No",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub

							}
						});
				AlertDialog alert = builder.create();
				alert.show();

			}
		});
	}

	@SuppressWarnings("deprecation")
	public void showNetworkDialog(final String message) {
		// final exit of application
		AlertDialog.Builder builder = new AlertDialog.Builder(
				MyReportDetailsImage.this);
		builder.setTitle(getResources().getString(R.string.app_name));
		if (message.equals("gps")) {
			builder.setMessage(getResources().getString(R.string.gps_message));
		} else if (message.equals("internet")) {
			builder.setMessage(getResources().getString(R.string.net_message));
		}
		AlertDialog alert = builder.create();
		alert.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				if (message.equals("gps")) {
					Intent i = new Intent(
							android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
					startActivityForResult(i, 1);
				} else if (message.equals("internet")) {
					Intent i = new Intent(
							android.provider.Settings.ACTION_WIRELESS_SETTINGS);
					startActivityForResult(i, 2);
					Intent i1 = new Intent(
							android.provider.Settings.ACTION_WIFI_SETTINGS);
					startActivityForResult(i1, 3);
				} else {
					// do nothing
				}
			}
		});
		// Showing Alert Message
		alert.show();
	}

	protected void displayDialog() {
		dialog = new Dialog(MyReportDetailsImage.this, R.style.FullHeightDialog);
		dialog.setContentView(R.layout.range_dialog);
		dialog.show();

		final EditText range_input = (EditText) dialog
				.findViewById(R.id.range_input);
		Button submit_btn = (Button) dialog.findViewById(R.id.submit_btn);
		Button cancel_btn = (Button) dialog.findViewById(R.id.cancel_btn);

		submit_btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				commentstxt = range_input.getText().toString();
				if (commentstxt.length() > 0) {
					issuetype_id = myreportdetails.getId();
					userid = new SharePreferenceClass(MyReportDetailsImage.this)
							.getUserid();

					PostCommentAsync postcommentasync = new PostCommentAsync(
							MyReportDetailsImage.this);
					postcommentasync.postcommentinterfaces = MyReportDetailsImage.this;
					postcommentasync.execute(new SharePreferenceClass(
							MyReportDetailsImage.this).getUserid(),
							issuetype_id, commentstxt);

				}

			}
		});

		cancel_btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();

			}
		});
	}

	@Override
	public void onReportDetailsImagenoData() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onReportDetailsImageStarted() {
		// TODO Auto-generated method stub
		if (null != mProgressDialog) {
			mProgressDialog.show();
		}
	}

	@Override
	public void onReportDetailsImagecompleted(
			MyReportDetailsDescriptionlist list) {
		if (null != mProgressDialog) {
			mProgressDialog.dismiss();
		}
		myreportdetails = list;
		descriptiodatecreated.setText(myreportdetails.getIssucreated());
		issueid.setText(myreportdetails.getFirstname());
		imageloder.DisplayImage(myreportdetails.getIssueicon(),
				imageindescription);
		descriptionsms.setText(myreportdetails.getIssuesmsvalue());
		descriptionplusvalue.setText(myreportdetails.getIssueratingplus());
		descriptionminusvalue.setText(myreportdetails.getIssueratingminus());
		descriptiontxt.setText(myreportdetails.getIssuedescription());
		issuetype.setText(myreportdetails.getIssuestatus());
		myreporttype.setText(myreportdetails.getIssuetypenmae() + "  ["
				+ myreportdetails.getIssuereportid() + "]");
		/*myrepoticon.setImageBitmap(getGoogleMapThumbnail(
				Double.parseDouble(myreportdetails.getLat()),
				Double.parseDouble(myreportdetails.getLon())));*/
		
		//myreportaddress.setText(myreportdetails.getIssueaddress());

	}

	public static Bitmap getGoogleMapThumbnail(double lati, double longi) {
		String URL = "http://maps.google.com/maps/api/staticmap?center=" + lati
				+ "," + longi + "&zoom=15&size=600x200&sensor=false";
		Bitmap bmp = null;
		HttpClient httpclient = new DefaultHttpClient();
		HttpGet request = new HttpGet(URL);

		InputStream in = null;
		try {
			in = httpclient.execute(request).getEntity().getContent();
			bmp = BitmapFactory.decodeStream(in);
			in.close();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return bmp;
	}

	@Override
	public void onReportIssueWithCommentsnoData() {

		listcomments.setText(R.string.nocomments);

	}

	@Override
	public void onReportIssueStarted() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onReportIssueWithCommentsCompleted(
			List<MyReportDetailscomments> list) {
		listreportwithcomments.addAll(list);

		// MyRepoertscomentadapter mycomentsadpter=new
		// MyRepoertscomentadapter(this,listreportwithcomments);
		// listcomments.setAdapter(mycomentsadpter);

		Log.d("hh", "hh" + listreportwithcomments.size());

		String comm = "";
		for (int i = 0; i < listreportwithcomments.size(); i++) {

			comm = comm + "\n" + listreportwithcomments.get(i).getComments()
					+ "\n" + "- By "
					+ listreportwithcomments.get(i).getFirstname() + " "
					+ listreportwithcomments.get(i).getLastname()
					+ " Posted on "
					+ listreportwithcomments.get(i).getCreated() + "\n\n";

		}

		listcomments.setText(comm);

	}

	@Override
	public void onGetallIssueStarted() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onGetallIssuenoData() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onGetallIssueCompleted(List<GetAllIssueDetails> list) {

	}

	@Override
	public void onpostcommentstarted() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onpostcommentcompleted() {
		// TODO Auto-generated method stub
		dialog.dismiss();
		Toast toast = Toast.makeText(MyReportDetailsImage.this,
				R.string.commentpostsuccessfull, Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();

	}

	@Override
	public void onrateIssueStarted() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onrateIssueCompleted() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onpostcommentcompletednodata() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onrateIssueDuel() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPostCommentlength() {
		// TODO Auto-generated method stub
		Toast toast = Toast.makeText(MyReportDetailsImage.this,
				R.string.enter25character, Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();

	}

	@Override
	public void started() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onCompleted() {
		// TODO Auto-generated method stub
		Toast toast = Toast.makeText(MyReportDetailsImage.this,
				R.string.issuedeleted, Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();

	}

}
