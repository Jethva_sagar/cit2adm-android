package com.ivisionr.cit2adm;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ivisionr.cit2adm.Util.SharePreferenceClass;
import com.ivisionr.cit2adm.Util.Webservicelink;
import com.ivisionr.cit2adm.Util.otherStaticValue;
import com.ivisionr.cit2adm.async.ServerResponceCheck;
import com.ivisionr.cit2adm.bin.User;
import com.ivisionr.cit2adm.custom.UploadManager;
import com.ivisionr.cit2adm.database.DatabaseHandler;
import com.ivisionr.cit2adm.database.UserProfile;

public class EnterNewPassword extends ActivityExceptionDemo
{
	private Button btnback, btnResetPassword;
	EditText etResetPassword;
	DatabaseHandler db;
	ArrayList<HashMap<String, String>> userRecord;
	ServerResponceCheck serverResponceCheck;
	SharePreferenceClass sharePreferenceClass;
	private String userId="";

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		db = new DatabaseHandler(this);
		
		setContentView(R.layout.resetpassword);
		connectToXml();
		sharePreferenceClass = new SharePreferenceClass(this);
		serverResponceCheck = new ServerResponceCheck(this);
		btnback.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				// TODO Auto-generated method stub

				finish();

			}
		});

		btnResetPassword.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				
				//userRecord = databasehandler.getProfileAllDataFromDb();
				SharePreferenceClass spc = new SharePreferenceClass(getApplicationContext());
				userId = spc.getUserid();
				//userRecord = db.getProfileAllDataFromDb();
				User myUser = (User) db.getUser(userId);
				
				String userName = null;
				String email = null;
				for (int i = 0; i < userRecord.size(); i++)
				{
					userName = userRecord.get(i).get(UserProfile.KEY_username);
					email = userRecord.get(i).get(UserProfile.KEY_email);
				}
				Log.d("email", email);
				Log.d("username", userName);

				Log.d("password", etResetPassword.getText().toString());
				new EnterNewPassTask().execute(userName, etResetPassword
						.getText().toString().trim(), email);
			}
		});

	}

	protected void connectToXml()
	{
		btnback = (Button) findViewById(R.id.forgotmypassword_back_btn);
		btnResetPassword = (Button) findViewById(R.id.Reset_password_Activity_submitbtn);
		etResetPassword = (EditText) findViewById(R.id.et_reset_password);
	}

	class EnterNewPassTask extends AsyncTask<String, Void, String>
	{

		private ProgressDialog pd;

		@Override
		protected void onPreExecute()
		{
			// TODO Auto-generated method stub
			super.onPreExecute();
			if (pd == null || pd.isShowing() == false)
			{
				pd = new ProgressDialog(EnterNewPassword.this);
				pd.setTitle("Please Wait ");
				pd.setMessage("Submitting Data");
				pd.show();

			}
		}

		@Override
		protected String doInBackground(String... params)
		{
			JSONObject jsonResetPassword = new JSONObject();

			try
			{

				jsonResetPassword.put("username", params[0]);
				jsonResetPassword.put("password", params[1]);
				jsonResetPassword.put("email", params[2]);

			}
			catch (JSONException e)
			{

				e.printStackTrace();
			}
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("data", jsonResetPassword
					.toString()));
			try
			{
				return UploadManager.uploadDataToUrlGet(nameValuePairs,
						Webservicelink.serverForgotPasswordURL);
			}
			catch (UnsupportedEncodingException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (ClientProtocolException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result)
		{
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			Log.d("reset", result);
			if (pd != null && pd.isShowing()) pd.dismiss();
			String error = "";
			JSONObject errorCode = null;
			try
			{
				errorCode = new JSONObject(result);
				error = errorCode.getString("error_code");
			}
			catch (JSONException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			HashMap<String, String> hasmapError = ServerResponceCheck.set();
			if (hasmapError.containsKey(error))
			{
				if (error.equals("S20300"))
				{
					Toast.makeText(getApplicationContext(),
							hasmapError.get(error).toString(),
							Toast.LENGTH_SHORT).show();
					sharePreferenceClass.setValue_string(
							otherStaticValue.loginState, "no");
					Intent intent = new Intent(EnterNewPassword.this,
							LoginActivity.class);
					startActivity(intent);
					finish();
				}
				else
				{
					Toast.makeText(getApplicationContext(),
							hasmapError.get(error).toString(),
							Toast.LENGTH_SHORT).show();
				}
			}
		}

	}

}
