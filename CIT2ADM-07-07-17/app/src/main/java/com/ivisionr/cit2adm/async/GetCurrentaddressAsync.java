package com.ivisionr.cit2adm.async;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.ivisionr.cit2adm.Util.Utils;
import com.ivisionr.cit2adm.Util.Webservicelink;
import com.ivisionr.cit2adm.bin.GetAllIssueDetails;
import com.ivisionr.cit2adm.interfaces.CurrentaddressInterface;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.util.Log;

public class GetCurrentaddressAsync extends AsyncTask<String , Void, String> {
	
	

	private Context _context;
	private Activity actity;
	private String address="";
	public CurrentaddressInterface currentaddressInterface;


	public GetCurrentaddressAsync(Activity activity) {
		// TODO Auto-generated constructor stub
		this.actity = activity;
		this._context = activity;

		
	}

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
	protected String doInBackground(String... params) {
		try {
			currentaddress(params);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	private void currentaddress(String[] params) throws JSONException {
		// TODO Auto-generated method stubString result="";
        String result="";
		String lattitude=params[0];
		String longitude=params[1];
		Log.d("DEBUG", "Lat: " +lattitude);
		Log.d("DEBUG", "Lon: " +longitude);
		String url="";
		url=Webservicelink.currentAddress;

		JSONObject registerJson = new JSONObject();
		registerJson.put("lat",lattitude);
		registerJson.put("lon", longitude);
		
		List<NameValuePair> namevaluepair = new ArrayList<NameValuePair>();
		namevaluepair.add(new BasicNameValuePair("data", registerJson
				.toString()));
		//ResponseHandler<String> resHandler = new BasicResponseHandler();
		String paramString = URLEncodedUtils.format(namevaluepair, "utf-8");
		url += paramString;			
		Log.d("DEBUG", "URL: " +url);
		
		HttpClient httpClient = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost(url);			
		HttpResponse response = null;
		try {
			response = httpClient.execute(httpPost);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		try {
			HttpEntity entity = response.getEntity();
			InputStream instream = entity.getContent();
			result = Utils.convertStreamToString(instream);	
			Log.d("DEBUG", "Address URL: " +result);

			Log.d("DEBUG", "Result: " +result);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		JSONObject jsonObject=new JSONObject(result);
		Log.d("DEBUG", "URL: " +url);
		Log.d("DEBUG", "Result: " +result);
		//if(jsonObject.has("formatted_address"))
		if(jsonObject.has("jsonObject"))
		address=jsonObject.getString("formatted_address");
		Log.d("DEBUG", "Address: " +address);

		
		
		
	}
	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		currentaddressInterface.onCurrentAddress(address);
	}
}
