package com.ivisionr.cit2adm.custom;

import android.app.Activity;
import android.content.Intent;
import android.widget.Toast;

import com.ivisionr.cit2adm.LoginActivity;
import com.ivisionr.cit2adm.R;
import com.ivisionr.cit2adm.RegisterActivity;
import com.ivisionr.cit2adm.Util.SharePreferenceClass;
import com.ivisionr.cit2adm.Util.otherStaticValue;

public class CustomFunctionClass {

	

	static final String[] menuelementarray11 = new String[] { "Log in",
			"Registration", "MyProfile", "Neighbouring", "VOTE on Report ID",
			"IVisionR Associations",
			"Add/share Apps w/Facebook or Twitter or w/G+", "Parameters",
			"Send feedback", "GPS Location detail" };
	
	static final String[] reportelementarray = new String[] { "Sidewalk Obstacle",
		"Road sign High < 2.30m", "Sidewalk Large < 1.40 meter", "Pavement covering", "Post",
		"Street equipment",
		"Guter", "Crossfall",
		"Lighting", "Tree","Good example","Improvement needed","Train station","Airport","Others" };

	Activity customFunctionContext;
	 String[] rl1;
	 SharePreferenceClass sharePreferenceClass;
	
	public CustomFunctionClass(Activity context) {
		
		this.customFunctionContext = context;
	
		  rl1=context.getResources().getStringArray(R.array.englishfunctionlistofmenu);
	
		  sharePreferenceClass=new SharePreferenceClass(customFunctionContext);
	
	}
	
	public void event_of_grideview_item(String eventTag){
		
		if(eventTag.equals("LogIn|Register")){
			String loginState=sharePreferenceClass.getValue_string(otherStaticValue.loginState);
			if(loginState.equals("no")){
			Intent in=new Intent(this.customFunctionContext, LoginActivity.class);
			customFunctionContext.startActivity(in);
			}else{
				Toast.makeText(customFunctionContext, R.string.alreadylogin, Toast.LENGTH_LONG).show();
			}
			this.customFunctionContext.finish();
		}else if(eventTag.equals("Registration")){
			Intent in=new Intent(this.customFunctionContext, RegisterActivity.class);
			customFunctionContext.startActivity(in);
		//	this.customFunctionContext.finishAffinity();
		}
		
			
	}
	
	
	
}
