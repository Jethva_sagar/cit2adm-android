package com.ivisionr.cit2adm.Util;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;


/**
 * GPS Class have all details about location and its listners and details
 * @author GangaNaidu
 *
 */
public class GPSClass {

	private LocationManager mLocationManager;
	private LocationResult locationResult;
	private Handler mHandler = new Handler();
	
	private boolean gps_enabled = false;
	private boolean network_enabled = false;
	private boolean flag = true;

	/**this methos will give loactio details like latitude nad longtitude*/
	public boolean getLocation(Context context, LocationResult result){
		
		//I use LocationResult callback class to pass location value from MyLocation to user code.
		locationResult = result;
		if(mLocationManager == null)
			mLocationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

		//exceptions will be thrown if provider is not permitted.
		try{gps_enabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);}catch(Exception ex){}
		try{network_enabled = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);}catch(Exception ex){}

		//don't start listeners if no provider is enabled
		if(!gps_enabled && !network_enabled)
			return false;

		if(gps_enabled){
			if(mLocationManager != null){
				mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListenerGps);	
			}			
		}
		if(network_enabled){
			if(mLocationManager != null){
				mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListenerNetwork);	
			}
		}

		if(flag){   
			mHandler.removeCallbacks(updateView);
			mHandler.postDelayed(updateView, 100);
		}
		return true;
	}

	LocationListener locationListenerGps = new LocationListener() {
		public void onLocationChanged(Location location) {
			
			locationResult.gotLocation(location);
			mLocationManager.removeUpdates(this);
			mLocationManager.removeUpdates(locationListenerNetwork);
		}
		public void onProviderDisabled(String provider) {}
		public void onProviderEnabled(String provider) {}
		public void onStatusChanged(String provider, int status, Bundle extras) {}
	};

	LocationListener locationListenerNetwork = new LocationListener() {
		public void onLocationChanged(Location location) {
			locationResult.gotLocation(location);
			mLocationManager.removeUpdates(this);
			mLocationManager.removeUpdates(locationListenerGps);
		}
		public void onProviderDisabled(String provider) {}
		public void onProviderEnabled(String provider) {}
		public void onStatusChanged(String provider, int status, Bundle extras) {}
	};

	private Runnable updateView = new Runnable() {
		public void run() {

			if(!flag) {
				mLocationManager.removeUpdates(locationListenerGps);
				mLocationManager.removeUpdates(locationListenerNetwork);

				Location net_loc=null, gps_loc=null;
				if(gps_enabled) {
					if(mLocationManager != null) {
						gps_loc=mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);				
					}
				}

				if(network_enabled) {
					if(mLocationManager != null) {
						net_loc=mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);	
					}
				}

				//if there are both values use the latest one
				if(gps_loc!=null && net_loc!=null) {
					
					if(gps_loc.getTime()>net_loc.getTime())
						locationResult.gotLocation(gps_loc);
					else
						locationResult.gotLocation(net_loc);
					return;
				}
				if(net_loc!=null) {
					locationResult.gotLocation(net_loc);
					return;
				}
				if(gps_loc!=null) {
					
					locationResult.gotLocation(gps_loc);
					return;
				}
				
				locationResult.gotLocation(null);
			}
			if(flag)
				mHandler.postDelayed(this,5000);
			flag = false;
		}
	};

	//class reprasenting location details in another classes
	public static abstract class LocationResult{
		public abstract void gotLocation(Location location);
	}

	/** remove the values once not required */
	public void cancelTimer() { 
		mLocationManager.removeUpdates(locationListenerGps); 
		mLocationManager.removeUpdates(locationListenerNetwork); 
	} 
}
