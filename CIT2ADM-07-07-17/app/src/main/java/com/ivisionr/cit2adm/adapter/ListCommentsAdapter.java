package com.ivisionr.cit2adm.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ivisionr.cit2adm.R;
import com.ivisionr.cit2adm.bin.Issuedetailscomments;

public class ListCommentsAdapter  extends ArrayAdapter<Issuedetailscomments> {
	private Context mContext;
	private LayoutInflater inflater;



	public ListCommentsAdapter(Context context, List<Issuedetailscomments> news_arrayList) {
		super(context, R.layout.commentsdetails,R.id.commentstxt, news_arrayList);		
		this.mContext = context;
		inflater = LayoutInflater.from(context) ;

	}



	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		final Issuedetailscomments surveyList = (Issuedetailscomments) this.getItem( position );
		TextView commentstxt=null,issyecreated=null;
		
		if(convertView==null){
			convertView = inflater.inflate(R.layout.commentsdetails, null);
			commentstxt=(TextView)convertView.findViewById(R.id.commentstxt);
			issyecreated=(TextView)convertView.findViewById(R.id.commentsname);
			convertView.setTag(new ViewHolder(commentstxt,issyecreated));
			
			
		}else{
			ViewHolder viewHolder = (ViewHolder) convertView.getTag();
			commentstxt=viewHolder.commentstxt;
			issyecreated=viewHolder.issyecreated;
			
		}
		commentstxt.setText(surveyList.getComments());
		issyecreated.setText(surveyList.getFirstname()+" "+surveyList.getLastname()+" "+surveyList.getCreated());
		
		return convertView;
		
	}
	public class ViewHolder {

		private TextView commentstxt,issyecreated;
		
		public TextView getCommentstxt() {
			return commentstxt;
		}

		public void setCommentstxt(TextView commentstxt) {
			this.commentstxt = commentstxt;
		}

		public TextView getIssyecreated() {
			return issyecreated;
		}

		public void setIssyecreated(TextView issyecreated) {
			this.issyecreated = issyecreated;
		}

		public ViewHolder(TextView firstname,TextView datecreted) {
			this.commentstxt=firstname;
			this.issyecreated=datecreted;
			
			
		}


		

	}
}
