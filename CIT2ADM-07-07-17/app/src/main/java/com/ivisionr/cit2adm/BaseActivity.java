package com.ivisionr.cit2adm;


import com.ivisionr.cit2adm.Util.MobileUtils;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

public class BaseActivity extends FragmentActivity {

	String DEFAULT_PREFERENCE = "DEFAULT_PREFERENCE";

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		MobileUtils.setLanguage(this, this);
		super.onCreate(savedInstanceState);



	}
}
