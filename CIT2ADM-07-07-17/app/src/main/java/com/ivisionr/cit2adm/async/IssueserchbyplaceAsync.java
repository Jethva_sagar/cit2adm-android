package com.ivisionr.cit2adm.async;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ivisionr.cit2adm.Util.SharePreferenceClass;
import com.ivisionr.cit2adm.Util.Utils;
import com.ivisionr.cit2adm.Util.Webservicelink;
import com.ivisionr.cit2adm.bin.Issueserchbyplacebin;
import com.ivisionr.cit2adm.interfaces.Issueserchbyinterfaces;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class IssueserchbyplaceAsync  extends AsyncTask<String , Void, String>{
	private Context _context;
	private Activity actity;
	private Issueserchbyplacebin issueserchbyplace;
	private List<Issueserchbyplacebin> listissueserchbyplace;
	public Issueserchbyinterfaces issueserchbyinterface;
	private String checkerrorcode;
	private String serchplacelat="";
	private String serchplacelon="";

	public IssueserchbyplaceAsync(Activity activity)
	{
		this.actity=activity;
		this._context=activity;
		listissueserchbyplace=new ArrayList<Issueserchbyplacebin>();
	}
	@Override
	protected String doInBackground(String... params) {
		try {
			listissueserchbyplace=issueserchbyplace(params);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	private List<Issueserchbyplacebin> issueserchbyplace(String[] params) throws JSONException {

		String address=params[0];
		String result="";		
		String url = Webservicelink.serchissuebyplace;
		issueserchbyplace=new Issueserchbyplacebin();

		try
		{
			JSONObject registerJson = new JSONObject();
			registerJson.put("address",address );	
			List<NameValuePair> namevaluepair = new ArrayList<NameValuePair>();
			namevaluepair.add(new BasicNameValuePair("data", registerJson
					.toString()));			
			String paramString = URLEncodedUtils.format(namevaluepair, "utf-8");
			url += paramString;	

			Log.d("kartickurlis", "kartickurlis" +url);
			HttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(url);			
			HttpResponse response = null;
			try {
				response = httpClient.execute(httpPost);
			} catch (ClientProtocolException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} 
			try {
				HttpEntity entity = response.getEntity();
				InputStream instream = entity.getContent();
				result = Utils.convertStreamToString(instream);					

			} catch (IOException e) {				
				e.printStackTrace();
			}
		}catch(Exception e){
			e.printStackTrace();

		}

		JSONObject jsonObject=new JSONObject(result);
		if(jsonObject.has("lat")){

			serchplacelat=jsonObject.getString("lat");
			Log.d("serchlat", "serchlat" +serchplacelat);
			//new SharePreferenceClass(_context).saveserchplacelat(serchplacelat);
		}
	if(jsonObject.has("lon")){
		serchplacelon=jsonObject.getString("lon");
		Log.d("serchlon", "serchlon" +serchplacelon);
		//new SharePreferenceClass(_context).saveserchplacelon(serchplacelon);
	}

		checkerrorcode=jsonObject.getString("error_code");
		JSONArray jsonarray=jsonObject.getJSONArray("issues");

		for(int i=0;i<jsonarray.length();i++){
			issueserchbyplace=new Issueserchbyplacebin();
			JSONObject placeObject=(JSONObject) jsonarray.get(i);
			issueserchbyplace.setCreated(placeObject.getString("created"));
			issueserchbyplace.setId(placeObject.getString("id"));
			issueserchbyplace.setIssuetypeid(placeObject.getString("issue_type_id"));
			issueserchbyplace.setLat(placeObject.getString("lat"));
			issueserchbyplace.setLon(placeObject.getString("lon"));
			issueserchbyplace.setName(placeObject.getString("name"));
			issueserchbyplace.setReportid(placeObject.getString("report_id"));
			issueserchbyplace.setStatus(placeObject.getString("status"));
			listissueserchbyplace.add(issueserchbyplace);
		}

		return listissueserchbyplace;

	}
	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		if(checkerrorcode.equals("S21600")){
			issueserchbyinterface.onissuebyplace(listissueserchbyplace,serchplacelat,serchplacelon);
		}else{
			issueserchbyinterface.Issueserchbyplacenodata();
		}


	}
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		issueserchbyinterface.inissueserchbyplacestarted();
	}

}
