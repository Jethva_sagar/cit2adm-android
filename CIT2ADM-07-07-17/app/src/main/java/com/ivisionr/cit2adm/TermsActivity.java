package com.ivisionr.cit2adm;

import com.ivisionr.cit2adm.Util.LanguesCheckClass;
import com.ivisionr.cit2adm.Util.Utils;
import com.ivisionr.cit2adm.async.TermeandConditionasync;
import com.ivisionr.cit2adm.interfaces.TermsandConditionInterface;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

@SuppressLint("SetJavaScriptEnabled")
public class TermsActivity extends ActivityExceptionDemo implements TermsandConditionInterface{
	private Button closeBtn;
	private CheckBox check;
	private TextView txtterms;
	private ProgressDialog mProgressDialog;
	private Button backinTermsCocdition;
	private WebView mWebView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.terms);
		//txtterms=(TextView)findViewById(R.id.regisdoc);
		mWebView=(WebView)findViewById(R.id.regisdoc);
		WebSettings websettings=mWebView.getSettings();
		websettings.setJavaScriptEnabled(true);
		closeBtn=(Button)findViewById(R.id.closeBtn);
		
		mProgressDialog=new ProgressDialog(TermsActivity.this);
		mProgressDialog.setMessage(getResources().getText(R.string.loading));	
		
		//mWebView.loadData("Test", "text/html; charset=UTF-8", null);
		
		LanguesCheckClass languesCheckClass = new LanguesCheckClass(this);
		String langues = languesCheckClass.GetCurrentLangues();
		
		/*String terms_file = "";
		
		if(langues.equals(""))
			terms_file = "terms-en.html";
		else			
			terms_file = "terms-"+langues+".html";
		
		
		//mWebView.loadUrl("file:///android_asset/"+terms_file); //new.html is html file name.
		
		//mWebView.loadUrl(address);*/
		
		if(Utils.checkConnectivity(TermsActivity.this)){
			TermeandConditionasync termsandcondition =new TermeandConditionasync(TermsActivity.this);
			termsandcondition.termscondition=TermsActivity.this;
			termsandcondition.execute("en");
			
		}else{
			showNetworkDialog("internet");
		}
		
		closeBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				startActivity(new Intent(TermsActivity.this, GoomapActivity.class));
				finish();
				
			}
		});
		
		
		
	}

	@SuppressWarnings("deprecation")
	public void showNetworkDialog(final String message){
		// final exit of application
		AlertDialog.Builder builder = new AlertDialog.Builder(TermsActivity.this);
		builder.setTitle(getResources().getString(R.string.app_name));
		if(message.equals("gps")){
			builder.setMessage(getResources().getString(R.string.gps_message));
		}
		else if(message.equals("internet")){
			builder.setMessage(getResources().getString(R.string.net_message));
		}
		AlertDialog alert = builder.create();
		alert.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				if(message.equals("gps")){
					Intent i=new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);		
					startActivityForResult(i, 1);
				}
				else if(message.equals("internet")){
					Intent i=new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS);		
					startActivityForResult(i,2);		
					Intent i1=new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS);		
					startActivityForResult(i1,3);	
				}
				else{
					//do nothing
				}
			}
		});
		// Showing Alert Message
		alert.show();	
	}

	

	@Override
	public void onstarted() {
		// TODO Auto-generated method stub
		if(null !=mProgressDialog){
			mProgressDialog.show();
		}
		
	}

	@Override
	public void completedtermsandcondition(String address) {
		if(null !=mProgressDialog){
			//txtterms.setText(address);
			mWebView.loadUrl(address);
			Log.d("DEBUG", "Terms Activity Address:" +address);
			mProgressDialog.dismiss();
			
		}	
		
		
		
	}

}
