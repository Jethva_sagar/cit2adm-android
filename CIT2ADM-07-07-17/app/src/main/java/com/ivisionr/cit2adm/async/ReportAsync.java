package com.ivisionr.cit2adm.async;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.ivisionr.cit2adm.Util.Webservicelink;
import com.ivisionr.cit2adm.interfaces.ReportInterface;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class ReportAsync extends AsyncTask<String, Void, String>{
	public ReportInterface reportinterface;
	private Context _context;
	private Activity actity;
	private String userid, issuetypeid, mImahename, description, latitude, longitude, language, imagepath,  deviceid, versionCode, versionName;
	String page = "no";
	
	//UploadUtil up = new UploadUtil();

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		reportinterface.onReportpostastarted();
	}





	public ReportAsync(Activity activity) {
		this.actity=activity;
		this._context=activity;


	}





	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);	
		if(page.equals("S20800")){			
			reportinterface.onReportpostcompleted();
		}
		


	}





	@Override
	protected String doInBackground(String... params) {
        
		
		try {
			reportinServer(params);
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";

	}





	private void reportinServer(String[] reportData) throws JSONException, ClientProtocolException, IOException {

		String url = Webservicelink.serverReportURL;		
		userid=reportData[0];
		issuetypeid=reportData[1];
		mImahename=reportData[2];
		description=reportData[3];
		latitude=reportData[4];
		longitude=reportData[5];
		language=reportData[6];
		imagepath=reportData[7];
		deviceid=reportData[8];
		versionCode=reportData[9];
		versionName=reportData[10];
		
		//addr=reportData[8];
		
		/*Log.d("DEBUG", "User Id: "+userid);
		Log.d("DEBUG", "Issue Id: "+issuetypeid);
		Log.d("DEBUG", "Image Name: "+mImahename);
		Log.d("DEBUG", "Description: "+description);
		Log.d("DEBUG", "Lat: "+latitude);
		Log.d("DEBUG", "Lon: "+longitude);
		Log.d("DEBUG", "Langauge: "+language);
		Log.d("DEBUG", "Image Path: "+imagepath);*/
		Log.d("DEBUG", "Report Device Id: "+deviceid);
		
		
		JSONObject registerJson = new JSONObject();
		registerJson.put("user_id", userid);
		registerJson.put("issue_type_id", issuetypeid);
		registerJson.put("image_1", mImahename);
		registerJson.put("description", description);
		registerJson.put("lat", latitude);
		registerJson.put("lon", longitude);
		registerJson.put("lang", language);
		registerJson.put("imagepath", imagepath);
		registerJson.put("deviceid", deviceid);
		registerJson.put("versioncode", versionCode);
		registerJson.put("versionname", versionName);
		
		
		Log.d("DEBUG", "versionCode: "+versionCode);
		Log.d("DEBUG", "versionName: "+versionName);
		
		List<NameValuePair> namevaluepair = new ArrayList<NameValuePair>();
		namevaluepair.add(new BasicNameValuePair("data", registerJson.toString()));
		ResponseHandler<String> resHandler = new BasicResponseHandler();
		String paramString = URLEncodedUtils.format(namevaluepair, "utf-8");
		url += paramString;
		Log.d("DEBUG","URL for HTTP Post: " +url );
		DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(url);
		page = httpClient.execute(httpGet, resHandler);
		Log.d("DEBUG", "URL for HTTP Post: "+page);
		JSONObject jsobj = new JSONObject(page.toString());
		if (jsobj.has("error_code"))
		{
			page = jsobj.getString("error_code");	
			Log.d("DEBUG", "Error Code: " +page);
				
		}
		

	}







}
