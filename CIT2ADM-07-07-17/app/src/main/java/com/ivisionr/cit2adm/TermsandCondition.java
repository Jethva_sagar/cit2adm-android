package com.ivisionr.cit2adm;

import com.ivisionr.cit2adm.Util.Utils;
import com.ivisionr.cit2adm.async.TermeandConditionasync;
import com.ivisionr.cit2adm.interfaces.TermsandConditionInterface;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

@SuppressLint("SetJavaScriptEnabled")
public class TermsandCondition extends ActivityExceptionDemo implements TermsandConditionInterface{
	private Button accept,reject;
	private CheckBox check;
	private TextView txtterms;
	private ProgressDialog mProgressDialog;
	private Button backinTermsCocdition;
	private WebView mWebView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.registrationacceptance);
		accept=(Button)findViewById(R.id.accept);
		reject=(Button)findViewById(R.id.reject);
		//txtterms=(TextView)findViewById(R.id.regisdoc);
		mWebView=(WebView)findViewById(R.id.regisdoc);
		WebSettings websettings=mWebView.getSettings();
		websettings.setJavaScriptEnabled(true);
		
		backinTermsCocdition=(Button)findViewById(R.id.Registration_back_btn);
		mProgressDialog=new ProgressDialog(TermsandCondition.this);
		mProgressDialog.setMessage(getResources().getText(R.string.loading));		
		if(Utils.checkConnectivity(TermsandCondition.this)){
			TermeandConditionasync termsandcondition =new TermeandConditionasync(TermsandCondition.this);
			termsandcondition.termscondition=TermsandCondition.this;
			termsandcondition.execute("en");
			
		}else{
			showNetworkDialog("internet");
		}
		
		accept.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
				startActivity(new Intent(TermsandCondition.this,RegisterActivity.class));
				
			}
		});
		reject.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//startActivity(new Intent(TermsandCondition.this,RegisterActivity.class));
				finish();
				
			}
		});
		/*check.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(isChecked){
					accept.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							startActivity(new Intent(TermsandCondition.this,RegisterActivity.class));
							
						}
					});
					
				}
			}
		});*/
		
		
		reject.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				TermsandCondition.this.finish();
				
			}
		});
		backinTermsCocdition.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
				
			}
		});
	}

	@SuppressWarnings("deprecation")
	public void showNetworkDialog(final String message){
		// final exit of application
		AlertDialog.Builder builder = new AlertDialog.Builder(TermsandCondition.this);
		builder.setTitle(getResources().getString(R.string.app_name));
		if(message.equals("gps")){
			builder.setMessage(getResources().getString(R.string.gps_message));
		}
		else if(message.equals("internet")){
			builder.setMessage(getResources().getString(R.string.net_message));
		}
		AlertDialog alert = builder.create();
		alert.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				if(message.equals("gps")){
					Intent i=new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);		
					startActivityForResult(i, 1);
				}
				else if(message.equals("internet")){
					Intent i=new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS);		
					startActivityForResult(i,2);		
					Intent i1=new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS);		
					startActivityForResult(i1,3);	
				}
				else{
					//do nothing
				}
			}
		});
		// Showing Alert Message
		alert.show();	
	}

	

	@Override
	public void onstarted() {
		// TODO Auto-generated method stub
		if(null !=mProgressDialog){
			mProgressDialog.show();
		}
		
	}

	@Override
	public void completedtermsandcondition(String address) {
		if(null !=mProgressDialog){
			//txtterms.setText(address);
			mWebView.loadUrl(address);
			Log.d("checkurl", "checkurl" +address);
			mProgressDialog.dismiss();
			
		}	
		
		
		
	}

}
