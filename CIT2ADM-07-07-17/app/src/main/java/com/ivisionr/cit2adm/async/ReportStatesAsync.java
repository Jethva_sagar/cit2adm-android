package com.ivisionr.cit2adm.async;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.ivisionr.cit2adm.Util.Utils;
import com.ivisionr.cit2adm.Util.Webservicelink;
import com.ivisionr.cit2adm.bin.Reportstates;
import com.ivisionr.cit2adm.interfaces.ReportstatesInterfaces;



import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class ReportStatesAsync extends AsyncTask<String, Void, String> {
	private Context _context;
	private Activity actity;
	private String result="";
	public ReportstatesInterfaces reportstaesinterface;
	private Reportstates reportsttesbin;
	String valuecheck="";
	
	public ReportStatesAsync(Activity activity){
		this.actity=activity;
		this._context=activity;
	}
	
	@Override
	protected String doInBackground(String... params) {
		try {
			try {
				reportstates(params);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	private void reportstates(String[] params) throws IOException, JSONException {
		String url=Webservicelink.reportstatesurl;
		
		HttpClient httpClient = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost(url);			
		HttpResponse response = null;
		try {
			response = httpClient.execute(httpPost);
		} catch (ClientProtocolException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} 
		try {
			HttpEntity entity = response.getEntity();
			InputStream instream = entity.getContent();
			result = Utils.convertStreamToString(instream);
			reportsttesbin=getresult(result);
			
			Log.d("checkresult", "checkresult" +result);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		
		
	}
	private Reportstates getresult(String result) throws JSONException {
		reportsttesbin= new Reportstates();
		JSONObject jsonObject=new JSONObject(result);
		String valuecheck=jsonObject.getString("error_code");	
		Log.e("response", result);
		if(jsonObject.has("global")){
			JSONObject json=jsonObject.getJSONObject("global");
			if(json.has("issues")){
				JSONObject jsonstring=json.getJSONObject("issues");
				try {
					reportsttesbin.setOpenglobal(jsonstring.getString("counteropen"));
					reportsttesbin.setAssignedglobal(jsonstring.getString("counterassigned"));
					reportsttesbin.setInprogressglobal(jsonstring.getString("counterprogress"));
					
					reportsttesbin.setClosedglobal(jsonstring.getString("counterclosed"));
					if(jsonstring.has("countercompleted"))
						reportsttesbin.setCompletedglobal(jsonstring.getString("countercompleted"));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
			}
		}
			if(jsonObject.has("last30days")){
				JSONObject jsons=jsonObject.getJSONObject("last30days");
				if(jsons.has("issues")){
					JSONObject jsonstring=jsons.getJSONObject("issues");					
					reportsttesbin.setOpen30days(jsonstring.getString("counteropen"));
					if(jsonstring.has("counterassigned"))
					reportsttesbin.setAssigned30days(jsonstring.getString("counterassigned"));
					reportsttesbin.setInprogress30days(jsonstring.getString("counterprogress"));
					reportsttesbin.setCompleted30days(jsonstring.getString("countercompleted"));
					reportsttesbin.setClosed30days(jsonstring.getString("counterclosed"));
					
					
				}
			
		}
			if(jsonObject.has("last180days")){
				JSONObject jsons=jsonObject.getJSONObject("last180days");
				if(jsons.has("issues")){
					JSONObject jsonstring=jsons.getJSONObject("issues");					
					reportsttesbin.setOpen180days(jsonstring.getString("counteropen"));
					reportsttesbin.setAssigned180days(jsonstring.getString("counterassigned"));
					reportsttesbin.setInprogress180days(jsonstring.getString("counterprogress"));
					reportsttesbin.setCompleted180days(jsonstring.getString("countercompleted"));
					reportsttesbin.setClosed180days(jsonstring.getString("counterclosed"));
					
					
				}
			
		}

		return reportsttesbin;
		
		
	}
	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		if(valuecheck!= null && valuecheck.equals("S21600"));{
			reportstaesinterface.onreportstatescompleted(reportsttesbin);
		}
	}
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		reportstaesinterface.onreportstatesstaerted();
	}

}
