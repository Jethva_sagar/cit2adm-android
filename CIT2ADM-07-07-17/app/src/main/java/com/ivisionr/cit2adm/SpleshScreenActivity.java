package com.ivisionr.cit2adm;


import java.util.Locale;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ivisionr.cit2adm.Util.ConnectionDetector;
import com.ivisionr.cit2adm.Util.SharePreferenceClass;
import com.ivisionr.cit2adm.Util.Utils;

public class SpleshScreenActivity extends ActivityExceptionDemo
{

	//	private static int SPLASH_TIME_OUT = 4000;
	ConnectionDetector connectionDetector;
	TextView splash_screen_url;
	RelativeLayout ll_splash;
	private String lat,lon;
	private TextView tabtocontinue;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.newsplashscreen);
		tabtocontinue=(TextView)findViewById(R.id.tabtocontinue);
		//splash_screen_url = (TextView) findViewById(R.id.splash_screen_url);
		//ll_splash = (RelativeLayout) findViewById(R.id.ll_splash);
		connectionDetector = new ConnectionDetector(this);
		
		
		
		
		if(Utils.checkConnectivity(SpleshScreenActivity.this)){
			
			tabtocontinue.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View v)
				{
					// TODO Auto-generated method stub
					new SharePreferenceClass(SpleshScreenActivity.this).setLat("");
					new SharePreferenceClass(SpleshScreenActivity.this).setLon("");
					
					
					/*Intent in = new Intent(SpleshScreenActivity.this,
							GoomapActivity.class);
					startActivity(in);
					finish();*/
					
					Intent in = new Intent(SpleshScreenActivity.this,
							LandingScreenActivity.class);
					startActivity(in);
					finish();
					
				}
			});
			splash_screen_url.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View v)
				{
					// TODO Auto-generated method stub
					//Log.v("urlHit", "true");

					Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri
							.parse("http://www.ivisionr.com "));
					startActivity(browserIntent);


				}
			});
			
		}else{
			showNetworkDialog("internet");
		}
		tabtocontinue.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				// TODO Auto-generated method stub
				new SharePreferenceClass(SpleshScreenActivity.this).setLat("");
				new SharePreferenceClass(SpleshScreenActivity.this).setLon("");
				Intent in = new Intent(SpleshScreenActivity.this,
						LandingScreenActivity.class);
				startActivity(in);
				finish();
			}
		});
		splash_screen_url.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				// TODO Auto-generated method stub
				//Log.v("urlHit", "true");

				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri
						.parse("http://www.ivisionr.com "));
				startActivity(browserIntent);


			}
		});

	
	}

	@SuppressWarnings("deprecation")
	public void showNetworkDialog(final String message){
		// final exit of application
		AlertDialog.Builder builder = new AlertDialog.Builder(SpleshScreenActivity.this);
		builder.setTitle(getResources().getString(R.string.app_name));
		if(message.equals("gps")){
			builder.setMessage(getResources().getString(R.string.gps_message));
		}
		else if(message.equals("internet")){
			builder.setMessage(getResources().getString(R.string.net_message));
		}
		AlertDialog alert = builder.create();
		alert.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				if(message.equals("gps")){
					Intent i=new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);		
					startActivityForResult(i, 1);
				}
				else if(message.equals("internet")){
					Intent i=new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS);		
					startActivityForResult(i,2);		
					Intent i1=new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS);		
					startActivityForResult(i1,3);	
				}
				else{
					//do nothing
				}
			}
		});
		// Showing Alert Message
		alert.show();	
	}
		
	
	@Override
  	protected void onResume() {
  		Log.d("DEBUG", "App Resumed");
  		super.onResume();
  		
  		
  		/*Locale current = getResources().getConfiguration().locale;
		
		Log.d("DEBUG", "GoomapActivity Default Locale String - "+current);
		
		Log.d("DEBUG", "GoomapActivity Default Country String - "+current.getDisplayCountry());
		Log.d("DEBUG", "GoomapActivity Default Display String - "+current.getDisplayName());
		Log.d("DEBUG", "GoomapActivity Default Language String - "+current.getDisplayLanguage());
		
		
  		
  		Locale locale = new Locale("es");
  		Locale.setDefault(locale);
  		Configuration config = new Configuration();
  		config.locale = locale;
  		getBaseContext().getResources().updateConfiguration(config,
  		      getBaseContext().getResources().getDisplayMetrics());*/
  	}
  	
	

		
}
