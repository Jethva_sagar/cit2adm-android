package com.ivisionr.cit2adm.async;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import com.ivisionr.cit2adm.Util.Utils;
import com.ivisionr.cit2adm.Util.Webservicelink;
import com.ivisionr.cit2adm.bin.Getissuebyid;
import com.ivisionr.cit2adm.interfaces.Issueserchbyidinterface;


import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class IssuserchbyidAsync extends AsyncTask<String , Void, String> {
	private Context _context;
	private Activity actity;
	public Issueserchbyidinterface issueserchbyid;
	public Getissuebyid getissuebin;
	private String id="";
	public IssuserchbyidAsync(Activity activity)
	{
		this.actity=activity;
		this._context=activity;

	}
	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub
		getissuebin=getDetilas(params);
		return null;
	}
	private Getissuebyid getDetilas(String[] params) {
		// TODO Auto-generated method stub
		String address=params[0];
		String result="";		
		String url = Webservicelink.issueserchbyid;
		getissuebin=new Getissuebyid();

		try{
			JSONObject registerJson = new JSONObject();
			registerJson.put("report_id",address );	
			List<NameValuePair> namevaluepair = new ArrayList<NameValuePair>();
			namevaluepair.add(new BasicNameValuePair("data", registerJson
					.toString()));			
			String paramString = URLEncodedUtils.format(namevaluepair, "utf-8");
			url += paramString;	

			Log.d("kartickurlis", "kartickurlis" +url);
			HttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(url);			
			HttpResponse response = null;
			try {
				response = httpClient.execute(httpPost);
			} catch (ClientProtocolException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} 
			try {
				HttpEntity entity = response.getEntity();
				InputStream instream = entity.getContent();
				result = Utils.convertStreamToString(instream);	
				Log.d("checkstring", "checkstring" +result);
				JSONObject jsonObject=new JSONObject(result);
				id=jsonObject.getString("id");
				Log.d("idcheck", "idcheck" +id);
				//getissuebin.setId(jsonObject.getString("id"));
				
				

			} catch (IOException e) {				
				e.printStackTrace();
			}
		}catch(Exception e){
			e.printStackTrace();

		}

		return null;
	}
	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		issueserchbyid.onissueserchbyidcompleted(id);
	}

}
