package com.ivisionr.cit2adm;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.ConfigurationBuilder;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ivisionr.cit2adm.Util.AlertDialogManager;
import com.ivisionr.cit2adm.Util.ConnectionDetector;
import com.ivisionr.cit2adm.bin.User;

public class TwitterShare extends FragmentActivity {
	// Constants
	/**
	 * Register your here app https://dev.twitter.com/apps/new and get your
	 * consumer key and secret
	 * */
	static String TWITTER_CONSUMER_KEY = "nV4GEi2m6fZXAcbVeNMmtQyIj"; // place
																		// your
																		// cosumer
																		// key
																		// here
	static String TWITTER_CONSUMER_SECRET = "NpQ00qmWbWQq0SzgVlKGEESOI1jlHeb0hnnNkISvGaxGtNL2fC"; // place
																									// your
																									// consumer
																									// secret
																									// here

	// Preference Constants
	static String PREFERENCE_NAME = "twitter_oauth";
	static final String PREF_KEY_OAUTH_TOKEN = "oauth_token";
	static final String PREF_KEY_OAUTH_SECRET = "oauth_token_secret";
	static final String PREF_KEY_TWITTER_LOGIN = "isTwitterLogedIn";

	public static final String TWITTER_CALLBACK_URL = "oauth://t4jsample";
	// public static final String TWITTER_CALLBACK_URL =
	// "http://clematistech.com/";

	// Twitter oauth urls
	static final String URL_TWITTER_AUTH = "auth_url";
	static final String URL_TWITTER_OAUTH_VERIFIER = "oauth_verifier";
	static final String URL_TWITTER_OAUTH_TOKEN = "oauth_token";
	// String link="http://bit.ly/1cojrdS";

	String twtrtag = null, desc = null, url_str = null;
	// Login button
	// private Button btnLoginTwitter;
	// Update status button
	private Button btnUpdateStatus;
	// Logout button
	private Button btnLogoutTwitter;
	// EditText for update
	EditText txtUpdate, edtComment;
	RelativeLayout rl1, rl2;
	// lbl update
	TextView lblUpdate;
	TextView lblUserName;
	ImageView imgview;

	Bitmap bmp;

	// Progress dialog
	ProgressDialog pDialog;

	// Twitter
	private static Twitter twitter;
	private static RequestToken requestToken;
	private WebView mWebView;

	URL newurl = null;

	// Shared Preferences
	private static SharedPreferences mSharedPreferences;

	// Internet Connection detector
	private ConnectionDetector cd;

	AlertDialogManager alert = new AlertDialogManager();

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		setContentView(R.layout.activity_twitter_share);

		twtrtag = "aa";
		desc = "bb";
		url_str = "http://cit2adm.prologicsoft.net/html/";

		// All UI elements
		mWebView = (WebView) findViewById(R.id.webViewTwitter);
		btnUpdateStatus = (Button) findViewById(R.id.btnUpdateStatus);
		btnLogoutTwitter = (Button) findViewById(R.id.btnLogoutTwitter);
		edtComment = (EditText) findViewById(R.id.edtComment);
		lblUpdate = (TextView) findViewById(R.id.txtDesc);
		imgview = (ImageView) findViewById(R.id.icon);
		rl1 = (RelativeLayout) findViewById(R.id.relativeLayout);
		rl2 = (RelativeLayout) findViewById(R.id.RelativeLayout1);
		// Shared Preferences
		mSharedPreferences = TwitterShare.this
				.getSharedPreferences("MyPref", 0);

		lblUpdate
				.setText("Find NearBy lets you locate the nearest places around you");
		if (twtrtag.compareTo("2") == 0) {
			try {
				newurl = new URL(url_str);
			} catch (MalformedURLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				bmp = BitmapFactory.decodeStream(newurl.openConnection()
						.getInputStream());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			imgview.setImageBitmap(bmp);
		}
		// if(twtrtag.compareTo("2")==0)
		// {
		// lblUpdate.setText("You could use this session inside this app. \nhttp://bit.ly/1cojrdS");
		// System.out.println("twitteertag :"+twtrtag);
		// }

		if (android.os.Build.VERSION.SDK_INT > 9) {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
					.permitAll().build();
			StrictMode.setThreadPolicy(policy);

			cd = new ConnectionDetector(TwitterShare.this);

			// Check if Internet present
			if (!cd.isConnectingToInternet()) {
				// Internet Connection is not present
				alert.showAlertDialog(TwitterShare.this,
						"Internet Connection Error",
						"Please connect to working Internet connection", false);
				// stop executing code by return
				return;
			}

			// Check if twitter keys are set
			if (TWITTER_CONSUMER_KEY.trim().length() == 0
					|| TWITTER_CONSUMER_SECRET.trim().length() == 0) {
				// Internet Connection is not present
				alert.showAlertDialog(TwitterShare.this,
						"Twitter oAuth tokens",
						"Please set your twitter oauth tokens first!", false);
				// stop executing code by return
				return;
			}

			/**
			 * Button click event to Update Status, will call
			 * updateTwitterStatus() function
			 * */
			btnUpdateStatus.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// Call update status function

					// System.out.println("twittertag :"+twtrtag);
					// Get the status from EditText
					String status = edtComment.getText().toString() + "\n"
							+ " " + "\n " + url_str;

					// Check for blank text
					if (status.trim().length() > 0) {
						// update status
						new updateTwitterStatus().execute(status);
					} else {
						// EditText is empty
						Toast.makeText(TwitterShare.this,
								R.string.enterstatusmessage, Toast.LENGTH_SHORT)
								.show();
					}
				}
			});

			/**
			 * Button click event for logout from twitter
			 * */
			btnLogoutTwitter.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// Call logout twitter function
					logoutFromTwitter();
				}
			});

			/**
			 * This if conditions is tested once is redirected from twitter
			 * page. Parse the uri to get oAuth Verifier
			 * */
			if (!isTwitterLoggedInAlready()) {
				Uri uri = TwitterShare.this.getIntent().getData();
				if (uri != null
						&& uri.toString().startsWith(TWITTER_CALLBACK_URL)) {
					// oAuth verifier
					String verifier = uri
							.getQueryParameter(URL_TWITTER_OAUTH_VERIFIER);

					try {
						// Get the access token
						AccessToken accessToken = twitter.getOAuthAccessToken(requestToken, verifier);

						// Shared Preferences
						Editor e = mSharedPreferences.edit();

						// After getting access token, access token secret
						// store them in application preferences
						e.putString(PREF_KEY_OAUTH_TOKEN,
								accessToken.getToken());
						e.putString(PREF_KEY_OAUTH_SECRET,
								accessToken.getTokenSecret());
						// Store login status - true
						e.putBoolean(PREF_KEY_TWITTER_LOGIN, true);
						e.commit(); // save changes
						long userID = accessToken.getUserId();
						////User user = twitter.showUser(userID);
						//String username = user.getName();
						/*lblUserName.setText(Html.fromHtml("<b>Welcome "
								+ username + "</b>"));*/
					} catch (Exception e) {
						// Check log for login errors
						Log.e("Twitter Login Error", "> " + e.getMessage());
					}
				}
			}
		}
		loginToTwitter();
		/*
		 * if(btnLogoutTwitter.getVisibility() == View.GONE){ finish(); }
		 */
		// btnLogoutTwitter.setVisibility(View.VISIBLE);
	}

	/**
	 * Function to login twitter
	 * */
	private void loginToTwitter() {

		if (!isTwitterLoggedInAlready()) {
			// btnLoginTwitter.setVisibility(View.VISIBLE);
			mWebView.setVisibility(View.VISIBLE);
			// lblUpdate.setVisibility(View.GONE);
			// txtUpdate.setVisibility(View.GONE);
			btnUpdateStatus.setVisibility(View.GONE);
			btnLogoutTwitter.setVisibility(View.GONE);
			ConfigurationBuilder builder = new ConfigurationBuilder();
			builder.setOAuthConsumerKey(TWITTER_CONSUMER_KEY);
			builder.setOAuthConsumerSecret(TWITTER_CONSUMER_SECRET);
			twitter4j.conf.Configuration configuration = builder.build();

			TwitterFactory factory = new TwitterFactory(configuration);
			twitter = factory.getInstance();

			WebSettings webSettings = mWebView.getSettings();
			webSettings.setJavaScriptEnabled(true);

			try {
				requestToken = twitter
						.getOAuthRequestToken(TWITTER_CALLBACK_URL);

				Log.d("token", "token" + requestToken);
				mWebView.loadUrl(requestToken.getAuthenticationURL());
				// finish();
			} catch (TwitterException e) {
				e.printStackTrace();
			}
		} else {
			// user already logged into twitter
			/*
			 * Toast.makeText(TwitterFragment.this,
			 * "Already Logged into twitter", Toast.LENGTH_LONG).show();
			 */
			// Show Update Twitter
			// btnLoginTwitter.setVisibility(View.GONE);
			// lblUpdate.setVisibility(View.VISIBLE);
			// txtUpdate.setVisibility(View.VISIBLE);
			edtComment.setVisibility(View.VISIBLE);
			rl1.setVisibility(View.VISIBLE);
			btnUpdateStatus.setVisibility(View.VISIBLE);
			btnLogoutTwitter.setVisibility(View.VISIBLE);
		}
	}

	/**
	 * Function to update status
	 * */
	class updateTwitterStatus extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(TwitterShare.this);
			pDialog.setMessage("Updating to twitter...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		/**
		 * getting Places JSON
		 * */
		protected String doInBackground(String... args) {

			String status = args[0];
			try {
				ConfigurationBuilder builder = new ConfigurationBuilder();
				builder.setOAuthConsumerKey(TWITTER_CONSUMER_KEY);
				builder.setOAuthConsumerSecret(TWITTER_CONSUMER_SECRET);

				// Access Token
				String access_token = mSharedPreferences.getString(
						PREF_KEY_OAUTH_TOKEN, "");
				// Access Token Secret
				String access_token_secret = mSharedPreferences.getString(
						PREF_KEY_OAUTH_SECRET, "");

				AccessToken accessToken = new AccessToken(access_token,
						access_token_secret);
				Twitter twitter = new TwitterFactory(builder.build())
						.getInstance(accessToken);

				// Update status
				twitter4j.Status response = twitter.updateStatus(status);

				Log.d("Status", "> " + response.getText());
			} catch (TwitterException e) {
				// Error in updating status
				Log.d("Twitter Update Error", e.getMessage());
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog and show
		 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
		 * from background thread, otherwise you will get error
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			pDialog.dismiss();
			// updating UI from Background Thread
			TwitterShare.this.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(TwitterShare.this, R.string.statustweeted,
							Toast.LENGTH_SHORT).show();
					// Clearing EditText field
					edtComment.setText("");
					// getFragmentManager().popBackStack();

				}
			});
		}

	}

	/**
	 * Function to logout from twitter It will just clear the application shared
	 * preferences
	 * */
	private void logoutFromTwitter() {
		// Clear the shared preferences
		Editor e = mSharedPreferences.edit();
		e.remove(PREF_KEY_OAUTH_TOKEN);
		e.remove(PREF_KEY_OAUTH_SECRET);
		e.remove(PREF_KEY_TWITTER_LOGIN);
		e.commit();

		// After this take the appropriate action
		// I am showing the hiding/showing buttons again
		// You might not needed this code
		btnLogoutTwitter.setVisibility(View.GONE);
		btnUpdateStatus.setVisibility(View.GONE);
		// txtUpdate.setVisibility(View.GONE);
		// lblUpdate.setVisibility(View.GONE);
		edtComment.setText("");
		edtComment.setVisibility(View.GONE);
		rl1.setVisibility(View.GONE);
		mWebView.setVisibility(View.GONE);
		rl2.setVisibility(View.GONE);
		TwitterShare.this.finish();

	}

	@Override
	public void onBackPressed() {

		mWebView.setVisibility(View.GONE);
		rl2.setVisibility(View.GONE);
		TwitterShare.this.finish();

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	/**
	 * Check user already logged in your application using twitter Login flag is
	 * fetched from Shared Preferences
	 * */
	private boolean isTwitterLoggedInAlready() {
		// return twitter login status from Shared Preferences
		return mSharedPreferences.getBoolean(PREF_KEY_TWITTER_LOGIN, false);
	}
}