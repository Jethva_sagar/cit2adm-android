package com.ivisionr.cit2adm;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.ivisionr.cit2adm.Util.SharePreferenceClass;
import com.ivisionr.cit2adm.Util.Webservicelink;
import com.ivisionr.cit2adm.async.ServerResponceCheck;
import com.ivisionr.cit2adm.bin.User;
import com.ivisionr.cit2adm.custom.UploadManager;
import com.ivisionr.cit2adm.database.DatabaseHandler;

public class EditProfile extends ActivityExceptionDemo
{

	Button btnBackToMyprofile, btnSubmit;
	EditText etFName, etLName, etUName, etEmail, etCity, etZip, etAssoCode, etAccessId;
	Spinner spCountry;
	ArrayList<HashMap<String, String>> userRecord;
	HashMap<String, String> hmGetRecord;
	DatabaseHandler db;
	static String userId;
	String[] country;
	User myUser ;
	

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edit_profile);
		connectToXml();
		db = new DatabaseHandler(this);
		
		SharePreferenceClass spc = new SharePreferenceClass(getApplicationContext());
		userId = spc.getUserid();
		//userRecord = db.getProfileAllDataFromDb();
		myUser = (User) db.getUser(userId);
				
		country = getResources().getStringArray(R.array.countryname);
		
		
		
		
		if (myUser.getEmail() != null)
		{
			

			etEmail.setText(myUser.getEmail());
			etFName.setText(myUser.getFirst_name());
			etLName.setText(myUser.getLast_name());
			etCity.setText(myUser.getCity());
			etUName.setText(myUser.getUsername());
			etUName.setEnabled(false);
			
			for (int j = 0; j < country.length; j++)
			{
				if (country[j].equals(myUser.getCountry()))
				{
					spCountry.setSelection(j);
				}
			}

			etZip.setText(myUser.getZipcode());
		}
		
		///Log.d("ID", userId);
		btnBackToMyprofile.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				Intent intent = new Intent(EditProfile.this,
						MyProfileActivity.class);
				startActivity(intent);
				finish();
			}
		});
		btnSubmit.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				
				
				if (etFName.getText().toString().matches("")) {
					Toast toast = Toast.makeText(getApplicationContext(),
							R.string.firstnamenotfound, Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}
				
				else if (etLName.getText().toString().matches("")) {
					Toast toast = Toast.makeText(getApplicationContext(),
							R.string.lastnamenotfound, Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}
				
								
				else if (etZip.getText().toString().matches("")) {
					Toast toast = Toast.makeText(getApplicationContext(),
							R.string.zipnotfound, Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}

				else if (etEmail.getText().toString().matches("")) {
					Toast toast = Toast.makeText(getApplicationContext(),
							R.string.emailnotfound, Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}
				else {
					new UpdateProfileTask().execute(userId, etFName.getText()
						.toString(), etLName.getText().toString(), etCity
						.getText().toString(), spCountry.getSelectedItem()
						.toString(), etZip.getText().toString(), etEmail
						.getText().toString(), etUName.getText().toString(), etAccessId.getText().toString());
				}
			}
		});
	}

	protected void connectToXml()
	{
		etFName = (EditText) findViewById(R.id.myprofile_edit_fname_edt);
		etLName = (EditText) findViewById(R.id.myprofile_edit_lname_edt);
		etUName = (EditText) findViewById(R.id.myprofile_edit_user_edt);
		etEmail = (EditText) findViewById(R.id.myprofile_edit_email_edt);
		etCity = (EditText) findViewById(R.id.myprofile_city_edt);
		etZip = (EditText) findViewById(R.id.myprofile_edit_zipe_edt);
		spCountry = (Spinner) findViewById(R.id.myprofile_edit_contry_spiner);
		btnBackToMyprofile = (Button) findViewById(R.id.myprofile_edit_back_btn);
		btnSubmit = (Button) findViewById(R.id.btn_edit_submit);
		//etAssoCode = (EditText)findViewById(R.id.asscode);
		etAccessId = (EditText)findViewById(R.id.myprofile_access_id);
	}

	

	

	class UpdateProfileTask extends AsyncTask<String, Void, String>
	{

		private ProgressDialog pd;

		@Override
		protected void onPreExecute()
		{
			// TODO Auto-generated method stub
			super.onPreExecute();
			if (pd == null || pd.isShowing() == false)
			{
				pd = new ProgressDialog(EditProfile.this);
				pd.setTitle(getResources().getText(R.string.pleasewait));
				pd.setMessage(getResources().getText(R.string.submitting));
				pd.show();

			}
		}

		@Override
		protected String doInBackground(String... params)
		{

			JSONObject updateJson = new JSONObject();

			try
			{
				Log.d("DEBUG", "Edit Profile Id: "+params[0]);
				Log.d("DEBUG", "Edit Profile First Name: "+params[1]);
				Log.d("DEBUG", "Edit Profile Last Name: "+params[2]);
				Log.d("DEBUG", "Edit Profile City: "+params[3]);
				Log.d("DEBUG", "Edit Profile Country: "+params[4]);
				Log.d("DEBUG", "Edit Profile Zip: "+params[5]);
				Log.d("DEBUG", "Edit Profile Email: "+params[6]);
				Log.d("DEBUG", "Edit Profile Username: "+params[7]);
				Log.d("DEBUG", "Edit Profile Security Code: "+params[8]);
				
				
				updateJson.put("id", params[0]);
				updateJson.put("first_name", params[1]);
				updateJson.put("last_name", params[2]);
				updateJson.put("city", params[3]);
				updateJson.put("country", params[4]);
				updateJson.put("zip", params[5]);
				updateJson.put("email", params[6]);
				updateJson.put("username", params[7]);
				updateJson.put("security_code", params[8]);
				
			}
			catch (JSONException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(7);
			nameValuePairs.add(new BasicNameValuePair("data", updateJson.toString()));

			try
			{
				return UploadManager.uploadDataToUrlGet(nameValuePairs,
						Webservicelink.serverUpdateProfileUrl);
			}
			catch (UnsupportedEncodingException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (ClientProtocolException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result)
		{
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (pd != null && pd.isShowing()) pd.dismiss();
			Log.d("DEBUG", "Edit Profile JS: "+result);

			String error = "", promo_expiry="";
			JSONObject errorCode = null;
			
			try
			{
				errorCode = new JSONObject(result);
				error = errorCode.getString("error_code");
				if(errorCode.has("promo_expiry")) {					
					promo_expiry = errorCode.getString("promo_expiry");					
					Log.d("DEBUG", "Edit Profile Promo Expiry: "+promo_expiry);
					
				} else {
					promo_expiry = myUser.getMembership_expiry_date();
					/*try {
						if(myUser.getMembership_expiry_date() != null && myUser.getMembership_expiry_date().length() > 0) {
							SimpleDateFormat inputDateFormate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
							SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yy hh:mm:ss");
							Date date = inputDateFormate.parse(myUser.getMembership_expiry_date());
							promo_expiry = myUser.getMembership_expiry_date();//dateFormatter.format(date);
						}
						
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}*/
									
				}
			}
			catch (JSONException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			HashMap<String, String> hasmapError = ServerResponceCheck.set();

		
			if (error.equals("S20511"))
			{
				Toast toast = Toast.makeText(getApplicationContext(),
						R.string.invalidemail, Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
			}
			else if (error.equals("S20223"))
			{
				Toast toast = Toast.makeText(getApplicationContext(),
						R.string.promocodenotfound, Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
			}
			else if (error.equals("S20224"))
			{
				Toast toast = Toast.makeText(getApplicationContext(),
						R.string.promocodeexired, Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
			}
			else if (error.equals("S20519") || error.equals("S20520"))
			{
				Toast toast = Toast.makeText(getApplicationContext(),
						R.string.useridnotpassed, Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
			}
			else if (error.equals("S20516"))
			{
				Toast toast = Toast.makeText(getApplicationContext(),
						R.string.duplicateemail, Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
			}
			else if (error.equals("S20517"))
			{
				Toast toast = Toast.makeText(getApplicationContext(),
						R.string.duplicateusername, Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
			}
			else if (error.equals("S20500"))
			{
				Toast.makeText(getApplicationContext(),
						hasmapError.get(error).toString(),
						Toast.LENGTH_SHORT).show();
				//ContentValues cv = getProfileValueForUpdate();
				
				User user = new User();
				
				
				
				
				new SharePreferenceClass(EditProfile.this).saveMembershipExpiry(String.valueOf(promo_expiry));
				new SharePreferenceClass(EditProfile.this).savestatus("1");
				
				user.setFirst_name(etFName.getText().toString());
				user.setLast_name(etLName.getText().toString());
				user.setUsername(etUName.getText().toString());
				user.setEmail(etEmail.getText().toString());
				user.setCity(etCity.getText().toString());
				user.setCountry(spCountry.getSelectedItem().toString());
				user.setZipcode(etZip.getText().toString());	
				user.setMembership_expiry_date(promo_expiry);
				user.setAccess_id(etAccessId.getText().toString());
				user.setId(userId);
				PackageManager manager = EditProfile.this.getPackageManager();
				PackageInfo info=null;
				try {
					info = manager.getPackageInfo( EditProfile.this.getPackageName(), 0);
					Log.d("DEBUG",info.versionName+"+++++++++++++++"+info.versionName);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				/***
				 * getting device unique id
				 */
				 String device_id = Secure.getString( EditProfile.this.getContentResolver(),
		                Secure.ANDROID_ID);
				Log.d("DEBUG", "ServerCommunication User Id: "+userId);
				user.setDevice_id(device_id);
				user.setApp_version_code(""+info.versionCode);
				user.setApp_version_name(info.versionName);
				
				
				db.updateUser(user);
				
				//int i = databasehandler.updateUserProfile(cv);
				//Log.d("data", "" + cv);
				//Log.d("data", "" + i);
				Intent intent = new Intent(getApplicationContext(),
						MyProfileActivity.class);
				startActivity(intent);
				finish();
			}
			else
			{
				Toast.makeText(getApplicationContext(),
						hasmapError.get(error).toString(),
						Toast.LENGTH_SHORT).show();
			}
		}

	}

	
	
	@Override
	public void onBackPressed()
	{
		// TODO Auto-generated method stub
		super.onBackPressed();
		Intent intent = new Intent(EditProfile.this, MyProfileActivity.class);
		startActivity(intent);
		finish();
	}
}
