package com.ivisionr.cit2adm.interfaces;

import java.util.List;

import com.ivisionr.cit2adm.bin.Myvotes;


public interface Myvotesinterfaces {
	public void onstarted();
	public void noData();
	public void onCompleted(List<Myvotes> listmyvotes);

}
