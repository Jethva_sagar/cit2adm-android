package com.ivisionr.cit2adm.Util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.ivisionr.cit2adm.R;



public class AlertDialogManager {
	/**
	 * Function to display simple Alert Dialog
	 * @param context - application context
	 * @param title - alert dialog title
	 * @param message - alert message
	 * @param status - success/failure (used to set icon)
	 * 				 - pass null if you don't want icon
	 * */
	Context mcontext;
	
	@SuppressWarnings("deprecation")
	public void showAlertDialog(Context context, String title, String message,
			Boolean status) {
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();

		// Setting Dialog Title
		alertDialog.setTitle(title);

		// Setting Dialog Message
		alertDialog.setMessage(message);

		if(status != null)
			// Setting alert dialog icon
			alertDialog.setIcon((status) ? R.drawable.appicon : R.drawable.appicon);

		// Setting OK Button
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
//				Intent i = new Intent(AlertDialogManager.this, HomePage.class);    
//				startActivity(i);
			}
		});
		// Showing Alert Message
		alertDialog.show();	
	}
}
