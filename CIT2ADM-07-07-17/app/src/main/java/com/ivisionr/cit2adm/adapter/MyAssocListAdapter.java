package com.ivisionr.cit2adm.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ivisionr.cit2adm.IvisionrAssociation;
import com.ivisionr.cit2adm.R;
import com.ivisionr.cit2adm.bin.Association;
import com.ivisionr.cit2adm.bin.MyNeighbourDetails;

public class MyAssocListAdapter extends ArrayAdapter<Association>{
	private LayoutInflater layoutinflate;
	private Context mContext;

	public MyAssocListAdapter(Context context,List<Association> mylist) {
		super(context, R.layout.myassoclist,R.id.name,mylist);
		this.mContext=context;
		layoutinflate=LayoutInflater.from(context);		

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		final Association assocd = (Association) this.getItem(position);

		TextView name=null,address=null;
		if(convertView==null){
			convertView=layoutinflate.inflate(R.layout.myassoclist, null);
			name=(TextView) convertView.findViewById(R.id.name);
		    //lastname=(TextView) convertView.findViewById(R.id.lastname);
			address=(TextView) convertView.findViewById(R.id.address);
			//zip=(TextView) convertView.findViewById(R.id.zipcode);
			convertView.setTag(new ViewHolder(name,address));

		}else{
			ViewHolder viewholder=(ViewHolder)convertView.getTag();
			name=viewholder.name;
			address=viewholder.address;
		}
		name.setText(assocd.getName());
		//lastname.setText(myneighbourlist.getLastname());
		StringBuffer addr = new StringBuffer();
		
		
		addr.append(assocd.getAddress_1()+" ") ;
		
		if(!addr.equals(""))
			//addr += " "+assocd.getAddress_2();
		addr.append(assocd.getAddress_2()+" ");
		else
			//addr += assocd.getAddress_2();
		addr.append(assocd.getAddress_2()+" ");
		
		if(!addr.equals(""))
			//addr += " "+assocd.getCity();
		addr.append(assocd.getCity()+" ");
		else
			//addr += assocd.getCity();
		addr.append(assocd.getCity()+" ");
		if(!addr.equals(""))
			//addr += " "+assocd.getState();
		addr.append(assocd.getState()+" ");
		else
			//addr += assocd.getState();
		addr.append(assocd.getState()+" ");

		if(!addr.equals(""))
			//addr += " "+assocd.getZip();
			addr.append(assocd.getZip()+" ");
		else
			//addr += assocd.getZip();
		addr.append(assocd.getZip()+" ");
		if(!addr.equals(""))
			//addr += " "+assocd.get();
			addr.append(assocd.getCountry()+" ");
		else
			//addr += assocd.getZip();
			addr.append(assocd.getCountry()+" ");
		
		address.setText(addr.toString().trim());
		return convertView;
	}


	public class ViewHolder{

		private TextView name, address;
		public ViewHolder(TextView name,TextView address){
			this.name=name;
			this.address=address;

		}

		public TextView getName() {
			return name;
		}

		public void setFirstname(TextView name) {
			this.name = name;
		}


		public TextView getAddress() {
			return address;
		}

		public void setCity(TextView address) {
			this.address = address;
		}

	}

}
