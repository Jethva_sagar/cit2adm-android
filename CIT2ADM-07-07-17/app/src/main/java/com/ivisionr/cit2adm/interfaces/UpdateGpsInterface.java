package com.ivisionr.cit2adm.interfaces;

public interface UpdateGpsInterface {
	
	public void onStarted();
	public void onCompleted(String errorcode);

}
